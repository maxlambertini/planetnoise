from distutils.core import setup, Extension
import sys

if sys.platform == "win32":
    # To build for Windows:
    # 1. Install MingW-W64-builds from https://mingw-w64.org/doku.php/download
    #    It is important to change the default to 64-bit when installing if a
    #    64-bit Python is installed in windows.
    # 2. Put the bin/ folder inside x86_64-8.1.0-posix-seh-rt_v6-rev0 in your
    #    system PATH when compiling.
    # 3. The code below will moneky-patch distutils to work.
    import distutils.cygwinccompiler
    distutils.cygwinccompiler.get_msvcr = lambda: []
    # Escaping works differently.
    CONFIG_VERSION = '\\"2019-07-09\\"'
    # Make sure that pthreads is linked statically, otherwise we run into problems
    # on computers where it is not installed.
    extra_link_args = ["-Wl,-Bstatic", "-lpthread"]

_sources = """../../libnoise-noqt/libnoise/src/noise/model/cylinder.cpp
../../libnoise-noqt/libnoise/src/noise/model/line.cpp
../../libnoise-noqt/libnoise/src/noise/model/plane.cpp
../../libnoise-noqt/libnoise/src/noise/model/sphere.cpp
../../libnoise-noqt/libnoise/src/noise/module/abs.cpp
../../libnoise-noqt/libnoise/src/noise/module/add.cpp
../../libnoise-noqt/libnoise/src/noise/module/avg.cpp
../../libnoise-noqt/libnoise/src/noise/module/avg4.cpp
../../libnoise-noqt/libnoise/src/noise/module/billow.cpp
../../libnoise-noqt/libnoise/src/noise/module/blend.cpp
../../libnoise-noqt/libnoise/src/noise/module/cache.cpp
../../libnoise-noqt/libnoise/src/noise/module/checkerboard.cpp
../../libnoise-noqt/libnoise/src/noise/module/clamp.cpp
../../libnoise-noqt/libnoise/src/noise/module/const.cpp
../../libnoise-noqt/libnoise/src/noise/module/cos.cpp
../../libnoise-noqt/libnoise/src/noise/module/curve.cpp
../../libnoise-noqt/libnoise/src/noise/module/cylinders.cpp
../../libnoise-noqt/libnoise/src/noise/module/displace.cpp
../../libnoise-noqt/libnoise/src/noise/module/exponent.cpp
../../libnoise-noqt/libnoise/src/noise/module/invert.cpp
../../libnoise-noqt/libnoise/src/noise/module/max.cpp
../../libnoise-noqt/libnoise/src/noise/module/min.cpp
../../libnoise-noqt/libnoise/src/noise/module/modulebase.cpp
../../libnoise-noqt/libnoise/src/noise/module/multiply.cpp
../../libnoise-noqt/libnoise/src/noise/module/perlin.cpp
../../libnoise-noqt/libnoise/src/noise/module/power.cpp
../../libnoise-noqt/libnoise/src/noise/module/ridgedmulti.cpp
../../libnoise-noqt/libnoise/src/noise/module/ridgedmulti2.cpp
../../libnoise-noqt/libnoise/src/noise/module/rotatepoint.cpp
../../libnoise-noqt/libnoise/src/noise/module/scalebias.cpp
../../libnoise-noqt/libnoise/src/noise/module/scalepoint.cpp
../../libnoise-noqt/libnoise/src/noise/module/select.cpp
../../libnoise-noqt/libnoise/src/noise/module/spheres.cpp
../../libnoise-noqt/libnoise/src/noise/module/terrace.cpp
../../libnoise-noqt/libnoise/src/noise/module/translatepoint.cpp
../../libnoise-noqt/libnoise/src/noise/module/turbulence.cpp
../../libnoise-noqt/libnoise/src/noise/module/turbulence2.cpp
../../libnoise-noqt/libnoise/src/noise/module/turbulence_billow.cpp
../../libnoise-noqt/libnoise/src/noise/module/turbulence_ridged.cpp
../../libnoise-noqt/libnoise/src/noise/module/voronoi.cpp
../../libnoise-noqt/libnoise/src/noise/latlon.cpp
../../libnoise-noqt/libnoise/src/noise/noisegen.cpp
../../libnoise-noqt/libnoise/src/noiseutils.cpp
../../libnoise-noqt/libnoise/json.cpp
../../libnoise-noqt/libnoise-helpers/texturebuilder/heightmapdescriptor.cpp
../../libnoise-noqt/libnoise-helpers/texturebuilder/imagedescriptor.cpp
../../libnoise-noqt/libnoise-helpers/texturebuilder/moduledescriptor.cpp
../../libnoise-noqt/libnoise-helpers/texturebuilder/noisemapbuilderdescriptor.cpp
../../libnoise-noqt/libnoise-helpers/texturebuilder/rendererdescriptor.cpp
../../libnoise-noqt/libnoise-helpers/texturebuilder/texturebuilder.cpp
../../libnoise-noqt/libnoise-helpers/texturebuilder/textureworkflowcreator.cpp
../../libnoise-noqt/stb/stb_vorbis.c
../../py_planetnoise.cpp""".split("\n")

_include_dirs=[
    "./../../libnoise-noqt/libnoise/src",
    "./../../libnoise-noqt/json",
    "./../../libnoise-noqt/libnoise-helpers",
    "./../../libnoise-noqt/stb"
]



module1 = Extension('planetnoise',
                    define_macros = [('MAJOR_VERSION', '0'),
                                     ('MINOR_VERSION', '1'),
                                     ('MS_WIN64', None)],
                    include_dirs = _include_dirs,
                    language="c++",
                    sources = _sources)

setup (name = 'planetnoise',
       version = '0.1',
       description = 'Wrapper around C++ planetnoise',
       author = 'Massimiliano Lambertini',
       author_email = 'daboss@lamboz.net',
       url = 'https://planetnoise.lamboz.net',
       long_description = '''
A wrapper around libnoise and planetnoise
''',
       ext_modules = [module1])