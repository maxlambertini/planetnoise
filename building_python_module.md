# Building and Installing Planetnoise Python Module

A word of warning: I am not an expert macos programmer. The module was mainly built on a linux machine, and then ported to windows. Macos instructions stem from my little experience with this OS.  

## Installing Planetnoise on Linux

While Planetnoise can be compiled into a python 2.7.x module, I assume you are using Python 3. 

### Ubuntu (and `apt`-based distros)

1. Install these packages:

~~~
sudo apt install python3-pip build-essential libssl-dev libffi-dev python3-dev python3-venv
~~~

2. Execute this command: 

~~~
python3 -m pip install 'planetnoise @ git+https://gitlab.com/maxlambertini/planetnoise'
~~~



## Installing Planetnoise on Windows using mingw32

1. Installing a Mingw32 C++ compiler. 

I use [TDM-GCC](https://jmeubank.github.io/tdm-gcc/) compiler. It comes with a quick and easy installer, that takes care of all naughty stuff like configuring environment variables.

At the end of the installation, open a DOS prompt and then launch the following commands:

~~~
gcc -v

g++ -v
~~~

If TDM-GCC has been installed correctly, you should get the following response from these commands:

~~~
C:\Users\max>gcc -v
Using built-in specs.
COLLECT_GCC=gcc
COLLECT_LTO_WRAPPER=C:/opt/dev/TDM-GCC-64/bin/../libexec/gcc/x86_64-w64-mingw32/10.3.0/lto-wrapper.exe
Target: x86_64-w64-mingw32
Configured with: ../../../src/gcc-git-10.3.0/configure --build=x86_64-w64-mingw32 --enable-targets=all --enable-languages=ada,c,c++,fortran,jit,lto,objc,obj-c++ --enable-libgomp --enable-lto --enable-graphite --enable-cxx-flags=-DWINPTHREAD_STATIC --disable-build-with-cxx --disable-build-poststage1-with-cxx --enable-libstdcxx-debug --enable-threads=posix --enable-version-specific-runtime-libs --enable-fully-dynamic-string --enable-libstdcxx-filesystem-ts=yes --disable-libstdcxx-pch --enable-libstdcxx-threads --enable-libstdcxx-time=yes --enable-mingw-wildcard --with-gnu-ld --disable-werror --enable-nls --disable-win32-registry --enable-large-address-aware --disable-rpath --disable-symvers --prefix=/mingw64tdm --with-local-prefix=/mingw64tdm --with-pkgversion=tdm64-1 --with-bugurl=https://github.com/jmeubank/tdm-gcc/issues
Thread model: posix
Supported LTO compression algorithms: zlib zstd
gcc version 10.3.0 (tdm64-1)

C:\Users\max>g++ -v
Using built-in specs.
COLLECT_GCC=g++
COLLECT_LTO_WRAPPER=C:/opt/dev/TDM-GCC-64/bin/../libexec/gcc/x86_64-w64-mingw32/10.3.0/lto-wrapper.exe
Target: x86_64-w64-mingw32
Configured with: ../../../src/gcc-git-10.3.0/configure --build=x86_64-w64-mingw32 --enable-targets=all --enable-languages=ada,c,c++,fortran,jit,lto,objc,obj-c++ --enable-libgomp --enable-lto --enable-graphite --enable-cxx-flags=-DWINPTHREAD_STATIC --disable-build-with-cxx --disable-build-poststage1-with-cxx --enable-libstdcxx-debug --enable-threads=posix --enable-version-specific-runtime-libs --enable-fully-dynamic-string --enable-libstdcxx-filesystem-ts=yes --disable-libstdcxx-pch --enable-libstdcxx-threads --enable-libstdcxx-time=yes --enable-mingw-wildcard --with-gnu-ld --disable-werror --enable-nls --disable-win32-registry --enable-large-address-aware --disable-rpath --disable-symvers --prefix=/mingw64tdm --with-local-prefix=/mingw64tdm --with-pkgversion=tdm64-1 --with-bugurl=https://github.com/jmeubank/tdm-gcc/issues
Thread model: posix
Supported LTO compression algorithms: zlib zstd
gcc version 10.3.0 (tdm64-1)
~~~


2. Compiling and installing the module. 

Check if you have `pip` installed. Open a command prompt and launch this command:

~~~
python -m pip -V
~~~

You should receive a message similar to this one:

~~~
(venv) F:\dev\venv\test1>python -m pip -V

pip 22.2.2 from F:\dev\venv\test1\venv\lib\site-packages\pip (python 3.9)
~~~

if you installed python on window using the installer from [python.org](https://python.org) `pip` should already be there

Then, install the module from source: 

~~~
python -m pip install --global-option build_ext --global-option --compiler=mingw32  "planetnoise @ git+https://gitlab.com/maxlambertini/planetnoise"
~~~


## Install Planetnoise on MacOS

1. Check if python3 is installed by launching `python3 -V`

2. If not, make sure you've installed homebrew. If it's not installed, open a terminal and install it this way:

~~~
/bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
~~~

2. Then, install `pyenv` to manage Python versions:

~~~
brew install pyenv
~~~

3. Install latest python3 version:

~~~
pyenv install-latest
~~~

4. Make sure clang is installed


5. Execute this command: 

~~~
CPPFLAGS=-std=c++14 python3 -m pip install 'planetnoise @ git+https://gitlab.com/maxlambertini/planetnoise'
~~~

This way, we'll force clang to 

use C++14 standard when building planetnoise. 


