import planetnoise
from planetnoise import TextureBuilder, ModuleDescriptor as MD, ImageDescriptor as ID, HeightMapDescriptor as HMD
from planetnoise import RendererDescriptor as RD
from planetnoise import NoiseMapBuilderDescriptor as NMB


BeginLayer_Module=MD.Perlin ( name='BeginLayer_Module',  
	enableRandom=True, 
	lac=3.078592062, 
	pers=0.301463991404, 
	seed=0, 
	freq=2.36660003662, 
	oct=6)
CylinderPole=MD.Cylinders ( name='CylinderPole',  
	freq=0.5, 
	enableRandom=False)
ExtraLayer_0_Module=MD.Perlin ( name='ExtraLayer_0_Module',  
	enableRandom=True, 
	lac=3.507584095, 
	pers=0.327006012201, 
	seed=0, 
	freq=2.26749992371, 
	oct=6)
ExtraLayer_1_Module=MD.Voronoi ( name='ExtraLayer_1_Module',  
	disp=0.0, 
	enableRandom=True, 
	seed=0, 
	freq=2.70409989357, 
	enableDist=True)
MiddleLayer_Module=MD.Perlin ( name='MiddleLayer_Module',  
	enableRandom=True, 
	lac=3.20115208626, 
	pers=0.308712005615, 
	seed=0, 
	freq=2.45070004463, 
	oct=6)
RidgedBumpMapModule=MD.Perlin ( name='RidgedBumpMapModule',  
	enableRandom=False, 
	lac=3.6, 
	pers=0.3, 
	seed=0, 
	freq=6.3, 
	oct=8)
SeaModule=MD.Perlin ( name='SeaModule',  
	enableRandom=True, 
	lac=3.6283, 
	pers=0.296816, 
	seed=0, 
	freq=1.1234, 
	oct=8)
AATurboPoles=MD.Turbulence ( name='AATurboPoles',  
	src1=CylinderPole, 
	pow=0.27363, 
	enableRandom=False, 
	seed=0, 
	freq=3.907, 
	rough=3.2321)
AATurboSea=MD.Turbulence ( name='AATurboSea',  
	src1=SeaModule, 
	pow=0.31732, 
	enableRandom=False, 
	seed=0, 
	freq=1.1964, 
	rough=1.9554)
RidgedBumpMapModule_e=MD.Exponent ( name='RidgedBumpMapModule_e',  
	src1=RidgedBumpMapModule, 
	enableRandom=False, 
	exp=1.25)
RidgedBumpMapModule_e_t=MD.Turbulence ( name='RidgedBumpMapModule_e_t',  
	src1=RidgedBumpMapModule_e, 
	pow=0.05, 
	enableRandom=False, 
	seed=0, 
	freq=0.75, 
	rough=1.25)

BaseImage=ID.create('BaseImage')
img_bump=ID.create('img_bump')
img_geo=ID.create('img_geo')
img_spec=ID.create('img_spec')


BeginLayer_heightMap=HMD.create('BeginLayer_heightMap')
ExtraLayer_0_heightMap=HMD.create('ExtraLayer_0_heightMap')
ExtraLayer_1_heightMap=HMD.create('ExtraLayer_1_heightMap')
MiddleLayer_heightMap=HMD.create('MiddleLayer_heightMap')
heightMapPole=HMD.create('heightMapPole')
heightMapSea=HMD.create('heightMapSea')
bump_heightMap=HMD.create('bump_heightMap')
geo_heightMap=HMD.create('geo_heightMap')
spec_heightMap=HMD.create('spec_heightMap')

BeginLayer_noiseMapBuilder1 = NMB.create('BeginLayer_noiseMapBuilder1', module=BeginLayer_Module, heightMap=BeginLayer_heightMap)
ExtraLayer_0_noiseMapBuilder1 = NMB.create('ExtraLayer_0_noiseMapBuilder1', module=ExtraLayer_0_Module, heightMap=ExtraLayer_0_heightMap)
ExtraLayer_1_noiseMapBuilder1 = NMB.create('ExtraLayer_1_noiseMapBuilder1', module=ExtraLayer_1_Module, heightMap=ExtraLayer_1_heightMap)
MiddleLayer_noiseMapBuilder1 = NMB.create('MiddleLayer_noiseMapBuilder1', module=MiddleLayer_Module, heightMap=MiddleLayer_heightMap)
heightMapBuilderPole = NMB.create('heightMapBuilderPole', module=AATurboPoles, heightMap=heightMapPole)
heightMapBuilderSea = NMB.create('heightMapBuilderSea', module=AATurboSea, heightMap=heightMapSea)
noiseMapBuilderBump = NMB.create('noiseMapBuilderBump', module=RidgedBumpMapModule_e_t, heightMap=bump_heightMap)
noiseMapBuilderGeo = NMB.create('noiseMapBuilderGeo', module=MiddleLayer_Module, heightMap=geo_heightMap)
noiseMapBuilderSpec = NMB.create('noiseMapBuilderSpec', module=MiddleLayer_Module, heightMap=spec_heightMap)

BeginLayer_renderer=RD.create (  name='BeginLayer_renderer',  
	destImage=BaseImage, 
	lightBrightness=2.0, 
	randomGradient=False, 
	gradientInfo=[
		( -1.0, 0, 0, 0, 255 ), 
		( -0.5, 56, 53, 52, 255 ), 
		( 0.0, 55, 57, 52, 255 ), 
		( 0.5, 55, 56, 52, 255 ), 
		( 1.0, 184, 112, 48, 255 )], 
	lightContrast=1.4, 
	heightMap=BeginLayer_heightMap, 
	enabledLight=False)

MiddleLayer_renderer=RD.create (  name='MiddleLayer_renderer',  
	destImage=BaseImage, 
	lightBrightness=2.0, 
	randomGradient=False, 
	gradientInfo=[
		( -1.0, 52, 51, 49, 6 ), 
		( -0.714285714286, 52, 50, 48, 1 ), 
		( -0.428571428571, 55, 53, 51, 12 ), 
		( -0.142857142857, 55, 53, 51, 15 ), 
		( 0.142857142857, 52, 50, 48, 28 ), 
		( 0.428571428571, 54, 52, 50, 26 ), 
		( 0.714285714286, 57, 54, 52, 27 ), 
		( 1.0, 160, 84, 42, 255 )], 
	lightContrast=1.4, 
	heightMap=MiddleLayer_heightMap, 
	backgroundImage=BaseImage, 
	enabledLight=False, 
	bumpMap=BeginLayer_heightMap)

ExtraLayer_0_renderer=RD.create (  name='ExtraLayer_0_renderer',  
	destImage=BaseImage, 
	lightBrightness=2.0, 
	randomGradient=False, 
	gradientInfo=[
		( -1.0, 94, 57, 55, 20 ), 
		( -0.714285714286, 96, 59, 57, 10 ), 
		( -0.428571428571, 95, 59, 56, 13 ), 
		( -0.142857142857, 92, 57, 54, 1 ), 
		( 0.142857142857, 92, 56, 53, 192 ), 
		( 0.428571428571, 91, 54, 51, 27 ), 
		( 0.714285714286, 90, 52, 49, 192 ), 
		( 1.0, 196, 52, 114, 255 )], 
	lightContrast=1.4, 
	heightMap=ExtraLayer_0_heightMap, 
	backgroundImage=BaseImage, 
	enabledLight=False, 
	bumpMap=BeginLayer_heightMap)

ExtraLayer_1_renderer=RD.create (  name='ExtraLayer_1_renderer',  
	destImage=BaseImage, 
	lightBrightness=2.0, 
	randomGradient=False, 
	gradientInfo=[
		( -1.0, 98, 57, 56, 192 ), 
		( -0.714285714286, 102, 60, 57, 5 ), 
		( -0.428571428571, 104, 62, 58, 1 ), 
		( -0.142857142857, 100, 57, 55, 17 ), 
		( 0.142857142857, 103, 57, 55, 13 ), 
		( 0.428571428571, 104, 55, 55, 14 ), 
		( 0.714285714286, 104, 57, 56, 31 ), 
		( 1.0, 164, 161, 158, 255 )], 
	lightContrast=1.4, 
	heightMap=ExtraLayer_1_heightMap, 
	backgroundImage=BaseImage, 
	enabledLight=False, 
	bumpMap=BeginLayer_heightMap)

snow_renderer=RD.create (  name='snow_renderer',  
	destImage=BaseImage, 
	lightBrightness=2.1, 
	randomGradient=False, 
	gradientInfo=[
		( -1.0, 0, 0, 0, 0 ), 
		( -0.4, 0, 0, 0, 0 ), 
		( -0.23, 255, 255, 255, 0 ), 
		( 0.0, 255, 255, 255, 128 ), 
		( 0.23, 255, 255, 255, 0 ), 
		( 0.6, 0, 0, 0, 0 ), 
		( 1.0, 0, 0, 0, 0 )], 
	lightContrast=2.1, 
	heightMap=bump_heightMap, 
	backgroundImage=BaseImage, 
	enabledLight=True)

GrassLayer_renderer=RD.create (  name='GrassLayer_renderer',  
	destImage=BaseImage, 
	lightBrightness=1.0, 
	randomGradient=False, 
	gradientInfo=[
		( -1.0, 114, 111, 68, 0 ), 
		( -0.71, 93, 105, 53, 0 ), 
		( -0.57, 74, 75, 65, 0 ), 
		( -0.42, 76, 84, 47, 0 ), 
		( -0.28, 46, 51, 19, 141 ), 
		( -0.039, 46, 51, 19, 140 ), 
		( 0.21, 76, 84, 47, 140 ), 
		( 0.57, 103, 131, 34, 9 ), 
		( 0.6, 73, 73, 48, 0 ), 
		( 1.0, 99, 168, 165, 0 )], 
	lightContrast=1.0, 
	heightMap=BeginLayer_heightMap, 
	backgroundImage=BaseImage, 
	enabledLight=False)

DesertLayer_renderer=RD.create (  name='DesertLayer_renderer',  
	destImage=BaseImage, 
	lightBrightness=1.0, 
	randomGradient=False, 
	gradientInfo=[
		( -1.0, 254, 238, 197, 0 ), 
		( -0.93, 237, 235, 212, 0 ), 
		( -0.87, 223, 214, 183, 30 ), 
		( -0.81, 228, 168, 134, 75 ), 
		( -0.75, 237, 235, 212, 45 ), 
		( -0.68, 224, 220, 56, 0 ), 
		( 1.0, 99, 168, 165, 0 )], 
	lightContrast=1.0, 
	heightMap=heightMapPole, 
	backgroundImage=BaseImage, 
	enabledLight=False)

RendererSea=RD.create (  name='RendererSea',  
	destImage=BaseImage, 
	lightBrightness=2.0, 
	randomGradient=False, 
	gradientInfo=[
		( -1.0, 20, 36, 101, 255 ), 
		( 0.26938, 23, 50, 115, 255 ), 
		( 0.28438, 26, 58, 123, 255 ), 
		( 0.29438, 34, 62, 145, 255 ), 
		( 0.29448, 226, 230, 212, 128 ), 
		( 0.40203, 226, 230, 212, 64 ), 
		( 0.50203, 192, 189, 191, 32 ), 
		( 0.75203, 173, 253, 194, 0 ), 
		( 1.0, 255, 255, 255, 0 )], 
	lightContrast=1.4, 
	heightMap=heightMapSea, 
	backgroundImage=BaseImage, 
	enabledLight=False, 
	bumpMap=BeginLayer_heightMap)

RendererPole=RD.create (  name='RendererPole',  
	destImage=BaseImage, 
	lightBrightness=1.0, 
	randomGradient=False, 
	gradientInfo=[
		( -1.0, 0, 0, 0, 0 ), 
		( 0.340186, 230, 240, 250, 0 ), 
		( 0.3420885, 248, 254, 255, 255 ), 
		( 1.0, 255, 255, 255, 255 )], 
	lightContrast=1.0, 
	heightMap=heightMapPole, 
	backgroundImage=BaseImage, 
	enabledLight=False, 
	bumpMap=BeginLayer_heightMap)

bumpmap_renderer=RD.create (  name='bumpmap_renderer',  
	destImage=img_bump, 
	lightBrightness=1.0, 
	randomGradient=False, 
	gradientInfo=[
		( -1.0, 0, 0, 0, 255 ), 
		( -0.4, 0, 0, 0, 255 ), 
		( 0.0, 255, 255, 255, 255 ), 
		( 0.6, 0, 0, 0, 255 ), 
		( 1.0, 0, 0, 0, 255 )], 
	lightContrast=1.0, 
	heightMap=bump_heightMap, 
	enabledLight=False)

bumpmap_renderer_mask=RD.create (  name='bumpmap_renderer_mask',  
	destImage=img_bump, 
	lightBrightness=1.0, 
	randomGradient=False, 
	gradientInfo=[
		( -1.0, 0, 0, 0, 255 ), 
		( 0.40253, 0, 0, 0, 255 ), 
		( 0.55203, 0, 0, 0, 0 ), 
		( 1.0, 0, 0, 0, 0 )], 
	lightContrast=1.0, 
	heightMap=heightMapSea, 
	backgroundImage=img_bump, 
	enabledLight=False)

specmap_renderer=RD.create (  name='specmap_renderer',  
	destImage=img_spec, 
	lightBrightness=1.0, 
	randomGradient=False, 
	gradientInfo=[
		( -1.0, 255, 255, 255, 255 ), 
		( 1.0, 255, 255, 255, 255 )], 
	lightContrast=1.0, 
	heightMap=spec_heightMap, 
	enabledLight=False)

specmap_renderer_spec=RD.create (  name='specmap_renderer_spec',  
	destImage=img_spec, 
	lightBrightness=1.0, 
	randomGradient=False, 
	gradientInfo=[
		( -1.0, 255, 255, 255, 255 ), 
		( 0.29437999, 255, 255, 255, 255 ), 
		( 0.29438, 0, 0, 0, 255 ), 
		( 1.0, 0, 0, 0, 255 )], 
	lightContrast=1.0, 
	heightMap=heightMapSea, 
	backgroundImage=img_spec, 
	enabledLight=False)

geo_renderer=RD.create (  name='geo_renderer',  
	destImage=img_geo, 
	lightBrightness=1.0, 
	randomGradient=False, 
	gradientInfo=[
		( -1.0, 255, 255, 255, 255 ), 
		( 1.0, 255, 255, 255, 255 )], 
	lightContrast=1.0, 
	heightMap=geo_heightMap, 
	enabledLight=False)

geomap_renderer_geo=RD.create (  name='geomap_renderer_geo',  
	destImage=img_geo, 
	lightBrightness=1.0, 
	randomGradient=False, 
	gradientInfo=[
		( -1.0, 255, 255, 255, 255 ), 
		( 0.29437999, 255, 255, 255, 255 ), 
		( 0.29438, 0, 0, 0, 255 ), 
		( 0.29488, 224, 224, 224, 255 ), 
		( 0.34538, 224, 224, 224, 255 ), 
		( 1.0, 224, 224, 224, 255 )], 
	lightContrast=1.0, 
	heightMap=heightMapSea, 
	backgroundImage=img_geo, 
	enabledLight=False)
BeginLayer_renderer.setRandomFactor(20,40,60)

MiddleLayer_renderer.setRandomFactor(20,40,60)

ExtraLayer_0_renderer.setRandomFactor(20,40,60)

ExtraLayer_1_renderer.setRandomFactor(20,40,60)


texture = planetnoise.TextureBuilder()

texture.appendModuleDescriptor([AATurboPoles,AATurboSea,BeginLayer_Module,CylinderPole,ExtraLayer_0_Module,ExtraLayer_1_Module,MiddleLayer_Module,RidgedBumpMapModule,RidgedBumpMapModule_e,RidgedBumpMapModule_e_t,SeaModule])        # add modules to builder
texture.appendImageDescriptor([BaseImage,img_bump,img_geo,img_spec])           # add images to builder
texture.appendHeightMapDescriptor([BeginLayer_heightMap,ExtraLayer_0_heightMap,ExtraLayer_1_heightMap,MiddleLayer_heightMap,heightMapPole,heightMapSea,bump_heightMap,geo_heightMap,spec_heightMap])   # add heightmaps to builder
texture.appendNoiseMapBuilderDescriptor([BeginLayer_noiseMapBuilder1,ExtraLayer_0_noiseMapBuilder1,ExtraLayer_1_noiseMapBuilder1,MiddleLayer_noiseMapBuilder1,heightMapBuilderPole,heightMapBuilderSea,noiseMapBuilderBump,noiseMapBuilderGeo,noiseMapBuilderSpec]) # add heightmapbuilder
texture.appendRendererDescriptor([BeginLayer_renderer,MiddleLayer_renderer,ExtraLayer_0_renderer,ExtraLayer_1_renderer,snow_renderer,GrassLayer_renderer,DesertLayer_renderer,RendererSea,RendererPole,bumpmap_renderer,bumpmap_renderer_mask,specmap_renderer,specmap_renderer_spec,geo_renderer,geomap_renderer_geo])     # finally, lets add the renderer

texture.outputFileName = "Texture_planet02"
texture.buildTextureFromJsonString(texture.jsonString(), ".")


