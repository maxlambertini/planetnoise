import planetnoise
from planetnoise import TextureBuilder, ModuleDescriptor as MD, ImageDescriptor as ID, HeightMapDescriptor as HMD
from planetnoise import RendererDescriptor as RD
from planetnoise import NoiseMapBuilderDescriptor as NMB

# let's build the perlin noise descriptor
# seed = 0 means "totally random"
perlin = MD.Perlin( name="Perlin", 
                    seed=0, 
                    freq=1.2,
                     lac=3.8, 
                    pers=0.25,
                      oct=8)

voronoi = MD.Voronoi(  name="Voronoi",
                       seed=0,
                       freq=7.3,
                       disp=1.1)

billow = MD.Billow ( name="Billow",
                     seed=0,
                     freq=1.0,
                      lac=3.0,
                     pers=0.3,
                      oct=8)

select = MD.Select ( name="Select",
                      src1=perlin,
                      src2=billow,
                       ctl=voronoi,
                    uBound=0.5,
                    lBound=-0.5,
                     value=1.0)



# then the image descriptor, which will hold rendered image data
image = ID.create("Image")

# then the heightmap, which will hold heightmap data
heightmap = HMD.create("Heightmap")


# connect the noise module with heightmap
heightmapBuilder = NMB.create("hmbuilder", module=select, heightMap=heightmap)




# this is a simple gradient we'll use to render land areas:
planet_gradient = [                
    (-1.0  , 20, 20, 80,255),    
    (-0.1  , 30, 30,128,255),
    ( 0.1  , 40, 40,144,255),
    ( 0.11,255,255,192,255),
    ( 0.31,  0,128,  0,255),
    ( 0.95,100, 50,  0,255),
    ( 1.0 ,255,255,255,255)
]


# let's  create the first Renderer descriptor, which renders an HeightMap to an Image.
# we're using a "land-only gradient" 
renderer  = RD.create(name="renderer", 
    heightMap=heightmap,       # name of the heightmap you want to render
    gradientInfo=planet_gradient,
    destImage=image,
    lightContrast = 2.0,
    enabledLight= True)



# then let's assemble everything together in the TextureBuilder
texture = planetnoise.TextureBuilder()

texture.appendModuleDescriptor([perlin,select,voronoi,billow])        # add modules to builder
texture.appendImageDescriptor(image)           # add images to builder
texture.appendHeightMapDescriptor(heightmap)   # add heightmaps to builder
texture.appendNoiseMapBuilderDescriptor(heightmapBuilder) # add heightmapbuilder
texture.appendRendererDescriptor(renderer)     # finally, lets add the renderer

jsonTexture = texture.jsonString()
with open ("Planet_select.texjson","w") as f:
    f.write(jsonTexture)

texture.outputFileName = "Planet_Select"
texture.buildTexture(".")

