import planetnoise
from planetnoise import TextureBuilder, ModuleDescriptor as MD, ImageDescriptor as ID, HeightMapDescriptor as HMD
from planetnoise import RendererDescriptor as RD
from planetnoise import NoiseMapBuilderDescriptor as NMB


BeginLayer_Module=MD.Perlin ( name='BeginLayer_Module',  
	enableRandom=True, 
	lac=3.13120007515, 
	pers=0.31049400568, 
	seed=0, 
	freq=2.46015000343, 
	oct=6)
CylinderPole=MD.Cylinders ( name='CylinderPole',  
	freq=0.5, 
	enableRandom=False)
MiddleLayer_Module=MD.Perlin ( name='MiddleLayer_Module',  
	enableRandom=True, 
	lac=3.41510391235, 
	pers=0.307716012001, 
	seed=0, 
	freq=2.28555011749, 
	oct=6)
RidgedBumpMapModule=MD.Perlin ( name='RidgedBumpMapModule',  
	enableRandom=False, 
	lac=3.6, 
	pers=0.3, 
	seed=0, 
	freq=6.3, 
	oct=8)
SeaModule=MD.Perlin ( name='SeaModule',  
	enableRandom=True, 
	lac=3.6528, 
	pers=0.352696, 
	seed=0, 
	freq=1.16308, 
	oct=8)
AATurboPoles=MD.Turbulence ( name='AATurboPoles',  
	src1=CylinderPole, 
	pow=0.29685, 
	enableRandom=False, 
	seed=0, 
	freq=4.5798, 
	rough=3.26005)
AATurboSea=MD.Turbulence ( name='AATurboSea',  
	src1=SeaModule, 
	pow=0.18336, 
	enableRandom=False, 
	seed=0, 
	freq=1.1798, 
	rough=2.139)
RidgedBumpMapModule_e=MD.Exponent ( name='RidgedBumpMapModule_e',  
	src1=RidgedBumpMapModule, 
	enableRandom=False, 
	exp=1.25)
RidgedBumpMapModule_e_t=MD.Turbulence ( name='RidgedBumpMapModule_e_t',  
	src1=RidgedBumpMapModule_e, 
	pow=0.05, 
	enableRandom=False, 
	seed=0, 
	freq=0.75, 
	rough=1.25)

BaseImage=ID.create('BaseImage')
img_bump=ID.create('img_bump')
img_geo=ID.create('img_geo')
img_spec=ID.create('img_spec')


BeginLayer_heightMap=HMD.create('BeginLayer_heightMap')
MiddleLayer_heightMap=HMD.create('MiddleLayer_heightMap')
heightMapPole=HMD.create('heightMapPole')
heightMapSea=HMD.create('heightMapSea')
bump_heightMap=HMD.create('bump_heightMap')
geo_heightMap=HMD.create('geo_heightMap')
spec_heightMap=HMD.create('spec_heightMap')

BeginLayer_noiseMapBuilder1 = NMB.create('BeginLayer_noiseMapBuilder1', module=BeginLayer_Module, heightMap=BeginLayer_heightMap)
MiddleLayer_noiseMapBuilder1 = NMB.create('MiddleLayer_noiseMapBuilder1', module=MiddleLayer_Module, heightMap=MiddleLayer_heightMap)
heightMapBuilderPole = NMB.create('heightMapBuilderPole', module=AATurboPoles, heightMap=heightMapPole)
heightMapBuilderSea = NMB.create('heightMapBuilderSea', module=AATurboSea, heightMap=heightMapSea)
noiseMapBuilderBump = NMB.create('noiseMapBuilderBump', module=RidgedBumpMapModule_e_t, heightMap=bump_heightMap)
noiseMapBuilderGeo = NMB.create('noiseMapBuilderGeo', module=MiddleLayer_Module, heightMap=geo_heightMap)
noiseMapBuilderSpec = NMB.create('noiseMapBuilderSpec', module=MiddleLayer_Module, heightMap=spec_heightMap)

BeginLayer_renderer=RD.create (  name='BeginLayer_renderer',  
	destImage=BaseImage, 
	lightBrightness=2.0, 
	randomGradient=False, 
	gradientInfo=[
		( -1.0, 97, 57, 56, 255 ), 
		( -0.333333333333, 56, 53, 52, 255 ), 
		( 0.333333333333, 100, 98, 56, 255 ), 
		( 0.99, 55, 57, 53, 255 ), 
		( 1.0, 201, 178, 248, 255 )], 
	lightContrast=1.4, 
	heightMap=BeginLayer_heightMap, 
	enabledLight=False)

MiddleLayer_renderer=RD.create (  name='MiddleLayer_renderer',  
	destImage=BaseImage, 
	lightBrightness=2.0, 
	randomGradient=False, 
	gradientInfo=[
		( -1.0, 57, 56, 53, 10 ), 
		( -0.666666666667, 57, 55, 52, 18 ), 
		( -0.333333333333, 60, 57, 55, 14 ), 
		( -1.11022302463e-16, 60, 57, 55, 26 ), 
		( 0.333333333333, 57, 54, 52, 24 ), 
		( 0.666666666667, 59, 56, 54, 192 ), 
		( 1.0, 62, 58, 56, 5 ), 
		( 1.0, 74, 161, 119, 255 )], 
	lightContrast=1.4, 
	heightMap=MiddleLayer_heightMap, 
	backgroundImage=BaseImage, 
	enabledLight=False, 
	bumpMap=BeginLayer_heightMap)

snow_renderer=RD.create (  name='snow_renderer',  
	destImage=BaseImage, 
	lightBrightness=2.1, 
	randomGradient=False, 
	gradientInfo=[
		( -1.0, 0, 0, 0, 0 ), 
		( -0.4, 0, 0, 0, 0 ), 
		( -0.23, 255, 255, 255, 0 ), 
		( 0.0, 255, 255, 255, 128 ), 
		( 0.23, 255, 255, 255, 0 ), 
		( 0.6, 0, 0, 0, 0 ), 
		( 1.0, 0, 0, 0, 0 )], 
	lightContrast=2.1, 
	heightMap=bump_heightMap, 
	backgroundImage=BaseImage, 
	enabledLight=True)

GrassLayer_renderer=RD.create (  name='GrassLayer_renderer',  
	destImage=BaseImage, 
	lightBrightness=1.0, 
	randomGradient=False, 
	gradientInfo=[
		( -1.0, 114, 111, 68, 0 ), 
		( -0.71, 93, 105, 53, 0 ), 
		( -0.57, 74, 75, 65, 0 ), 
		( -0.42, 76, 84, 47, 0 ), 
		( -0.28, 46, 51, 19, 141 ), 
		( -0.039, 46, 51, 19, 140 ), 
		( 0.21, 76, 84, 47, 140 ), 
		( 0.57, 103, 131, 34, 9 ), 
		( 0.6, 73, 73, 48, 0 ), 
		( 1.0, 99, 168, 165, 0 )], 
	lightContrast=1.0, 
	heightMap=BeginLayer_heightMap, 
	backgroundImage=BaseImage, 
	enabledLight=False)

DesertLayer_renderer=RD.create (  name='DesertLayer_renderer',  
	destImage=BaseImage, 
	lightBrightness=1.0, 
	randomGradient=False, 
	gradientInfo=[
		( -1.0, 254, 238, 197, 0 ), 
		( -0.93, 237, 235, 212, 0 ), 
		( -0.87, 223, 214, 183, 30 ), 
		( -0.81, 228, 168, 134, 75 ), 
		( -0.75, 237, 235, 212, 45 ), 
		( -0.68, 224, 220, 56, 0 ), 
		( 1.0, 99, 168, 165, 0 )], 
	lightContrast=1.0, 
	heightMap=heightMapPole, 
	backgroundImage=BaseImage, 
	enabledLight=False)

RendererSea=RD.create (  name='RendererSea',  
	destImage=BaseImage, 
	lightBrightness=2.0, 
	randomGradient=False, 
	gradientInfo=[
		( -1.0, 20, 36, 101, 255 ), 
		( 0.24446, 23, 50, 115, 255 ), 
		( 0.25946, 26, 58, 123, 255 ), 
		( 0.26946, 34, 62, 145, 255 ), 
		( 0.26956, 146, 67, 4, 128 ), 
		( 0.36919, 146, 67, 4, 64 ), 
		( 0.46919, 153, 192, 171, 32 ), 
		( 0.71919, 231, 231, 231, 0 ), 
		( 1.0, 255, 255, 255, 0 )], 
	lightContrast=1.4, 
	heightMap=heightMapSea, 
	backgroundImage=BaseImage, 
	enabledLight=False, 
	bumpMap=BeginLayer_heightMap)

RendererPole=RD.create (  name='RendererPole',  
	destImage=BaseImage, 
	lightBrightness=1.0, 
	randomGradient=False, 
	gradientInfo=[
		( -1.0, 0, 0, 0, 0 ), 
		( 0.262178, 230, 240, 250, 0 ), 
		( 0.262974, 248, 254, 255, 255 ), 
		( 1.0, 255, 255, 255, 255 )], 
	lightContrast=1.0, 
	heightMap=heightMapPole, 
	backgroundImage=BaseImage, 
	enabledLight=False, 
	bumpMap=BeginLayer_heightMap)

bumpmap_renderer=RD.create (  name='bumpmap_renderer',  
	destImage=img_bump, 
	lightBrightness=1.0, 
	randomGradient=False, 
	gradientInfo=[
		( -1.0, 0, 0, 0, 255 ), 
		( -0.4, 0, 0, 0, 255 ), 
		( 0.0, 255, 255, 255, 255 ), 
		( 0.6, 0, 0, 0, 255 ), 
		( 1.0, 0, 0, 0, 255 )], 
	lightContrast=1.0, 
	heightMap=bump_heightMap, 
	enabledLight=False)

bumpmap_renderer_mask=RD.create (  name='bumpmap_renderer_mask',  
	destImage=img_bump, 
	lightBrightness=1.0, 
	randomGradient=False, 
	gradientInfo=[
		( -1.0, 0, 0, 0, 255 ), 
		( 0.36969, 0, 0, 0, 255 ), 
		( 0.51919, 0, 0, 0, 0 ), 
		( 1.0, 0, 0, 0, 0 )], 
	lightContrast=1.0, 
	heightMap=heightMapSea, 
	backgroundImage=img_bump, 
	enabledLight=False)

specmap_renderer=RD.create (  name='specmap_renderer',  
	destImage=img_spec, 
	lightBrightness=1.0, 
	randomGradient=False, 
	gradientInfo=[
		( -1.0, 255, 255, 255, 255 ), 
		( 1.0, 255, 255, 255, 255 )], 
	lightContrast=1.0, 
	heightMap=spec_heightMap, 
	enabledLight=False)

specmap_renderer_spec=RD.create (  name='specmap_renderer_spec',  
	destImage=img_spec, 
	lightBrightness=1.0, 
	randomGradient=False, 
	gradientInfo=[
		( -1.0, 255, 255, 255, 255 ), 
		( 0.26945999, 255, 255, 255, 255 ), 
		( 0.26946, 0, 0, 0, 255 ), 
		( 1.0, 0, 0, 0, 255 )], 
	lightContrast=1.0, 
	heightMap=heightMapSea, 
	backgroundImage=img_spec, 
	enabledLight=False)

geo_renderer=RD.create (  name='geo_renderer',  
	destImage=img_geo, 
	lightBrightness=1.0, 
	randomGradient=False, 
	gradientInfo=[
		( -1.0, 255, 255, 255, 255 ), 
		( 1.0, 255, 255, 255, 255 )], 
	lightContrast=1.0, 
	heightMap=geo_heightMap, 
	enabledLight=False)

geomap_renderer_geo=RD.create (  name='geomap_renderer_geo',  
	destImage=img_geo, 
	lightBrightness=1.0, 
	randomGradient=False, 
	gradientInfo=[
		( -1.0, 255, 255, 255, 255 ), 
		( 0.26945999, 255, 255, 255, 255 ), 
		( 0.26946, 0, 0, 0, 255 ), 
		( 0.26996, 224, 224, 224, 255 ), 
		( 0.32046, 224, 224, 224, 255 ), 
		( 1.0, 224, 224, 224, 255 )], 
	lightContrast=1.0, 
	heightMap=heightMapSea, 
	backgroundImage=img_geo, 
	enabledLight=False)
BeginLayer_renderer.setRandomFactor(20,40,60)

MiddleLayer_renderer.setRandomFactor(20,40,60)


texture = planetnoise.TextureBuilder()

texture.appendModuleDescriptor([AATurboPoles,AATurboSea,BeginLayer_Module,CylinderPole,MiddleLayer_Module,RidgedBumpMapModule,RidgedBumpMapModule_e,RidgedBumpMapModule_e_t,SeaModule])        # add modules to builder
texture.appendImageDescriptor([BaseImage,img_bump,img_geo,img_spec])           # add images to builder
texture.appendHeightMapDescriptor([BeginLayer_heightMap,MiddleLayer_heightMap,heightMapPole,heightMapSea,bump_heightMap,geo_heightMap,spec_heightMap])   # add heightmaps to builder
texture.appendNoiseMapBuilderDescriptor([BeginLayer_noiseMapBuilder1,MiddleLayer_noiseMapBuilder1,heightMapBuilderPole,heightMapBuilderSea,noiseMapBuilderBump,noiseMapBuilderGeo,noiseMapBuilderSpec]) # add heightmapbuilder
texture.appendRendererDescriptor([BeginLayer_renderer,MiddleLayer_renderer,snow_renderer,GrassLayer_renderer,DesertLayer_renderer,RendererSea,RendererPole,bumpmap_renderer,bumpmap_renderer_mask,specmap_renderer,specmap_renderer_spec,geo_renderer,geomap_renderer_geo])     # finally, lets add the renderer

json = texture.jsonString()
with open("ciccio.texjson","w") as f:
	f.write(json)

texture.outputFileName = "Texture_planet01"
texture.buildTextureFromJsonString(json, ".")


