import planetnoise
from planetnoise import TextureBuilder, ModuleDescriptor as MD, ImageDescriptor as ID, HeightMapDescriptor as HMD
from planetnoise import RendererDescriptor as RD
from planetnoise import NoiseMapBuilderDescriptor as NMB

# let's build the perlin noise descriptor
# seed = 0 means "totally random"
perlin = MD.Perlin( name="Perlin", 
    seed=0, 
    freq=1.2,
    lac=1.8, 
    pers=0.6,
    oct=8
)

# let's build another Perlin Descriptor for sea:
# take care to use a different name from other modules!
perlin_sea = MD.Perlin( name="PerlinSea", 
    seed=0, 
    freq=1.2,
    lac=1.8, 
    pers=0.6,
    oct=8
)


# then the image descriptor, which will hold rendered image data
image = ID.create("Image")

# then the heightmap, which will hold heightmap data
heightmap = HMD.create("Heightmap")

# we need an heightmap for sea, too. 
heightmapSea = HMD.create("HeightmapSea")

# connect the noise module with heightmap
heightmapBuilder = NMB.create("hmbuilder", module=perlin, heightMap=heightmap)

# we also need a hm builder for sea. 
heightmapBuilderSea = NMB.create("hmbuilderSea", module=perlin_sea, heightMap=heightmapSea)


# this is a simple gradient we'll use to render land areas:
land_gradient = [                
    ( -1.0,255,255,192,255),
    ( 0.01,  0,128,  0,255),
    ( 0.95,100, 50,  0,255),
    ( 1.0 ,255,255,255,255)
]

# this is a sea gradient. It's sea blue until 55%, then there's a little sand strip
# that becomes transparent. We'll use it to generate a blue map with a cutout area that makes for 
# the continents' profiles. 
sea_gradient = [
    (-1.0  , 20, 20, 80,255),    
    (-0.1  , 30, 30,128,255),
    ( 0.1  , 40, 40,144,255),
    ( 0.101,255,255,192,255),
    ( 0.210,255,255,192,  0),
    ( 1.0  ,255,255,192,  0),
]

# let's  create the first Renderer descriptor, which renders an HeightMap to an Image.
# we're using a "land-only gradient" 
renderer  = RD.create(name="renderer", 
    heightMap=heightmap,       # name of the heightmap you want to render
    gradientInfo = land_gradient,
    destImage=image,
    lightContrast = 2.0,
    enabledLight= True)

# then the second: we'll use the sea gradient to create a "sea overlay" on the map. 
# this will make our world map slightly more natural, since land and sea have their own generator.
renderer_sea  = RD.create(name="renderer_sea", 
    heightMap=heightmapSea,       # name of the heightmap you want to render
    gradientInfo = sea_gradient,
    destImage=image,
    backgroundImage= image,
    lightContrast = 2.0,
    enabledLight= False)


# then let's assemble everything together in the TextureBuilder
texture = planetnoise.TextureBuilder()

texture.appendModuleDescriptor(perlin.name, perlin)        # add modules to builder
texture.appendModuleDescriptor(perlin_sea.name, perlin_sea)        

texture.appendImageDescriptor(image.name, image)           # add images to builder

texture.appendHeightMapDescriptor(heightmap.name, heightmap)   # add heightmaps to builder
texture.appendHeightMapDescriptor(heightmapSea.name, heightmapSea)   

texture.appendNoiseMapBuilderDescriptor(heightmapBuilder.name, heightmapBuilder) # add heightmapbuilder
texture.appendNoiseMapBuilderDescriptor(heightmapBuilderSea.name, heightmapBuilderSea) 

texture.appendRendererDescriptor(renderer.name, renderer)     # finally, lets add the renderer
texture.appendRendererDescriptor(renderer_sea.name, renderer_sea)     # finally, lets add the renderer

jsonTexture = texture.jsonString()
with open ("Planet_02.texjson","w") as f:
    f.write(jsonTexture)

texture.outputFileName = "Planet_02"
texture.buildTextureFromJsonString(texture.jsonString(), ".")

