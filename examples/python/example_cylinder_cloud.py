import planetnoise
from planetnoise import TextureBuilder, ModuleDescriptor as MD, ImageDescriptor as ID, HeightMapDescriptor as HMD
from planetnoise import RendererDescriptor as RD
from planetnoise import NoiseMapBuilderDescriptor as NMB

# let's build the perlin noise descriptor
# seed = 0 means "totally random"

texture = planetnoise.TextureBuilder()

cylinder = MD.Cylinder("cylinder", 0.5)
turbo1 = MD.Turbulence("turbo1",cylinder,0,8.25,0.25,9.05)
turbo2 = MD.Turbulence("turbo2",cylinder,0,7.50,0.12,11.05)

texture.appendModuleDescriptor([
    cylinder,turbo1,turbo2
])

# then the image descriptor, which will hold rendered image data
image = ID.create("Alpha")
imageCloud = ID.create("Cloud")

texture.appendImageDescriptor([image, imageCloud])

# then the heightmap, which will hold heightmap data
heightmap1 = HMD.create("Heightmap1")
heightmap2 = HMD.create("Heightmap2")

texture.appendHeightMapDescriptor([
    heightmap1,heightmap2
])

# connect the noise module with heightmap
heightmapBuilder1 = NMB.create("hmbuilder1", module=turbo1, heightMap=heightmap1)
heightmapBuilder2 = NMB.create("hmbuilder2", module=turbo2, heightMap=heightmap2)

texture.appendNoiseMapBuilderDescriptor([
    heightmapBuilder1,heightmapBuilder2
])

# this is a simple gradient we'll use to render land areas:
cloud_gradient_1 = [                
    ( 1.0  , 48, 48, 48,255),
    ( 0.20 , 48, 48, 48,255),
    ( -0.19 ,192,192,192,255),
    ( -0.49 ,192,192,192,255),
    ( -0.55 ,255,255,255,255),
    ( -0.95  ,255,255,255,255),
    ( -0.96,0,0,0,255),
    ( -1.0 ,0,0,192,255),
]

cloud_gradient_2 = [                
    (-1.0  ,192,192,192,255),
    (-0.95 ,192,192,192,255),
    (-0.85  ,192,192,192,  0),
    (-0.40 ,192,192,192,  0),
    (-0.38 ,192,192,192,255),
    (-0.08 ,192,192,192,255),
    (-0.07 ,192,192,192,  0),
    ( 0.47 ,192,192,192,  0),
    ( 0.52 ,140,140,140,255),
    ( 0.72 ,140,140,140,255),
    ( 0.79 ,140,140,140,0),
    ( 1.0  ,255,255,255,0),
]

cloud_color_1 = [                
    ( 1.0  , 255,255,255,255),
    ( 0.20 , 255,255,255,255),
    ( -0.19 ,80,160,255,255),
    ( -0.49 ,80,160,255,255),
    ( -1.0  ,96,192,255,255),
]





# let's  create the first Renderer descriptor, which renders an HeightMap to an Image.
# we're using a "land-only gradient" 
renderer1  = RD.create(name="renderer1", 
    heightMap=heightmap1,       # name of the heightmap you want to render
    gradientInfo=cloud_gradient_1,
    destImage=image,
    lightContrast = 2.0,
    enabledLight= False)

renderer2  = RD.create(name="renderer2", 
    heightMap=heightmap2,       # name of the heightmap you want to render
    gradientInfo=cloud_gradient_2,
    destImage=image,
    backgroundImage=image,
    lightContrast = 2.0,
    enabledLight= False)

rendererC1  = RD.create(name="rendererC", 
    heightMap=heightmap2,       # name of the heightmap you want to render
    gradientInfo=cloud_color_1,
    destImage=imageCloud,
    lightContrast = 2.0,
    enabledLight= False)


texture.appendRendererDescriptor([
    renderer1,renderer2,rendererC1
])


jsonTexture = texture.jsonString()
with open ("Planet_cylinder_cloud.texjson","w") as f:
    f.write(jsonTexture)

texture.outputFileName = "Planet_cylinder_cloud"
texture.buildTexture(".")

