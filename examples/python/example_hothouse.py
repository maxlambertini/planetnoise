# How to build a texture:

# To build a texture you operate this way:

# First you build and connect one or more modules. 

# Modules can be:
# - Noise generators: these generate the fractals that will make up your world
# - Transformers: these modules transform the output of another module (for instance, Turbulence)
# - Combiners: these modules process the input of two or more modules ( for instance, min, max, average)

# Then you create the images descriptors, that will store binary image data. You can create and render 
# more than one image with a single TextureBuilder

# Then you create the heightmaps, that store modules' output in terms of height level, and range from
# -1.0 to 1.0

# After that, you connect heightmaps to modules by creating heightmap builders.

# You then must render heightmaps to images: to do this, you create Renderers, that connect heightmap 
# to images and render them using custom gradients. This way, you can render not only eartlike maps, but
# marslike maps, jupiterlike maps.

# Moreover, renderers are stackable: you can have a renderer write its heightmap data on a background image
# descriptor. This is the key to get realistic planetary maps. 

# After you are done creating these descriptor, you create a TextureBuilder. It is the object that binds all 
# this stuff together and outputs planetary maps. 

# You append these objects to TextureBuilder, and tell it to generate the textures. 

# Follow these examples to get a feel of planetnoise lib. 

import planetnoise
from planetnoise import TextureBuilder, ModuleDescriptor as MD, ImageDescriptor as ID, HeightMapDescriptor as HMD
from planetnoise import RendererDescriptor as RD
from planetnoise import NoiseMapBuilderDescriptor as NMB
import utils.gradients as GD
import random

def _r(fmin, fmax):
    return fmin + random.random()*(fmax-fmin)

# let's build the perlin noise descriptor
# seed = 0 means "totally random"
perlin = MD.Perlin( name="Perlin", 
    seed=0, 
    freq=_r(1.0,1.5),
    lac=_r(1.0,1.6), 
    pers=_r(0.2,0.4),
    oct=10
)
rotator = MD.RotatePoint("rotate", perlin, 15,0,-15)
stretcher = MD.ScalePoint("stretcher", rotator, 1.0,20.0,2.0)
turbo = MD.Turbulence("turbo",stretcher, 0, 2.5,0.12,1.5)

# then the image descriptor, which will hold rendered image data
image = ID.create("Image")

# then the heightmap, which will hold heightmap data
heightmap = HMD.create("Heightmap")


# connect the noise module with heightmap
heightmapBuilder = NMB.create("hmbuilder", module=turbo, heightMap=heightmap)




# this is a simple gradient we'll use to render land areas:
planet_gradient = GD.evolve_gradient (7, dh=3, ds=10, dv=30)
rot_gradient = GD.random_gradient(7, True, 32, 160)


# let's  create the first Renderer descriptor, which renders an HeightMap to an Image.
# we're using a "land-only gradient" 
renderer  = RD.create(name="renderer", 
    heightMap=heightmap,       # name of the heightmap you want to render
    gradientInfo=planet_gradient,
    destImage=image,
    lightContrast = 2.0,
    enabledLight= False)




# then let's assemble everything together in the TextureBuilder
texture = planetnoise.TextureBuilder()

texture.appendModuleDescriptor([perlin,rotator,stretcher,turbo])        # add modules to builder
texture.appendImageDescriptor([image])           # add images to builder
texture.appendHeightMapDescriptor([heightmap])   # add heightmaps to builder
texture.appendNoiseMapBuilderDescriptor([heightmapBuilder]) # add heightmapbuilder
texture.appendRendererDescriptor([renderer])     # finally, lets add the renderer

print (texture.jsonString())

texture.outputFileName = "Planet_HotHouse"
texture.buildTextureFromJsonString(texture.jsonString(), ".")

