import planetnoise
from planetnoise import TextureBuilder, ModuleDescriptor as MD, ImageDescriptor as ID, HeightMapDescriptor as HMD
from planetnoise import RendererDescriptor as RD
from planetnoise import NoiseMapBuilderDescriptor as NMB
import random

_EARTH_TONES="""#555142
#434237
#836539
#a29259
#d4cc9a
#f0d696
#cfbfb9
#cec5ad
#8d8468
#6e6969
#885132
#3d2b1f
#302621
#4f1507
#712f2c
#44382b
#37290e
#0e695f
#0b5509
#184a45
#448811
#002200
#0a481e
#005f56
#4b6d41
#00cc99
#5e6737
#749551
#7a1f3d
#922b05
#8f4c3a
#3e0007
#6d1008
#c69c5d
#b96902
#cc5500
#c87606
#a83c09
#f0944d
#be7249
#bf9b0c
#f6df9f
#f2e5b4
#fce081
#ede9ad
#eee78e"""



def hex_to_rgb(value):
    value = value.lstrip('#')
    lv = len(value)
    return tuple(int(value[i:i + lv // 3], 16) for i in range(0, lv, lv // 3))


EARTH_TONES = [ hex_to_rgb(v.strip())  for v in _EARTH_TONES.split("\n")]

def _r(a,b):
    return float(a + random.random()*(b-a))

class EarthTexture(object):
    def __init__(self, path, fil) :
        self.tb = TextureBuilder()
        self.path = path
        self.fil = fil

    def create(self):
        self.tb.setSize(1200,600)
        base = MD.Perlin("Base",0, _r(0.5,1.0), _r(3.0,5.0), _r(0.25,0.4), 6,False)
        blender = MD.Turbulence("Blender", 
            base , 
            0 , 
            _r(4.0,9.0),
             _r(0.1,0.5), 
             _r(1.01,3.01), 
             False)   
        sea = MD.Perlin("Sea",0,_r(0.9,1.9), _r(3.25,4.25), _r(0.22,0.38) )
        ridge = MD.RidgedMulti("Ridge",0,_r(0.9,1.9),_r(3.25,4.25),8)
        mountains = MD.Voronoi("Mountains",0,2.2,2.3,True)
        blender2 = MD.Turbulence("Blender2",base,0, _r(6.0,10.0), _r(0.1,0.4), _r(1.5,4.0))
        pole = MD.Cylinders("Pole",0.5)
        turboPole = MD.Turbulence("TurboPole",pole,0,1.2,0.26,8.002)
        blenderSea = MD.RotatePoint("BlenderSea",sea,90.0,0.3,1.1)
        displacer = MD.Avg("Displacer",ridge, mountains)
        turboDisp = MD.Turbulence("TurboDisp",displacer,0, _r(1.0,5.0), _r(0.05,0.3), _r(1.02,2.0))
        turboExp = MD.Exponent("TurboExp",turboDisp ,1.4)
        turboCurve = MD.Curve("TurboCurve",turboExp, [
                (-1.0,-1.0),
                ( 0.0, _r(-0.5,0.5)),
                ( 0.4, _r(-0.3,0.7)),
                ( 0.7, _r(0.5,0.8)),
                ( 1.0, _r(0.6,1.0))
            ])
        blenderBase = MD.Select("BlenderBase", blender,  sea, ridge, _r(0.5,0.8),  _r(-0.5,-0.1), 0.5, False)
        modules = [
            base, blender, sea, ridge, mountains, blender2, pole, turboPole,
            blenderBase, blenderSea, displacer, turboDisp, turboExp, turboCurve
        ]

        s0 = _r(-0.2,0.3)
        s1 = s0+0.001
        s2 = s1+.05
        s3 = s2+.05

        random.shuffle(EARTH_TONES)
        e = EARTH_TONES

        land = [
            (-1.0, e[0][0], e[0][1], e[0][2], 255),
            (-0.33, e[1][0], e[1][1], e[1][2], 255),
            (0.33, e[5][0], e[5][1], e[5][2], 255),
            (1.0, e[6][0], e[6][1], e[6][2], 255)
        ]
        sea = [
            (-1.0, 20, 60, 90,255),
            (s0,  20,  40, 120,255),
            (s1,  10,  80,  30,255),
            (s2,  20,100,   20,128),
            (s3,  10,120,   00,0),
            (0.7001,  76, 64,   00,0),
            (0.9997, 255, 224, 192,0),
            (   1.0, 255, 255, 255,0)
        ]

        sea_black = [
            (-1.0, 0, 0, 0,255),
            (s0,  0,  0, 0,255),
            (s0+.15,  0,  0,  0,128),
            (s0+.30,  0,  0,  0,128),
            (   1.0, 0, 0, 0,0)
        ]

        pole = [
            (-1.000, 255, 255, 255,   0),
            ( 0.199, 255, 255, 255,   0),
            ( 0.200, 255, 255, 255, 255),
            ( 1.000, 255, 255, 255, 255)
        ]

        height = [
            (-1.0,  0,  0,  0,255),
            ( 1.0,255,255,255,255)
        ]

        im = ID.create("Image")
        imBump = ID.create("Bump")

        hm = HMD.create("MapBuilder")
        hmSea = HMD.create("SeaBuilder")
        hmPole = HMD.create("PoleBuilder")
        hmBump = HMD.create("BumpBuilder")

        nbmBuilders = [
        planetnoise.NoiseMapBuilderDescriptor.create("landBuilder",blenderBase,hm),
        planetnoise.NoiseMapBuilderDescriptor.create("waterBuilder",blenderSea,hmSea),
        planetnoise.NoiseMapBuilderDescriptor.create("poleBuilder",turboPole,hmPole),
        planetnoise.NoiseMapBuilderDescriptor.create("bumpBuilder",turboCurve,hmBump)
        ]

        self.tb.setSize(1024,512)
        self.tb.appendHeightMapDescriptor(hm)
        self.tb.appendHeightMapDescriptor(hmSea)
        self.tb.appendHeightMapDescriptor(hmPole)
        self.tb.appendHeightMapDescriptor(hmBump)

        self.tb.appendImageDescriptor(im)
        self.tb.appendImageDescriptor(imBump)
        for mod in modules:
            self.tb.appendModuleDescriptor(mod)
        for bld in nbmBuilders:
            self.tb.appendNoiseMapBuilderDescriptor(bld)

        renderers = [
        RD.create("bumpRenderer",hmBump,height,imBump),
        RD.create("terraRenderer",hm,land,im, bumpMap=hmBump, enabledLight=True, lightContrast=2.0),
        RD.create("mareRenderer",hmSea,sea,im,im),
        RD.create("poleRenderer",hmPole,pole,im,im),
        RD.create("bumpBlackSea",hmSea,sea_black,imBump,imBump)
        ]

        for r in renderers:
            r.setRandomFactor(0,0,0)

        [ self.tb.appendRendererDescriptor(rn) for rn in renderers ]

        self.json = self.tb.jsonString()

    def render_and_save(self):
        with open("%s.texjson" % (self.fil), "w" ) as f:
            f.write(self.json)

        self.tb.clearAllContainers()
        self.tb.outputFileName = self.fil
        self.tb.buildTextureFromJsonString(self.json, ".")



fil = "Klass"

for i in range (0,8):
    et = EarthTexture(".", "%s_%s" % (fil, i))
    et.create()
    et.render_and_save()
