#!/usr/bin/python

from OpenGL.GLUT import *
from OpenGL.GLU import *
from OpenGL.GL import *
import sys
import math
from PIL import Image as Image
import numpy

class MyWnd:
    def __init__(self,texture_path, normal_path = None):
        self.texture_id = 0
        self.normal_id = None
        self.angle = 0
        self.zpos = 0.0
        self.zstep = 0.03;
        self.texture_path = texture_path
        self.normal_path = normal_path

    def run_scene(self):
        glutInit()
        glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGB | GLUT_DEPTH)
        glutInitWindowSize(700, 700)
        glutCreateWindow(b'Minimal sphere OpenGL')
        self.lightning()
        self.texture_id = self.read_texture(self.texture_path)
        if (self.normal_path != None):
            self.normal_id = self.read_texture(self.normal_path)
        glutDisplayFunc(self.draw_sphere)
        glMatrixMode(GL_PROJECTION)
        gluPerspective(40, 1, 1, 40)
        glutMainLoop()
        

    def lightning(self):
        glEnable(GL_DEPTH_TEST)
        glEnable(GL_LIGHTING)
        glEnable(GL_BLEND)
        glLightfv(GL_LIGHT0, GL_POSITION, [10, 4, 10, 1])
        glLightfv(GL_LIGHT0, GL_DIFFUSE, [0.8, 1, 0.8, 1])
        glLightf(GL_LIGHT0, GL_CONSTANT_ATTENUATION, 0.1)
        glLightf(GL_LIGHT0, GL_LINEAR_ATTENUATION, 0.05)
        glEnable(GL_LIGHT0)
        return        
    
    def read_texture(self,filename):
        img = Image.open(filename)
        img_data = numpy.array(list(img.getdata()), numpy.int8)
        texture_id = glGenTextures(1)
        glBindTexture(GL_TEXTURE_2D, texture_id) # This is what's missing
        glPixelStorei(GL_UNPACK_ALIGNMENT, 1)
        glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP)
        glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP)
        glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT)
        glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT)
        glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST)
        glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST)
        glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_DECAL)
        glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, img.size[0], img.size[1], 0,
                    GL_RGBA, GL_UNSIGNED_BYTE, img_data)
        return texture_id    

    def draw_sphere(self):
        glMatrixMode(GL_MODELVIEW)
        glLoadIdentity()
        gluLookAt(math.cos(self.angle)*4, math.sin(self.angle)*4, self.zpos, 0, 0, 0, 0.0,0.0,1.0)
        self.angle = self.angle+0.01
        self.zpos += self.zstep
        if (self.zpos > 5 or self.zpos < -5):
            self.zstep = - self.zstep
        glEnable(GL_DEPTH_TEST)
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT)
        glBindTexture(GL_TEXTURE_2D, self.texture_id)
        glBindTexture(GL_TEXTURE_2D, self.normal_id)

        glEnable(GL_TEXTURE_2D)

        qobj = gluNewQuadric()
        gluQuadricTexture(qobj, GL_TRUE)
        gluQuadricNormals(qobj, GL_SMOOTH)
        #gluQuadricOrientation(qobj, GL_OUTSIDE)
        gluSphere(qobj, 1, 50, 50)
        gluDeleteQuadric(qobj)

        glDisable(GL_TEXTURE_2D)

        glutSwapBuffers()
        glutPostRedisplay()
        
wnd = MyWnd(sys.argv[1])
if (len (sys.argv) == 3):
    wnd = MyWnd(sys.argv[1], sys.argv[2])
wnd.run_scene()
