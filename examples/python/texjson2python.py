PREAMBLE="""
import planetnoise
from planetnoise import TextureBuilder, ModuleDescriptor as MD, ImageDescriptor as ID, 
from planetnoise import HeightMapDescriptor as HMD
from planetnoise import RendererDescriptor as RD
from planetnoise import NoiseMapBuilderDescriptor as NMB

"""


PY_PREAMBLE= """import planetnoise
from planetnoise import TextureBuilder, ModuleDescriptor as MD, ImageDescriptor as ID, HeightMapDescriptor as HMD
from planetnoise import RendererDescriptor as RD
from planetnoise import NoiseMapBuilderDescriptor as NMB

class Texture :
    def __init__(self, textureName='{name}'):
        self.texture = planetnoise.TextureBuilder()
        self.texture.setSize({size})
        self.textureName = textureName
        self.texture.addRandomFactors([{randomFactors}])
"""

PY_POSTAMBLE = """    
        self.texture.appendModuleDescriptor([{mods}])        # add modules to builder
        self.texture.appendImageDescriptor([{images}])           # add images to builder
        self.texture.appendHeightMapDescriptor([{heightmaps}])   # add heightmaps to builder
        self.texture.appendNoiseMapBuilderDescriptor([{builders}]) # add heightmapbuilder
        self.texture.appendRendererDescriptor([{renderers}])     # finally, lets add the renderer
        self.texture.outputFileName = self.textureName

    def build_texture(self):
        self.texture.buildTextureFromJsonString(self.texture.jsonString(), ".")

if __name__ == "__main__":
    txt = Texture('{name}')
    txt.build_texture()
"""

import sys
import os
import json
import glob

class TJConverter:
    def __init__ (self, filename):
        self.json_data = ""
        self.modules =None
        self.heightmap_builders =None
        self.heightmaps = None
        self.images = None
        self.renderers = None
        self.a_randomFactors = None
        self.a_size = None
        self.randomFactors =None
        self.size= ""

        self._modObj = []
        self._hmObj = []
        self._hmbObj = []
        self._imObj = []
        self._rndObj = []

        self._mods = []
        self._heightmaps = []
        self.filename = filename
        self.moduleList = planetnoise.moduleList

    def handle_heightmaps(self, builders):
        _hm = []
        _hmb=[]
        for bld in builders:
            name = bld["name"] # builder's name
            dest = bld["dest"] # heightmap's name
            self._hmObj.append(dest)
            self._hmbObj.append(name)
            source = bld["source"] # source module
            _hm.append("        {}=HMD.create('{}')".format(dest,dest))
            _hmb.append("        {}=NMB.create('{}', module={}, heightMap={})".format(name,name, source,dest))

        return "\n{}\n\n{}\n".format("\n".join(_hm), "\n".join(_hmb))

    def handle_images(self,im):
        _im=[]
        for img in im:
            name = img
            self._imObj.append(name)
            _im.append("        {}=ID.create('{}')".format(name,name))
        return "\n{}\n".format("\n".join(_im))

# Bad naming in the past rears its ugly head.
#
    def handle_modules(self,mods):
        special_keys = ['src1','src2','src3','src4','ctl']
        modlines = []
        keyseq = []
        modDict={}
        for mod in mods:
            if mod["type"]=="Exp":
                mod["type"]="Exponent"
                name=mod["name"]
                if name[-2:]=="_e":
                    mod["src1"]=name[:-2]
            modDict[mod["name"]]=mod;
        
        sortedMods=[]

        def inspectModSrc1(prop, mod):
            if not prop in mod:
                return None
            else:
                #print (mod)
                m1 = modDict[mod[prop]]
                if prop in m1:
                    inspectModSrc1(prop,m1)
                    if m1["name"] in sortedMods:
                        return None
                    else:
                        sortedMods.append(m1["name"])
                        return None
                else:
                    if m1["name"] in sortedMods:
                        return None
                    else:
                        sortedMods.append(m1["name"])
                        return None

        for mod in mods:
            if not "src1" in mod:
                sortedMods.append(mod["name"])

        for mod in mods:
            if "ctl" in mod:
                inspectModSrc1("ctl",mod)
            if "src4" in mod:
                inspectModSrc1("src4",mod)
            if "src3" in mod:
                inspectModSrc1("src3",mod)
            if "src2" in mod:
                inspectModSrc1("src2",mod)
            if "src1" in mod:
                inspectModSrc1("src1",mod)

        for mod in mods:
            if not mod["name"] in sortedMods:
                sortedMods.append(mod["name"])                

        print(sortedMods)

        for modn in sortedMods:
            mod = modDict[modn]
            modk = mod.keys()
            if mod["type"] in self.moduleList:
                couples = []
                self._modObj.append(modn)
                s = "{}=MD.{} ( name='{}',  ".format(mod["name"],mod["type"],mod["name"])
                for k in modk:
                    if k != "type" and k != "name":
                        v = mod[k]
                        if isinstance(v,int) or isinstance(v,float) or isinstance(v,bool) or k in special_keys:
                            couples.append("\n                {}={}".format(k,v))
                        if isinstance(v,str) and k not in special_keys:
                            couples.append("\n            {}='{}'".format(k,v))
                        if isinstance(v,list):
                            subvals = []
                            for sv in v:
                                if isinstance(sv,list):
                                    subvals.append( "\n                    ( {} )".format( ", ".join ([ str(xx) for xx in sv]) ))
                                elif isinstance(sv, str):
                                    subvals.append("'{}'".format(sv))
                                else:
                                    subvals.append('{}'.format(sv))
                            couples.append("\n            {}=[{}]".format(k,", ".join(subvals)))
                sOut = "        {} {})".format(s, ", ".join(couples))
                modlines.append(sOut)
        return "\n".join(modlines)


    # Bad naming in the past rears its ugly head.
    #
    def handle_renderers(self, renderers):
        special_keys = ['alphaImage','backgroundImage','bumpMap','destImage','ctl','heightMap']
        post_init_keys = ["randomFactor"]
        _renderers = []
        posts=[]

        for mod in renderers:
            modk = mod.keys()
            couples = []
            
            s = "        {}=RD.create (  name='{}', ".format(mod["name"],mod["name"])
            self._rndObj.append(mod["name"])
            for k in modk:
                if k != 'name':
                    v = mod[k]
                    if isinstance(v,int) or isinstance(v,float) or isinstance(v,bool) or k in special_keys:
                        if k in special_keys and v=="":
                            pass
                        else:
                            couples.append("\n            {}={}".format(k,v))
                    if isinstance(v,str) and k not in special_keys:
                        couples.append("\n            {}='{}'".format(k,v))
                    if isinstance(v,list):
                        subvals = []
                        for sv in v:
                            if isinstance(sv,list):
                                subvals.append( "\n                ( {} )".format( ", ".join ([ str(xx) for xx in sv]) ))
                            elif isinstance(sv, str):
                                subvals.append("'{}'".format(sv))
                            else:
                                subvals.append('{}'.format(sv))
                        if k in post_init_keys:
                            sLine = "        {}.setRandomFactor({},{},{})".format(mod["name"],subvals[0], subvals[1], subvals[2])
                            if sLine not in posts:
                                posts.append(sLine)
                        else:
                            couples.append("\n                {}=[{}]".format(k,", ".join(subvals)))
            sOut = "{} {})".format(s, ", ".join(couples))
            _renderers.append(sOut)
            # print ("\n# POSTINIT: \n".join(posts))
        
        return "{}\n{}".format(
                "\n\n".join(_renderers),
                "\n\n".join(posts))



    def generate_code(self,textJsonFile):
        j_path, j_filename = os.path.split(textJsonFile)
        j_class = j_filename.replace(".texjson","").replace("-","_").replace(".","_")

        outputs = []

        outputs.append(PY_PREAMBLE.format(name=j_class, size=self.size, randomFactors=self.randomFactors))

        outputs.append(self.handle_modules(self.modules))
        outputs.append(self.handle_images(self.images))
        outputs.append(self.handle_heightmaps(self.heightmap_builders))
        outputs.append(self.handle_renderers(self.renderers))

        outputs.append(PY_POSTAMBLE.format(
            mods = ",".join(self._modObj),
            images = ",".join(self._imObj),
            heightmaps = ",".join(self._hmObj),
            builders = ",".join(self._hmbObj),
            renderers = ",".join(self._rndObj),
            name = j_class
        ))

        with open("./textures/"+j_class+".py", "w") as f:
            f.write("\n\n".join(outputs))

    def load (self):
        #try:
            tex = self.filename
            print ("Loading from ", tex)
            with open(tex,"r") as f:
                self.json_data = f.read()

            jsonObj = json.loads(self.json_data)    

            self.modules = jsonObj["modules"]
            self.heightmap_builders = jsonObj["heightMapBuilders"]
            self.heightmaps = jsonObj["heightMaps"]
            self.images = jsonObj["images"]
            self.renderers = jsonObj["renderers"]
            if "randomFactors" in jsonObj:
                a_randomFactors = jsonObj["randomFactors"]
            else:
                a_randomFactors=[]
            a_size = jsonObj["size"]
            self.randomFactors = ", ".join([str(xx) for xx in a_randomFactors])
            self.size= "{},{}".format(a_size[0], a_size[1])

            print ("Generating from ", tex)
            self.generate_code(tex)        
        # except Exception as exc:
        #     print ("Error generating from ", tex)
        #     print (exc)

texjsons = glob.glob("./*.texjson")
for tex in texjsons:
    conv = TJConverter(tex)
    conv.load()



