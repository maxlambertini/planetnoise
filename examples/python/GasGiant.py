import planetnoise
from planetnoise import TextureBuilder, ModuleDescriptor as MD, ImageDescriptor as ID, HeightMapDescriptor as HMD
from planetnoise import RendererDescriptor as RD
from planetnoise import NoiseMapBuilderDescriptor as NMB



baseGG=MD.Cylinders ( 
	enableRandom=False, 
	freq=2.5, 
	name='baseGG')
baseGG2=MD.Cylinders ( 
	enableRandom=True, 
	freq=1.5, 
	name='baseGG2')
rot=MD.RotatePoint ( 
	enableRandom=False, 
	name='rot', 
	src1=baseGG2, 
	x=1, 
	y=1, 
	z=1.1)
rot2=MD.RotatePoint ( 
	enableRandom=False, 
	name='rot2', 
	src1=baseGG, 
	x=-1, 
	y=-1, 
	z=-1)
pert2=MD.Turbulence ( 
	enableRandom=True, 
	freq=0.9, 
	name='pert2', 
	pow=0.15, 
	rough=7, 
	seed=0, 
	src1=rot2)
pert3=MD.Turbulence ( 
	enableRandom=True, 
	freq=0.82, 
	name='pert3', 
	pow=0.03, 
	rough=4, 
	seed=0, 
	src1=rot)

image1=ID.create('image1')


cloudMap=HMD.create('cloudMap')
heightMap=HMD.create('heightMap')

cloudMapBuilder = NMB.create('cloudMapBuilder', module=pert3, heightMap=cloudMap)
noiseMapBuilder1 = NMB.create('noiseMapBuilder1', module=pert2, heightMap=heightMap)

renderer0001=RD.create ( 
	destImage=image1, 
	enabledLight=False, 
	gradientInfo=[
		( -1, 53, 244, 175, 255 ), 
		( -0.761325, 57, 231, 143, 255 ), 
		( -0.4166, 67, 208, 121, 255 ), 
		( -0.31172500000000003, 80, 205, 148, 255 ), 
		( -0.012975000000000025, 80, 191, 155, 255 ), 
		( 0.10587499999999998, 74, 185, 147, 255 ), 
		( 0.231175, 73, 189, 127, 255 ), 
		( 0.41715, 91, 208, 160, 255 ), 
		( 0.66495, 101, 224, 179, 255 ), 
		( 0.9141250000000001, 89, 228, 200, 255 ), 
		( 1, 92, 237, 174, 255 )], 
	heightMap=heightMap, 
	lightBrightness=1, 
	lightContrast=1, 
	name='renderer0001', 
	randomGradient=True)

renderer0002=RD.create ( 
	backgroundImage=image1, 
	destImage=image1, 
	enabledLight=False, 
	gradientInfo=[
		( -1, 153, 184, 213, 0 ), 
		( -0.6990825688073394, 99, 56, 184, 0 ), 
		( -0.5239199999999999, 184, 141, 204, 255 ), 
		( -0.36513761467889905, 200, 148, 179, 0 ), 
		( -0.2770642201834862, 207, 140, 224, 0 ), 
		( -0.06735999999999986, 103, 152, 150, 255 ), 
		( 0.18165137614678906, 103, 177, 105, 0 ), 
		( 0.2697247706422019, 64, 68, 69, 0 ), 
		( 0.3440000000000003, 80, 192, 171, 255 ), 
		( 0.4862385321100917, 98, 145, 100, 0 ), 
		( 0.7202000000000002, 181, 197, 177, 255 ), 
		( 0.8825688073394495, 89, 40, 135, 0 ), 
		( 1, 158, 160, 128, 0 )], 
	heightMap=cloudMap, 
	lightBrightness=1, 
	lightContrast=1, 
	name='renderer0002', 
	randomGradient=False)



texture = planetnoise.TextureBuilder()

texture.appendModuleDescriptor([baseGG,baseGG2,pert2,pert3,rot,rot2])        # add modules to builder
texture.appendImageDescriptor([image1])           # add images to builder
texture.appendHeightMapDescriptor([cloudMap,heightMap])   # add heightmaps to builder
texture.appendNoiseMapBuilderDescriptor([cloudMapBuilder,noiseMapBuilder1]) # add heightmapbuilder
texture.appendRendererDescriptor([renderer0001,renderer0002])     # finally, lets add the renderer

texture.outputFileName = "Texture_GasGiant_01"
texture.buildTextureFromJsonString(texture.jsonString(), ".")


