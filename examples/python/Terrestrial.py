import planetnoise
from planetnoise import TextureBuilder, ModuleDescriptor as MD, ImageDescriptor as ID, HeightMapDescriptor as HMD
from planetnoise import RendererDescriptor as RD
from planetnoise import NoiseMapBuilderDescriptor as NMB

class Terrestrial :
    def __init__(self, textureName='Terrestrial'):
        self.texture = planetnoise.TextureBuilder()
        self.textureName = textureName

        BeginLayer_Module=MD.Perlin ( name='BeginLayer_Module',   
                enableRandom=True, 
                freq=2.7437500953674316, 
                lac=3.0984320640563965, 
                oct=6, 
                pers=0.3055500090122223, 
                seed=0)
        CylinderPole=MD.Cylinders ( name='CylinderPole',   
                enableRandom=False, 
                freq=0.5)
        MiddleLayer_Module=MD.Perlin ( name='MiddleLayer_Module',   
                enableRandom=True, 
                freq=2.4082000255584717, 
                lac=3.1059200763702393, 
                oct=6, 
                pers=0.29984399676322937, 
                seed=0)
        RidgedBumpMapModule=MD.Perlin ( name='RidgedBumpMapModule',   
                enableRandom=False, 
                freq=6.3, 
                lac=3.6, 
                oct=8, 
                pers=0.3, 
                seed=0)
        SeaModule=MD.Perlin ( name='SeaModule',   
                enableRandom=True, 
                freq=1.37504, 
                lac=3.50375, 
                oct=8, 
                pers=0.33056, 
                seed=0)
        AATurboPoles=MD.Turbulence ( name='AATurboPoles',   
                enableRandom=False, 
                freq=4.8536, 
                pow=0.25544999999999995, 
                rough=3.17075, 
                seed=0, 
                src1=CylinderPole)
        AATurboSea=MD.Turbulence ( name='AATurboSea',   
                enableRandom=False, 
                freq=0.7837000000000001, 
                pow=0.26364000000000004, 
                rough=1.2802, 
                seed=0, 
                src1=SeaModule)
        RidgedBumpMapModule_e=MD.Exponent ( name='RidgedBumpMapModule_e',   
                enableRandom=False, 
                exp=1.25, 
                src1=RidgedBumpMapModule)
        RidgedBumpMapModule_e_t=MD.Turbulence ( name='RidgedBumpMapModule_e_t',   
                enableRandom=False, 
                freq=0.75, 
                pow=0.05, 
                rough=1.25, 
                seed=0, 
                src1=RidgedBumpMapModule_e)

        BaseImage=ID.create('BaseImage')
        img_bump=ID.create('img_bump')
        img_geo=ID.create('img_geo')
        img_spec=ID.create('img_spec')


        BeginLayer_heightMap=HMD.create('BeginLayer_heightMap')
        MiddleLayer_heightMap=HMD.create('MiddleLayer_heightMap')
        heightMapPole=HMD.create('heightMapPole')
        heightMapSea=HMD.create('heightMapSea')
        bump_heightMap=HMD.create('bump_heightMap')
        geo_heightMap=HMD.create('geo_heightMap')
        spec_heightMap=HMD.create('spec_heightMap')

        BeginLayer_noiseMapBuilder1=NMB.create('BeginLayer_noiseMapBuilder1', module=BeginLayer_Module, heightMap=BeginLayer_heightMap)
        MiddleLayer_noiseMapBuilder1=NMB.create('MiddleLayer_noiseMapBuilder1', module=MiddleLayer_Module, heightMap=MiddleLayer_heightMap)
        heightMapBuilderPole=NMB.create('heightMapBuilderPole', module=AATurboPoles, heightMap=heightMapPole)
        heightMapBuilderSea=NMB.create('heightMapBuilderSea', module=AATurboSea, heightMap=heightMapSea)
        noiseMapBuilderBump=NMB.create('noiseMapBuilderBump', module=RidgedBumpMapModule_e_t, heightMap=bump_heightMap)
        noiseMapBuilderGeo=NMB.create('noiseMapBuilderGeo', module=MiddleLayer_Module, heightMap=geo_heightMap)
        noiseMapBuilderSpec=NMB.create('noiseMapBuilderSpec', module=MiddleLayer_Module, heightMap=spec_heightMap)

        BeginLayer_renderer=RD.create (  name='BeginLayer_renderer',  
            destImage=BaseImage, 
            enabledLight=False, 
                gradientInfo=[
                ( -1.0, 99, 97, 56, 255 ), 
                ( -0.5, 56, 53, 52, 255 ), 
                ( 0.0, 51, 52, 49, 255 ), 
                ( 0.5, 100, 97, 57, 255 ), 
                ( 1.0, 26, 163, 167, 255 )], 
            heightMap=BeginLayer_heightMap, 
            lightBrightness=2.0, 
            lightContrast=1.4, 
            randomGradient=False)

        MiddleLayer_renderer=RD.create (  name='MiddleLayer_renderer',  
            backgroundImage=BaseImage, 
            bumpMap=BeginLayer_heightMap, 
            destImage=BaseImage, 
            enabledLight=False, 
                gradientInfo=[
                ( -1.0, 101, 98, 96, 25 ), 
                ( -0.7142857142857143, 101, 97, 94, 23 ), 
                ( -0.4285714285714286, 104, 100, 97, 32 ), 
                ( -0.1428571428571429, 104, 100, 97, 24 ), 
                ( 0.1428571428571428, 101, 97, 94, 17 ), 
                ( 0.4285714285714285, 103, 99, 96, 3 ), 
                ( 0.7142857142857142, 106, 101, 98, 192 ), 
                ( 1.0, 233, 190, 214, 255 )], 
            heightMap=MiddleLayer_heightMap, 
            lightBrightness=2.0, 
            lightContrast=1.4, 
            randomGradient=False)

        snow_renderer=RD.create (  name='snow_renderer',  
            backgroundImage=BaseImage, 
            destImage=BaseImage, 
            enabledLight=True, 
                gradientInfo=[
                ( -1.0, 0, 0, 0, 0 ), 
                ( -0.4, 0, 0, 0, 0 ), 
                ( -0.23, 255, 255, 255, 0 ), 
                ( 0.0, 255, 255, 255, 128 ), 
                ( 0.23, 255, 255, 255, 0 ), 
                ( 0.6, 0, 0, 0, 0 ), 
                ( 1.0, 0, 0, 0, 0 )], 
            heightMap=bump_heightMap, 
            lightBrightness=2.1, 
            lightContrast=2.1, 
            randomGradient=False)

        GrassLayer_renderer=RD.create (  name='GrassLayer_renderer',  
            backgroundImage=BaseImage, 
            destImage=BaseImage, 
            enabledLight=False, 
                gradientInfo=[
                ( -1.0, 114, 111, 68, 0 ), 
                ( -0.71, 93, 105, 53, 0 ), 
                ( -0.57, 74, 75, 65, 0 ), 
                ( -0.42, 76, 84, 47, 0 ), 
                ( -0.28, 46, 51, 19, 141 ), 
                ( -0.039, 46, 51, 19, 140 ), 
                ( 0.21, 76, 84, 47, 140 ), 
                ( 0.57, 103, 131, 34, 9 ), 
                ( 0.6, 73, 73, 48, 0 ), 
                ( 1.0, 99, 168, 165, 0 )], 
            heightMap=BeginLayer_heightMap, 
            lightBrightness=1.0, 
            lightContrast=1.0, 
            randomGradient=False)

        DesertLayer_renderer=RD.create (  name='DesertLayer_renderer',  
            backgroundImage=BaseImage, 
            destImage=BaseImage, 
            enabledLight=False, 
                gradientInfo=[
                ( -1.0, 254, 238, 197, 0 ), 
                ( -0.93, 237, 235, 212, 0 ), 
                ( -0.87, 223, 214, 183, 30 ), 
                ( -0.81, 228, 168, 134, 75 ), 
                ( -0.75, 237, 235, 212, 45 ), 
                ( -0.68, 224, 220, 56, 0 ), 
                ( 1.0, 99, 168, 165, 0 )], 
            heightMap=heightMapPole, 
            lightBrightness=1.0, 
            lightContrast=1.0, 
            randomGradient=False)

        RendererSea=RD.create (  name='RendererSea',  
            backgroundImage=BaseImage, 
            bumpMap=BeginLayer_heightMap, 
            destImage=BaseImage, 
            enabledLight=False, 
                gradientInfo=[
                ( -1.0, 20, 36, 101, 255 ), 
                ( 0.06667999999999999, 23, 50, 115, 255 ), 
                ( 0.08168, 26, 58, 123, 255 ), 
                ( 0.09168, 34, 62, 145, 255 ), 
                ( 0.09178, 197, 252, 184, 128 ), 
                ( 0.22332000000000002, 197, 252, 184, 64 ), 
                ( 0.32332000000000005, 236, 238, 221, 32 ), 
                ( 0.57332, 194, 235, 196, 0 ), 
                ( 1.0, 255, 255, 255, 0 )], 
            heightMap=heightMapSea, 
            lightBrightness=2.0, 
            lightContrast=1.4, 
            randomGradient=False)

        RendererPole=RD.create (  name='RendererPole',  
            backgroundImage=BaseImage, 
            bumpMap=BeginLayer_heightMap, 
            destImage=BaseImage, 
            enabledLight=False, 
                gradientInfo=[
                ( -1.0, 0, 0, 0, 0 ), 
                ( 0.23426, 230, 240, 250, 0 ), 
                ( 0.23483, 248, 254, 255, 255 ), 
                ( 1.0, 255, 255, 255, 255 )], 
            heightMap=heightMapPole, 
            lightBrightness=1.0, 
            lightContrast=1.0, 
            randomGradient=False)

        bumpmap_renderer=RD.create (  name='bumpmap_renderer',  
            destImage=img_bump, 
            enabledLight=False, 
                gradientInfo=[
                ( -1.0, 0, 0, 0, 255 ), 
                ( -0.4, 0, 0, 0, 255 ), 
                ( 0.0, 255, 255, 255, 255 ), 
                ( 0.6, 0, 0, 0, 255 ), 
                ( 1.0, 0, 0, 0, 255 )], 
            heightMap=bump_heightMap, 
            lightBrightness=1.0, 
            lightContrast=1.0, 
            randomGradient=False)

        bumpmap_renderer_mask=RD.create (  name='bumpmap_renderer_mask',  
            backgroundImage=img_bump, 
            destImage=img_bump, 
            enabledLight=False, 
                gradientInfo=[
                ( -1.0, 0, 0, 0, 255 ), 
                ( 0.22382000000000002, 0, 0, 0, 255 ), 
                ( 0.37332, 0, 0, 0, 0 ), 
                ( 1.0, 0, 0, 0, 0 )], 
            heightMap=heightMapSea, 
            lightBrightness=1.0, 
            lightContrast=1.0, 
            randomGradient=False)

        specmap_renderer=RD.create (  name='specmap_renderer',  
            destImage=img_spec, 
            enabledLight=False, 
                gradientInfo=[
                ( -1.0, 255, 255, 255, 255 ), 
                ( 1.0, 255, 255, 255, 255 )], 
            heightMap=spec_heightMap, 
            lightBrightness=1.0, 
            lightContrast=1.0, 
            randomGradient=False)

        specmap_renderer_spec=RD.create (  name='specmap_renderer_spec',  
            backgroundImage=img_spec, 
            destImage=img_spec, 
            enabledLight=False, 
                gradientInfo=[
                ( -1.0, 255, 255, 255, 255 ), 
                ( 0.09167999, 255, 255, 255, 255 ), 
                ( 0.09168, 0, 0, 0, 255 ), 
                ( 1.0, 0, 0, 0, 255 )], 
            heightMap=heightMapSea, 
            lightBrightness=1.0, 
            lightContrast=1.0, 
            randomGradient=False)

        geo_renderer=RD.create (  name='geo_renderer',  
            destImage=img_geo, 
            enabledLight=False, 
                gradientInfo=[
                ( -1.0, 255, 255, 255, 255 ), 
                ( 1.0, 255, 255, 255, 255 )], 
            heightMap=geo_heightMap, 
            lightBrightness=1.0, 
            lightContrast=1.0, 
            randomGradient=False)

        geomap_renderer_geo=RD.create (  name='geomap_renderer_geo',  
            backgroundImage=img_geo, 
            destImage=img_geo, 
            enabledLight=False, 
                gradientInfo=[
                ( -1.0, 255, 255, 255, 255 ), 
                ( 0.09167999, 255, 255, 255, 255 ), 
                ( 0.09168, 0, 0, 0, 255 ), 
                ( 0.09218, 224, 224, 224, 255 ), 
                ( 0.14268, 224, 224, 224, 255 ), 
                ( 1.0, 224, 224, 224, 255 )], 
            heightMap=heightMapSea, 
            lightBrightness=1.0, 
            lightContrast=1.0, 
            randomGradient=False)
        BeginLayer_renderer.setRandomFactor(20,40,60)

        MiddleLayer_renderer.setRandomFactor(20,40,60)
    
        self.texture.appendModuleDescriptor([AATurboPoles,AATurboSea,BeginLayer_Module,CylinderPole,MiddleLayer_Module,RidgedBumpMapModule,RidgedBumpMapModule_e,RidgedBumpMapModule_e_t,SeaModule])        # add modules to builder
        self.texture.appendImageDescriptor([BaseImage,img_bump,img_geo,img_spec])           # add images to builder
        self.texture.appendHeightMapDescriptor([BeginLayer_heightMap,MiddleLayer_heightMap,heightMapPole,heightMapSea,bump_heightMap,geo_heightMap,spec_heightMap])   # add heightmaps to builder
        self.texture.appendNoiseMapBuilderDescriptor([BeginLayer_noiseMapBuilder1,MiddleLayer_noiseMapBuilder1,heightMapBuilderPole,heightMapBuilderSea,noiseMapBuilderBump,noiseMapBuilderGeo,noiseMapBuilderSpec]) # add heightmapbuilder
        self.texture.appendRendererDescriptor([BeginLayer_renderer,MiddleLayer_renderer,snow_renderer,GrassLayer_renderer,DesertLayer_renderer,RendererSea,RendererPole,bumpmap_renderer,bumpmap_renderer_mask,specmap_renderer,specmap_renderer_spec,geo_renderer,geomap_renderer_geo])     # finally, lets add the renderer
        self.texture.outputFileName = self.textureName

    def build_texture(self):
        self.texture.buildTextureFromJsonString(self.texture.jsonString(), ".")

if __name__ == "__main__":
    txt = Terrestrial('Terrestrial')
    txt.build_texture()

