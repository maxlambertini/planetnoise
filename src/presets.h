#ifndef PRESETS_H
#define PRESETS_H

#include <vector>
#include <map>





static std::string OUTRE2 = R"TX(
    {
    "bounds": [
        -90,
        90,
        -180,
        180
    ],
    "builderType": "sphere",
    "bumpMap": "",
    "cloudMap": "",
    "colorMap": "BaseImage",
    "heightMapBuilders": [
        {
            "dest": "BeginLayer_heightMap",
            "name": "BeginLayer_noiseMapBuilder1",
            "seamless": true,
            "source": "BeginLayer_Clamped"
        },
        {
            "dest": "EndLayer_heightMap",
            "name": "EndLayer_noiseMapBuilder1",
            "seamless": false,
            "source": "EndLayer_Module"
        }
    ],
    "heightMaps": [
        "BeginLayer_heightMap",
        "EndLayer_heightMap"
    ],
    "images": [
        "BaseImage"
    ],
    "modules": [
        {
            "enableRandom": false,
            "lbound": -0.3,
            "name": "BeginLayer_Clamped",
            "src1": "BeginLayer_Inverted",
            "type": "Clamp",
            "ubound": 0.6
        },
        {
            "enableRandom": false,
            "name": "BeginLayer_Inverted",
            "src1": "BeginLayer_Module",
            "type": "Invert"
        },
        {
            "enableRandom": true,
            "freq": 10.4632,
            "lac": 3.2,
            "name": "BeginLayer_Module",
            "oct": 6,
            "seed": 0,
            "type": "RidgedMulti"
        },
        {
            "enableRandom": true,
            "freq": 1.343,
            "lac": 2.66756,
            "name": "EndLayer_Module",
            "oct": 6,
            "seed": 0,
            "type": "RidgedMulti"
        }
    ],
    "randomFactors": [
        0.4,
        0.5,
        0.3,
        0.3,
        0.2,
        0.2,
        0.2,
        0.2,
        0.8,
        0.1
    ],
    "reflectionMap": "",
    "renderers": [
        {
            "backgroundImage": "",
            "destImage": "BaseImage",
            "enabledLight": true,
            "gradientInfo": [
                [
                    -1,
                    36,
                    114,
                    246,
                    255
                ],
                [
                    -0.6825749999999999,
                    35,
                    116,
                    220,
                    255
                ],
                [
                    -0.43049999999999994,
                    26,
                    87,
                    203,
                    255
                ],
                [
                    -0.12042499999999995,
                    20,
                    45,
                    183,
                    255
                ],
                [
                    0.15082500000000004,
                    19,
                    26,
                    201,
                    255
                ],
                [
                    0.39812500000000006,
                    31,
                    30,
                    217,
                    255
                ],
                [
                    0.7236500000000001,
                    37,
                    70,
                    198,
                    255
                ],
                [
                    0.9534250000000002,
                    32,
                    83,
                    219,
                    255
                ],
                [
                    1,
                    13,
                    28,
                    190,
                    255
                ]
            ],
            "heightMap": "BeginLayer_heightMap",
            "lightBrightness": 2,
            "lightContrast": 1.15,
            "name": "BeginLayer_renderer",
            "randomGradient": true
        },
        {
            "backgroundImage": "BaseImage",
            "destImage": "BaseImage",
            "enabledLight": true,
            "gradientInfo": [
                [
                    -0.8333333333333334,
                    132,
                    247,
                    234,
                    0
                ],
                [
                    -0.6666666666666667,
                    161,
                    250,
                    215,
                    255
                ],
                [
                    -0.5000000000000001,
                    230,
                    252,
                    240,
                    255
                ],
                [
                    -0.3333333333333335,
                    241,
                    249,
                    175,
                    255
                ],
                [
                    -0.16666666666666685,
                    0,
                    0,
                    0,
                    255
                ],
                [
                    0.4999999999999998,
                    47,
                    224,
                    243,
                    0
                ],
                [
                    0.6666666666666665,
                    166,
                    250,
                    118,
                    0
                ],
                [
                    0.833333333333333,
                    122,
                    139,
                    250,
                    0
                ],
                [
                    1,
                    192,
                    191,
                    189,
                    0
                ]
            ],
            "heightMap": "EndLayer_heightMap",
            "lightBrightness": 2,
            "lightContrast": 1.14,
            "name": "EndLayer_renderer",
            "randomFactor": [
                90,
                2,
                2
            ],
            "randomGradient": false
        }
    ],
    "size": [
        1024,
        512
    ]
}

)TX";




static std::string DESERT = R"TX(
    {
    "bounds": [
        -90,
        90,
        -180,
        180
    ],
    "builderType": "sphere",
    "bumpMap": "",
    "cloudMap": "",
    "colorMap": "image1",
    "heightMapBuilders": [
        {
            "dest": "destMap",
            "name": "builder",
            "seamless": true,
            "source": "ScaleBiasTurbo"
        }
    ],
    "heightMaps": [
        "destMap"
    ],
    "images": [
        "image1"
    ],
    "modules": [
        {
            "ctl": "ModuleVoronoi",
            "enableRandom": false,
            "name": "Blender",
            "src1": "ModulePerlin",
            "src2": "ModuleRMF",
            "type": "Blend"
        },
        {
            "enableRandom": false,
            "freq": 0.65,
            "lac": 2.3,
            "name": "ModulePerlin",
            "oct": 6,
            "pers": 0.15,
            "seed": 0,
            "type": "Billow"
        },
        {
            "enableRandom": false,
            "freq": 1.65,
            "lac": 0.3,
            "name": "ModuleRMF",
            "oct": 6,
            "seed": 0,
            "type": "RidgedMulti"
        },
        {
            "enableDist": true,
            "enableRandom": false,
            "freq": 0.65,
            "name": "ModuleVoronoi",
            "seed": 0,
            "type": "Voronoi"
        },
        {
            "bias": 1,
            "enableRandom": false,
            "name": "ScaleBias",
            "scale": -0.65,
            "src1": "Scaled",
            "type": "ScaleBias"
        },
        {
            "enableRandom": false,
            "freq": 2.5,
            "name": "ScaleBiasTurbo",
            "pow": 0.12,
            "rough": 4,
            "seed": 0,
            "src1": "ScaleBias",
            "type": "Turbulence"
        },
        {
            "enableRandom": false,
            "name": "Scaled",
            "src1": "Blender",
            "type": "ScalePoint",
            "x": 14,
            "y": 14,
            "z": 14
        }
    ],
    "randomFactors": [
        0
    ],
    "reflectionMap": "",
    "renderers": [
        {
            "backgroundImage": "",
            "destImage": "image1",
            "enabledLight": true,
            "gradientInfo": [
                [
                    -1,
                    200,
                    220,
                    215,
                    255
                ],
                [
                    -0.6,
                    134,
                    128,
                    152,
                    255
                ],
                [
                    0,
                    161,
                    168,
                    171,
                    255
                ],
                [
                    0.6000000000000001,
                    203,
                    189,
                    171,
                    255
                ],
                [
                    1,
                    216,
                    211,
                    237,
                    255
                ]
            ],
            "heightMap": "destMap",
            "lightBrightness": 2.1,
            "lightContrast": 0.0025,
            "name": "renderer1",
            "randomFactor": [
                140,
                6,
                6
            ],
            "randomGradient": false
        }
    ],
    "size": [
        1024,
        512
    ]
}

)TX";




static std::string OUTRE = R"TX(
    {
    "bounds": [
        -90,
        90,
        -180,
        180
    ],
    "builderType": "sphere",
    "bumpMap": "",
    "cloudMap": "",
    "colorMap": "BaseImage",
    "heightMapBuilders": [
        {
            "dest": "BeginLayer_heightMap",
            "name": "BeginLayer_noiseMapBuilder1",
            "seamless": true,
            "source": "BeginLayer_Module"
        },
        {
            "dest": "EndLayer_heightMap",
            "name": "EndLayer_noiseMapBuilder1",
            "seamless": false,
            "source": "Module_20"
        }
    ],
    "heightMaps": [
        "BeginLayer_heightMap",
        "EndLayer_heightMap"
    ],
    "images": [
        "BaseImage"
    ],
    "modules": [
        {
            "enableRandom": true,
            "freq": 10.4632,
            "lac": 3.2,
            "name": "BeginLayer_Module",
            "oct": 6,
            "pers": 0.3,
            "seed": 0,
            "type": "Perlin"
        },
        {
            "enableRandom": true,
            "freq": 1.343,
            "lac": 3.2,
            "name": "EndLayer_Module",
            "oct": 6,
            "pers": 0.3,
            "seed": 0,
            "type": "Billow"
        },
        {
            "enableRandom": false,
            "name": "Module_20",
            "src1": "BeginLayer_Module",
            "src2": "EndLayer_Module",
            "type": "Min"
        }
    ],
    "randomFactors": [
        0.4,
        0.5,
        0.3,
        0.3,
        0.2,
        0.2,
        0.2,
        0.2,
        0.8,
        0.1
    ],
    "reflectionMap": "",
    "renderers": [
        {
            "backgroundImage": "",
            "destImage": "BaseImage",
            "enabledLight": true,
            "gradientInfo": [
                [
                    -1,
                    42,
                    38,
                    157,
                    255
                ],
                [
                    -0.719875,
                    44,
                    73,
                    169,
                    255
                ],
                [
                    -0.5229250000000001,
                    40,
                    52,
                    154,
                    255
                ],
                [
                    -0.29370000000000007,
                    47,
                    74,
                    146,
                    255
                ],
                [
                    -0.013025000000000064,
                    53,
                    89,
                    166,
                    255
                ],
                [
                    0.12497499999999995,
                    37,
                    84,
                    139,
                    255
                ],
                [
                    0.45549999999999996,
                    39,
                    61,
                    129,
                    255
                ],
                [
                    0.752575,
                    41,
                    80,
                    142,
                    255
                ],
                [
                    0.90925,
                    32,
                    68,
                    143,
                    255
                ],
                [
                    1,
                    22,
                    63,
                    116,
                    255
                ]
            ],
            "heightMap": "BeginLayer_heightMap",
            "lightBrightness": 2,
            "lightContrast": 1.15,
            "name": "BeginLayer_renderer",
            "randomGradient": true
        },
        {
            "backgroundImage": "BaseImage",
            "destImage": "BaseImage",
            "enabledLight": true,
            "gradientInfo": [
                [
                    -0.8333333333333334,
                    246,
                    197,
                    134,
                    0
                ],
                [
                    -0.6666666666666667,
                    164,
                    250,
                    196,
                    255
                ],
                [
                    -0.5000000000000001,
                    234,
                    251,
                    244,
                    255
                ],
                [
                    -0.3333333333333335,
                    177,
                    253,
                    234,
                    255
                ],
                [
                    -0.16666666666666685,
                    244,
                    254,
                    207,
                    255
                ],
                [
                    0.4999999999999998,
                    50,
                    171,
                    244,
                    0
                ],
                [
                    0.6666666666666665,
                    248,
                    165,
                    115,
                    0
                ],
                [
                    0.833333333333333,
                    122,
                    144,
                    247,
                    0
                ],
                [
                    1,
                    189,
                    192,
                    189,
                    0
                ]
            ],
            "heightMap": "EndLayer_heightMap",
            "lightBrightness": 2,
            "lightContrast": 1.14,
            "name": "EndLayer_renderer",
            "randomGradient": false
        }
    ],
    "size": [
        1024,
        512
    ]
}

)TX";




static std::string PREGARDEN2 = R"TX(
    {
    "bounds": [
        -90,
        90,
        -180,
        180
    ],
    "builderType": "sphere",
    "bumpMap": "image1",
    "cloudMap": "imageBump",
    "colorMap": "ImageSea",
    "heightMapBuilders": [
        {
            "dest": "heightMapPole",
            "name": "heightMapBuilderPole",
            "seamless": true,
            "source": "turboPoles"
        },
        {
            "dest": "heightMapDesert",
            "name": "noiseMapBDesert",
            "seamless": false,
            "source": "ModuleDesert"
        },
        {
            "dest": "heightMap",
            "name": "noiseMapBuilder1",
            "seamless": false,
            "source": "Module1"
        },
        {
            "dest": "HeightMapSea",
            "name": "noiseMapSea",
            "seamless": false,
            "source": "ModuleSea"
        }
    ],
    "heightMaps": [
        "HeightMapSea",
        "heightMap",
        "heightMapDesert",
        "heightMapPole"
    ],
    "images": [
        "ImageSea",
        "image1",
        "imageBump"
    ],
    "modules": [
        {
            "enableRandom": true,
            "freq": 4.2,
            "lac": 2.2,
            "name": "Module1",
            "oct": 6,
            "pers": 0.24,
            "seed": 0,
            "type": "Perlin"
        },
        {
            "enableRandom": true,
            "freq": 4.5,
            "lac": 4.3,
            "name": "ModuleDesert",
            "oct": 6,
            "pers": 0.35,
            "seed": 0,
            "type": "Billow"
        },
        {
            "enableRandom": true,
            "freq": 1.8,
            "lac": 5.3,
            "name": "ModuleSea",
            "oct": 6,
            "pers": 0.23,
            "seed": 0,
            "type": "Perlin"
        },
        {
            "enableRandom": true,
            "freq": 2.5,
            "name": "TurboDesert",
            "pow": 0.55,
            "rough": 1.2,
            "seed": 0,
            "src1": "ModuleDesert",
            "type": "Turbulence"
        },
        {
            "enableRandom": false,
            "freq": 0.5,
            "name": "cylinderPole",
            "type": "Cylinders"
        },
        {
            "enableRandom": true,
            "freq": 4.5,
            "name": "turboPoles",
            "pow": 0.2,
            "rough": 3.2,
            "seed": 0,
            "src1": "cylinderPole",
            "type": "Turbulence"
        }
    ],
    "randomFactors": [
        0
    ],
    "reflectionMap": "imageMountains",
    "renderers": [
        {
            "backgroundImage": "",
            "destImage": "image1",
            "enabledLight": true,
            "gradientInfo": [
                [
                    -1,
                    33,
                    86,
                    91,
                    255
                ],
                [
                    -0.05604719764011801,
                    186,
                    106,
                    41,
                    255
                ],
                [
                    0.9156626506024097,
                    63,
                    131,
                    41,
                    255
                ],
                [
                    1,
                    211,
                    201,
                    192,
                    255
                ]
            ],
            "heightMap": "heightMap",
            "lightBrightness": 2,
            "lightContrast": 1.5,
            "name": "renderer1",
            "randomFactor": [
                25,
                50,
                50
            ],
            "randomGradient": false
        },
        {
            "backgroundImage": "image1",
            "destImage": "image1",
            "enabledLight": true,
            "gradientInfo": [
                [
                    -1,
                    251,
                    247,
                    244,
                    0
                ],
                [
                    -0.7991967871485943,
                    7,
                    0,
                    0,
                    0
                ],
                [
                    -0.028112449799196804,
                    208,
                    209,
                    95,
                    65
                ],
                [
                    1,
                    232,
                    212,
                    117,
                    135
                ]
            ],
            "heightMap": "heightMapDesert",
            "lightBrightness": 2,
            "lightContrast": 1.4,
            "name": "renderer2",
            "randomGradient": false
        },
        {
            "backgroundImage": "image1",
            "destImage": "ImageSea",
            "enabledLight": false,
            "gradientInfo": [
                [
                    -1,
                    31,
                    37,
                    165,
                    255
                ],
                [
                    -0.5903614457831325,
                    97,
                    39,
                    226,
                    255
                ],
                [
                    0.053211009174311874,
                    59,
                    88,
                    205,
                    255
                ],
                [
                    0.18899082568807346,
                    17,
                    236,
                    227,
                    255
                ],
                [
                    0.20733944954128436,
                    57,
                    138,
                    114,
                    255
                ],
                [
                    0.3174311926605504,
                    104,
                    58,
                    30,
                    255
                ],
                [
                    0.4128440366972477,
                    199,
                    211,
                    133,
                    0
                ],
                [
                    0.5060240963855422,
                    114,
                    188,
                    225,
                    0
                ],
                [
                    0.9036144578313252,
                    86,
                    140,
                    69,
                    255
                ],
                [
                    0.9397590361445782,
                    78,
                    205,
                    138,
                    255
                ],
                [
                    1,
                    21,
                    230,
                    149,
                    255
                ]
            ],
            "heightMap": "HeightMapSea",
            "lightBrightness": 1,
            "lightContrast": 1,
            "name": "renderer3",
            "randomFactor": [
                4,
                25,
                25
            ],
            "randomGradient": false
        },
        {
            "backgroundImage": "ImageSea",
            "destImage": "ImageSea",
            "enabledLight": false,
            "gradientInfo": [
                [
                    -1,
                    0,
                    0,
                    0,
                    0
                ],
                [
                    0.7394495412844038,
                    254,
                    251,
                    248,
                    0
                ],
                [
                    0.7908256880733946,
                    255,
                    253,
                    251,
                    255
                ],
                [
                    1,
                    255,
                    254,
                    252,
                    255
                ]
            ],
            "heightMap": "heightMapPole",
            "lightBrightness": 1,
            "lightContrast": 1,
            "name": "renderer4",
            "randomGradient": false
        },
        {
            "backgroundImage": "",
            "destImage": "imageBump",
            "enabledLight": false,
            "gradientInfo": [
                [
                    -1,
                    255,
                    255,
                    255,
                    255
                ],
                [
                    0.167,
                    255,
                    255,
                    255,
                    255
                ],
                [
                    0.168,
                    0,
                    0,
                    0,
                    255
                ],
                [
                    0.91,
                    0,
                    0,
                    0,
                    255
                ],
                [
                    0.911,
                    255,
                    255,
                    255,
                    255
                ],
                [
                    1,
                    255,
                    255,
                    255,
                    255
                ]
            ],
            "heightMap": "HeightMapSea",
            "lightBrightness": 1,
            "lightContrast": 1,
            "name": "renderer5",
            "randomGradient": false
        }
    ],
    "size": [
        1600,
        800
    ]
}

)TX";




static std::string FUNKYCLOUD = R"TX(
    {
    "bounds": [
        -90,
        90,
        -180,
        180
    ],
    "builderType": "sphere",
    "bumpMap": "img01ColorMap",
    "cloudMap": "",
    "colorMap": "img02CloudMap",
    "heightMapBuilders": [
        {
            "dest": "destMap",
            "name": "builder",
            "seamless": true,
            "source": "ModuleScale"
        },
        {
            "dest": "destMap2",
            "name": "builder2",
            "seamless": true,
            "source": "displacer"
        }
    ],
    "heightMaps": [
        "destMap",
        "destMap2"
    ],
    "images": [
        "img01ColorMap",
        "img02CloudMap"
    ],
    "modules": [
        {
            "enableRandom": false,
            "freq": 1.35,
            "lac": 3.7,
            "name": "Module2",
            "oct": 5,
            "pers": 0.25,
            "seed": 0,
            "type": "Perlin"
        },
        {
            "enableRandom": false,
            "name": "ModuleScale",
            "src1": "Module2",
            "type": "ScalePoint",
            "x": 0.9,
            "y": 1,
            "z": 1.1
        },
        {
            "name": "c3",
            "type": "Cylinder"
        },
        {
            "ctl": "c3",
            "enableRandom": false,
            "name": "displacer",
            "src1": "Module2",
            "src2": "m1",
            "src3": "m3",
            "src4": "m2",
            "type": "Displace"
        },
        {
            "enableRandom": false,
            "freq": 1.35,
            "lac": 3.7,
            "name": "m1",
            "oct": 5,
            "pers": 0.35,
            "seed": 0,
            "type": "Billow"
        },
        {
            "enableRandom": false,
            "freq": 1.35,
            "lac": 3.7,
            "name": "m2",
            "oct": 5,
            "seed": 0,
            "type": "RidgedMulti"
        },
        {
            "enableDist": true,
            "enableRandom": false,
            "freq": 1.35,
            "name": "m3",
            "seed": 0,
            "type": "Voronoi"
        }
    ],
    "randomFactors": [
        0
    ],
    "reflectionMap": "",
    "renderers": [
        {
            "backgroundImage": "",
            "destImage": "img01ColorMap",
            "enabledLight": true,
            "gradientInfo": [
                [
                    -1,
                    0,
                    0,
                    96,
                    255
                ],
                [
                    -0.6,
                    0,
                    64,
                    192,
                    255
                ],
                [
                    0.16,
                    0,
                    128,
                    255,
                    255
                ],
                [
                    0.17,
                    255,
                    160,
                    0,
                    255
                ],
                [
                    0.28,
                    0,
                    128,
                    0,
                    255
                ],
                [
                    0.95,
                    96,
                    46,
                    0,
                    255
                ],
                [
                    1,
                    224,
                    248,
                    255,
                    255
                ]
            ],
            "heightMap": "destMap",
            "lightBrightness": 2,
            "lightContrast": 3.5,
            "name": "renderer10",
            "randomGradient": false
        },
        {
            "backgroundImage": "",
            "destImage": "img02CloudMap",
            "enabledLight": true,
            "gradientInfo": [
                [
                    -1,
                    120,
                    250,
                    9,
                    0
                ],
                [
                    -0.95,
                    0,
                    6,
                    3,
                    0
                ],
                [
                    -0.75,
                    1,
                    1,
                    2,
                    192
                ],
                [
                    -0.55,
                    9,
                    6,
                    8,
                    128
                ],
                [
                    -0.25,
                    173,
                    2,
                    194,
                    0
                ],
                [
                    -0.10999999999999999,
                    3,
                    77,
                    252,
                    0
                ],
                [
                    0.41999999999999993,
                    209,
                    215,
                    206,
                    0
                ],
                [
                    0.6499999999999999,
                    123,
                    234,
                    201,
                    224
                ],
                [
                    0.8500000000000001,
                    126,
                    160,
                    252,
                    224
                ],
                [
                    1,
                    133,
                    225,
                    251,
                    223
                ]
            ],
            "heightMap": "destMap2",
            "lightBrightness": 2,
            "lightContrast": 0.01,
            "name": "renderer11",
            "randomFactor": [
                100,
                10,
                10
            ],
            "randomGradient": false
        }
    ],
    "size": [
        1024,
        512
    ]
}

)TX";




static std::string EARTHLIKE_12 = R"TX(
    {
    "bounds": [
        -90,
        90,
        -180,
        180
    ],
    "builderType": "sphere",
    "bumpMap": "imageNormal",
    "cloudMap": "ZZAlpha",
    "colorMap": "BaseImage",
    "heightMapBuilders": [
        {
            "dest": "AlphaEquatorialCyl_heightMap",
            "name": "AlphaEquatorialCyl_noiseMapBuilder1",
            "seamless": true,
            "source": "AlphaEquatorialCylTurbo"
        },
        {
            "dest": "BeginLayer_heightMap",
            "name": "BeginLayer_noiseMapBuilder1",
            "seamless": false,
            "source": "BeginLayer_Module"
        },
        {
            "dest": "BumpMap_heightMap",
            "name": "BumpMap_noiseMapBuilder1",
            "seamless": true,
            "source": "BumpTurbo"
        },
        {
            "dest": "EndLayer_heightMap",
            "name": "EndLayer_noiseMapBuilder1",
            "seamless": false,
            "source": "EndLayer_Module"
        },
        {
            "dest": "Poles_heightMap",
            "name": "Poles_noiseMapBuilder1",
            "seamless": false,
            "source": "PolesTurbolence2"
        },
        {
            "dest": "SeaWorkflow_heightMap",
            "name": "SeaWorkflow_noiseMapBuilder1",
            "seamless": false,
            "source": "SeaworkflowTurbo"
        },
        {
            "dest": "ZZAlpha_heightMap",
            "name": "ZZAlpha_noiseMapBuilder1",
            "seamless": true,
            "source": "ZZAlpha_Module"
        }
    ],
    "heightMaps": [
        "AlphaEquatorialCyl_heightMap",
        "BeginLayer_heightMap",
        "BumpMap_heightMap",
        "EndLayer_heightMap",
        "Poles_heightMap",
        "SeaWorkflow_heightMap",
        "ZZAlpha_heightMap"
    ],
    "images": [
        "AlphaEquatorialCyl",
        "BaseImage",
        "ImageSpecular",
        "ZZAlpha",
        "imageNormal"
    ],
    "modules": [
        {
            "enableRandom": false,
            "name": "AlphaEquatorialCylRot",
            "src1": "AlphaEquatorialCyl_Module",
            "type": "RotatePoint",
            "x": 12.5,
            "y": -3.2,
            "z": 1
        },
        {
            "enableRandom": false,
            "freq": 5.4,
            "name": "AlphaEquatorialCylTurbo",
            "pow": 0.0625,
            "rough": 1.125,
            "seed": 0,
            "src1": "AlphaEquatorialCylRot",
            "type": "Turbulence2"
        },
        {
            "enableRandom": true,
            "freq": 0.85,
            "name": "AlphaEquatorialCyl_Module",
            "type": "Cylinders"
        },
        {
            "enableRandom": true,
            "freq": 2.416,
            "lac": 4.42688,
            "name": "BeginLayer_Module",
            "oct": 6,
            "pers": 0.3,
            "seed": 0,
            "type": "Perlin"
        },
        {
            "enableRandom": false,
            "name": "BumpCurve",
            "src1": "BumpMap_Module",
            "type": "Invert"
        },
        {
            "enableRandom": true,
            "freq": 2.9,
            "lac": 4.9,
            "name": "BumpMap_Module",
            "oct": 8,
            "seed": 0,
            "type": "RidgedMulti"
        },
        {
            "enableRandom": false,
            "freq": 5.3,
            "name": "BumpTurbo",
            "pow": 0.04,
            "rough": 1.1,
            "seed": 0,
            "src1": "BumpCurve",
            "type": "Turbulence2"
        },
        {
            "enableRandom": true,
            "freq": 1.82225,
            "lac": 4.22733,
            "name": "EndLayer_Module",
            "oct": 6,
            "seed": 0,
            "type": "RidgedMulti"
        },
        {
            "enableRandom": false,
            "freq": 9.3,
            "name": "PolesTurbolence2",
            "pow": 0.25,
            "rough": 2.5,
            "seed": 0,
            "src1": "Poles_Module",
            "type": "Turbulence"
        },
        {
            "enableRandom": true,
            "freq": 0.5,
            "name": "Poles_Module",
            "type": "Cylinders"
        },
        {
            "enableRandom": true,
            "freq": 1.8,
            "lac": 5.4,
            "name": "SeaWorkflow_Module",
            "oct": 8,
            "pers": 0.21,
            "seed": 0,
            "type": "Perlin"
        },
        {
            "enableRandom": false,
            "freq": 5.3,
            "name": "SeaworkflowTurbo",
            "pow": 0.06,
            "rough": 1.1,
            "seed": 0,
            "src1": "SeaWorkflow_Module",
            "type": "Turbulence"
        },
        {
            "enableRandom": true,
            "freq": 2.3936,
            "lac": 2.76096,
            "name": "ZZAlpha_Module",
            "oct": 6,
            "pers": 0.3,
            "seed": 0,
            "type": "Perlin"
        }
    ],
    "randomFactors": [
        0
    ],
    "reflectionMap": "imageSpecular",
    "renderers": [
        {
            "alphaImage": "",
            "backgroundImage": "",
            "bumpMap": "",
            "destImage": "BaseImage",
            "enabledLight": false,
            "gradientInfo": [
                [
                    -1,
                    115,
                    207,
                    146,
                    255
                ],
                [
                    -0.6,
                    52,
                    189,
                    10,
                    255
                ],
                [
                    0,
                    97,
                    169,
                    98,
                    255
                ],
                [
                    0.6000000000000001,
                    144,
                    198,
                    12,
                    255
                ],
                [
                    1,
                    97,
                    207,
                    112,
                    255
                ]
            ],
            "heightMap": "BeginLayer_heightMap",
            "lightBrightness": 1,
            "lightContrast": 1,
            "name": "BeginLayer_renderer",
            "randomGradient": false
        },
        {
            "alphaImage": "",
            "backgroundImage": "",
            "bumpMap": "",
            "destImage": "ZZAlpha",
            "enabledLight": false,
            "gradientInfo": [
                [
                    -1,
                    0,
                    0,
                    0,
                    255
                ],
                [
                    0.15229357798165144,
                    0,
                    0,
                    0,
                    255
                ],
                [
                    0.581651376146789,
                    212,
                    210,
                    202,
                    255
                ],
                [
                    0.8055045871559634,
                    239,
                    239,
                    239,
                    255
                ],
                [
                    1,
                    255,
                    255,
                    255,
                    255
                ]
            ],
            "heightMap": "ZZAlpha_heightMap",
            "lightBrightness": 1,
            "lightContrast": 1,
            "name": "ZZAlpha_renderer",
            "randomGradient": false
        },
        {
            "alphaImage": "ZZAlpha",
            "backgroundImage": "BaseImage",
            "bumpMap": "BumpMap_heightMap",
            "destImage": "BaseImage",
            "enabledLight": false,
            "gradientInfo": [
                [
                    -1,
                    211,
                    195,
                    138,
                    255
                ],
                [
                    -0.6,
                    206,
                    201,
                    145,
                    255
                ],
                [
                    0,
                    206,
                    175,
                    102,
                    255
                ],
                [
                    0.6000000000000001,
                    198,
                    187,
                    87,
                    255
                ],
                [
                    0.8201834862385322,
                    175,
                    147,
                    42,
                    255
                ],
                [
                    1,
                    175,
                    153,
                    41,
                    255
                ]
            ],
            "heightMap": "EndLayer_heightMap",
            "lightBrightness": 2,
            "lightContrast": 1.3,
            "name": "EndLayer_renderer",
            "randomGradient": false
        },
        {
            "alphaImage": "",
            "backgroundImage": "BaseImage",
            "bumpMap": "BumpMap_heightMap",
            "destImage": "BaseImage",
            "enabledLight": false,
            "gradientInfo": [
                [
                    -1,
                    255,
                    255,
                    255,
                    255
                ],
                [
                    -0.07889908256880729,
                    255,
                    255,
                    255,
                    0
                ],
                [
                    -2.220446049250313e-16,
                    189,
                    240,
                    215,
                    0
                ],
                [
                    0.16666666666666652,
                    210,
                    245,
                    227,
                    0
                ],
                [
                    0.2844036697247707,
                    159,
                    232,
                    189,
                    0
                ],
                [
                    0.33333333333333304,
                    226,
                    249,
                    237,
                    0
                ],
                [
                    0.4999999999999998,
                    228,
                    249,
                    240,
                    0
                ],
                [
                    0.6666666666666665,
                    246,
                    253,
                    250,
                    0
                ],
                [
                    1,
                    255,
                    255,
                    255,
                    0
                ]
            ],
            "heightMap": "BumpMap_heightMap",
            "lightBrightness": 2,
            "lightContrast": 1.2,
            "name": "BumpMap_renderer",
            "randomGradient": false
        },
        {
            "alphaImage": "",
            "backgroundImage": "",
            "bumpMap": "",
            "destImage": "AlphaEquatorialCyl",
            "enabledLight": false,
            "gradientInfo": [
                [
                    -1,
                    0,
                    0,
                    0,
                    255
                ],
                [
                    -0.6146788990825688,
                    0,
                    0,
                    0,
                    255
                ],
                [
                    0.6587155963302753,
                    255,
                    255,
                    255,
                    255
                ],
                [
                    1,
                    255,
                    255,
                    255,
                    255
                ]
            ],
            "heightMap": "AlphaEquatorialCyl_heightMap",
            "lightBrightness": 1,
            "lightContrast": 1,
            "name": "AlphaEquatorialCyl_renderer",
            "randomGradient": false
        },
        {
            "alphaImage": "",
            "backgroundImage": "BaseImage",
            "bumpMap": "",
            "destImage": "BaseImage",
            "enabledLight": false,
            "gradientInfo": [
                [
                    -1,
                    60,
                    138,
                    203,
                    255
                ],
                [
                    -0.06788990825688068,
                    60,
                    173,
                    227,
                    255
                ],
                [
                    0.17064220183486234,
                    96,
                    185,
                    216,
                    255
                ],
                [
                    0.19999999999999996,
                    235,
                    228,
                    171,
                    255
                ],
                [
                    0.28,
                    235,
                    228,
                    171,
                    211
                ],
                [
                    0.4422018348623853,
                    235,
                    228,
                    171,
                    0
                ],
                [
                    0.8599999999999999,
                    92,
                    181,
                    59,
                    0
                ],
                [
                    1,
                    0,
                    64,
                    255,
                    255
                ]
            ],
            "heightMap": "SeaWorkflow_heightMap",
            "lightBrightness": 2,
            "lightContrast": 1.4,
            "name": "SeaWorkflow_renderer",
            "randomGradient": false
        },
        {
            "alphaImage": "AlphaEquatorialCyl",
            "backgroundImage": "BaseImage",
            "bumpMap": "",
            "destImage": "BaseImage",
            "enabledLight": false,
            "gradientInfo": [
                [
                    -1,
                    46,
                    146,
                    165,
                    255
                ],
                [
                    -0.06788990825688068,
                    60,
                    173,
                    227,
                    255
                ],
                [
                    0.17064220183486234,
                    96,
                    185,
                    216,
                    255
                ],
                [
                    0.19999999999999996,
                    69,
                    174,
                    72,
                    255
                ],
                [
                    0.28,
                    30,
                    148,
                    34,
                    211
                ],
                [
                    0.4422018348623853,
                    235,
                    228,
                    171,
                    0
                ],
                [
                    0.8599999999999999,
                    92,
                    181,
                    59,
                    0
                ],
                [
                    1,
                    0,
                    64,
                    255,
                    255
                ]
            ],
            "heightMap": "SeaWorkflow_heightMap",
            "lightBrightness": 2,
            "lightContrast": 1.4,
            "name": "SeaWorkflow_renderer2",
            "randomGradient": false
        },
        {
            "alphaImage": "",
            "backgroundImage": "BaseImage",
            "bumpMap": "",
            "destImage": "BaseImage",
            "enabledLight": false,
            "gradientInfo": [
                [
                    -1,
                    169,
                    169,
                    169,
                    0
                ],
                [
                    0.36146788990825685,
                    235,
                    235,
                    235,
                    0
                ],
                [
                    0.38348623853211006,
                    255,
                    255,
                    255,
                    255
                ],
                [
                    1,
                    255,
                    255,
                    255,
                    255
                ]
            ],
            "heightMap": "Poles_heightMap",
            "lightBrightness": 2,
            "lightContrast": 1.4,
            "name": "Poles_renderer",
            "randomGradient": false
        },
        {
            "alphaImage": "",
            "backgroundImage": "",
            "bumpMap": "",
            "destImage": "ImageSpecular",
            "enabledLight": false,
            "gradientInfo": [
                [
                    -1,
                    255,
                    255,
                    255,
                    255
                ],
                [
                    0.12660550458715591,
                    255,
                    255,
                    255,
                    255
                ],
                [
                    0.15596330275229353,
                    0,
                    0,
                    0,
                    255
                ],
                [
                    1,
                    0,
                    0,
                    0,
                    255
                ]
            ],
            "heightMap": "SeaWorkflow_heightMap",
            "lightBrightness": 2,
            "lightContrast": 1.4,
            "name": "Specular_renderer",
            "randomGradient": false
        },
        {
            "alphaImage": "",
            "backgroundImage": "",
            "bumpMap": "",
            "destImage": "imageNormal",
            "enabledLight": false,
            "gradientInfo": [
                [
                    -1,
                    0,
                    0,
                    0,
                    255
                ],
                [
                    1,
                    255,
                    255,
                    255,
                    255
                ]
            ],
            "heightMap": "BumpMap_heightMap",
            "lightBrightness": 1,
            "lightContrast": 1,
            "name": "renderer0010",
            "randomGradient": false
        },
        {
            "alphaImage": "",
            "backgroundImage": "imageNormal",
            "bumpMap": "",
            "destImage": "imageNormal",
            "enabledLight": false,
            "gradientInfo": [
                [
                    -1,
                    0,
                    0,
                    0,
                    255
                ],
                [
                    0.16799999999999993,
                    0,
                    0,
                    0,
                    255
                ],
                [
                    0.16999999999999993,
                    0,
                    0,
                    0,
                    255
                ],
                [
                    0.5743119266055046,
                    0,
                    0,
                    0,
                    0
                ],
                [
                    1,
                    255,
                    255,
                    255,
                    0
                ]
            ],
            "heightMap": "SeaWorkflow_heightMap",
            "lightBrightness": 1,
            "lightContrast": 1,
            "name": "renderer0011",
            "randomGradient": false
        }
    ],
    "size": [
        1600,
        800
    ]
}

)TX";




static std::string ROCKBALL_02 = R"TX(
    {
    "bounds": [
        -90,
        90,
        -180,
        180
    ],
    "builderType": "sphere",
    "bumpMap": "",
    "cloudMap": "height",
    "colorMap": "BaseImage",
    "heightMapBuilders": [
        {
            "dest": "BeginLayer_heightMap",
            "name": "BeginLayer_noiseMapBuilder1",
            "seamless": false,
            "source": "demoInvert"
        },
        {
            "dest": "EndLayer_heightMap",
            "name": "EndLayer_noiseMapBuilder1",
            "seamless": false,
            "source": "EndLayer_Module"
        },
        {
            "dest": "reflecMap",
            "name": "reflecMap",
            "seamless": false,
            "source": "EndLayer_Module"
        }
    ],
    "heightMaps": [
        "BeginLayer_heightMap",
        "EndLayer_heightMap",
        "reflecMap"
    ],
    "images": [
        "BaseImage",
        "height",
        "reflec"
    ],
    "modules": [
        {
            "enableRandom": true,
            "freq": 3.4,
            "lac": 3.2,
            "name": "BeginLayer_Module",
            "oct": 6,
            "seed": 0,
            "type": "RidgedMulti"
        },
        {
            "enableRandom": false,
            "name": "DemoAvg",
            "src1": "BeginLayer_Module",
            "src2": "EndLayer_Module",
            "type": "Avg"
        },
        {
            "enableRandom": true,
            "freq": 2.5,
            "lac": 3.2,
            "name": "EndLayer_Module",
            "oct": 6,
            "pers": 0.3,
            "seed": 0,
            "type": "Perlin"
        },
        {
            "cpoints": [
                [
                    -1,
                    -1
                ],
                [
                    -0.5,
                    0.5
                ],
                [
                    -0.2,
                    -0.5
                ],
                [
                    0.2,
                    -0.5
                ],
                [
                    0.5,
                    0.7
                ],
                [
                    1,
                    -1
                ]
            ],
            "enableRandom": false,
            "name": "demoCurve",
            "src1": "DemoAvg",
            "type": "Curve"
        },
        {
            "cpoints": [
                [
                    -1,
                    1
                ],
                [
                    -0.5,
                    -1
                ],
                [
                    0.5,
                    -1
                ],
                [
                    1,
                    1
                ]
            ],
            "enableRandom": false,
            "name": "demoInvert",
            "src1": "demoCurve",
            "type": "Curve"
        }
    ],
    "randomFactors": [
        0.1,
        0.2,
        0.4,
        0.8,
        0.4,
        0.8,
        0.4,
        0.3,
        0.1,
        0.05
    ],
    "reflectionMap": "reflec",
    "renderers": [
        {
            "alphaImage": "",
            "backgroundImage": "",
            "bumpMap": "",
            "destImage": "BaseImage",
            "enabledLight": false,
            "gradientInfo": [
                [
                    -1,
                    163,
                    163,
                    163,
                    255
                ],
                [
                    -0.6,
                    190,
                    180,
                    182,
                    255
                ],
                [
                    0,
                    167,
                    169,
                    156,
                    255
                ],
                [
                    0.6000000000000001,
                    196,
                    192,
                    186,
                    255
                ],
                [
                    1,
                    255,
                    229,
                    213,
                    255
                ]
            ],
            "heightMap": "BeginLayer_heightMap",
            "lightBrightness": 2.2,
            "lightContrast": 0.4,
            "name": "BeginLayer_renderer",
            "randomFactor": [
                12,
                1,
                1
            ],
            "randomGradient": false
        },
        {
            "alphaImage": "",
            "backgroundImage": "",
            "bumpMap": "",
            "destImage": "height",
            "enabledLight": false,
            "gradientInfo": [
                [
                    -1,
                    0,
                    0,
                    0,
                    255
                ],
                [
                    1,
                    255,
                    255,
                    255,
                    255
                ]
            ],
            "heightMap": "BeginLayer_heightMap",
            "lightBrightness": 1,
            "lightContrast": 1,
            "name": "rendererHeight",
            "randomGradient": false
        },
        {
            "alphaImage": "",
            "backgroundImage": "",
            "bumpMap": "",
            "destImage": "reflec",
            "enabledLight": false,
            "gradientInfo": [
                [
                    -1,
                    0,
                    0,
                    0,
                    255
                ],
                [
                    1,
                    172,
                    172,
                    172,
                    255
                ]
            ],
            "heightMap": "reflecMap",
            "lightBrightness": 1,
            "lightContrast": 1,
            "name": "renderer0003",
            "randomGradient": false
        }
    ],
    "size": [
        1280,
        640
    ]
}

)TX";




static std::string PREGARDEN3 = R"TX(
    {
    "bounds": [
        -90,
        90,
        -180,
        180
    ],
    "builderType": "sphere",
    "bumpMap": "",
    "cloudMap": "",
    "colorMap": "BaseImage",
    "heightMapBuilders": [
        {
            "dest": "BeginLayer_heightMap",
            "name": "BeginLayer_noiseMapBuilder1",
            "seamless": true,
            "source": "Max_Module_40"
        },
        {
            "dest": "EndLayer_heightMap",
            "name": "EndLayer_noiseMapBuilder1",
            "seamless": true,
            "source": "Turbulence_Module_70"
        },
        {
            "dest": "heightMap_Pole",
            "name": "hmbPole",
            "seamless": false,
            "source": "Turbulence_Module_100"
        }
    ],
    "heightMaps": [
        "BeginLayer_heightMap",
        "EndLayer_heightMap",
        "heightMap_Pole"
    ],
    "images": [
        "BaseImage"
    ],
    "modules": [
        {
            "enableRandom": false,
            "name": "Avg_Module_30",
            "src1": "EndLayer_Module",
            "src2": "Curve_Module_20",
            "type": "Avg"
        },
        {
            "disp": 0,
            "enableDist": true,
            "enableRandom": true,
            "freq": 3.1,
            "name": "BeginLayer_Module",
            "seed": 0,
            "type": "Voronoi"
        },
        {
            "cpoints": [
                [
                    -1,
                    -1
                ],
                [
                    -0.2,
                    -0.2
                ],
                [
                    0,
                    0.5
                ],
                [
                    0.2,
                    -0.2
                ],
                [
                    1,
                    -1
                ]
            ],
            "enableRandom": false,
            "name": "Curve_Module_20",
            "src1": "BeginLayer_Module",
            "type": "Curve"
        },
        {
            "enableRandom": false,
            "freq": 0.5,
            "name": "Cylinders_Module_90",
            "type": "Cylinders"
        },
        {
            "enableRandom": true,
            "freq": 4.3,
            "lac": 2.70822,
            "name": "EndLayer_Module",
            "oct": 10,
            "seed": 0,
            "type": "RidgedMulti"
        },
        {
            "enableRandom": true,
            "name": "Max_Module_40",
            "src1": "Turbulence_Module_50",
            "src2": "EndLayer_Module",
            "type": "Max"
        },
        {
            "enableRandom": true,
            "freq": 1.9,
            "lac": 2.6,
            "name": "Perlin_Module_60",
            "oct": 10,
            "pers": 0.38,
            "seed": 0,
            "type": "Perlin"
        },
        {
            "enableRandom": true,
            "freq": 2.5,
            "lac": 3.2,
            "name": "Perlin_Module_80",
            "oct": 6,
            "pers": 0.3,
            "seed": 0,
            "type": "Perlin"
        },
        {
            "enableRandom": true,
            "freq": 1.85,
            "name": "Turbulence_Module_100",
            "pow": 0.23,
            "rough": 2.5,
            "seed": 0,
            "src1": "Cylinders_Module_90",
            "type": "Turbulence"
        },
        {
            "enableRandom": true,
            "freq": 5.3,
            "name": "Turbulence_Module_50",
            "pow": 0.2,
            "rough": 1.3,
            "seed": 0,
            "src1": "Avg_Module_30",
            "type": "Turbulence"
        },
        {
            "enableRandom": true,
            "freq": 1.4,
            "name": "Turbulence_Module_70",
            "pow": 0.1,
            "rough": 1.1,
            "seed": 0,
            "src1": "Perlin_Module_60",
            "type": "Turbulence"
        }
    ],
    "randomFactors": [
        0,
        0.1,
        0.2,
        0.3,
        0.1,
        0.3,
        0.2,
        0.1
    ],
    "reflectionMap": "",
    "renderers": [
        {
            "backgroundImage": "",
            "destImage": "BaseImage",
            "enabledLight": true,
            "gradientInfo": [
                [
                    -1,
                    237,
                    204,
                    175,
                    255
                ],
                [
                    -0.95356,
                    252,
                    255,
                    178,
                    255
                ],
                [
                    -0.85356,
                    169,
                    209,
                    164,
                    255
                ],
                [
                    -0.6345381526104418,
                    185,
                    242,
                    179,
                    255
                ],
                [
                    -0.52572,
                    241,
                    240,
                    172,
                    255
                ],
                [
                    -0.37427999999999995,
                    241,
                    240,
                    172,
                    255
                ],
                [
                    -0.2570281124497992,
                    213,
                    247,
                    216,
                    255
                ],
                [
                    0.07475999999999994,
                    219,
                    247,
                    213,
                    255
                ],
                [
                    0.17476000000000003,
                    255,
                    228,
                    133,
                    255
                ],
                [
                    0.28191999999999995,
                    214,
                    196,
                    143,
                    255
                ],
                [
                    0.38192000000000004,
                    251,
                    228,
                    171,
                    255
                ],
                [
                    0.61984,
                    251,
                    228,
                    171,
                    255
                ],
                [
                    0.71984,
                    253,
                    233,
                    178,
                    255
                ],
                [
                    1,
                    237,
                    240,
                    183,
                    255
                ]
            ],
            "heightMap": "BeginLayer_heightMap",
            "lightBrightness": 2,
            "lightContrast": 1.4,
            "name": "BeginLayer_renderer",
            "randomGradient": true
        },
        {
            "backgroundImage": "BaseImage",
            "destImage": "BaseImage",
            "enabledLight": false,
            "gradientInfo": [
                [
                    -1,
                    15,
                    46,
                    118,
                    255
                ],
                [
                    -0.04417670682730923,
                    14,
                    166,
                    185,
                    255
                ],
                [
                    0.24899598393574296,
                    26,
                    162,
                    222,
                    255
                ],
                [
                    0.2610441767068272,
                    212,
                    210,
                    121,
                    255
                ],
                [
                    0.32530120481927716,
                    63,
                    147,
                    66,
                    255
                ],
                [
                    0.6907630522088353,
                    47,
                    199,
                    47,
                    0
                ],
                [
                    0.7116465863453815,
                    77,
                    187,
                    55,
                    0
                ],
                [
                    0.8016465863453816,
                    77,
                    187,
                    55,
                    0
                ],
                [
                    0.8618072289156627,
                    184,
                    172,
                    57,
                    95
                ],
                [
                    0.9159036144578312,
                    184,
                    172,
                    57,
                    128
                ],
                [
                    1,
                    184,
                    172,
                    57,
                    64
                ]
            ],
            "heightMap": "EndLayer_heightMap",
            "lightBrightness": 1,
            "lightContrast": 1,
            "name": "renderer0002",
            "randomGradient": false
        },
        {
            "backgroundImage": "BaseImage",
            "destImage": "BaseImage",
            "enabledLight": false,
            "gradientInfo": [
                [
                    -1,
                    0,
                    0,
                    0,
                    0
                ],
                [
                    0.5100401606425702,
                    238,
                    238,
                    238,
                    0
                ],
                [
                    0.5341365461847389,
                    238,
                    244,
                    255,
                    255
                ],
                [
                    1,
                    238,
                    246,
                    255,
                    255
                ]
            ],
            "heightMap": "heightMap_Pole",
            "lightBrightness": 1,
            "lightContrast": 1,
            "name": "renderer0003",
            "randomGradient": false
        }
    ],
    "size": [
        1280,
        640
    ]
}

)TX";




static std::string CANYON_01 = R"TX(
    {
    "bounds": [
        -90,
        90,
        -180,
        180
    ],
    "builderType": "sphere",
    "bumpMap": "",
    "cloudMap": "",
    "colorMap": "BaseImage",
    "heightMapBuilders": [
        {
            "dest": "BeginLayer_heightMap",
            "name": "BeginLayer_noiseMapBuilder1",
            "seamless": false,
            "source": "demoInvert"
        },
        {
            "dest": "EndLayer_heightMap",
            "name": "EndLayer_noiseMapBuilder1",
            "seamless": false,
            "source": "EndLayer_Module"
        }
    ],
    "heightMaps": [
        "BeginLayer_heightMap",
        "EndLayer_heightMap"
    ],
    "images": [
        "BaseImage"
    ],
    "modules": [
        {
            "enableRandom": true,
            "freq": 3.4,
            "lac": 3.2,
            "name": "BeginLayer_Module",
            "oct": 6,
            "seed": 0,
            "type": "RidgedMulti"
        },
        {
            "enableRandom": false,
            "name": "DemoAvg",
            "src1": "BeginLayer_Module",
            "src2": "EndLayer_Module",
            "type": "Avg"
        },
        {
            "enableRandom": true,
            "freq": 2.5,
            "lac": 3.2,
            "name": "EndLayer_Module",
            "oct": 6,
            "pers": 0.3,
            "seed": 0,
            "type": "Perlin"
        },
        {
            "cpoints": [
                [
                    -1,
                    -1
                ],
                [
                    -0.5,
                    0.5
                ],
                [
                    -0.2,
                    -0.5
                ],
                [
                    0.2,
                    -0.5
                ],
                [
                    0.5,
                    0.7
                ],
                [
                    1,
                    -1
                ]
            ],
            "enableRandom": false,
            "name": "demoCurve",
            "src1": "DemoAvg",
            "type": "Curve"
        },
        {
            "cpoints": [
                [
                    -1,
                    1
                ],
                [
                    -0.5,
                    -1
                ],
                [
                    0.5,
                    -1
                ],
                [
                    1,
                    1
                ]
            ],
            "enableRandom": false,
            "name": "demoInvert",
            "src1": "demoCurve",
            "type": "Curve"
        }
    ],
    "randomFactors": [
        0.1,
        0.2,
        0.4,
        0.8,
        0.4,
        0.8,
        0.4,
        0.3,
        0.1,
        0.05
    ],
    "reflectionMap": "",
    "renderers": [
        {
            "backgroundImage": "",
            "destImage": "BaseImage",
            "enabledLight": true,
            "gradientInfo": [
                [
                    -1,
                    164,
                    69,
                    22,
                    255
                ],
                [
                    -0.6,
                    189,
                    10,
                    70,
                    255
                ],
                [
                    0,
                    169,
                    125,
                    22,
                    255
                ],
                [
                    0.6000000000000001,
                    198,
                    105,
                    12,
                    255
                ],
                [
                    1,
                    255,
                    94,
                    13,
                    255
                ]
            ],
            "heightMap": "BeginLayer_heightMap",
            "lightBrightness": 2.2,
            "lightContrast": 0.4,
            "name": "BeginLayer_renderer",
            "randomFactor": [
                12,
                1,
                1
            ],
            "randomGradient": false
        }
    ],
    "size": [
        1280,
        640
    ]
}

)TX";




static std::string EARTHLIKE_ISLAND = R"TX(
    {
    "bounds": [
        -90,
        90,
        -180,
        180
    ],
    "builderType": "sphere",
    "bumpMap": "",
    "cloudMap": "",
    "colorMap": "image1",
    "heightMapBuilders": [
        {
            "dest": "destMap",
            "name": "builder",
            "seamless": true,
            "source": "ModulePerlin"
        }
    ],
    "heightMaps": [
        "destMap",
        "destMap2",
        "destMap3",
        "destMap4"
    ],
    "images": [
        "image1"
    ],
    "modules": [
        {
            "enableRandom": false,
            "name": "Abs",
            "src1": "CtlPerlin",
            "type": "Abs"
        },
        {
            "enableRandom": false,
            "freq": 3.1,
            "lac": 2.7,
            "name": "CtlPerlin",
            "oct": 5,
            "pers": 0.45,
            "seed": 0,
            "type": "Billow"
        },
        {
            "enableRandom": false,
            "freq": 0.1,
            "lac": 3.7,
            "name": "CtlPerlin2",
            "oct": 5,
            "pers": 0.25,
            "seed": 0,
            "type": "Perlin"
        },
        {
            "enableRandom": false,
            "freq": 1.2,
            "name": "Module2",
            "pow": 0.15,
            "rough": 1.3,
            "seed": 0,
            "src1": "ModulePerlin",
            "type": "Turbulence"
        },
        {
            "enableRandom": false,
            "freq": 1.65,
            "lac": 2.3,
            "name": "ModulePerlin",
            "oct": 6,
            "pers": 0.45,
            "seed": 0,
            "type": "Billow"
        },
        {
            "bias": -1,
            "enableRandom": false,
            "name": "ScAbs",
            "scale": 2,
            "src1": "CtlPerlin",
            "type": "ScaleBias"
        },
        {
            "enableRandom": false,
            "name": "const",
            "type": "Const",
            "value": 0
        },
        {
            "ctl": "CtlPerlin2",
            "enableRandom": false,
            "name": "sel",
            "src1": "CtlPerlin",
            "src2": "ModulePerlin",
            "type": "Blend"
        }
    ],
    "randomFactors": [
        0
    ],
    "reflectionMap": "",
    "renderers": [
        {
            "backgroundImage": "",
            "destImage": "image1",
            "enabledLight": true,
            "gradientInfo": [
                [
                    -1,
                    17,
                    63,
                    161,
                    255
                ],
                [
                    0.30000000000000004,
                    69,
                    77,
                    201,
                    255
                ],
                [
                    0.3999999999999999,
                    67,
                    186,
                    202,
                    255
                ],
                [
                    0.41999999999999993,
                    203,
                    230,
                    175,
                    255
                ],
                [
                    0.5,
                    215,
                    236,
                    164,
                    255
                ],
                [
                    0.8955823293172691,
                    136,
                    185,
                    62,
                    255
                ],
                [
                    1,
                    191,
                    167,
                    121,
                    255
                ]
            ],
            "heightMap": "destMap",
            "lightBrightness": 2.1,
            "lightContrast": 0.01,
            "name": "renderer1",
            "randomFactor": [
                16,
                1,
                1
            ],
            "randomGradient": false
        }
    ],
    "size": [
        1024,
        512
    ]
}

)TX";




static std::string GASGIANT4 = R"TX(
    {
    "bounds": [
        -90,
        90,
        -180,
        180
    ],
    "builderType": "sphere",
    "bumpMap": "",
    "cloudMap": "",
    "colorMap": "BaseImage",
    "heightMapBuilders": [
        {
            "dest": "BeginLayer_heightMap",
            "name": "BeginLayer_noiseMapBuilder1",
            "seamless": true,
            "source": "Turboz"
        },
        {
            "dest": "EndLayer_heightMap",
            "name": "EndLayer_noiseMapBuilder1",
            "seamless": true,
            "source": "TurboOuter"
        }
    ],
    "heightMaps": [
        "BeginLayer_heightMap",
        "EndLayer_heightMap"
    ],
    "images": [
        "BaseImage"
    ],
    "modules": [
        {
            "enableRandom": true,
            "freq": 0.98,
            "name": "BeginLayer_Module",
            "type": "Cylinders"
        },
        {
            "enableRandom": false,
            "freq": 0.25,
            "name": "CylOuter",
            "type": "Cylinders"
        },
        {
            "enableDist": true,
            "enableRandom": true,
            "freq": 5.2844999999999995,
            "name": "EndLayer_Module",
            "seed": 0,
            "type": "Voronoi"
        },
        {
            "enableRandom": false,
            "freq": 2.5,
            "name": "TurboOuter",
            "pow": 0.3,
            "rough": 1.8,
            "seed": 0,
            "src1": "CylOuter",
            "type": "Turbulence"
        },
        {
            "enableRandom": true,
            "freq": 1.4,
            "name": "Turboz",
            "pow": 0.09,
            "rough": 1.13,
            "seed": 0,
            "src1": "BeginLayer_Module",
            "type": "Turbulence"
        }
    ],
    "randomFactors": [
        0,
        0.1,
        0.2,
        0.3,
        0.2,
        0.1,
        0.09,
        0.1,
        0.8
    ],
    "reflectionMap": "",
    "renderers": [
        {
            "backgroundImage": "",
            "destImage": "BaseImage",
            "enabledLight": false,
            "gradientInfo": [
                [
                    -1,
                    124,
                    20,
                    12,
                    255
                ],
                [
                    -0.70245,
                    119,
                    41,
                    6,
                    255
                ],
                [
                    -0.44877500000000003,
                    124,
                    26,
                    0,
                    255
                ],
                [
                    -0.32155,
                    98,
                    47,
                    6,
                    255
                ],
                [
                    -0.06057499999999999,
                    76,
                    51,
                    8,
                    255
                ],
                [
                    0.23985,
                    70,
                    35,
                    3,
                    255
                ],
                [
                    0.474225,
                    67,
                    38,
                    7,
                    255
                ],
                [
                    0.687,
                    80,
                    61,
                    3,
                    255
                ],
                [
                    1,
                    105,
                    78,
                    7,
                    255
                ]
            ],
            "heightMap": "BeginLayer_heightMap",
            "lightBrightness": 1,
            "lightContrast": 1,
            "name": "BeginLayer_renderer",
            "randomFactor": [
                3,
                2,
                2
            ],
            "randomGradient": true
        },
        {
            "backgroundImage": "BaseImage",
            "destImage": "BaseImage",
            "enabledLight": false,
            "gradientInfo": [
                [
                    -1,
                    227,
                    228,
                    198,
                    0
                ],
                [
                    -0.62928,
                    228,
                    231,
                    213,
                    0
                ],
                [
                    -0.15229357798165133,
                    196,
                    255,
                    244,
                    0
                ],
                [
                    -0.06055045871559628,
                    196,
                    255,
                    252,
                    98
                ],
                [
                    0.005504587155963359,
                    218,
                    247,
                    175,
                    64
                ],
                [
                    0.05688073394495419,
                    175,
                    249,
                    228,
                    92
                ],
                [
                    0.0972477064220183,
                    236,
                    240,
                    199,
                    64
                ],
                [
                    0.4311926605504588,
                    225,
                    240,
                    201,
                    0
                ],
                [
                    0.5909200000000001,
                    229,
                    231,
                    222,
                    0
                ],
                [
                    0.7592800000000002,
                    220,
                    229,
                    226,
                    0
                ],
                [
                    0.8592800000000003,
                    198,
                    225,
                    168,
                    0
                ],
                [
                    0.9448000000000003,
                    166,
                    214,
                    226,
                    0
                ],
                [
                    1,
                    191,
                    195,
                    245,
                    0
                ]
            ],
            "heightMap": "EndLayer_heightMap",
            "lightBrightness": 1,
            "lightContrast": 1,
            "name": "renderer0002",
            "randomFactor": [
                90,
                1,
                1
            ],
            "randomGradient": false
        }
    ],
    "size": [
        1280,
        640
    ]
}

)TX";




static std::string POSTGARDEN = R"TX(
    {
    "bounds": [
        -90,
        90,
        -180,
        180
    ],
    "builderType": "sphere",
    "bumpMap": "image1",
    "cloudMap": "imageBump",
    "colorMap": "ImageSea",
    "heightMapBuilders": [
        {
            "dest": "heightMapPole",
            "name": "heightMapBuilderPole",
            "seamless": true,
            "source": "turboPoles"
        },
        {
            "dest": "heightMapDesert",
            "name": "noiseMapBDesert",
            "seamless": false,
            "source": "ModuleDesert"
        },
        {
            "dest": "heightMap",
            "name": "noiseMapBuilder1",
            "seamless": false,
            "source": "Module1"
        },
        {
            "dest": "HeightMapSea",
            "name": "noiseMapSea",
            "seamless": false,
            "source": "ModuleSea"
        }
    ],
    "heightMaps": [
        "HeightMapSea",
        "heightMap",
        "heightMapDesert",
        "heightMapPole"
    ],
    "images": [
        "ImageSea",
        "image1",
        "imageBump"
    ],
    "modules": [
        {
            "enableRandom": true,
            "freq": 4.2,
            "lac": 2.2,
            "name": "Module1",
            "oct": 6,
            "pers": 0.24,
            "seed": 0,
            "type": "Perlin"
        },
        {
            "enableRandom": true,
            "freq": 4.5,
            "lac": 4.3,
            "name": "ModuleDesert",
            "oct": 6,
            "pers": 0.35,
            "seed": 0,
            "type": "Billow"
        },
        {
            "enableRandom": true,
            "freq": 1.8,
            "lac": 5.3,
            "name": "ModuleSea",
            "oct": 6,
            "pers": 0.23,
            "seed": 0,
            "type": "Perlin"
        },
        {
            "enableRandom": true,
            "freq": 2.5,
            "name": "TurboDesert",
            "pow": 0.55,
            "rough": 1.2,
            "seed": 0,
            "src1": "ModuleDesert",
            "type": "Turbulence"
        },
        {
            "enableRandom": false,
            "freq": 0.5,
            "name": "cylinderPole",
            "type": "Cylinders"
        },
        {
            "enableRandom": true,
            "freq": 4.5,
            "name": "turboPoles",
            "pow": 0.2,
            "rough": 3.2,
            "seed": 0,
            "src1": "cylinderPole",
            "type": "Turbulence"
        }
    ],
    "randomFactors": [
        0
    ],
    "reflectionMap": "imageMountains",
    "renderers": [
        {
            "backgroundImage": "",
            "destImage": "image1",
            "enabledLight": true,
            "gradientInfo": [
                [
                    -1,
                    34,
                    64,
                    54,
                    255
                ],
                [
                    -0.05604719764011801,
                    172,
                    173,
                    62,
                    255
                ],
                [
                    0.9156626506024097,
                    19,
                    87,
                    13,
                    255
                ],
                [
                    1,
                    193,
                    175,
                    143,
                    255
                ]
            ],
            "heightMap": "heightMap",
            "lightBrightness": 2,
            "lightContrast": 1.5,
            "name": "renderer1",
            "randomFactor": [
                75,
                50,
                50
            ],
            "randomGradient": false
        },
        {
            "backgroundImage": "image1",
            "destImage": "image1",
            "enabledLight": true,
            "gradientInfo": [
                [
                    -1,
                    251,
                    247,
                    244,
                    0
                ],
                [
                    -0.7991967871485943,
                    7,
                    0,
                    0,
                    0
                ],
                [
                    -0.028112449799196804,
                    208,
                    209,
                    95,
                    65
                ],
                [
                    1,
                    232,
                    212,
                    117,
                    135
                ]
            ],
            "heightMap": "heightMapDesert",
            "lightBrightness": 2,
            "lightContrast": 1.4,
            "name": "renderer2",
            "randomGradient": false
        },
        {
            "backgroundImage": "image1",
            "destImage": "ImageSea",
            "enabledLight": false,
            "gradientInfo": [
                [
                    -1,
                    55,
                    51,
                    187,
                    255
                ],
                [
                    -0.5903614457831325,
                    74,
                    33,
                    205,
                    255
                ],
                [
                    0.053211009174311874,
                    75,
                    105,
                    224,
                    255
                ],
                [
                    0.18899082568807346,
                    22,
                    228,
                    229,
                    255
                ],
                [
                    0.20733944954128436,
                    74,
                    147,
                    127,
                    255
                ],
                [
                    0.3174311926605504,
                    113,
                    60,
                    24,
                    255
                ],
                [
                    0.4128440366972477,
                    172,
                    193,
                    105,
                    0
                ],
                [
                    0.5060240963855422,
                    127,
                    179,
                    218,
                    0
                ],
                [
                    0.9036144578313252,
                    77,
                    120,
                    63,
                    255
                ],
                [
                    0.9397590361445782,
                    93,
                    202,
                    139,
                    255
                ],
                [
                    1,
                    13,
                    220,
                    154,
                    255
                ]
            ],
            "heightMap": "HeightMapSea",
            "lightBrightness": 1,
            "lightContrast": 1,
            "name": "renderer3",
            "randomFactor": [
                4,
                25,
                25
            ],
            "randomGradient": false
        },
        {
            "backgroundImage": "ImageSea",
            "destImage": "ImageSea",
            "enabledLight": false,
            "gradientInfo": [
                [
                    -1,
                    0,
                    0,
                    0,
                    0
                ],
                [
                    0.7394495412844038,
                    254,
                    251,
                    248,
                    0
                ],
                [
                    0.7908256880733946,
                    255,
                    253,
                    251,
                    255
                ],
                [
                    1,
                    255,
                    254,
                    252,
                    255
                ]
            ],
            "heightMap": "heightMapPole",
            "lightBrightness": 1,
            "lightContrast": 1,
            "name": "renderer4",
            "randomGradient": false
        },
        {
            "backgroundImage": "",
            "destImage": "imageBump",
            "enabledLight": false,
            "gradientInfo": [
                [
                    -1,
                    255,
                    255,
                    255,
                    255
                ],
                [
                    0.167,
                    255,
                    255,
                    255,
                    255
                ],
                [
                    0.168,
                    0,
                    0,
                    0,
                    255
                ],
                [
                    0.91,
                    0,
                    0,
                    0,
                    255
                ],
                [
                    0.911,
                    255,
                    255,
                    255,
                    255
                ],
                [
                    1,
                    255,
                    255,
                    255,
                    255
                ]
            ],
            "heightMap": "HeightMapSea",
            "lightBrightness": 1,
            "lightContrast": 1,
            "name": "renderer5",
            "randomGradient": false
        }
    ],
    "size": [
        1600,
        800
    ]
}

)TX";




static std::string HOTHOUSE = R"TX(
    {
    "bounds": [
        -90,
        90,
        -180,
        180
    ],
    "builderType": "sphere",
    "bumpMap": "heightMap",
    "cloudMap": "",
    "colorMap": "image1",
    "heightMapBuilders": [
        {
            "dest": "destMap",
            "name": "builder",
            "seamless": true,
            "source": "Abs"
        }
    ],
    "heightMaps": [
        "destMap"
    ],
    "images": [
        "image1"
    ],
    "modules": [
        {
            "enableRandom": false,
            "name": "Abs",
            "src1": "Module2",
            "type": "Abs"
        },
        {
            "enableRandom": false,
            "name": "Module2",
            "src1": "ModulePerlin",
            "type": "ScalePoint",
            "x": 1.5,
            "y": 11,
            "z": 1
        },
        {
            "enableRandom": false,
            "freq": 1.65,
            "lac": 1.3,
            "name": "ModulePerlin",
            "oct": 6,
            "pers": 0.15,
            "seed": 0,
            "type": "Billow"
        },
        {
            "name": "ModulePerlin2",
            "type": "Billow2"
        }
    ],
    "randomFactors": [
        0
    ],
    "reflectionMap": "",
    "renderers": [
        {
            "backgroundImage": "",
            "destImage": "image1",
            "enabledLight": true,
            "gradientInfo": [
                [
                    0,
                    181,
                    195,
                    153,
                    255
                ],
                [
                    0.5,
                    219,
                    196,
                    144,
                    255
                ],
                [
                    1,
                    78,
                    185,
                    94,
                    255
                ]
            ],
            "heightMap": "destMap",
            "lightBrightness": 2.1,
            "lightContrast": 0.1,
            "name": "renderer1",
            "randomFactor": [
                53,
                10,
                30
            ],
            "randomGradient": false
        }
    ],
    "size": [
        1280,
        640
    ]
}

)TX";



static std::string DESERT_CREAM = R"TX(
    {
    "bounds": [
        -90,
        90,
        -180,
        180
    ],
    "builderType": "sphere",
    "bumpMap": "",
    "cloudMap": "",
    "colorMap": "BaseImage",
    "heightMapBuilders": [
        {
            "dest": "BeginLayer_heightMap",
            "name": "BeginLayer_noiseMapBuilder1",
            "seamless": true,
            "source": "BeginLayer_Module"
        },
        {
            "dest": "EndLayer_heightMap",
            "name": "EndLayer_noiseMapBuilder1",
            "seamless": true,
            "source": "Module_40"
        }
    ],
    "heightMaps": [
        "BeginLayer_heightMap",
        "EndLayer_heightMap"
    ],
    "images": [
        "BaseImage"
    ],
    "modules": [
        {
            "enableDist": true,
            "enableRandom": true,
            "freq": 4.3,
            "name": "BeginLayer_Module",
            "seed": 0,
            "type": "Voronoi"
        },
        {
            "cpoints": [
                [
                    -1,
                    -1
                ],
                [
                    -0.55,
                    0.5
                ],
                [
                    -0.25,
                    0.5
                ],
                [
                    1,
                    -1
                ]
            ],
            "enableRandom": true,
            "name": "EndLayer_Module",
            "src1": "BeginLayer_Module",
            "type": "Curve"
        },
        {
            "enableRandom": false,
            "freq": 1.4,
            "name": "Module_20",
            "pow": 0.21,
            "rough": 4.2,
            "seed": 0,
            "src1": "EndLayer_Module",
            "type": "Turbulence"
        },
        {
            "enableRandom": false,
            "freq": 1.8,
            "lac": 2.3,
            "name": "Module_30",
            "oct": 6,
            "pers": 0.4,
            "seed": 0,
            "type": "Perlin"
        },
        {
            "enableRandom": false,
            "name": "Module_40",
            "src1": "Module_30",
            "src2": "Module_50",
            "type": "Avg"
        },
        {
            "bias": 0.5,
            "enableRandom": false,
            "name": "Module_50",
            "scale": 0.5,
            "src1": "Module_20",
            "type": "ScaleBias"
        }
    ],
    "randomFactors": [
        0.1,
        0.2,
        0.4,
        0.6,
        0.8
    ],
    "reflectionMap": "",
    "renderers": [
        {
            "backgroundImage": "",
            "destImage": "BaseImage",
            "enabledLight": true,
            "gradientInfo": [
                [
                    -1,
                    237,
                    201,
                    118,
                    255
                ],
                [
                    -0.6,
                    211,
                    201,
                    183,
                    255
                ],
                [
                    0,
                    227,
                    208,
                    195,
                    255
                ],
                [
                    0.6000000000000001,
                    193,
                    219,
                    207,
                    255
                ],
                [
                    1,
                    187,
                    219,
                    191,
                    255
                ]
            ],
            "heightMap": "EndLayer_heightMap",
            "lightBrightness": 2,
            "lightContrast": 1.5,
            "name": "BeginLayer_renderer",
            "randomFactor": [
                75,
                25,
                25
            ],
            "randomGradient": false
        }
    ],
    "size": [
        1024,
        512
    ]
}

)TX";




static std::string ICEBALL = R"TX(
    {
    "bounds": [
        -90,
        90,
        -180,
        180
    ],
    "builderType": "sphere",
    "bumpMap": "heightMap",
    "cloudMap": "",
    "colorMap": "colorMap",
    "heightMapBuilders": [
        {
            "dest": "destMap",
            "name": "builder",
            "seamless": true,
            "source": "ModulePerlin"
        },
        {
            "dest": "destMap2",
            "name": "builder2",
            "seamless": true,
            "source": "ScAbs"
        }
    ],
    "heightMaps": [
        "destMap",
        "destMap2",
        "destMap3",
        "destMap4"
    ],
    "images": [
        "colorMap",
        "heightMap"
    ],
    "modules": [
        {
            "enableRandom": false,
            "name": "Abs",
            "src1": "CtlPerlin",
            "type": "Abs"
        },
        {
            "enableRandom": false,
            "freq": 3.1,
            "lac": 2.7,
            "name": "CtlPerlin",
            "oct": 5,
            "pers": 0.45,
            "seed": 0,
            "type": "Billow"
        },
        {
            "enableRandom": false,
            "freq": 0.1,
            "lac": 3.7,
            "name": "CtlPerlin2",
            "oct": 5,
            "pers": 0.25,
            "seed": 0,
            "type": "Perlin"
        },
        {
            "enableRandom": false,
            "freq": 0.3,
            "name": "CtlSphere",
            "type": "Spheres"
        },
        {
            "enableRandom": false,
            "freq": 1.2,
            "name": "Module2",
            "pow": 0.15,
            "rough": 1.3,
            "seed": 0,
            "src1": "ModulePerlin",
            "type": "Turbulence"
        },
        {
            "enableRandom": false,
            "freq": 1.65,
            "lac": 2.3,
            "name": "ModulePerlin",
            "oct": 6,
            "pers": 0.45,
            "seed": 0,
            "type": "Perlin"
        },
        {
            "bias": 1,
            "enableRandom": false,
            "name": "ScAbs",
            "scale": 2,
            "src1": "CtlPerlin",
            "type": "ScaleBias"
        },
        {
            "enableRandom": false,
            "name": "const",
            "type": "Const",
            "value": 0
        },
        {
            "ctl": "CtlSphere",
            "enableRandom": false,
            "name": "sel",
            "src1": "CtlSphere",
            "src2": "ModulePerlin",
            "type": "Blend"
        }
    ],
    "randomFactors": [
        0
    ],
    "reflectionMap": "",
    "renderers": [
        {
            "backgroundImage": "",
            "destImage": "colorMap",
            "enabledLight": true,
            "gradientInfo": [
                [
                    -1,
                    217,
                    200,
                    189,
                    255
                ],
                [
                    0.1200000000000001,
                    199,
                    218,
                    139,
                    255
                ],
                [
                    1,
                    214,
                    220,
                    192,
                    255
                ]
            ],
            "heightMap": "destMap2",
            "lightBrightness": 2.1,
            "lightContrast": 0.2,
            "name": "renderer1",
            "randomFactor": [
                70,
                20,
                20
            ],
            "randomGradient": false
        },
        {
            "backgroundImage": "colorMap",
            "destImage": "colorMap",
            "enabledLight": true,
            "gradientInfo": [
                [
                    -1,
                    50,
                    26,
                    103,
                    0
                ],
                [
                    -0.6,
                    150,
                    143,
                    70,
                    0
                ],
                [
                    0.1100000000000001,
                    96,
                    124,
                    147,
                    0
                ],
                [
                    0.1200000000000001,
                    13,
                    108,
                    69,
                    64
                ],
                [
                    0.55,
                    132,
                    135,
                    93,
                    128
                ],
                [
                    0.95,
                    70,
                    132,
                    89,
                    192
                ],
                [
                    1,
                    92,
                    35,
                    21,
                    192
                ]
            ],
            "heightMap": "destMap",
            "lightBrightness": 2.1,
            "lightContrast": 0.2,
            "name": "renderer2",
            "randomFactor": [
                120,
                40,
                40
            ],
            "randomGradient": false
        },
        {
            "backgroundImage": "",
            "destImage": "heightMap",
            "enabledLight": true,
            "gradientInfo": [
                [
                    1,
                    0,
                    0,
                    0,
                    255
                ],
                [
                    0.12,
                    128,
                    128,
                    128,
                    255
                ],
                [
                    -1,
                    255,
                    255,
                    255,
                    255
                ]
            ],
            "heightMap": "destMap2",
            "lightBrightness": 2.1,
            "lightContrast": 0.2,
            "name": "renderer3",
            "randomGradient": false
        }
    ],
    "size": [
        1280,
        640
    ]
}

)TX";




static std::string GLACIER = R"TX(
    {
    "bounds": [
        -90,
        90,
        -180,
        180
    ],
    "builderType": "sphere",
    "bumpMap": "image1",
    "cloudMap": "imageBump",
    "colorMap": "ImageSea",
    "heightMapBuilders": [
        {
            "dest": "heightMapPole",
            "name": "heightMapBuilderPole",
            "seamless": true,
            "source": "turboPoles"
        },
        {
            "dest": "heightMapDesert",
            "name": "noiseMapBDesert",
            "seamless": false,
            "source": "ModuleDesert"
        },
        {
            "dest": "heightMap",
            "name": "noiseMapBuilder1",
            "seamless": false,
            "source": "Module1"
        },
        {
            "dest": "HeightMapSea",
            "name": "noiseMapSea",
            "seamless": false,
            "source": "ModuleSea"
        }
    ],
    "heightMaps": [
        "HeightMapSea",
        "heightMap",
        "heightMapDesert",
        "heightMapPole"
    ],
    "images": [
        "ImageSea",
        "image1",
        "imageBump"
    ],
    "modules": [
        {
            "enableRandom": true,
            "freq": 4.2,
            "lac": 2.2,
            "name": "Module1",
            "oct": 6,
            "pers": 0.24,
            "seed": 0,
            "type": "Perlin"
        },
        {
            "enableRandom": true,
            "freq": 4.5,
            "lac": 4.3,
            "name": "ModuleDesert",
            "oct": 6,
            "pers": 0.35,
            "seed": 0,
            "type": "Billow"
        },
        {
            "enableRandom": true,
            "freq": 1.8,
            "lac": 5.3,
            "name": "ModuleSea",
            "oct": 6,
            "pers": 0.23,
            "seed": 0,
            "type": "Perlin"
        },
        {
            "enableRandom": true,
            "freq": 2.5,
            "name": "TurboDesert",
            "pow": 0.55,
            "rough": 1.2,
            "seed": 0,
            "src1": "ModuleDesert",
            "type": "Turbulence"
        },
        {
            "enableRandom": false,
            "freq": 0.5,
            "name": "cylinderPole",
            "type": "Cylinders"
        },
        {
            "enableRandom": true,
            "freq": 4.5,
            "name": "turboPoles",
            "pow": 0.2,
            "rough": 3.2,
            "seed": 0,
            "src1": "cylinderPole",
            "type": "Turbulence"
        }
    ],
    "randomFactors": [
        0
    ],
    "reflectionMap": "imageMountains",
    "renderers": [
        {
            "backgroundImage": "",
            "destImage": "image1",
            "enabledLight": true,
            "gradientInfo": [
                [
                    -1,
                    12,
                    97,
                    0,
                    255
                ],
                [
                    -0.05604719764011801,
                    115,
                    146,
                    0,
                    255
                ],
                [
                    0.9156626506024097,
                    0,
                    195,
                    0,
                    255
                ],
                [
                    1,
                    255,
                    255,
                    255,
                    255
                ]
            ],
            "heightMap": "heightMap",
            "lightBrightness": 2,
            "lightContrast": 1.5,
            "name": "renderer1",
            "randomGradient": false
        },
        {
            "backgroundImage": "image1",
            "destImage": "image1",
            "enabledLight": true,
            "gradientInfo": [
                [
                    -1,
                    251,
                    247,
                    244,
                    0
                ],
                [
                    -0.7991967871485943,
                    7,
                    0,
                    0,
                    0
                ],
                [
                    -0.028112449799196804,
                    208,
                    209,
                    95,
                    65
                ],
                [
                    1,
                    232,
                    212,
                    117,
                    135
                ]
            ],
            "heightMap": "heightMapDesert",
            "lightBrightness": 2,
            "lightContrast": 1.4,
            "name": "renderer2",
            "randomGradient": false
        },
        {
            "backgroundImage": "image1",
            "destImage": "ImageSea",
            "enabledLight": false,
            "gradientInfo": [
                [
                    -1,
                    0,
                    31,
                    174,
                    255
                ],
                [
                    -0.5903614457831325,
                    0,
                    14,
                    239,
                    255
                ],
                [
                    0.024096385542168752,
                    0,
                    80,
                    235,
                    255
                ],
                [
                    0.16867469879518082,
                    85,
                    226,
                    234,
                    255
                ],
                [
                    0.18338108882521498,
                    109,
                    185,
                    114,
                    255
                ],
                [
                    0.2530120481927711,
                    120,
                    166,
                    59,
                    255
                ],
                [
                    0.3373493975903614,
                    214,
                    176,
                    146,
                    0
                ],
                [
                    0.5060240963855422,
                    127,
                    207,
                    251,
                    0
                ],
                [
                    0.9036144578313252,
                    129,
                    167,
                    59,
                    255
                ],
                [
                    0.9397590361445782,
                    117,
                    225,
                    251,
                    255
                ],
                [
                    1,
                    0,
                    203,
                    224,
                    255
                ]
            ],
            "heightMap": "HeightMapSea",
            "lightBrightness": 1,
            "lightContrast": 1,
            "name": "renderer3",
            "randomGradient": false
        },
        {
            "backgroundImage": "ImageSea",
            "destImage": "ImageSea",
            "enabledLight": false,
            "gradientInfo": [
                [
                    -1,
                    0,
                    0,
                    0,
                    0
                ],
                [
                    -0.655045871559633,
                    254,
                    251,
                    248,
                    0
                ],
                [
                    -0.4495412844036697,
                    255,
                    253,
                    251,
                    255
                ],
                [
                    1,
                    255,
                    254,
                    252,
                    255
                ]
            ],
            "heightMap": "heightMapPole",
            "lightBrightness": 1,
            "lightContrast": 1,
            "name": "renderer4",
            "randomGradient": false
        },
        {
            "backgroundImage": "",
            "destImage": "imageBump",
            "enabledLight": false,
            "gradientInfo": [
                [
                    -1,
                    255,
                    255,
                    255,
                    255
                ],
                [
                    0.167,
                    255,
                    255,
                    255,
                    255
                ],
                [
                    0.168,
                    0,
                    0,
                    0,
                    255
                ],
                [
                    0.91,
                    0,
                    0,
                    0,
                    255
                ],
                [
                    0.911,
                    255,
                    255,
                    255,
                    255
                ],
                [
                    1,
                    255,
                    255,
                    255,
                    255
                ]
            ],
            "heightMap": "HeightMapSea",
            "lightBrightness": 1,
            "lightContrast": 1,
            "name": "renderer5",
            "randomGradient": false
        }
    ],
    "size": [
        1600,
        800
    ]
}

)TX";




static std::string GASGIANT3 = R"TX(
    {
    "bounds": [
        -90,
        90,
        -180,
        180
    ],
    "builderType": "sphere",
    "bumpMap": "",
    "cloudMap": "",
    "colorMap": "BaseImage",
    "heightMapBuilders": [
        {
            "dest": "BeginLayer_heightMap",
            "name": "BeginLayer_noiseMapBuilder1",
            "seamless": true,
            "source": "Turboz"
        },
        {
            "dest": "EndLayer_heightMap",
            "name": "EndLayer_noiseMapBuilder1",
            "seamless": true,
            "source": "EndLayer_Module"
        }
    ],
    "heightMaps": [
        "BeginLayer_heightMap",
        "EndLayer_heightMap"
    ],
    "images": [
        "BaseImage"
    ],
    "modules": [
        {
            "enableRandom": true,
            "freq": 0.98,
            "name": "BeginLayer_Module",
            "type": "Cylinders"
        },
        {
            "enableDist": true,
            "enableRandom": true,
            "freq": 5.2844999999999995,
            "name": "EndLayer_Module",
            "seed": 0,
            "type": "Voronoi"
        },
        {
            "enableRandom": true,
            "freq": 1.4,
            "name": "Turboz",
            "pow": 0.09,
            "rough": 1.13,
            "seed": 0,
            "src1": "BeginLayer_Module",
            "type": "Turbulence"
        }
    ],
    "randomFactors": [
        0,
        0.1,
        0.2,
        0.3,
        0.2,
        0.1,
        0.09,
        0.1,
        0.8
    ],
    "reflectionMap": "",
    "renderers": [
        {
            "backgroundImage": "",
            "destImage": "BaseImage",
            "enabledLight": false,
            "gradientInfo": [
                [
                    -1,
                    55,
                    97,
                    74,
                    255
                ],
                [
                    -0.8573999999999999,
                    60,
                    95,
                    67,
                    255
                ],
                [
                    -0.6830999999999999,
                    51,
                    81,
                    58,
                    255
                ],
                [
                    -0.34284999999999993,
                    64,
                    110,
                    75,
                    255
                ],
                [
                    -0.23649999999999993,
                    61,
                    111,
                    82,
                    255
                ],
                [
                    -0.04322499999999993,
                    62,
                    101,
                    84,
                    255
                ],
                [
                    0.22117500000000004,
                    71,
                    126,
                    91,
                    255
                ],
                [
                    0.52025,
                    65,
                    111,
                    71,
                    255
                ],
                [
                    0.853475,
                    58,
                    112,
                    66,
                    255
                ],
                [
                    1,
                    63,
                    113,
                    79,
                    255
                ]
            ],
            "heightMap": "BeginLayer_heightMap",
            "lightBrightness": 1,
            "lightContrast": 1,
            "name": "BeginLayer_renderer",
            "randomFactor": [
                3,
                2,
                2
            ],
            "randomGradient": true
        }
    ],
    "size": [
        1280,
        640
    ]
}

)TX";




static std::string PREGARDEN = R"TX(
    {
    "bounds": [
        -90,
        90,
        -180,
        180
    ],
    "builderType": "sphere",
    "bumpMap": "image1",
    "cloudMap": "imageBump",
    "colorMap": "ImageSea",
    "heightMapBuilders": [
        {
            "dest": "heightMapPole",
            "name": "heightMapBuilderPole",
            "seamless": true,
            "source": "turboPoles"
        },
        {
            "dest": "heightMapDesert",
            "name": "noiseMapBDesert",
            "seamless": false,
            "source": "ModuleDesert"
        },
        {
            "dest": "heightMap",
            "name": "noiseMapBuilder1",
            "seamless": false,
            "source": "Module1"
        },
        {
            "dest": "HeightMapSea",
            "name": "noiseMapSea",
            "seamless": false,
            "source": "ModuleSea"
        }
    ],
    "heightMaps": [
        "HeightMapSea",
        "heightMap",
        "heightMapDesert",
        "heightMapPole"
    ],
    "images": [
        "ImageSea",
        "image1",
        "imageBump"
    ],
    "modules": [
        {
            "enableRandom": true,
            "freq": 4.2,
            "lac": 2.2,
            "name": "Module1",
            "oct": 6,
            "pers": 0.24,
            "seed": 0,
            "type": "Perlin"
        },
        {
            "enableRandom": true,
            "freq": 4.5,
            "lac": 4.3,
            "name": "ModuleDesert",
            "oct": 6,
            "pers": 0.35,
            "seed": 0,
            "type": "Billow"
        },
        {
            "enableRandom": true,
            "freq": 1.8,
            "lac": 5.3,
            "name": "ModuleSea",
            "oct": 6,
            "pers": 0.23,
            "seed": 0,
            "type": "Perlin"
        },
        {
            "enableRandom": true,
            "freq": 2.5,
            "name": "TurboDesert",
            "pow": 0.55,
            "rough": 1.2,
            "seed": 0,
            "src1": "ModuleDesert",
            "type": "Turbulence"
        },
        {
            "enableRandom": false,
            "freq": 0.5,
            "name": "cylinderPole",
            "type": "Cylinders"
        },
        {
            "enableRandom": true,
            "freq": 4.5,
            "name": "turboPoles",
            "pow": 0.2,
            "rough": 3.2,
            "seed": 0,
            "src1": "cylinderPole",
            "type": "Turbulence"
        }
    ],
    "randomFactors": [
        0
    ],
    "reflectionMap": "imageMountains",
    "renderers": [
        {
            "backgroundImage": "",
            "destImage": "image1",
            "enabledLight": true,
            "gradientInfo": [
                [
                    -1,
                    14,
                    77,
                    42,
                    255
                ],
                [
                    -0.05604719764011801,
                    150,
                    117,
                    7,
                    255
                ],
                [
                    0.9156626506024097,
                    76,
                    145,
                    23,
                    255
                ],
                [
                    1,
                    231,
                    223,
                    212,
                    255
                ]
            ],
            "heightMap": "heightMap",
            "lightBrightness": 2,
            "lightContrast": 1.5,
            "name": "renderer1",
            "randomFactor": [
                25,
                50,
                50
            ],
            "randomGradient": false
        },
        {
            "backgroundImage": "image1",
            "destImage": "image1",
            "enabledLight": true,
            "gradientInfo": [
                [
                    -1,
                    251,
                    247,
                    244,
                    0
                ],
                [
                    -0.7991967871485943,
                    7,
                    0,
                    0,
                    0
                ],
                [
                    -0.028112449799196804,
                    208,
                    209,
                    95,
                    65
                ],
                [
                    1,
                    232,
                    212,
                    117,
                    135
                ]
            ],
            "heightMap": "heightMapDesert",
            "lightBrightness": 2,
            "lightContrast": 1.4,
            "name": "renderer2",
            "randomGradient": false
        },
        {
            "backgroundImage": "image1",
            "destImage": "ImageSea",
            "enabledLight": false,
            "gradientInfo": [
                [
                    -1,
                    3,
                    33,
                    151,
                    255
                ],
                [
                    -0.5903614457831325,
                    99,
                    20,
                    218,
                    255
                ],
                [
                    -0.2660550458715596,
                    22,
                    119,
                    232,
                    255
                ],
                [
                    -0.22201834862385317,
                    80,
                    215,
                    203,
                    255
                ],
                [
                    -0.17064220183486234,
                    95,
                    184,
                    145,
                    255
                ],
                [
                    -0.1119266055045871,
                    126,
                    152,
                    52,
                    255
                ],
                [
                    0.3373493975903614,
                    196,
                    192,
                    134,
                    0
                ],
                [
                    0.5060240963855422,
                    112,
                    153,
                    252,
                    0
                ],
                [
                    0.9036144578313252,
                    113,
                    172,
                    47,
                    255
                ],
                [
                    0.9397590361445782,
                    116,
                    239,
                    202,
                    255
                ],
                [
                    1,
                    10,
                    199,
                    178,
                    255
                ]
            ],
            "heightMap": "HeightMapSea",
            "lightBrightness": 1,
            "lightContrast": 1,
            "name": "renderer3",
            "randomFactor": [
                25,
                25,
                25
            ],
            "randomGradient": false
        },
        {
            "backgroundImage": "ImageSea",
            "destImage": "ImageSea",
            "enabledLight": false,
            "gradientInfo": [
                [
                    -1,
                    0,
                    0,
                    0,
                    0
                ],
                [
                    0.7394495412844038,
                    254,
                    251,
                    248,
                    0
                ],
                [
                    0.7908256880733946,
                    255,
                    253,
                    251,
                    255
                ],
                [
                    1,
                    255,
                    254,
                    252,
                    255
                ]
            ],
            "heightMap": "heightMapPole",
            "lightBrightness": 1,
            "lightContrast": 1,
            "name": "renderer4",
            "randomGradient": false
        },
        {
            "backgroundImage": "",
            "destImage": "imageBump",
            "enabledLight": false,
            "gradientInfo": [
                [
                    -1,
                    255,
                    255,
                    255,
                    255
                ],
                [
                    0.167,
                    255,
                    255,
                    255,
                    255
                ],
                [
                    0.168,
                    0,
                    0,
                    0,
                    255
                ],
                [
                    0.91,
                    0,
                    0,
                    0,
                    255
                ],
                [
                    0.911,
                    255,
                    255,
                    255,
                    255
                ],
                [
                    1,
                    255,
                    255,
                    255,
                    255
                ]
            ],
            "heightMap": "HeightMapSea",
            "lightBrightness": 1,
            "lightContrast": 1,
            "name": "renderer5",
            "randomGradient": false
        }
    ],
    "size": [
        1600,
        800
    ]
}

)TX";




static std::string GASGIANT22 = R"TX(
    {
    "bounds": [
        -90,
        90,
        -180,
        180
    ],
    "builderType": "sphere",
    "bumpMap": "",
    "cloudMap": "",
    "colorMap": "BaseImage",
    "heightMapBuilders": [
        {
            "dest": "BeginLayer_heightMap",
            "name": "BeginLayer_noiseMapBuilder1",
            "seamless": true,
            "source": "Turbulence2_Module_40"
        },
        {
            "dest": "EndLayer_heightMap",
            "name": "EndLayer_noiseMapBuilder1",
            "seamless": true,
            "source": "EndLayer_Module"
        }
    ],
    "heightMaps": [
        "BeginLayer_heightMap",
        "EndLayer_heightMap"
    ],
    "images": [
        "BaseImage"
    ],
    "modules": [
        {
            "enableRandom": true,
            "freq": 0.98,
            "name": "BeginLayer_Module",
            "type": "Cylinders"
        },
        {
            "disp": 0,
            "enableDist": true,
            "enableRandom": true,
            "freq": 5.2844999999999995,
            "name": "EndLayer_Module",
            "seed": 0,
            "type": "Voronoi"
        },
        {
            "enableRandom": false,
            "name": "ScalePoint_Module_30",
            "src1": "Turboz",
            "type": "ScalePoint",
            "x": 1,
            "y": 10,
            "z": 1
        },
        {
            "enableRandom": true,
            "freq": 1.4,
            "name": "Turboz",
            "pow": 0.09,
            "rough": 1.13,
            "seed": 0,
            "src1": "BeginLayer_Module",
            "type": "Turbulence"
        },
        {
            "enableRandom": false,
            "freq": 9.2,
            "name": "Turbulence2_Module_40",
            "pow": 0.016,
            "rough": 1.05,
            "seed": 0,
            "src1": "ScalePoint_Module_30",
            "type": "Turbulence2"
        }
    ],
    "randomFactors": [
        0,
        0.1,
        0.2,
        0.3,
        0.2,
        0.1,
        0.09,
        0.1,
        0.8
    ],
    "reflectionMap": "",
    "renderers": [
        {
            "alphaImage": "",
            "backgroundImage": "",
            "bumpMap": "",
            "destImage": "BaseImage",
            "enabledLight": false,
            "gradientInfo": [
                [
                    -1,
                    186,
                    219,
                    250,
                    255
                ],
                [
                    -0.68545,
                    153,
                    174,
                    225,
                    255
                ],
                [
                    -0.4934,
                    175,
                    204,
                    231,
                    255
                ],
                [
                    -0.18085,
                    177,
                    195,
                    221,
                    255
                ],
                [
                    -0.052775000000000016,
                    160,
                    167,
                    191,
                    255
                ],
                [
                    0.21697500000000003,
                    179,
                    181,
                    203,
                    255
                ],
                [
                    0.49785,
                    159,
                    164,
                    195,
                    255
                ],
                [
                    0.7215,
                    153,
                    168,
                    204,
                    255
                ],
                [
                    1,
                    135,
                    154,
                    181,
                    255
                ]
            ],
            "heightMap": "BeginLayer_heightMap",
            "lightBrightness": 1,
            "lightContrast": 1,
            "name": "BeginLayer_renderer",
            "randomFactor": [
                3,
                2,
                2
            ],
            "randomGradient": true
        }
    ],
    "size": [
        1280,
        640
    ]
}

)TX";




static std::string OUTRE4 = R"TX(
    {
    "bounds": [
        -90,
        90,
        -180,
        180
    ],
    "builderType": "sphere",
    "bumpMap": "01heightMap",
    "cloudMap": "imageBump",
    "colorMap": "01heightMap",
    "heightMapBuilders": [
        {
            "dest": "destMap",
            "name": "builder",
            "seamless": true,
            "source": "ModuleSelect"
        },
        {
            "dest": "destMap2",
            "name": "builder2",
            "seamless": true,
            "source": "displacer"
        },
        {
            "dest": "destMap3",
            "name": "builderDesert",
            "seamless": true,
            "source": "SelectDesert"
        }
    ],
    "heightMaps": [
        "destMap",
        "destMap2",
        "destMap3"
    ],
    "images": [
        "01heightMap",
        "02cloudMap",
        "03bumpmap"
    ],
    "modules": [
        {
            "enableRandom": true,
            "freq": 1.35,
            "lac": 2.7,
            "name": "Module2",
            "oct": 5,
            "pers": 0.25,
            "seed": 0,
            "type": "Perlin"
        },
        {
            "enableRandom": true,
            "name": "ModuleScale",
            "src1": "Module2",
            "type": "ScalePoint",
            "x": 0.9,
            "y": 1,
            "z": 1.1
        },
        {
            "ctl": "m1",
            "enableRandom": false,
            "lbound": -0.3,
            "name": "ModuleSelect",
            "src1": "ModuleScale",
            "src2": "m2",
            "type": "Select",
            "ubound": 0.5,
            "value": 0.7
        },
        {
            "enableRandom": false,
            "freq": 1.4,
            "lac": 2.2,
            "name": "PerlinDesert",
            "oct": 4,
            "pers": 0.3,
            "seed": 0,
            "type": "Perlin"
        },
        {
            "ctl": "ModuleSelect",
            "enableRandom": false,
            "lbound": 0.1,
            "name": "SelectDesert",
            "src1": "ModuleSelect",
            "src2": "PerlinDesert",
            "type": "Select",
            "ubound": 0.5,
            "value": 0.7
        },
        {
            "name": "c3",
            "type": "Cylinder"
        },
        {
            "ctl": "m2",
            "enableRandom": false,
            "name": "displacer",
            "src1": "Module2",
            "src2": "m1",
            "src3": "m3",
            "src4": "turbo1",
            "type": "Displace"
        },
        {
            "enableRandom": false,
            "freq": 1.35,
            "lac": 3.7,
            "name": "m1",
            "oct": 5,
            "pers": 0.35,
            "seed": 0,
            "type": "Billow"
        },
        {
            "enableRandom": true,
            "freq": 1.35,
            "lac": 3.7,
            "name": "m2",
            "oct": 5,
            "seed": 0,
            "type": "RidgedMulti"
        },
        {
            "enableDist": true,
            "enableRandom": false,
            "freq": 1.35,
            "name": "m3",
            "seed": 0,
            "type": "Voronoi"
        },
        {
            "enableRandom": false,
            "freq": 2.5,
            "name": "turbo1",
            "pow": 0.02,
            "rough": 1.5,
            "seed": 0,
            "src1": "m3",
            "type": "Turbulence"
        }
    ],
    "randomFactors": [
        0.1,
        0.1,
        0.2,
        0.13,
        0.154,
        0.131,
        0.1231
    ],
    "reflectionMap": "imageMountains",
    "renderers": [
        {
            "backgroundImage": "",
            "destImage": "01heightMap",
            "enabledLight": true,
            "gradientInfo": [
                [
                    -1,
                    59,
                    59,
                    65,
                    255
                ],
                [
                    -0.6,
                    226,
                    206,
                    203,
                    255
                ],
                [
                    -0.18,
                    120,
                    192,
                    169,
                    255
                ],
                [
                    -0.17,
                    181,
                    180,
                    147,
                    255
                ],
                [
                    0.28,
                    40,
                    150,
                    16,
                    255
                ],
                [
                    0.95,
                    158,
                    145,
                    139,
                    255
                ],
                [
                    1,
                    127,
                    136,
                    149,
                    255
                ]
            ],
            "heightMap": "destMap",
            "lightBrightness": 2,
            "lightContrast": 0.5,
            "name": "renderer10",
            "randomFactor": [
                5,
                20,
                40
            ],
            "randomGradient": false
        },
        {
            "backgroundImage": "",
            "destImage": "02cloudMap",
            "enabledLight": false,
            "gradientInfo": [
                [
                    -1,
                    17,
                    175,
                    225,
                    0
                ],
                [
                    -0.95,
                    15,
                    42,
                    216,
                    0
                ],
                [
                    -0.75,
                    232,
                    232,
                    232,
                    192
                ],
                [
                    -0.55,
                    140,
                    166,
                    129,
                    128
                ],
                [
                    -0.25,
                    138,
                    137,
                    202,
                    0
                ],
                [
                    -0.10999999999999999,
                    45,
                    229,
                    115,
                    0
                ],
                [
                    0.41999999999999993,
                    189,
                    246,
                    231,
                    0
                ],
                [
                    0.6499999999999999,
                    195,
                    198,
                    196,
                    224
                ],
                [
                    0.8500000000000001,
                    199,
                    211,
                    206,
                    224
                ],
                [
                    1,
                    241,
                    241,
                    241,
                    223
                ]
            ],
            "heightMap": "destMap2",
            "lightBrightness": 2,
            "lightContrast": 0.01,
            "name": "renderer11",
            "randomGradient": false
        },
        {
            "backgroundImage": "",
            "destImage": "03bumpmap",
            "enabledLight": false,
            "gradientInfo": [
                [
                    -1,
                    0,
                    0,
                    0,
                    255
                ],
                [
                    0,
                    0,
                    0,
                    0,
                    255
                ],
                [
                    1,
                    255,
                    255,
                    255,
                    255
                ]
            ],
            "heightMap": "destMap",
            "lightBrightness": 2,
            "lightContrast": 0.5,
            "name": "renderer12",
            "randomGradient": false
        },
        {
            "backgroundImage": "01heightMap",
            "destImage": "01heightMap",
            "enabledLight": false,
            "gradientInfo": [
                [
                    -1,
                    207,
                    188,
                    185,
                    0
                ],
                [
                    0.1,
                    109,
                    103,
                    84,
                    0
                ],
                [
                    0.2,
                    126,
                    68,
                    39,
                    92
                ],
                [
                    0.41,
                    118,
                    105,
                    101,
                    64
                ],
                [
                    0.46,
                    223,
                    216,
                    189,
                    0
                ],
                [
                    1,
                    76,
                    71,
                    88,
                    0
                ]
            ],
            "heightMap": "destMap3",
            "lightBrightness": 2,
            "lightContrast": 0.5,
            "name": "renderer13",
            "randomFactor": [
                4,
                20,
                10
            ],
            "randomGradient": false
        },
        {
            "backgroundImage": "01heightMap",
            "destImage": "01heightMap",
            "enabledLight": false,
            "gradientInfo": [
                [
                    -1,
                    177,
                    202,
                    194,
                    0
                ],
                [
                    0.1,
                    36,
                    187,
                    124,
                    0
                ],
                [
                    0.201,
                    156,
                    208,
                    190,
                    48
                ],
                [
                    0.31,
                    205,
                    128,
                    148,
                    64
                ],
                [
                    1,
                    89,
                    90,
                    85,
                    0
                ]
            ],
            "heightMap": "destMap3",
            "lightBrightness": 2,
            "lightContrast": 0.5,
            "name": "renderer14",
            "randomFactor": [
                30,
                20,
                10
            ],
            "randomGradient": false
        }
    ],
    "size": [
        1280,
        640
    ]
}

)TX";




static std::string EARTHLIKE_01 = R"TX(
    {
    "bounds": [
        -90,
        90,
        -180,
        180
    ],
    "builderType": "sphere",
    "bumpMap": "imgNormal",
    "cloudMap": "imgSpecular",
    "colorMap": "imgColorMap",
    "heightMapBuilders": [
        {
            "dest": "destMap",
            "name": "builder",
            "seamless": true,
            "source": "Avg_Module"
        },
        {
            "dest": "destMap2",
            "name": "builder2",
            "seamless": true,
            "source": "displacer"
        }
    ],
    "heightMaps": [
        "destMap",
        "destMap2"
    ],
    "images": [
        "imgColorMap",
        "imgNormal",
        "imgSpecular"
    ],
    "modules": [
        {
            "enableRandom": false,
            "name": "Avg_Module",
            "src1": "Module2",
            "src2": "displacer",
            "type": "Avg"
        },
        {
            "enableRandom": false,
            "freq": 1.2,
            "lac": 5.7,
            "name": "Module2",
            "oct": 4,
            "pers": 0.22,
            "seed": 0,
            "type": "Perlin"
        },
        {
            "name": "ModuleScale",
            "type": "Scale"
        },
        {
            "name": "c3",
            "type": "Cylinder"
        },
        {
            "ctl": "m1",
            "enableRandom": false,
            "name": "displacer",
            "src1": "Module2",
            "src2": "m1",
            "src3": "c3",
            "src4": "m3",
            "type": "Displace"
        },
        {
            "enableRandom": false,
            "freq": 1.35,
            "lac": 3.7,
            "name": "m1",
            "oct": 3,
            "pers": 0.35,
            "seed": 0,
            "type": "Billow"
        },
        {
            "enableRandom": false,
            "freq": 1.35,
            "lac": 3.7,
            "name": "m2",
            "oct": 4,
            "seed": 0,
            "type": "RidgedMulti"
        },
        {
            "disp": 0,
            "enableDist": true,
            "enableRandom": false,
            "freq": 1.35,
            "name": "m3",
            "seed": 0,
            "type": "Voronoi"
        }
    ],
    "randomFactors": [
        0
    ],
    "reflectionMap": "imgSpecular",
    "renderers": [
        {
            "alphaImage": "",
            "backgroundImage": "",
            "bumpMap": "",
            "destImage": "imgColorMap",
            "enabledLight": false,
            "gradientInfo": [
                [
                    -1,
                    201,
                    206,
                    145,
                    255
                ],
                [
                    -0.4972477064220183,
                    255,
                    216,
                    150,
                    255
                ],
                [
                    -0.291743119266055,
                    205,
                    205,
                    122,
                    255
                ],
                [
                    0.5302752293577981,
                    112,
                    177,
                    54,
                    255
                ],
                [
                    0.95,
                    96,
                    46,
                    0,
                    255
                ],
                [
                    1,
                    224,
                    248,
                    255,
                    255
                ]
            ],
            "heightMap": "destMap",
            "lightBrightness": 2,
            "lightContrast": 3.1,
            "name": "renderer9",
            "randomGradient": false
        },
        {
            "alphaImage": "",
            "backgroundImage": "imgColorMap",
            "bumpMap": "",
            "destImage": "imgColorMap",
            "enabledLight": false,
            "gradientInfo": [
                [
                    -1,
                    0,
                    0,
                    96,
                    255
                ],
                [
                    -0.6,
                    0,
                    64,
                    192,
                    255
                ],
                [
                    0.15321100917431188,
                    0,
                    128,
                    255,
                    255
                ],
                [
                    0.1699082568807339,
                    255,
                    216,
                    150,
                    255
                ],
                [
                    0.2550458715596331,
                    205,
                    205,
                    122,
                    225
                ],
                [
                    0.4715596330275229,
                    112,
                    177,
                    54,
                    0
                ],
                [
                    0.9192660550458716,
                    96,
                    46,
                    0,
                    0
                ],
                [
                    1,
                    224,
                    248,
                    255,
                    255
                ]
            ],
            "heightMap": "destMap",
            "lightBrightness": 2,
            "lightContrast": 1.2,
            "name": "renderer10",
            "randomGradient": false
        },
        {
            "alphaImage": "",
            "backgroundImage": "",
            "bumpMap": "",
            "destImage": "imgNormal",
            "enabledLight": false,
            "gradientInfo": [
                [
                    -1,
                    0,
                    0,
                    0,
                    255
                ],
                [
                    0.15321100917431188,
                    0,
                    0,
                    0,
                    255
                ],
                [
                    1,
                    255,
                    248,
                    255,
                    255
                ]
            ],
            "heightMap": "destMap",
            "lightBrightness": 2,
            "lightContrast": 1.2,
            "name": "rendererNormal",
            "randomGradient": false
        },
        {
            "alphaImage": "",
            "backgroundImage": "",
            "bumpMap": "",
            "destImage": "imgSpecular",
            "enabledLight": false,
            "gradientInfo": [
                [
                    -1,
                    255,
                    255,
                    255,
                    255
                ],
                [
                    0.15321100917431188,
                    255,
                    255,
                    255,
                    255
                ],
                [
                    0.16321100917431186,
                    0,
                    0,
                    0,
                    255
                ],
                [
                    1,
                    0,
                    0,
                    0,
                    255
                ]
            ],
            "heightMap": "destMap",
            "lightBrightness": 2,
            "lightContrast": 1.2,
            "name": "rendererSpecular",
            "randomGradient": false
        }
    ],
    "size": [
        1600,
        800
    ]
}

)TX";




static std::string ROCKBALL_03 = R"TX(
    {
    "bounds": [
        -90,
        90,
        -180,
        180
    ],
    "builderType": "sphere",
    "bumpMap": "BaseImage",
    "cloudMap": "",
    "colorMap": "ZZAlpha",
    "heightMapBuilders": [
        {
            "dest": "BeginLayer_heightMap",
            "name": "BeginLayer_noiseMapBuilder1",
            "seamless": true,
            "source": "Turbulence2_Module_30"
        },
        {
            "dest": "EndLayer_heightMap",
            "name": "EndLayer_noiseMapBuilder1",
            "seamless": true,
            "source": "EndLayer_Module"
        }
    ],
    "heightMaps": [
        "BeginLayer_heightMap",
        "EndLayer_heightMap"
    ],
    "images": [
        "BaseImage",
        "ZZAlpha"
    ],
    "modules": [
        {
            "disp": 0,
            "enableDist": true,
            "enableRandom": true,
            "freq": 5.5,
            "name": "BeginLayer_Module",
            "seed": 0,
            "type": "Voronoi"
        },
        {
            "cpoints": [
                [
                    -1,
                    0.8
                ],
                [
                    -0.5,
                    0.7
                ],
                [
                    -0.1,
                    -0.3
                ],
                [
                    0,
                    -0.5
                ],
                [
                    0.1,
                    0.3
                ],
                [
                    0.5,
                    0.7
                ],
                [
                    0.7,
                    0.8
                ],
                [
                    1,
                    0.7
                ]
            ],
            "enableRandom": false,
            "name": "Curve_Module_20",
            "src1": "BeginLayer_Module",
            "type": "Curve"
        },
        {
            "enableRandom": true,
            "freq": 5.82225,
            "lac": 1.3,
            "name": "EndLayer_Module",
            "oct": 9,
            "seed": 0,
            "type": "RidgedMulti"
        },
        {
            "enableRandom": false,
            "freq": 2.3,
            "name": "Turbulence2_Module_30",
            "pow": 0.09,
            "rough": 2.3,
            "seed": 0,
            "src1": "Curve_Module_20",
            "type": "Turbulence2"
        }
    ],
    "randomFactors": [
        0,
        0.1,
        0.2,
        0.3,
        0.4,
        0.5,
        0.4,
        0.3,
        0.2,
        0.1,
        0.1
    ],
    "reflectionMap": "",
    "renderers": [
        {
            "alphaImage": "",
            "backgroundImage": "",
            "bumpMap": "EndLayer_heightMap",
            "destImage": "ZZAlpha",
            "enabledLight": false,
            "gradientInfo": [
                [
                    -1,
                    22,
                    42,
                    18,
                    255
                ],
                [
                    -0.871375,
                    33,
                    71,
                    34,
                    255
                ],
                [
                    -0.5242,
                    31,
                    61,
                    27,
                    255
                ],
                [
                    -0.227425,
                    55,
                    88,
                    44,
                    255
                ],
                [
                    0.07325000000000004,
                    74,
                    95,
                    53,
                    255
                ],
                [
                    0.17782500000000012,
                    86,
                    124,
                    62,
                    255
                ],
                [
                    0.4689000000000001,
                    48,
                    96,
                    76,
                    255
                ],
                [
                    0.7334499999999999,
                    92,
                    81,
                    43,
                    255
                ],
                [
                    1,
                    93,
                    117,
                    61,
                    255
                ]
            ],
            "heightMap": "BeginLayer_heightMap",
            "lightBrightness": 2,
            "lightContrast": 0.3,
            "name": "BeginLayer_renderer",
            "randomFactor": [
                25,
                1,
                1
            ],
            "randomGradient": false
        },
        {
            "alphaImage": "",
            "backgroundImage": "",
            "bumpMap": "BeginLayer_heightMap",
            "destImage": "BaseImage",
            "enabledLight": false,
            "gradientInfo": [
                [
                    -1,
                    0,
                    0,
                    0,
                    255
                ],
                [
                    1,
                    255,
                    255,
                    255,
                    255
                ]
            ],
            "heightMap": "EndLayer_heightMap",
            "lightBrightness": 2,
            "lightContrast": 0.85,
            "name": "ZZDone",
            "randomGradient": false
        }
    ],
    "size": [
        1280,
        640
    ]
}

)TX";




static std::string POSTGARDEN_3 = R"TX(
    {
    "bounds": [
        -90,
        90,
        -180,
        180
    ],
    "builderType": "sphere",
    "bumpMap": "",
    "cloudMap": "",
    "colorMap": "BaseImage",
    "heightMapBuilders": [
        {
            "dest": "BeginLayer_heightMap",
            "name": "BeginLayer_noiseMapBuilder1",
            "seamless": true,
            "source": "Max_Module_40"
        },
        {
            "dest": "EndLayer_heightMap",
            "name": "EndLayer_noiseMapBuilder1",
            "seamless": true,
            "source": "Turbulence_Module_70"
        },
        {
            "dest": "heightMap_Pole",
            "name": "hmbPole",
            "seamless": false,
            "source": "Turbulence_Module_100"
        }
    ],
    "heightMaps": [
        "BeginLayer_heightMap",
        "EndLayer_heightMap",
        "heightMap_Pole"
    ],
    "images": [
        "BaseImage"
    ],
    "modules": [
        {
            "enableRandom": false,
            "name": "Avg_Module_30",
            "src1": "EndLayer_Module",
            "src2": "Curve_Module_20",
            "type": "Avg"
        },
        {
            "disp": 0,
            "enableDist": true,
            "enableRandom": true,
            "freq": 3.1,
            "name": "BeginLayer_Module",
            "seed": 0,
            "type": "Voronoi"
        },
        {
            "cpoints": [
                [
                    -1,
                    -1
                ],
                [
                    -0.2,
                    -0.2
                ],
                [
                    0,
                    0.5
                ],
                [
                    0.2,
                    -0.2
                ],
                [
                    1,
                    -1
                ]
            ],
            "enableRandom": false,
            "name": "Curve_Module_20",
            "src1": "BeginLayer_Module",
            "type": "Curve"
        },
        {
            "enableRandom": false,
            "freq": 0.5,
            "name": "Cylinders_Module_90",
            "type": "Cylinders"
        },
        {
            "enableRandom": true,
            "freq": 4.3,
            "lac": 2.70822,
            "name": "EndLayer_Module",
            "oct": 10,
            "seed": 0,
            "type": "RidgedMulti"
        },
        {
            "enableRandom": true,
            "name": "Max_Module_40",
            "src1": "Turbulence_Module_50",
            "src2": "EndLayer_Module",
            "type": "Max"
        },
        {
            "enableRandom": true,
            "freq": 1.9,
            "lac": 2.6,
            "name": "Perlin_Module_60",
            "oct": 10,
            "pers": 0.38,
            "seed": 0,
            "type": "Perlin"
        },
        {
            "enableRandom": true,
            "freq": 2.5,
            "lac": 3.2,
            "name": "Perlin_Module_80",
            "oct": 6,
            "pers": 0.3,
            "seed": 0,
            "type": "Perlin"
        },
        {
            "enableRandom": true,
            "freq": 1.85,
            "name": "Turbulence_Module_100",
            "pow": 0.23,
            "rough": 2.5,
            "seed": 0,
            "src1": "Cylinders_Module_90",
            "type": "Turbulence"
        },
        {
            "enableRandom": true,
            "freq": 5.3,
            "name": "Turbulence_Module_50",
            "pow": 0.2,
            "rough": 1.3,
            "seed": 0,
            "src1": "Avg_Module_30",
            "type": "Turbulence"
        },
        {
            "enableRandom": true,
            "freq": 1.4,
            "name": "Turbulence_Module_70",
            "pow": 0.1,
            "rough": 1.1,
            "seed": 0,
            "src1": "Perlin_Module_60",
            "type": "Turbulence"
        }
    ],
    "randomFactors": [
        0,
        0.1,
        0.2,
        0.3,
        0.1,
        0.3,
        0.2,
        0.1
    ],
    "reflectionMap": "",
    "renderers": [
        {
            "backgroundImage": "",
            "destImage": "BaseImage",
            "enabledLight": true,
            "gradientInfo": [
                [
                    -1,
                    237,
                    204,
                    175,
                    255
                ],
                [
                    -0.95356,
                    252,
                    255,
                    178,
                    255
                ],
                [
                    -0.85356,
                    169,
                    209,
                    164,
                    255
                ],
                [
                    -0.6345381526104418,
                    185,
                    242,
                    179,
                    255
                ],
                [
                    -0.52572,
                    241,
                    240,
                    172,
                    255
                ],
                [
                    -0.37427999999999995,
                    241,
                    240,
                    172,
                    255
                ],
                [
                    -0.2570281124497992,
                    213,
                    247,
                    216,
                    255
                ],
                [
                    0.07475999999999994,
                    219,
                    247,
                    213,
                    255
                ],
                [
                    0.17476000000000003,
                    255,
                    228,
                    133,
                    255
                ],
                [
                    0.28191999999999995,
                    214,
                    196,
                    143,
                    255
                ],
                [
                    0.38192000000000004,
                    251,
                    228,
                    171,
                    255
                ],
                [
                    0.61984,
                    251,
                    228,
                    171,
                    255
                ],
                [
                    0.71984,
                    253,
                    233,
                    178,
                    255
                ],
                [
                    1,
                    237,
                    240,
                    183,
                    255
                ]
            ],
            "heightMap": "BeginLayer_heightMap",
            "lightBrightness": 2,
            "lightContrast": 1.4,
            "name": "BeginLayer_renderer",
            "randomGradient": true
        },
        {
            "backgroundImage": "BaseImage",
            "destImage": "BaseImage",
            "enabledLight": false,
            "gradientInfo": [
                [
                    -1,
                    15,
                    46,
                    118,
                    255
                ],
                [
                    -0.04417670682730923,
                    14,
                    166,
                    185,
                    255
                ],
                [
                    0.24899598393574296,
                    26,
                    162,
                    222,
                    255
                ],
                [
                    0.2610441767068272,
                    212,
                    210,
                    121,
                    255
                ],
                [
                    0.32530120481927716,
                    63,
                    147,
                    66,
                    255
                ],
                [
                    0.6907630522088353,
                    47,
                    199,
                    47,
                    0
                ],
                [
                    0.7116465863453815,
                    77,
                    187,
                    55,
                    0
                ],
                [
                    0.8016465863453816,
                    77,
                    187,
                    55,
                    0
                ],
                [
                    0.8618072289156627,
                    184,
                    172,
                    57,
                    95
                ],
                [
                    0.9159036144578312,
                    184,
                    172,
                    57,
                    128
                ],
                [
                    1,
                    184,
                    172,
                    57,
                    64
                ]
            ],
            "heightMap": "EndLayer_heightMap",
            "lightBrightness": 1,
            "lightContrast": 1,
            "name": "renderer0002",
            "randomGradient": false
        },
        {
            "backgroundImage": "BaseImage",
            "destImage": "BaseImage",
            "enabledLight": false,
            "gradientInfo": [
                [
                    -1,
                    0,
                    0,
                    0,
                    0
                ],
                [
                    0.5100401606425702,
                    238,
                    238,
                    238,
                    0
                ],
                [
                    0.5341365461847389,
                    238,
                    244,
                    255,
                    255
                ],
                [
                    1,
                    238,
                    246,
                    255,
                    255
                ]
            ],
            "heightMap": "heightMap_Pole",
            "lightBrightness": 1,
            "lightContrast": 1,
            "name": "renderer0003",
            "randomGradient": false
        }
    ],
    "size": [
        1280,
        640
    ]
}

)TX";




static std::string MULTILAYERED_EARTHLIKERND = R"TX(
    {
    "bounds": [
        -90,
        90,
        -180,
        180
    ],
    "builderType": "sphere",
    "bumpMap": "image1",
    "cloudMap": "imageBump",
    "colorMap": "ImageSea",
    "heightMapBuilders": [
        {
            "dest": "heightMapPole",
            "name": "heightMapBuilderPole",
            "seamless": true,
            "source": "turboPoles"
        },
        {
            "dest": "heightMapDesert",
            "name": "noiseMapBDesert",
            "seamless": false,
            "source": "ModuleDesert"
        },
        {
            "dest": "heightMap",
            "name": "noiseMapBuilder1",
            "seamless": false,
            "source": "Module1"
        },
        {
            "dest": "HeightMapSea",
            "name": "noiseMapSea",
            "seamless": false,
            "source": "ModuleSea"
        }
    ],
    "heightMaps": [
        "HeightMapSea",
        "heightMap",
        "heightMapDesert",
        "heightMapPole"
    ],
    "images": [
        "ImageSea",
        "image1",
        "imageBump"
    ],
    "modules": [
        {
            "enableRandom": true,
            "freq": 4.2,
            "lac": 2.2,
            "name": "Module1",
            "oct": 6,
            "pers": 0.24,
            "seed": 0,
            "type": "Perlin"
        },
        {
            "enableRandom": true,
            "freq": 4.5,
            "lac": 4.3,
            "name": "ModuleDesert",
            "oct": 6,
            "pers": 0.35,
            "seed": 0,
            "type": "Billow"
        },
        {
            "enableRandom": true,
            "freq": 1.8,
            "lac": 5.3,
            "name": "ModuleSea",
            "oct": 6,
            "pers": 0.23,
            "seed": 0,
            "type": "Perlin"
        },
        {
            "enableRandom": true,
            "freq": 2.5,
            "name": "TurboDesert",
            "pow": 0.55,
            "rough": 1.2,
            "seed": 0,
            "src1": "ModuleDesert",
            "type": "Turbulence"
        },
        {
            "enableRandom": false,
            "freq": 0.5,
            "name": "cylinderPole",
            "type": "Cylinders"
        },
        {
            "enableRandom": true,
            "freq": 4.5,
            "name": "turboPoles",
            "pow": 0.2,
            "rough": 3.2,
            "seed": 0,
            "src1": "cylinderPole",
            "type": "Turbulence"
        }
    ],
    "randomFactors": [
        0
    ],
    "reflectionMap": "imageMountains",
    "renderers": [
        {
            "backgroundImage": "",
            "destImage": "image1",
            "enabledLight": true,
            "gradientInfo": [
                [
                    -1,
                    147,
                    46,
                    17,
                    255
                ],
                [
                    -0.68995,
                    140,
                    31,
                    15,
                    255
                ],
                [
                    -0.411025,
                    117,
                    26,
                    7,
                    255
                ],
                [
                    -0.17184999999999997,
                    123,
                    20,
                    9,
                    255
                ],
                [
                    -0.02519999999999997,
                    104,
                    35,
                    10,
                    255
                ],
                [
                    0.15407500000000005,
                    103,
                    10,
                    5,
                    255
                ],
                [
                    0.28520000000000006,
                    85,
                    26,
                    10,
                    255
                ],
                [
                    0.54115,
                    78,
                    12,
                    10,
                    255
                ],
                [
                    0.6923250000000001,
                    105,
                    18,
                    6,
                    255
                ],
                [
                    0.9071250000000001,
                    112,
                    22,
                    15,
                    255
                ],
                [
                    1,
                    108,
                    40,
                    13,
                    255
                ]
            ],
            "heightMap": "heightMap",
            "lightBrightness": 2,
            "lightContrast": 1.5,
            "name": "renderer1",
            "randomFactor": [
                70,
                2,
                2
            ],
            "randomGradient": true
        },
        {
            "backgroundImage": "image1",
            "destImage": "image1",
            "enabledLight": true,
            "gradientInfo": [
                [
                    -1,
                    251,
                    247,
                    244,
                    0
                ],
                [
                    -0.7991967871485943,
                    7,
                    0,
                    0,
                    0
                ],
                [
                    -0.028112449799196804,
                    208,
                    209,
                    95,
                    65
                ],
                [
                    1,
                    232,
                    212,
                    117,
                    135
                ]
            ],
            "heightMap": "heightMapDesert",
            "lightBrightness": 2,
            "lightContrast": 1.4,
            "name": "renderer2",
            "randomGradient": false
        },
        {
            "backgroundImage": "image1",
            "destImage": "ImageSea",
            "enabledLight": false,
            "gradientInfo": [
                [
                    -1,
                    0,
                    31,
                    174,
                    255
                ],
                [
                    -0.5903614457831325,
                    0,
                    14,
                    239,
                    255
                ],
                [
                    -0.4417670682730924,
                    0,
                    80,
                    235,
                    255
                ],
                [
                    -0.22489959839357432,
                    85,
                    226,
                    234,
                    255
                ],
                [
                    -0.11646586345381527,
                    109,
                    185,
                    114,
                    255
                ],
                [
                    -0.04016064257028118,
                    120,
                    166,
                    59,
                    255
                ],
                [
                    0.052208835341365445,
                    214,
                    176,
                    146,
                    0
                ],
                [
                    0.5060240963855422,
                    127,
                    207,
                    251,
                    0
                ],
                [
                    0.9036144578313252,
                    129,
                    167,
                    59,
                    255
                ],
                [
                    0.9397590361445782,
                    117,
                    225,
                    251,
                    255
                ],
                [
                    1,
                    0,
                    203,
                    224,
                    255
                ]
            ],
            "heightMap": "HeightMapSea",
            "lightBrightness": 1,
            "lightContrast": 1,
            "name": "renderer3",
            "randomGradient": false
        },
        {
            "backgroundImage": "ImageSea",
            "destImage": "ImageSea",
            "enabledLight": false,
            "gradientInfo": [
                [
                    -1,
                    0,
                    0,
                    0,
                    0
                ],
                [
                    0.5943775100401607,
                    254,
                    251,
                    248,
                    0
                ],
                [
                    0.6265060240963856,
                    255,
                    253,
                    251,
                    255
                ],
                [
                    1,
                    255,
                    254,
                    252,
                    255
                ]
            ],
            "heightMap": "heightMapPole",
            "lightBrightness": 1,
            "lightContrast": 1,
            "name": "renderer4",
            "randomGradient": false
        },
        {
            "backgroundImage": "",
            "destImage": "imageBump",
            "enabledLight": false,
            "gradientInfo": [
                [
                    -1,
                    255,
                    255,
                    255,
                    255
                ],
                [
                    0.167,
                    255,
                    255,
                    255,
                    255
                ],
                [
                    0.168,
                    0,
                    0,
                    0,
                    255
                ],
                [
                    0.91,
                    0,
                    0,
                    0,
                    255
                ],
                [
                    0.911,
                    255,
                    255,
                    255,
                    255
                ],
                [
                    1,
                    255,
                    255,
                    255,
                    255
                ]
            ],
            "heightMap": "HeightMapSea",
            "lightBrightness": 1,
            "lightContrast": 1,
            "name": "renderer5",
            "randomGradient": false
        }
    ],
    "size": [
        1280,
        640
    ]
}

)TX";




static std::string EARTHLIKE_01_1 = R"TX(
    {
    "bounds": [
        -90,
        90,
        -180,
        180
    ],
    "builderType": "sphere",
    "bumpMap": "imgNormal",
    "cloudMap": "imgSpecular",
    "colorMap": "imgColorMap",
    "heightMapBuilders": [
        {
            "dest": "destMap",
            "name": "builder",
            "seamless": true,
            "source": "Avg_Module"
        },
        {
            "dest": "destMap2",
            "name": "builder2",
            "seamless": true,
            "source": "displacer"
        }
    ],
    "heightMaps": [
        "destMap",
        "destMap2"
    ],
    "images": [
        "imgColorMap",
        "imgNormal",
        "imgSpecular"
    ],
    "modules": [
        {
            "enableRandom": false,
            "name": "Avg_Module",
            "src1": "Module2",
            "src2": "displacer",
            "type": "Avg"
        },
        {
            "enableRandom": false,
            "freq": 1.2,
            "lac": 5.7,
            "name": "Module2",
            "oct": 4,
            "pers": 0.22,
            "seed": 0,
            "type": "Perlin"
        },
        {
            "name": "ModuleScale",
            "type": "Scale"
        },
        {
            "name": "c3",
            "type": "Cylinder"
        },
        {
            "ctl": "m1",
            "enableRandom": false,
            "name": "displacer",
            "src1": "Module2",
            "src2": "m1",
            "src3": "c3",
            "src4": "m3",
            "type": "Displace"
        },
        {
            "enableRandom": false,
            "freq": 1.35,
            "lac": 3.7,
            "name": "m1",
            "oct": 3,
            "pers": 0.35,
            "seed": 0,
            "type": "Billow"
        },
        {
            "enableRandom": false,
            "freq": 1.35,
            "lac": 3.7,
            "name": "m2",
            "oct": 4,
            "seed": 0,
            "type": "RidgedMulti"
        },
        {
            "disp": 0,
            "enableDist": true,
            "enableRandom": false,
            "freq": 1.35,
            "name": "m3",
            "seed": 0,
            "type": "Voronoi"
        }
    ],
    "randomFactors": [
        0
    ],
    "reflectionMap": "imgSpecular",
    "renderers": [
        {
            "alphaImage": "",
            "backgroundImage": "",
            "bumpMap": "",
            "destImage": "imgColorMap",
            "enabledLight": false,
            "gradientInfo": [
                [
                    -1,
                    201,
                    206,
                    145,
                    255
                ],
                [
                    -0.4972477064220183,
                    255,
                    216,
                    150,
                    255
                ],
                [
                    -0.291743119266055,
                    205,
                    205,
                    122,
                    255
                ],
                [
                    0.5302752293577981,
                    112,
                    177,
                    54,
                    255
                ],
                [
                    0.95,
                    96,
                    46,
                    0,
                    255
                ],
                [
                    1,
                    224,
                    248,
                    255,
                    255
                ]
            ],
            "heightMap": "destMap",
            "lightBrightness": 2,
            "lightContrast": 3.1,
            "name": "renderer9",
            "randomGradient": false
        },
        {
            "alphaImage": "",
            "backgroundImage": "imgColorMap",
            "bumpMap": "",
            "destImage": "imgColorMap",
            "enabledLight": false,
            "gradientInfo": [
                [
                    -1,
                    0,
                    0,
                    96,
                    255
                ],
                [
                    -0.6,
                    0,
                    64,
                    192,
                    255
                ],
                [
                    0.15321100917431188,
                    0,
                    128,
                    255,
                    255
                ],
                [
                    0.1699082568807339,
                    255,
                    216,
                    150,
                    255
                ],
                [
                    0.2550458715596331,
                    205,
                    205,
                    122,
                    225
                ],
                [
                    0.4715596330275229,
                    112,
                    177,
                    54,
                    0
                ],
                [
                    0.9192660550458716,
                    96,
                    46,
                    0,
                    0
                ],
                [
                    1,
                    224,
                    248,
                    255,
                    255
                ]
            ],
            "heightMap": "destMap",
            "lightBrightness": 2,
            "lightContrast": 1.2,
            "name": "renderer10",
            "randomGradient": false
        },
        {
            "alphaImage": "",
            "backgroundImage": "",
            "bumpMap": "",
            "destImage": "imgNormal",
            "enabledLight": false,
            "gradientInfo": [
                [
                    -1,
                    0,
                    0,
                    0,
                    255
                ],
                [
                    0.15321100917431188,
                    0,
                    0,
                    0,
                    255
                ],
                [
                    1,
                    255,
                    248,
                    255,
                    255
                ]
            ],
            "heightMap": "destMap",
            "lightBrightness": 2,
            "lightContrast": 1.2,
            "name": "rendererNormal",
            "randomGradient": false
        },
        {
            "alphaImage": "",
            "backgroundImage": "",
            "bumpMap": "",
            "destImage": "imgSpecular",
            "enabledLight": false,
            "gradientInfo": [
                [
                    -1,
                    255,
                    255,
                    255,
                    255
                ],
                [
                    0.15321100917431188,
                    255,
                    255,
                    255,
                    255
                ],
                [
                    0.16321100917431186,
                    0,
                    0,
                    0,
                    255
                ],
                [
                    1,
                    0,
                    0,
                    0,
                    255
                ]
            ],
            "heightMap": "destMap",
            "lightBrightness": 2,
            "lightContrast": 1.2,
            "name": "rendererSpecular",
            "randomGradient": false
        }
    ],
    "size": [
        1600,
        800
    ]
}

)TX";




static std::string EARTHLIKE_02 = R"TX(
    {
    "bounds": [
        -90,
        90,
        -180,
        180
    ],
    "builderType": "sphere",
    "bumpMap": "normal",
    "cloudMap": "specular",
    "colorMap": "image1",
    "heightMapBuilders": [
        {
            "dest": "destMap",
            "name": "builder",
            "seamless": true,
            "source": "Module2SB"
        },
        {
            "dest": "destMap2",
            "name": "builder2",
            "seamless": true,
            "source": "clampSB"
        },
        {
            "dest": "destMap3",
            "name": "builder3",
            "seamless": true,
            "source": "Module2ExpSB"
        },
        {
            "dest": "destMap4",
            "name": "builder4",
            "seamless": true,
            "source": "PolesTurbo"
        }
    ],
    "heightMaps": [
        "destMap",
        "destMap2",
        "destMap3",
        "destMap4"
    ],
    "images": [
        "image1",
        "normal",
        "specular"
    ],
    "modules": [
        {
            "bias": -0.1,
            "enableRandom": false,
            "name": "Ctl",
            "scale": 0.9,
            "src1": "CtlPerlin",
            "type": "ScaleBias"
        },
        {
            "enableRandom": false,
            "freq": 1.1,
            "lac": 5.7,
            "name": "CtlPerlin",
            "oct": 5,
            "pers": 0.35,
            "seed": 0,
            "type": "Perlin"
        },
        {
            "enableRandom": false,
            "freq": 0.1,
            "lac": 3.7,
            "name": "CtlPerlin2",
            "oct": 5,
            "pers": 0.25,
            "seed": 0,
            "type": "Perlin"
        },
        {
            "enableRandom": false,
            "freq": 1.2,
            "name": "Module2",
            "pow": 0.15,
            "rough": 1.3,
            "seed": 0,
            "src1": "ModulePerlin",
            "type": "Turbulence"
        },
        {
            "enableRandom": false,
            "exp": 1.15,
            "name": "Module2Exp",
            "src1": "Module2",
            "type": "Exponent"
        },
        {
            "bias": -0.15,
            "enableRandom": false,
            "name": "Module2ExpSB",
            "scale": -1,
            "src1": "Module2Exp",
            "type": "ScaleBias"
        },
        {
            "bias": -0.15,
            "enableRandom": false,
            "name": "Module2SB",
            "scale": -1,
            "src1": "Module2",
            "type": "ScaleBias"
        },
        {
            "enableRandom": false,
            "freq": 1.75,
            "lac": 2.3,
            "name": "ModulePerlin",
            "oct": 6,
            "pers": 0.45,
            "seed": 0,
            "type": "Perlin"
        },
        {
            "enableRandom": false,
            "freq": 3,
            "name": "PolesTurbo",
            "pow": 0.25,
            "rough": 6,
            "seed": 0,
            "src1": "Poli",
            "type": "Turbulence"
        },
        {
            "enableRandom": false,
            "freq": 0.5,
            "name": "Poli",
            "type": "Cylinders"
        },
        {
            "enableRandom": false,
            "lbound": 0,
            "name": "clamp",
            "src1": "sel",
            "type": "Clamp",
            "ubound": 1
        },
        {
            "enableRandom": false,
            "lbound": 0.1,
            "name": "clamp2",
            "src1": "sel2",
            "type": "Clamp",
            "ubound": 1
        },
        {
            "bias": -0.15,
            "enableRandom": false,
            "name": "clampSB",
            "scale": -1,
            "src1": "clamp",
            "type": "ScaleBias"
        },
        {
            "enableRandom": false,
            "name": "const",
            "type": "Const",
            "value": 0
        },
        {
            "ctl": "Ctl",
            "enableRandom": false,
            "lbound": 0.12,
            "name": "sel",
            "src1": "const",
            "src2": "Module2",
            "type": "Select",
            "ubound": 0.7,
            "value": 1
        },
        {
            "ctl": "CtlPerlin2",
            "enableRandom": false,
            "lbound": 0.4,
            "name": "sel2",
            "src1": "const",
            "src2": "Module2",
            "type": "Select",
            "ubound": 0.7,
            "value": 1
        }
    ],
    "randomFactors": [
        0
    ],
    "reflectionMap": "specular",
    "renderers": [
        {
            "alphaImage": "",
            "backgroundImage": "",
            "bumpMap": "",
            "destImage": "image1",
            "enabledLight": true,
            "gradientInfo": [
                [
                    -1,
                    17,
                    44,
                    102,
                    255
                ],
                [
                    -0.6,
                    33,
                    82,
                    112,
                    255
                ],
                [
                    0.1100000000000001,
                    92,
                    172,
                    183,
                    255
                ],
                [
                    0.1200000000000001,
                    92,
                    103,
                    38,
                    255
                ],
                [
                    0.55,
                    64,
                    71,
                    22,
                    255
                ],
                [
                    0.95,
                    68,
                    107,
                    43,
                    255
                ],
                [
                    1,
                    62,
                    73,
                    25,
                    255
                ]
            ],
            "heightMap": "destMap",
            "lightBrightness": 2.1,
            "lightContrast": 0.01,
            "name": "renderer1",
            "randomFactor": [
                1,
                1,
                1
            ],
            "randomGradient": false
        },
        {
            "alphaImage": "",
            "backgroundImage": "image1",
            "bumpMap": "",
            "destImage": "image1",
            "enabledLight": true,
            "gradientInfo": [
                [
                    -1,
                    46,
                    162,
                    106,
                    0
                ],
                [
                    -0.6,
                    37,
                    147,
                    181,
                    0
                ],
                [
                    -0.14,
                    70,
                    84,
                    173,
                    0
                ],
                [
                    0.1,
                    63,
                    56,
                    54,
                    0
                ],
                [
                    0.11,
                    22,
                    65,
                    37,
                    128
                ],
                [
                    0.22,
                    207,
                    205,
                    204,
                    255
                ],
                [
                    0.5,
                    126,
                    132,
                    120,
                    128
                ],
                [
                    0.71,
                    89,
                    81,
                    75,
                    0
                ],
                [
                    1,
                    141,
                    215,
                    151,
                    0
                ]
            ],
            "heightMap": "destMap2",
            "lightBrightness": 2,
            "lightContrast": 0.01,
            "name": "renderer2",
            "randomFactor": [
                20,
                40,
                30
            ],
            "randomGradient": false
        },
        {
            "alphaImage": "",
            "backgroundImage": "image1",
            "bumpMap": "",
            "destImage": "image1",
            "enabledLight": true,
            "gradientInfo": [
                [
                    -1,
                    0,
                    0,
                    0,
                    0
                ],
                [
                    0.4,
                    79,
                    149,
                    36,
                    0
                ],
                [
                    0.47,
                    179,
                    0,
                    36,
                    0
                ],
                [
                    0.33,
                    79,
                    99,
                    36,
                    0
                ],
                [
                    0.7,
                    79,
                    99,
                    36,
                    192
                ],
                [
                    0.93,
                    224,
                    248,
                    255,
                    128
                ],
                [
                    1,
                    224,
                    248,
                    255,
                    255
                ]
            ],
            "heightMap": "destMap3",
            "lightBrightness": 2.1,
            "lightContrast": 0.01,
            "name": "renderer3",
            "randomGradient": false
        },
        {
            "alphaImage": "",
            "backgroundImage": "image1",
            "bumpMap": "",
            "destImage": "image1",
            "enabledLight": true,
            "gradientInfo": [
                [
                    -1,
                    0,
                    0,
                    0,
                    0
                ],
                [
                    0.29,
                    224,
                    248,
                    255,
                    0
                ],
                [
                    0.3,
                    224,
                    248,
                    255,
                    255
                ],
                [
                    1,
                    224,
                    248,
                    255,
                    255
                ]
            ],
            "heightMap": "destMap4",
            "lightBrightness": 2.4,
            "lightContrast": 0.41,
            "name": "renderer4",
            "randomGradient": false
        },
        {
            "alphaImage": "",
            "backgroundImage": "",
            "bumpMap": "",
            "destImage": "specular",
            "enabledLight": false,
            "gradientInfo": [
                [
                    -1,
                    253,
                    252,
                    251,
                    255
                ],
                [
                    0.071559633027523,
                    253,
                    251,
                    249,
                    255
                ],
                [
                    0.10825688073394502,
                    1,
                    1,
                    1,
                    255
                ],
                [
                    0.1200000000000001,
                    1,
                    0,
                    0,
                    255
                ],
                [
                    0.55,
                    0,
                    0,
                    0,
                    255
                ],
                [
                    1,
                    1,
                    0,
                    0,
                    255
                ]
            ],
            "heightMap": "destMap",
            "lightBrightness": 2.1,
            "lightContrast": 0.01,
            "name": "rendererSpecular",
            "randomGradient": false
        },
        {
            "alphaImage": "",
            "backgroundImage": "",
            "bumpMap": "",
            "destImage": "normal",
            "enabledLight": false,
            "gradientInfo": [
                [
                    -1,
                    0,
                    0,
                    0,
                    255
                ],
                [
                    0.11926605504587151,
                    3,
                    0,
                    0,
                    255
                ],
                [
                    1,
                    252,
                    250,
                    247,
                    255
                ]
            ],
            "heightMap": "destMap",
            "lightBrightness": 2.1,
            "lightContrast": 0.01,
            "name": "rendererNormal",
            "randomGradient": false
        }
    ],
    "size": [
        1280,
        640
    ]
}

)TX";




static std::string GASGIANT5 = R"TX(
    {
    "bounds": [
        -90,
        90,
        -180,
        180
    ],
    "builderType": "sphere",
    "bumpMap": "",
    "cloudMap": "",
    "colorMap": "BaseImage",
    "heightMapBuilders": [
        {
            "dest": "BeginLayer_heightMap",
            "name": "BeginLayer_noiseMapBuilder1",
            "seamless": true,
            "source": "Curve_Module_30"
        },
        {
            "dest": "EndLayer_heightMap",
            "name": "EndLayer_noiseMapBuilder1",
            "seamless": true,
            "source": "EndLayer_Module"
        }
    ],
    "heightMaps": [
        "BeginLayer_heightMap",
        "EndLayer_heightMap"
    ],
    "images": [
        "BaseImage"
    ],
    "modules": [
        {
            "enableRandom": true,
            "freq": 1.25,
            "name": "BeginLayer_Module",
            "type": "Cylinders"
        },
        {
            "cpoints": [
                [
                    -1,
                    0.8
                ],
                [
                    -0.5,
                    0.2
                ],
                [
                    0,
                    0.5
                ],
                [
                    0.5,
                    -0.2
                ],
                [
                    1,
                    -0.8
                ]
            ],
            "enableRandom": false,
            "name": "Curve_Module_30",
            "src1": "Turboz",
            "type": "Curve"
        },
        {
            "disp": 0,
            "enableDist": true,
            "enableRandom": true,
            "freq": 5.2844999999999995,
            "name": "EndLayer_Module",
            "seed": 0,
            "type": "Voronoi"
        },
        {
            "enableRandom": true,
            "freq": 1.9,
            "name": "Turboz",
            "pow": 0.12,
            "rough": 1.25,
            "seed": 0,
            "src1": "BeginLayer_Module",
            "type": "Turbulence"
        }
    ],
    "randomFactors": [
        0,
        0.1,
        0.2,
        0.3,
        0.2,
        0.1,
        0.09,
        0.1,
        0.8
    ],
    "reflectionMap": "",
    "renderers": [
        {
            "backgroundImage": "",
            "destImage": "BaseImage",
            "enabledLight": false,
            "gradientInfo": [
                [
                    -1,
                    169,
                    108,
                    190,
                    255
                ],
                [
                    -0.8825,
                    134,
                    95,
                    179,
                    255
                ],
                [
                    -0.655175,
                    103,
                    90,
                    152,
                    255
                ],
                [
                    -0.50945,
                    136,
                    112,
                    176,
                    255
                ],
                [
                    -0.27877499999999994,
                    127,
                    102,
                    164,
                    255
                ],
                [
                    -0.023174999999999935,
                    127,
                    108,
                    180,
                    255
                ],
                [
                    0.13367500000000007,
                    107,
                    99,
                    161,
                    255
                ],
                [
                    0.30580000000000007,
                    101,
                    112,
                    177,
                    255
                ],
                [
                    0.46005000000000007,
                    117,
                    129,
                    192,
                    255
                ],
                [
                    0.6257750000000001,
                    129,
                    158,
                    204,
                    255
                ],
                [
                    0.7735000000000001,
                    119,
                    138,
                    189,
                    255
                ],
                [
                    1,
                    125,
                    149,
                    181,
                    255
                ]
            ],
            "heightMap": "BeginLayer_heightMap",
            "lightBrightness": 1,
            "lightContrast": 1,
            "name": "BeginLayer_renderer",
            "randomFactor": [
                3,
                2,
                2
            ],
            "randomGradient": true
        }
    ],
    "size": [
        1280,
        640
    ]
}

)TX";




static std::string DESERT2 = R"TX(
    {
    "bounds": [
        -90,
        90,
        -180,
        180
    ],
    "builderType": "sphere",
    "bumpMap": "",
    "cloudMap": "",
    "colorMap": "image1",
    "heightMapBuilders": [
        {
            "dest": "destMap",
            "name": "builder",
            "seamless": true,
            "source": "ScaleBias"
        },
        {
            "dest": "destMapH",
            "name": "builderh",
            "seamless": true,
            "source": "Scaled"
        },
        {
            "dest": "destMap2",
            "name": "builderv",
            "seamless": true,
            "source": "Max"
        }
    ],
    "heightMaps": [
        "destMap",
        "destMap2",
        "destMapH"
    ],
    "images": [
        "image1",
        "imageH"
    ],
    "modules": [
        {
            "ctl": "ModulePerlin",
            "enableRandom": false,
            "name": "Blender",
            "src1": "VoroTurbo",
            "src2": "ModuleRMF",
            "type": "Blend"
        },
        {
            "enableRandom": false,
            "name": "Invert",
            "src1": "Scaled",
            "type": "Invert"
        },
        {
            "enableRandom": false,
            "name": "Max",
            "src1": "ModulePerlin2",
            "src2": "ScaleBias",
            "type": "Max"
        },
        {
            "enableRandom": true,
            "freq": 0.65,
            "lac": 2.3,
            "name": "ModulePerlin",
            "oct": 6,
            "pers": 0.15,
            "seed": 0,
            "type": "Billow"
        },
        {
            "enableRandom": true,
            "freq": 1.65,
            "lac": 3.3,
            "name": "ModulePerlin2",
            "oct": 6,
            "pers": 0.35,
            "seed": 0,
            "type": "Perlin"
        },
        {
            "enableRandom": false,
            "freq": 1.65,
            "lac": 0.3,
            "name": "ModuleRMF",
            "oct": 6,
            "seed": 0,
            "type": "RidgedMulti"
        },
        {
            "enableDist": true,
            "enableRandom": true,
            "freq": 0.65,
            "name": "ModuleVoronoi",
            "seed": 0,
            "type": "Voronoi"
        },
        {
            "enableRandom": true,
            "freq": 2.5,
            "lac": 3.2,
            "name": "Module_100",
            "oct": 6,
            "pers": 0.3,
            "seed": 0,
            "type": "Perlin"
        },
        {
            "bias": 1,
            "enableRandom": true,
            "name": "ScaleBias",
            "scale": -0.65,
            "src1": "Scaled",
            "type": "ScaleBias"
        },
        {
            "enableRandom": false,
            "name": "Scaled",
            "src1": "Blender",
            "type": "ScalePoint",
            "x": 14,
            "y": 14,
            "z": 14
        },
        {
            "enableRandom": false,
            "freq": 2.5,
            "name": "VoroTurbo",
            "pow": 0.4,
            "rough": 1.3,
            "seed": 179442,
            "src1": "ModuleVoronoi",
            "type": "Turbulence"
        }
    ],
    "randomFactors": [
        0.3,
        0.4,
        0.2,
        0.2,
        0.2,
        0.5,
        0.8,
        0.2,
        1.1,
        1.4,
        2
    ],
    "reflectionMap": "",
    "renderers": [
        {
            "backgroundImage": "",
            "destImage": "image1",
            "enabledLight": true,
            "gradientInfo": [
                [
                    -0.4,
                    158,
                    35,
                    128,
                    255
                ],
                [
                    -0.30000000000000004,
                    138,
                    209,
                    108,
                    255
                ],
                [
                    -0.19999999999999996,
                    68,
                    29,
                    77,
                    255
                ],
                [
                    -0.09999999999999998,
                    116,
                    82,
                    42,
                    255
                ],
                [
                    0,
                    97,
                    168,
                    171,
                    255
                ],
                [
                    0.10000000000000009,
                    82,
                    138,
                    102,
                    255
                ],
                [
                    0.30000000000000004,
                    124,
                    89,
                    65,
                    255
                ],
                [
                    0.3999999999999999,
                    106,
                    132,
                    135,
                    255
                ]
            ],
            "heightMap": "destMap",
            "lightBrightness": 2.1,
            "lightContrast": 0.005,
            "name": "renderer10",
            "randomFactor": [
                120,
                100,
                100
            ],
            "randomGradient": false
        },
        {
            "backgroundImage": "image1",
            "destImage": "image1",
            "enabledLight": true,
            "gradientInfo": [
                [
                    -0.4,
                    211,
                    31,
                    21,
                    0
                ],
                [
                    -0.30000000000000004,
                    66,
                    116,
                    110,
                    0
                ],
                [
                    -0.19999999999999996,
                    109,
                    9,
                    80,
                    16
                ],
                [
                    -0.09999999999999998,
                    133,
                    54,
                    139,
                    24
                ],
                [
                    0,
                    99,
                    44,
                    142,
                    32
                ],
                [
                    0.10000000000000009,
                    249,
                    238,
                    149,
                    48
                ],
                [
                    0.30000000000000004,
                    131,
                    93,
                    101,
                    56
                ],
                [
                    0.3999999999999999,
                    78,
                    84,
                    107,
                    64
                ]
            ],
            "heightMap": "destMap2",
            "lightBrightness": 2.1,
            "lightContrast": 0.005,
            "name": "renderer11",
            "randomFactor": [
                170,
                140,
                140
            ],
            "randomGradient": false
        },
        {
            "backgroundImage": "",
            "destImage": "imageH",
            "enabledLight": false,
            "gradientInfo": [
                [
                    -1,
                    0,
                    0,
                    0,
                    255
                ],
                [
                    -0.9,
                    255,
                    255,
                    255,
                    255
                ],
                [
                    1,
                    255,
                    255,
                    255,
                    255
                ]
            ],
            "heightMap": "destMapH",
            "lightBrightness": 2.1,
            "lightContrast": 0.005,
            "name": "renderer12",
            "randomGradient": false
        }
    ],
    "size": [
        1280,
        640
    ]
}

)TX";




static std::string CLOUD = R"TX(
    {
    "bounds": [
        -90,
        90,
        -180,
        180
    ],
    "builderType": "sphere",
    "bumpMap": "img01ColorMap",
    "cloudMap": "",
    "colorMap": "img02CloudMap",
    "heightMapBuilders": [
        {
            "dest": "destMap",
            "name": "builder",
            "seamless": true,
            "source": "ModuleScale"
        },
        {
            "dest": "destMap2",
            "name": "builder2",
            "seamless": true,
            "source": "displacer"
        }
    ],
    "heightMaps": [
        "destMap",
        "destMap2"
    ],
    "images": [
        "img01ColorMap",
        "img02CloudMap"
    ],
    "modules": [
        {
            "enableRandom": false,
            "freq": 1.35,
            "lac": 3.7,
            "name": "Module2",
            "oct": 5,
            "pers": 0.25,
            "seed": 0,
            "type": "Perlin"
        },
        {
            "enableRandom": false,
            "name": "ModuleScale",
            "src1": "Module2",
            "type": "ScalePoint",
            "x": 0.9,
            "y": 1,
            "z": 1.1
        },
        {
            "name": "c3",
            "type": "Cylinder"
        },
        {
            "ctl": "c3",
            "enableRandom": false,
            "name": "displacer",
            "src1": "Module2",
            "src2": "m1",
            "src3": "m3",
            "src4": "m2",
            "type": "Displace"
        },
        {
            "enableRandom": false,
            "freq": 1.35,
            "lac": 3.7,
            "name": "m1",
            "oct": 5,
            "pers": 0.35,
            "seed": 0,
            "type": "Billow"
        },
        {
            "enableRandom": false,
            "freq": 1.35,
            "lac": 3.7,
            "name": "m2",
            "oct": 5,
            "seed": 0,
            "type": "RidgedMulti"
        },
        {
            "enableDist": true,
            "enableRandom": false,
            "freq": 1.35,
            "name": "m3",
            "seed": 0,
            "type": "Voronoi"
        }
    ],
    "randomFactors": [
        0
    ],
    "reflectionMap": "",
    "renderers": [
        {
            "backgroundImage": "",
            "destImage": "img01ColorMap",
            "enabledLight": true,
            "gradientInfo": [
                [
                    -1,
                    0,
                    0,
                    96,
                    255
                ],
                [
                    -0.6,
                    0,
                    64,
                    192,
                    255
                ],
                [
                    0.16,
                    0,
                    128,
                    255,
                    255
                ],
                [
                    0.17,
                    255,
                    160,
                    0,
                    255
                ],
                [
                    0.28,
                    0,
                    128,
                    0,
                    255
                ],
                [
                    0.95,
                    96,
                    46,
                    0,
                    255
                ],
                [
                    1,
                    224,
                    248,
                    255,
                    255
                ]
            ],
            "heightMap": "destMap",
            "lightBrightness": 2,
            "lightContrast": 3.5,
            "name": "renderer10",
            "randomGradient": false
        },
        {
            "backgroundImage": "",
            "destImage": "img02CloudMap",
            "enabledLight": true,
            "gradientInfo": [
                [
                    -1,
                    0,
                    0,
                    96,
                    0
                ],
                [
                    -0.95,
                    0,
                    0,
                    96,
                    0
                ],
                [
                    -0.75,
                    220,
                    220,
                    240,
                    192
                ],
                [
                    -0.55,
                    224,
                    248,
                    255,
                    128
                ],
                [
                    -0.25,
                    0,
                    64,
                    192,
                    0
                ],
                [
                    -0.11,
                    0,
                    128,
                    255,
                    0
                ],
                [
                    0.42,
                    220,
                    224,
                    224,
                    0
                ],
                [
                    0.65,
                    220,
                    220,
                    240,
                    224
                ],
                [
                    0.85,
                    224,
                    248,
                    255,
                    224
                ],
                [
                    1,
                    224,
                    248,
                    255,
                    223
                ]
            ],
            "heightMap": "destMap2",
            "lightBrightness": 2,
            "lightContrast": 0.01,
            "name": "renderer11",
            "randomGradient": false
        }
    ],
    "size": [
        1024,
        512
    ]
}

)TX";




static std::string GASGIANTOK = R"TX(
    {
    "bounds": [
        -90,
        90,
        -180,
        180
    ],
    "builderType": "sphere",
    "bumpMap": "",
    "cloudMap": "",
    "colorMap": "BaseImage",
    "heightMapBuilders": [
        {
            "dest": "destMap",
            "name": "builder",
            "seamless": true,
            "source": "Module2"
        },
        {
            "dest": "destMap2",
            "name": "builder2",
            "seamless": true,
            "source": "displacer"
        }
    ],
    "heightMaps": [
        "destMap",
        "destMap2"
    ],
    "images": [
        "BaseImage"
    ],
    "modules": [
        {
            "enableRandom": false,
            "freq": 1.35,
            "lac": 3.7,
            "name": "Module2",
            "oct": 5,
            "pers": 0.35,
            "seed": 0,
            "type": "Perlin"
        },
        {
            "ctl": "m3",
            "enableRandom": false,
            "name": "displacer",
            "src1": "Module2",
            "src2": "m3",
            "src3": "m1",
            "src4": "m2",
            "type": "Displace"
        },
        {
            "enableRandom": false,
            "freq": 1.35,
            "lac": 3.7,
            "name": "m1",
            "oct": 5,
            "pers": 0.35,
            "seed": 0,
            "type": "Billow"
        },
        {
            "enableRandom": false,
            "freq": 1.35,
            "lac": 3.7,
            "name": "m2",
            "oct": 5,
            "seed": 0,
            "type": "RidgedMulti"
        },
        {
            "enableDist": true,
            "enableRandom": false,
            "freq": 1.35,
            "name": "m3",
            "seed": 0,
            "type": "Voronoi"
        }
    ],
    "randomFactors": [
        0
    ],
    "reflectionMap": "",
    "renderers": [
        {
            "backgroundImage": "",
            "destImage": "BaseImage",
            "enabledLight": true,
            "gradientInfo": [
                [
                    -1,
                    118,
                    96,
                    40,
                    255
                ],
                [
                    -0.859075,
                    93,
                    66,
                    25,
                    255
                ],
                [
                    -0.620175,
                    115,
                    84,
                    26,
                    255
                ],
                [
                    -0.515775,
                    110,
                    66,
                    19,
                    255
                ],
                [
                    -0.24172499999999997,
                    119,
                    79,
                    27,
                    255
                ],
                [
                    0.005300000000000038,
                    129,
                    75,
                    27,
                    255
                ],
                [
                    0.21562500000000004,
                    111,
                    48,
                    20,
                    255
                ],
                [
                    0.562,
                    109,
                    48,
                    26,
                    255
                ],
                [
                    0.74055,
                    137,
                    74,
                    36,
                    255
                ],
                [
                    1,
                    142,
                    54,
                    37,
                    255
                ]
            ],
            "heightMap": "destMap2",
            "lightBrightness": 2,
            "lightContrast": 0.1,
            "name": "renderer11",
            "randomFactor": [
                1,
                1,
                1
            ],
            "randomGradient": true
        }
    ],
    "size": [
        1280,
        640
    ]
}

)TX";




static std::string ROCKBALL = R"TX(
    {
    "bounds": [
        -90,
        90,
        -180,
        180
    ],
    "builderType": "sphere",
    "bumpMap": "height",
    "cloudMap": "reflec",
    "colorMap": "image1",
    "heightMapBuilders": [
        {
            "dest": "destMap",
            "name": "builder",
            "seamless": true,
            "source": "ScAbs"
        }
    ],
    "heightMaps": [
        "destMap",
        "destMap2",
        "destMap3",
        "destMap4"
    ],
    "images": [
        "height",
        "image1",
        "reflec"
    ],
    "modules": [
        {
            "enableRandom": false,
            "name": "Abs",
            "src1": "CtlPerlin",
            "type": "Abs"
        },
        {
            "enableRandom": false,
            "freq": 3.1,
            "lac": 2.7,
            "name": "CtlPerlin",
            "oct": 5,
            "pers": 0.45,
            "seed": 0,
            "type": "Billow"
        },
        {
            "enableRandom": false,
            "freq": 0.1,
            "lac": 3.7,
            "name": "CtlPerlin2",
            "oct": 5,
            "pers": 0.25,
            "seed": 0,
            "type": "Perlin"
        },
        {
            "enableRandom": false,
            "freq": 0.3,
            "name": "CtlSphere",
            "type": "Spheres"
        },
        {
            "enableRandom": false,
            "freq": 1.2,
            "name": "Module2",
            "pow": 0.15,
            "rough": 1.3,
            "seed": 0,
            "src1": "ModulePerlin",
            "type": "Turbulence"
        },
        {
            "enableRandom": false,
            "freq": 1.65,
            "lac": 2.3,
            "name": "ModulePerlin",
            "oct": 6,
            "pers": 0.45,
            "seed": 0,
            "type": "Perlin"
        },
        {
            "bias": -1,
            "enableRandom": false,
            "name": "ScAbs",
            "scale": 2,
            "src1": "CtlPerlin",
            "type": "ScaleBias"
        },
        {
            "enableRandom": false,
            "name": "const",
            "type": "Const",
            "value": 0
        },
        {
            "ctl": "CtlSphere",
            "enableRandom": false,
            "name": "sel",
            "src1": "CtlSphere",
            "src2": "ModulePerlin",
            "type": "Blend"
        }
    ],
    "randomFactors": [
        0
    ],
    "reflectionMap": "reflec",
    "renderers": [
        {
            "alphaImage": "",
            "backgroundImage": "",
            "bumpMap": "",
            "destImage": "image1",
            "enabledLight": false,
            "gradientInfo": [
                [
                    -1,
                    140,
                    179,
                    194,
                    255
                ],
                [
                    -0.661625,
                    141,
                    177,
                    182,
                    255
                ],
                [
                    -0.4981,
                    161,
                    210,
                    211,
                    255
                ],
                [
                    -0.26635,
                    173,
                    221,
                    226,
                    255
                ],
                [
                    0.08257500000000007,
                    178,
                    214,
                    216,
                    255
                ],
                [
                    0.21735000000000015,
                    181,
                    202,
                    200,
                    255
                ],
                [
                    0.34604999999999997,
                    205,
                    219,
                    220,
                    255
                ],
                [
                    0.6529250000000002,
                    210,
                    222,
                    227,
                    255
                ],
                [
                    0.98865,
                    221,
                    222,
                    223,
                    255
                ],
                [
                    1,
                    189,
                    194,
                    206,
                    255
                ]
            ],
            "heightMap": "destMap",
            "lightBrightness": 2.1,
            "lightContrast": 1.2,
            "name": "renderer1",
            "randomGradient": false
        },
        {
            "alphaImage": "",
            "backgroundImage": "",
            "bumpMap": "",
            "destImage": "height",
            "enabledLight": false,
            "gradientInfo": [
                [
                    -1,
                    0,
                    0,
                    0,
                    255
                ],
                [
                    1,
                    255,
                    255,
                    255,
                    255
                ]
            ],
            "heightMap": "destMap",
            "lightBrightness": 1,
            "lightContrast": 1,
            "name": "rendererHeight",
            "randomGradient": false
        },
        {
            "alphaImage": "",
            "backgroundImage": "",
            "bumpMap": "",
            "destImage": "reflec",
            "enabledLight": false,
            "gradientInfo": [
                [
                    -1,
                    23,
                    23,
                    23,
                    255
                ],
                [
                    1,
                    134,
                    134,
                    134,
                    255
                ]
            ],
            "heightMap": "destMap",
            "lightBrightness": 1,
            "lightContrast": 1,
            "name": "renderReflec",
            "randomGradient": false
        }
    ],
    "size": [
        1024,
        512
    ]
}

)TX";




static std::string EARTHLIKE_04 = R"TX(
    {
    "bounds": [
        -90,
        90,
        -180,
        180
    ],
    "builderType": "sphere",
    "bumpMap": "normal",
    "cloudMap": "normal",
    "colorMap": "img01ColorMap",
    "heightMapBuilders": [
        {
            "dest": "destMap",
            "name": "builder",
            "seamless": true,
            "source": "ModuleScale"
        },
        {
            "dest": "destMap2",
            "name": "builder2",
            "seamless": true,
            "source": "displacer"
        }
    ],
    "heightMaps": [
        "destMap",
        "destMap2"
    ],
    "images": [
        "img01ColorMap",
        "img02CloudMap",
        "normal",
        "specular"
    ],
    "modules": [
        {
            "enableRandom": false,
            "freq": 1.35,
            "lac": 3.7,
            "name": "Module2",
            "oct": 5,
            "pers": 0.25,
            "seed": 0,
            "type": "Perlin"
        },
        {
            "enableRandom": false,
            "name": "ModuleScale",
            "src1": "Module2",
            "type": "ScalePoint",
            "x": 0.9,
            "y": 1,
            "z": 1.1
        },
        {
            "name": "c3",
            "type": "Cylinder"
        },
        {
            "ctl": "c3",
            "enableRandom": false,
            "name": "displacer",
            "src1": "Module2",
            "src2": "m1",
            "src3": "m3",
            "src4": "m2",
            "type": "Displace"
        },
        {
            "enableRandom": false,
            "freq": 1.35,
            "lac": 3.7,
            "name": "m1",
            "oct": 5,
            "pers": 0.35,
            "seed": 0,
            "type": "Billow"
        },
        {
            "enableRandom": false,
            "freq": 1.35,
            "lac": 3.7,
            "name": "m2",
            "oct": 5,
            "seed": 0,
            "type": "RidgedMulti"
        },
        {
            "disp": 0,
            "enableDist": true,
            "enableRandom": false,
            "freq": 1.35,
            "name": "m3",
            "seed": 0,
            "type": "Voronoi"
        }
    ],
    "randomFactors": [
        0
    ],
    "reflectionMap": "specular",
    "renderers": [
        {
            "alphaImage": "",
            "backgroundImage": "",
            "bumpMap": "",
            "destImage": "img01ColorMap",
            "enabledLight": false,
            "gradientInfo": [
                [
                    -1,
                    0,
                    0,
                    96,
                    255
                ],
                [
                    -0.6,
                    0,
                    64,
                    192,
                    255
                ],
                [
                    0.15999999999999992,
                    0,
                    128,
                    255,
                    255
                ],
                [
                    0.16999999999999993,
                    255,
                    160,
                    0,
                    255
                ],
                [
                    0.28,
                    0,
                    128,
                    0,
                    255
                ],
                [
                    0.95,
                    96,
                    46,
                    0,
                    255
                ],
                [
                    1,
                    224,
                    248,
                    255,
                    255
                ]
            ],
            "heightMap": "destMap",
            "lightBrightness": 2,
            "lightContrast": 0.5,
            "name": "renderer10",
            "randomGradient": false
        },
        {
            "alphaImage": "",
            "backgroundImage": "",
            "bumpMap": "",
            "destImage": "img02CloudMap",
            "enabledLight": true,
            "gradientInfo": [
                [
                    -1,
                    0,
                    0,
                    96,
                    0
                ],
                [
                    -0.95,
                    0,
                    0,
                    96,
                    0
                ],
                [
                    -0.75,
                    220,
                    220,
                    240,
                    192
                ],
                [
                    -0.55,
                    224,
                    248,
                    255,
                    128
                ],
                [
                    -0.25,
                    0,
                    64,
                    192,
                    0
                ],
                [
                    -0.10999999999999999,
                    0,
                    128,
                    255,
                    0
                ],
                [
                    0.41999999999999993,
                    220,
                    224,
                    224,
                    0
                ],
                [
                    0.6499999999999999,
                    220,
                    220,
                    240,
                    224
                ],
                [
                    0.8500000000000001,
                    224,
                    248,
                    255,
                    224
                ],
                [
                    1,
                    224,
                    248,
                    255,
                    223
                ]
            ],
            "heightMap": "destMap2",
            "lightBrightness": 2,
            "lightContrast": 0.01,
            "name": "renderer11",
            "randomGradient": false
        },
        {
            "alphaImage": "",
            "backgroundImage": "",
            "bumpMap": "",
            "destImage": "normal",
            "enabledLight": false,
            "gradientInfo": [
                [
                    -1,
                    0,
                    0,
                    0,
                    255
                ],
                [
                    0.16199999999999992,
                    0,
                    0,
                    0,
                    255
                ],
                [
                    1,
                    255,
                    255,
                    255,
                    255
                ]
            ],
            "heightMap": "destMap",
            "lightBrightness": 1,
            "lightContrast": 1,
            "name": "renderer0003",
            "randomGradient": false
        },
        {
            "alphaImage": "",
            "backgroundImage": "",
            "bumpMap": "",
            "destImage": "specular",
            "enabledLight": false,
            "gradientInfo": [
                [
                    -1,
                    255,
                    255,
                    255,
                    255
                ],
                [
                    0.15999999999999992,
                    255,
                    255,
                    255,
                    255
                ],
                [
                    0.2590673575129534,
                    0,
                    0,
                    0,
                    255
                ],
                [
                    1,
                    0,
                    0,
                    0,
                    255
                ]
            ],
            "heightMap": "destMap",
            "lightBrightness": 1,
            "lightContrast": 1,
            "name": "renderer0004",
            "randomGradient": false
        }
    ],
    "size": [
        1024,
        512
    ]
}

)TX";




static std::string EARTHLIKE_10 = R"TX(
    {
    "bounds": [
        -90,
        90,
        -180,
        180
    ],
    "builderType": "sphere",
    "bumpMap": "normal",
    "cloudMap": "",
    "colorMap": "BaseImage",
    "heightMapBuilders": [
        {
            "dest": "BeginLayer_heightMap",
            "name": "BeginLayer_noiseMapBuilder1",
            "seamless": true,
            "source": "Max_Module_40"
        },
        {
            "dest": "EndLayer_heightMap",
            "name": "EndLayer_noiseMapBuilder1",
            "seamless": true,
            "source": "Turbulence_Module_70"
        },
        {
            "dest": "heightMap_Pole",
            "name": "hmbPole",
            "seamless": false,
            "source": "Turbulence_Module_100"
        }
    ],
    "heightMaps": [
        "BeginLayer_heightMap",
        "EndLayer_heightMap",
        "heightMap_Pole"
    ],
    "images": [
        "BaseImage",
        "normal",
        "specular"
    ],
    "modules": [
        {
            "enableRandom": false,
            "name": "Avg_Module_30",
            "src1": "EndLayer_Module",
            "src2": "Curve_Module_20",
            "type": "Avg"
        },
        {
            "disp": 0,
            "enableDist": true,
            "enableRandom": true,
            "freq": 3.1,
            "name": "BeginLayer_Module",
            "seed": 0,
            "type": "Voronoi"
        },
        {
            "cpoints": [
                [
                    -1,
                    -1
                ],
                [
                    -0.2,
                    -0.2
                ],
                [
                    0,
                    0.5
                ],
                [
                    0.2,
                    -0.2
                ],
                [
                    1,
                    -1
                ]
            ],
            "enableRandom": false,
            "name": "Curve_Module_20",
            "src1": "BeginLayer_Module",
            "type": "Curve"
        },
        {
            "enableRandom": false,
            "freq": 0.5,
            "name": "Cylinders_Module_90",
            "type": "Cylinders"
        },
        {
            "enableRandom": true,
            "freq": 4.3,
            "lac": 2.70822,
            "name": "EndLayer_Module",
            "oct": 10,
            "seed": 0,
            "type": "RidgedMulti"
        },
        {
            "enableRandom": false,
            "name": "Max_Module_40",
            "src1": "Turbulence_Module_50",
            "src2": "EndLayer_Module",
            "type": "Max"
        },
        {
            "enableRandom": false,
            "freq": 1.9,
            "lac": 2.6,
            "name": "Perlin_Module_60",
            "oct": 10,
            "pers": 0.38,
            "seed": 0,
            "type": "Perlin"
        },
        {
            "enableRandom": false,
            "freq": 2.5,
            "lac": 3.2,
            "name": "Perlin_Module_80",
            "oct": 6,
            "pers": 0.3,
            "seed": 0,
            "type": "Perlin"
        },
        {
            "enableRandom": false,
            "freq": 1.85,
            "name": "Turbulence_Module_100",
            "pow": 0.23,
            "rough": 2.5,
            "seed": 0,
            "src1": "Cylinders_Module_90",
            "type": "Turbulence"
        },
        {
            "enableRandom": false,
            "freq": 5.3,
            "name": "Turbulence_Module_50",
            "pow": 0.2,
            "rough": 1.3,
            "seed": 0,
            "src1": "Avg_Module_30",
            "type": "Turbulence"
        },
        {
            "enableRandom": false,
            "freq": 1.4,
            "name": "Turbulence_Module_70",
            "pow": 0.1,
            "rough": 1.1,
            "seed": 0,
            "src1": "Perlin_Module_60",
            "type": "Turbulence"
        }
    ],
    "randomFactors": [
        0
    ],
    "reflectionMap": "specular",
    "renderers": [
        {
            "alphaImage": "",
            "backgroundImage": "",
            "bumpMap": "",
            "destImage": "BaseImage",
            "enabledLight": false,
            "gradientInfo": [
                [
                    -1,
                    237,
                    204,
                    175,
                    255
                ],
                [
                    -0.95356,
                    252,
                    255,
                    178,
                    255
                ],
                [
                    -0.85356,
                    169,
                    209,
                    164,
                    255
                ],
                [
                    -0.6345381526104418,
                    185,
                    242,
                    179,
                    255
                ],
                [
                    -0.52572,
                    241,
                    240,
                    172,
                    255
                ],
                [
                    -0.37427999999999995,
                    241,
                    240,
                    172,
                    255
                ],
                [
                    -0.2570281124497992,
                    213,
                    247,
                    216,
                    255
                ],
                [
                    0.07475999999999994,
                    219,
                    247,
                    213,
                    255
                ],
                [
                    0.17476000000000003,
                    255,
                    228,
                    133,
                    255
                ],
                [
                    0.28191999999999995,
                    214,
                    196,
                    143,
                    255
                ],
                [
                    0.38192000000000004,
                    251,
                    228,
                    171,
                    255
                ],
                [
                    0.61984,
                    251,
                    228,
                    171,
                    255
                ],
                [
                    0.71984,
                    253,
                    233,
                    178,
                    255
                ],
                [
                    1,
                    237,
                    240,
                    183,
                    255
                ]
            ],
            "heightMap": "BeginLayer_heightMap",
            "lightBrightness": 2,
            "lightContrast": 1.4,
            "name": "BeginLayer_renderer",
            "randomGradient": false
        },
        {
            "alphaImage": "",
            "backgroundImage": "BaseImage",
            "bumpMap": "",
            "destImage": "BaseImage",
            "enabledLight": false,
            "gradientInfo": [
                [
                    -1,
                    15,
                    46,
                    118,
                    255
                ],
                [
                    -0.012048192771084376,
                    26,
                    162,
                    222,
                    255
                ],
                [
                    0.028112449799196693,
                    14,
                    166,
                    185,
                    255
                ],
                [
                    0.04016064257028118,
                    212,
                    210,
                    121,
                    255
                ],
                [
                    0.15261044176706817,
                    63,
                    147,
                    66,
                    255
                ],
                [
                    0.5542168674698795,
                    47,
                    199,
                    47,
                    0
                ],
                [
                    0.9116465863453815,
                    77,
                    187,
                    55,
                    0
                ],
                [
                    0.9518072289156627,
                    184,
                    172,
                    57,
                    255
                ],
                [
                    0.9759036144578312,
                    49,
                    190,
                    255,
                    255
                ],
                [
                    1,
                    0,
                    200,
                    255,
                    255
                ]
            ],
            "heightMap": "EndLayer_heightMap",
            "lightBrightness": 1,
            "lightContrast": 1,
            "name": "renderer0002",
            "randomGradient": false
        },
        {
            "alphaImage": "",
            "backgroundImage": "BaseImage",
            "bumpMap": "",
            "destImage": "BaseImage",
            "enabledLight": false,
            "gradientInfo": [
                [
                    -1,
                    0,
                    0,
                    0,
                    0
                ],
                [
                    0.5100401606425702,
                    238,
                    238,
                    238,
                    0
                ],
                [
                    0.5341365461847389,
                    238,
                    244,
                    255,
                    255
                ],
                [
                    1,
                    238,
                    246,
                    255,
                    255
                ]
            ],
            "heightMap": "heightMap_Pole",
            "lightBrightness": 1,
            "lightContrast": 1,
            "name": "renderer0003",
            "randomGradient": false
        },
        {
            "alphaImage": "",
            "backgroundImage": "",
            "bumpMap": "",
            "destImage": "normal",
            "enabledLight": false,
            "gradientInfo": [
                [
                    -1,
                    0,
                    0,
                    0,
                    255
                ],
                [
                    -0.01200000000000001,
                    0,
                    0,
                    0,
                    255
                ],
                [
                    1,
                    255,
                    255,
                    255,
                    255
                ]
            ],
            "heightMap": "EndLayer_heightMap",
            "lightBrightness": 1,
            "lightContrast": 1,
            "name": "renderer0004",
            "randomGradient": false
        },
        {
            "alphaImage": "",
            "backgroundImage": "",
            "bumpMap": "",
            "destImage": "specular",
            "enabledLight": false,
            "gradientInfo": [
                [
                    -1,
                    255,
                    255,
                    255,
                    255
                ],
                [
                    -0.041450777202072575,
                    255,
                    255,
                    255,
                    255
                ],
                [
                    -0.01200000000000001,
                    0,
                    0,
                    0,
                    255
                ],
                [
                    1,
                    0,
                    0,
                    0,
                    255
                ]
            ],
            "heightMap": "EndLayer_heightMap",
            "lightBrightness": 1,
            "lightContrast": 1,
            "name": "renderer0005",
            "randomGradient": false
        }
    ],
    "size": [
        1280,
        640
    ]
}

)TX";





static std::string CHUNK = R"TX(
    {
    "bounds": [
        -90,
        90,
        -180,
        180
    ],
    "builderType": "sphere",
    "bumpMap": "",
    "cloudMap": "",
    "colorMap": "image1",
    "heightMapBuilders": [
        {
            "dest": "destMap",
            "name": "builder",
            "seamless": true,
            "source": "ScaleBiasTurbo"
        }
    ],
    "heightMaps": [
        "destMap"
    ],
    "images": [
        "image1"
    ],
    "modules": [
        {
            "ctl": "ModuleVoronoi",
            "enableRandom": false,
            "name": "Blender",
            "src1": "ModulePerlin",
            "src2": "ModuleRMF",
            "type": "Blend"
        },
        {
            "enableRandom": false,
            "freq": 0.65,
            "lac": 2.3,
            "name": "ModulePerlin",
            "oct": 6,
            "pers": 0.15,
            "seed": 0,
            "type": "Billow"
        },
        {
            "enableRandom": false,
            "freq": 1.65,
            "lac": 0.3,
            "name": "ModuleRMF",
            "oct": 6,
            "seed": 0,
            "type": "RidgedMulti"
        },
        {
            "enableDist": true,
            "enableRandom": false,
            "freq": 0.65,
            "name": "ModuleVoronoi",
            "seed": 0,
            "type": "Voronoi"
        },
        {
            "bias": 1,
            "enableRandom": false,
            "name": "ScaleBias",
            "scale": -0.65,
            "src1": "Scaled",
            "type": "ScaleBias"
        },
        {
            "enableRandom": false,
            "freq": 2.5,
            "name": "ScaleBiasTurbo",
            "pow": 0.12,
            "rough": 4,
            "seed": 0,
            "src1": "ScaleBias",
            "type": "Turbulence"
        },
        {
            "enableRandom": false,
            "name": "Scaled",
            "src1": "Blender",
            "type": "ScalePoint",
            "x": 14,
            "y": 14,
            "z": 14
        }
    ],
    "randomFactors": [
        0
    ],
    "reflectionMap": "",
    "renderers": [
        {
            "backgroundImage": "",
            "destImage": "image1",
            "enabledLight": true,
            "gradientInfo": [
                [
                    -1,
                    221,
                    228,
                    206,
                    255
                ],
                [
                    -0.6,
                    157,
                    145,
                    137,
                    255
                ],
                [
                    0,
                    161,
                    155,
                    150,
                    255
                ],
                [
                    0.6000000000000001,
                    193,
                    181,
                    161,
                    255
                ],
                [
                    1,
                    226,
                    228,
                    206,
                    255
                ]
            ],
            "heightMap": "destMap",
            "lightBrightness": 2.1,
            "lightContrast": 0.0035,
            "name": "renderer1",
            "randomFactor": [
                50,
                6,
                6
            ],
            "randomGradient": false
        }
    ],
    "size": [
        1024,
        512
    ]
}

)TX";




static std::string EARTHLIKE_11 = R"TX(
    {
    "bounds": [
        -90,
        90,
        -180,
        180
    ],
    "builderType": "sphere",
    "bumpMap": "normal",
    "cloudMap": "specular",
    "colorMap": "BaseImage",
    "heightMapBuilders": [
        {
            "dest": "BeginLayer_heightMap",
            "name": "BeginLayer_noiseMapBuilder1",
            "seamless": true,
            "source": "Max_Module_40"
        },
        {
            "dest": "EndLayer_heightMap",
            "name": "EndLayer_noiseMapBuilder1",
            "seamless": true,
            "source": "Turbulence_Module_70"
        },
        {
            "dest": "heightMap_Pole",
            "name": "hmbPole",
            "seamless": false,
            "source": "Turbulence_Module_100"
        }
    ],
    "heightMaps": [
        "BeginLayer_heightMap",
        "EndLayer_heightMap",
        "heightMap_Pole"
    ],
    "images": [
        "BaseImage",
        "normal",
        "specular"
    ],
    "modules": [
        {
            "enableRandom": false,
            "name": "Avg_Module_30",
            "src1": "EndLayer_Module",
            "src2": "Curve_Module_20",
            "type": "Avg"
        },
        {
            "disp": 0,
            "enableDist": true,
            "enableRandom": true,
            "freq": 3.1,
            "name": "BeginLayer_Module",
            "seed": 0,
            "type": "Voronoi"
        },
        {
            "cpoints": [
                [
                    -1,
                    -1
                ],
                [
                    -0.2,
                    -0.2
                ],
                [
                    0,
                    0.5
                ],
                [
                    0.2,
                    -0.2
                ],
                [
                    1,
                    -1
                ]
            ],
            "enableRandom": false,
            "name": "Curve_Module_20",
            "src1": "BeginLayer_Module",
            "type": "Curve"
        },
        {
            "enableRandom": false,
            "freq": 0.5,
            "name": "Cylinders_Module_90",
            "type": "Cylinders"
        },
        {
            "enableRandom": true,
            "freq": 4.3,
            "lac": 2.70822,
            "name": "EndLayer_Module",
            "oct": 10,
            "seed": 0,
            "type": "RidgedMulti"
        },
        {
            "enableRandom": false,
            "name": "Max_Module_40",
            "src1": "Turbulence_Module_50",
            "src2": "EndLayer_Module",
            "type": "Max"
        },
        {
            "enableRandom": false,
            "freq": 1.9,
            "lac": 2.6,
            "name": "Perlin_Module_60",
            "oct": 10,
            "pers": 0.38,
            "seed": 0,
            "type": "Perlin"
        },
        {
            "enableRandom": false,
            "freq": 2.5,
            "lac": 3.2,
            "name": "Perlin_Module_80",
            "oct": 6,
            "pers": 0.3,
            "seed": 0,
            "type": "Perlin"
        },
        {
            "enableRandom": false,
            "freq": 9.84,
            "name": "Turbulence_Module_100",
            "pow": 0.23,
            "rough": 2.5,
            "seed": 0,
            "src1": "Cylinders_Module_90",
            "type": "Turbulence"
        },
        {
            "enableRandom": false,
            "freq": 5.3,
            "name": "Turbulence_Module_50",
            "pow": 0.2,
            "rough": 1.3,
            "seed": 0,
            "src1": "Avg_Module_30",
            "type": "Turbulence"
        },
        {
            "enableRandom": false,
            "freq": 1.4,
            "name": "Turbulence_Module_70",
            "pow": 0.1,
            "rough": 1.1,
            "seed": 0,
            "src1": "Perlin_Module_60",
            "type": "Turbulence"
        }
    ],
    "randomFactors": [
        0
    ],
    "reflectionMap": "",
    "renderers": [
        {
            "alphaImage": "",
            "backgroundImage": "",
            "bumpMap": "",
            "destImage": "BaseImage",
            "enabledLight": false,
            "gradientInfo": [
                [
                    -1,
                    237,
                    204,
                    175,
                    255
                ],
                [
                    -0.95356,
                    252,
                    255,
                    178,
                    255
                ],
                [
                    -0.85356,
                    169,
                    209,
                    164,
                    255
                ],
                [
                    -0.6345381526104418,
                    185,
                    242,
                    179,
                    255
                ],
                [
                    -0.52572,
                    241,
                    240,
                    172,
                    255
                ],
                [
                    -0.37427999999999995,
                    241,
                    240,
                    172,
                    255
                ],
                [
                    -0.2570281124497992,
                    213,
                    247,
                    216,
                    255
                ],
                [
                    0.07475999999999994,
                    219,
                    247,
                    213,
                    255
                ],
                [
                    0.17476000000000003,
                    255,
                    228,
                    133,
                    255
                ],
                [
                    0.28191999999999995,
                    214,
                    196,
                    143,
                    255
                ],
                [
                    0.38192000000000004,
                    251,
                    228,
                    171,
                    255
                ],
                [
                    0.61984,
                    251,
                    228,
                    171,
                    255
                ],
                [
                    0.71984,
                    253,
                    233,
                    178,
                    255
                ],
                [
                    1,
                    237,
                    240,
                    183,
                    255
                ]
            ],
            "heightMap": "BeginLayer_heightMap",
            "lightBrightness": 2,
            "lightContrast": 1.4,
            "name": "BeginLayer_renderer",
            "randomGradient": false
        },
        {
            "alphaImage": "",
            "backgroundImage": "BaseImage",
            "bumpMap": "",
            "destImage": "BaseImage",
            "enabledLight": false,
            "gradientInfo": [
                [
                    -1,
                    15,
                    46,
                    118,
                    255
                ],
                [
                    -0.04417670682730923,
                    14,
                    166,
                    185,
                    255
                ],
                [
                    0.24899598393574296,
                    26,
                    162,
                    222,
                    255
                ],
                [
                    0.2610441767068272,
                    212,
                    210,
                    121,
                    255
                ],
                [
                    0.32530120481927716,
                    63,
                    147,
                    66,
                    255
                ],
                [
                    0.6907630522088353,
                    47,
                    199,
                    47,
                    0
                ],
                [
                    0.9116465863453815,
                    77,
                    187,
                    55,
                    0
                ],
                [
                    0.9518072289156627,
                    184,
                    172,
                    57,
                    255
                ],
                [
                    0.9759036144578312,
                    49,
                    190,
                    255,
                    255
                ],
                [
                    1,
                    0,
                    200,
                    255,
                    255
                ]
            ],
            "heightMap": "EndLayer_heightMap",
            "lightBrightness": 1,
            "lightContrast": 1,
            "name": "renderer0002",
            "randomGradient": false
        },
        {
            "alphaImage": "",
            "backgroundImage": "BaseImage",
            "bumpMap": "",
            "destImage": "BaseImage",
            "enabledLight": false,
            "gradientInfo": [
                [
                    -1,
                    0,
                    0,
                    0,
                    0
                ],
                [
                    0.5100401606425702,
                    238,
                    238,
                    238,
                    0
                ],
                [
                    0.5341365461847389,
                    238,
                    244,
                    255,
                    255
                ],
                [
                    1,
                    238,
                    246,
                    255,
                    255
                ]
            ],
            "heightMap": "heightMap_Pole",
            "lightBrightness": 1,
            "lightContrast": 1,
            "name": "renderer0003",
            "randomGradient": false
        },
        {
            "alphaImage": "",
            "backgroundImage": "",
            "bumpMap": "",
            "destImage": "normal",
            "enabledLight": false,
            "gradientInfo": [
                [
                    -1,
                    0,
                    0,
                    0,
                    255
                ],
                [
                    0.248,
                    0,
                    0,
                    0,
                    255
                ],
                [
                    0.952,
                    255,
                    255,
                    255,
                    255
                ],
                [
                    1,
                    0,
                    0,
                    0,
                    128
                ]
            ],
            "heightMap": "EndLayer_heightMap",
            "lightBrightness": 1,
            "lightContrast": 1,
            "name": "renderer0004",
            "randomGradient": false
        },
        {
            "alphaImage": "",
            "backgroundImage": "",
            "bumpMap": "",
            "destImage": "specular",
            "enabledLight": false,
            "gradientInfo": [
                [
                    -1,
                    255,
                    255,
                    255,
                    255
                ],
                [
                    0.244,
                    255,
                    255,
                    255,
                    255
                ],
                [
                    0.248,
                    0,
                    0,
                    0,
                    255
                ],
                [
                    1,
                    0,
                    0,
                    0,
                    255
                ]
            ],
            "heightMap": "EndLayer_heightMap",
            "lightBrightness": 1,
            "lightContrast": 1,
            "name": "renderer0005",
            "randomGradient": false
        }
    ],
    "size": [
        1280,
        640
    ]
}

)TX";




static std::string GASGIANT21 = R"TX(
    {
    "bounds": [
        -90,
        90,
        -180,
        180
    ],
    "builderType": "sphere",
    "bumpMap": "",
    "cloudMap": "",
    "colorMap": "BaseImage",
    "heightMapBuilders": [
        {
            "dest": "BeginLayer_heightMap",
            "name": "BeginLayer_noiseMapBuilder1",
            "seamless": false,
            "source": "BeginLayer_Turbo"
        }
    ],
    "heightMaps": [
        "BeginLayer_heightMap"
    ],
    "images": [
        "BaseImage"
    ],
    "modules": [
        {
            "enableRandom": true,
            "freq": 0.7,
            "name": "BeginLayer_Module",
            "type": "Cylinders"
        },
        {
            "enableRandom": false,
            "freq": 6.5,
            "name": "BeginLayer_Turbo",
            "pow": 0.03125,
            "rough": 2,
            "seed": 0,
            "src1": "BeginLayer_Module",
            "type": "Turbulence2"
        }
    ],
    "randomFactors": [
        0
    ],
    "reflectionMap": "",
    "renderers": [
        {
            "alphaImage": "",
            "backgroundImage": "",
            "bumpMap": "",
            "destImage": "BaseImage",
            "enabledLight": false,
            "gradientInfo": [
                [
                    -1,
                    21,
                    25,
                    135,
                    255
                ],
                [
                    -0.8524499999999999,
                    19,
                    42,
                    122,
                    255
                ],
                [
                    -0.5648749999999999,
                    23,
                    45,
                    104,
                    255
                ],
                [
                    -0.4119999999999999,
                    18,
                    28,
                    124,
                    255
                ],
                [
                    -0.24107499999999993,
                    13,
                    31,
                    112,
                    255
                ],
                [
                    0.0333500000000001,
                    4,
                    27,
                    87,
                    255
                ],
                [
                    0.16425000000000012,
                    10,
                    34,
                    106,
                    255
                ],
                [
                    0.3855750000000001,
                    9,
                    52,
                    108,
                    255
                ],
                [
                    0.7092250000000001,
                    7,
                    49,
                    98,
                    255
                ],
                [
                    1,
                    3,
                    57,
                    90,
                    255
                ]
            ],
            "heightMap": "BeginLayer_heightMap",
            "lightBrightness": 1,
            "lightContrast": 1,
            "name": "BeginLayer_renderer",
            "randomGradient": true
        }
    ],
    "size": [
        1280,
        640
    ]
}

)TX";




static std::string ALIENPEAKSVORONOI = R"TX(
    {
    "bounds": [
        -90,
        90,
        -180,
        180
    ],
    "builderType": "sphere",
    "bumpMap": "",
    "cloudMap": "",
    "colorMap": "image1",
    "heightMapBuilders": [
        {
            "dest": "heightMap",
            "name": "noiseMapBuilder1",
            "seamless": true,
            "source": "blendModule"
        }
    ],
    "heightMaps": [
        "heightMap"
    ],
    "images": [
        "image1"
    ],
    "modules": [
        {
            "enableRandom": false,
            "freq": 2.5,
            "lac": 3.2,
            "name": "basePerlin",
            "oct": 6,
            "pers": 0.3,
            "seed": 0,
            "type": "Perlin"
        },
        {
            "ctl": "scaleBase",
            "enableRandom": false,
            "name": "blendModule",
            "src1": "turbVoronoi",
            "src2": "rot",
            "type": "Blend"
        },
        {
            "enableRandom": false,
            "exp": 4,
            "name": "expBase",
            "src1": "rot",
            "type": "Exponent"
        },
        {
            "enableRandom": true,
            "name": "rot",
            "src1": "basePerlin",
            "type": "RotatePoint",
            "x": 45,
            "y": 45,
            "z": 45
        },
        {
            "bias": 0,
            "enableRandom": false,
            "name": "scaleBase",
            "scale": 0.7,
            "src1": "rot",
            "type": "ScaleBias"
        },
        {
            "enableRandom": false,
            "freq": 2.5,
            "name": "turbVoronoi",
            "pow": 0.18,
            "rough": 5,
            "seed": 0,
            "src1": "voronoi",
            "type": "Turbulence"
        },
        {
            "enableDist": true,
            "enableRandom": false,
            "freq": 5,
            "name": "voronoi",
            "seed": 0,
            "type": "Voronoi"
        }
    ],
    "randomFactors": [
        0.1,
        0.2,
        0.3,
        0.4,
        0.5,
        0.6,
        0.2,
        0.2,
        0.3
    ],
    "reflectionMap": "",
    "renderers": [
        {
            "backgroundImage": "",
            "destImage": "image1",
            "enabledLight": true,
            "gradientInfo": [
                [
                    -1,
                    127,
                    125,
                    133,
                    255
                ],
                [
                    -0.487525,
                    196,
                    166,
                    151,
                    255
                ],
                [
                    0.027225000000000055,
                    215,
                    129,
                    183,
                    255
                ],
                [
                    0.6468750000000001,
                    214,
                    216,
                    206,
                    255
                ],
                [
                    1,
                    83,
                    86,
                    231,
                    255
                ]
            ],
            "heightMap": "heightMap",
            "lightBrightness": 2,
            "lightContrast": 1.2,
            "name": "renderer0001",
            "randomFactor": [
                170,
                30,
                30
            ],
            "randomGradient": false
        }
    ],
    "size": [
        1280,
        640
    ]
}

)TX";




static std::string GASGIANTORIGINALOK = R"TX(
    {
    "bounds": [
        -90,
        90,
        -180,
        180
    ],
    "builderType": "sphere",
    "bumpMap": "",
    "cloudMap": "",
    "colorMap": "BaseImage",
    "heightMapBuilders": [
        {
            "dest": "cloudMap",
            "name": "cloudMapBuilder",
            "seamless": false,
            "source": "pert3"
        },
        {
            "dest": "heightMap",
            "name": "noiseMapBuilder1",
            "seamless": true,
            "source": "pert2"
        }
    ],
    "heightMaps": [
        "cloudMap",
        "heightMap"
    ],
    "images": [
        "BaseImage"
    ],
    "modules": [
        {
            "enableRandom": false,
            "freq": 2.5,
            "name": "baseGG",
            "type": "Cylinders"
        },
        {
            "enableRandom": true,
            "freq": 1.5,
            "name": "baseGG2",
            "type": "Cylinders"
        },
        {
            "enableRandom": true,
            "freq": 0.9,
            "name": "pert2",
            "pow": 0.15,
            "rough": 7,
            "seed": 0,
            "src1": "rot2",
            "type": "Turbulence"
        },
        {
            "enableRandom": true,
            "freq": 0.82,
            "name": "pert3",
            "pow": 0.03,
            "rough": 4,
            "seed": 0,
            "src1": "rot",
            "type": "Turbulence"
        },
        {
            "enableRandom": false,
            "name": "rot",
            "src1": "baseGG2",
            "type": "RotatePoint",
            "x": 1,
            "y": 1,
            "z": 1.1
        },
        {
            "enableRandom": false,
            "name": "rot2",
            "src1": "baseGG",
            "type": "RotatePoint",
            "x": -1,
            "y": -1,
            "z": -1
        }
    ],
    "randomFactors": [
        0.4,
        0.5,
        0.6,
        0.5,
        0.4,
        0.3,
        0.2,
        0.1
    ],
    "reflectionMap": "",
    "renderers": [
        {
            "backgroundImage": "",
            "destImage": "BaseImage",
            "enabledLight": false,
            "gradientInfo": [
                [
                    -1,
                    53,
                    244,
                    175,
                    255
                ],
                [
                    -0.761325,
                    57,
                    231,
                    143,
                    255
                ],
                [
                    -0.4166,
                    67,
                    208,
                    121,
                    255
                ],
                [
                    -0.31172500000000003,
                    80,
                    205,
                    148,
                    255
                ],
                [
                    -0.012975000000000025,
                    80,
                    191,
                    155,
                    255
                ],
                [
                    0.10587499999999998,
                    74,
                    185,
                    147,
                    255
                ],
                [
                    0.231175,
                    73,
                    189,
                    127,
                    255
                ],
                [
                    0.41715,
                    91,
                    208,
                    160,
                    255
                ],
                [
                    0.66495,
                    101,
                    224,
                    179,
                    255
                ],
                [
                    0.9141250000000001,
                    89,
                    228,
                    200,
                    255
                ],
                [
                    1,
                    92,
                    237,
                    174,
                    255
                ]
            ],
            "heightMap": "heightMap",
            "lightBrightness": 1,
            "lightContrast": 1,
            "name": "renderer0001",
            "randomFactor": [
                5,
                50,
                50
            ],
            "randomGradient": true
        },
        {
            "backgroundImage": "BaseImage",
            "destImage": "BaseImage",
            "enabledLight": false,
            "gradientInfo": [
                [
                    -1,
                    153,
                    184,
                    213,
                    0
                ],
                [
                    -0.6990825688073394,
                    99,
                    56,
                    184,
                    0
                ],
                [
                    -0.5239199999999999,
                    184,
                    141,
                    204,
                    255
                ],
                [
                    -0.36513761467889905,
                    200,
                    148,
                    179,
                    0
                ],
                [
                    -0.2770642201834862,
                    207,
                    140,
                    224,
                    0
                ],
                [
                    -0.06735999999999986,
                    103,
                    152,
                    150,
                    255
                ],
                [
                    0.18165137614678906,
                    103,
                    177,
                    105,
                    0
                ],
                [
                    0.2697247706422019,
                    64,
                    68,
                    69,
                    0
                ],
                [
                    0.3440000000000003,
                    80,
                    192,
                    171,
                    255
                ],
                [
                    0.4862385321100917,
                    98,
                    145,
                    100,
                    0
                ],
                [
                    0.7202000000000002,
                    181,
                    197,
                    177,
                    255
                ],
                [
                    0.8825688073394495,
                    89,
                    40,
                    135,
                    0
                ],
                [
                    1,
                    158,
                    160,
                    128,
                    0
                ]
            ],
            "heightMap": "cloudMap",
            "lightBrightness": 1,
            "lightContrast": 1,
            "name": "renderer0002",
            "randomFactor": [
                100,
                100,
                100
            ],
            "randomGradient": false
        }
    ],
    "size": [
        1024,
        512
    ]
}

)TX";




static std::string GASGIANT2OK = R"TX(
    {
    "bounds": [
        -90,
        90,
        -180,
        180
    ],
    "builderType": "sphere",
    "bumpMap": "",
    "cloudMap": "",
    "colorMap": "BaseImage",
    "heightMapBuilders": [
        {
            "dest": "cloudMap",
            "name": "cloudMapBuilder",
            "seamless": false,
            "source": "pert3"
        },
        {
            "dest": "heightMap",
            "name": "noiseMapBuilder1",
            "seamless": true,
            "source": "pert2"
        }
    ],
    "heightMaps": [
        "cloudMap",
        "heightMap"
    ],
    "images": [
        "BaseImage"
    ],
    "modules": [
        {
            "enableRandom": false,
            "freq": 1.5,
            "name": "baseGG",
            "type": "Cylinders"
        },
        {
            "enableRandom": true,
            "freq": 1.5,
            "name": "baseGG2",
            "type": "Cylinders"
        },
        {
            "enableRandom": true,
            "freq": 1.3,
            "name": "pert2",
            "pow": 0.2,
            "rough": 5.5,
            "seed": 0,
            "src1": "rot2",
            "type": "Turbulence"
        },
        {
            "enableRandom": true,
            "freq": 0.82,
            "name": "pert3",
            "pow": 0.03,
            "rough": 4,
            "seed": 0,
            "src1": "rot",
            "type": "Turbulence"
        },
        {
            "enableRandom": false,
            "name": "rot",
            "src1": "baseGG2",
            "type": "RotatePoint",
            "x": 1,
            "y": 1,
            "z": 1.1
        },
        {
            "enableRandom": false,
            "name": "rot2",
            "src1": "baseGG",
            "type": "RotatePoint",
            "x": -1,
            "y": -1,
            "z": -1
        }
    ],
    "randomFactors": [
        0.2,
        0.3,
        0.25,
        0.1,
        0.2,
        0.3,
        0.2,
        0.1
    ],
    "reflectionMap": "",
    "renderers": [
        {
            "backgroundImage": "",
            "destImage": "BaseImage",
            "enabledLight": false,
            "gradientInfo": [
                [
                    -1,
                    50,
                    113,
                    169,
                    255
                ],
                [
                    -0.37614678899082565,
                    188,
                    170,
                    236,
                    255
                ],
                [
                    -0.310091743119266,
                    36,
                    73,
                    43,
                    255
                ],
                [
                    0.36513761467889916,
                    138,
                    136,
                    79,
                    255
                ],
                [
                    0.4422018348623853,
                    162,
                    204,
                    150,
                    255
                ],
                [
                    1,
                    181,
                    188,
                    132,
                    255
                ]
            ],
            "heightMap": "heightMap",
            "lightBrightness": 1,
            "lightContrast": 1,
            "name": "renderer0001",
            "randomFactor": [
                90,
                50,
                50
            ],
            "randomGradient": false
        },
        {
            "backgroundImage": "BaseImage",
            "destImage": "BaseImage",
            "enabledLight": false,
            "gradientInfo": [
                [
                    -1,
                    62,
                    34,
                    97,
                    0
                ],
                [
                    -0.6990825688073394,
                    227,
                    240,
                    219,
                    0
                ],
                [
                    -0.5239199999999999,
                    218,
                    114,
                    234,
                    64
                ],
                [
                    -0.36513761467889905,
                    127,
                    122,
                    49,
                    0
                ],
                [
                    -0.2770642201834862,
                    82,
                    61,
                    97,
                    0
                ],
                [
                    -0.06735999999999986,
                    63,
                    54,
                    77,
                    53
                ],
                [
                    0.18165137614678906,
                    53,
                    75,
                    103,
                    0
                ],
                [
                    0.2697247706422019,
                    169,
                    172,
                    195,
                    0
                ],
                [
                    0.3440000000000003,
                    43,
                    187,
                    127,
                    67
                ],
                [
                    0.4862385321100917,
                    12,
                    80,
                    147,
                    0
                ],
                [
                    0.7202000000000002,
                    184,
                    118,
                    145,
                    66
                ],
                [
                    0.8825688073394495,
                    177,
                    185,
                    191,
                    0
                ],
                [
                    1,
                    109,
                    178,
                    160,
                    0
                ]
            ],
            "heightMap": "cloudMap",
            "lightBrightness": 1,
            "lightContrast": 1,
            "name": "renderer0002",
            "randomFactor": [
                100,
                100,
                100
            ],
            "randomGradient": false
        }
    ],
    "size": [
        1024,
        512
    ]
}

)TX";




static std::string EARTHLIKE_REALISTIC = R"TX(
    {
    "bounds": [
        -90,
        90,
        -180,
        180
    ],
    "builderType": "sphere",
    "bumpMap": "imageNormal",
    "cloudMap": "imageBump",
    "colorMap": "ImageSea",
    "heightMapBuilders": [
        {
            "dest": "TextureFlow_heightMap",
            "name": "TextureFlow_noiseMapBuilder1",
            "seamless": false,
            "source": "TextureFlow_Module"
        },
        {
            "dest": "heightMapPole",
            "name": "heightMapBuilderPole",
            "seamless": true,
            "source": "turboPoles"
        },
        {
            "dest": "heightMapDesert",
            "name": "noiseMapBDesert",
            "seamless": false,
            "source": "ModuleDesert"
        },
        {
            "dest": "heightMap",
            "name": "noiseMapBuilder1",
            "seamless": false,
            "source": "Module1"
        },
        {
            "dest": "HeightMapSea",
            "name": "noiseMapSea",
            "seamless": false,
            "source": "ModuleSea"
        }
    ],
    "heightMaps": [
        "HeightMapSea",
        "TextureFlow_heightMap",
        "heightMap",
        "heightMapDesert",
        "heightMapPole"
    ],
    "images": [
        "ImageSea",
        "image1",
        "imageBump",
        "imageNormal"
    ],
    "modules": [
        {
            "enableRandom": true,
            "freq": 4.2,
            "lac": 2.2,
            "name": "Module1",
            "oct": 6,
            "pers": 0.24,
            "seed": 0,
            "type": "Perlin"
        },
        {
            "enableRandom": true,
            "freq": 4.5,
            "lac": 4.3,
            "name": "ModuleDesert",
            "oct": 6,
            "pers": 0.35,
            "seed": 0,
            "type": "Billow"
        },
        {
            "enableRandom": true,
            "freq": 1.8,
            "lac": 5.3,
            "name": "ModuleSea",
            "oct": 6,
            "pers": 0.23,
            "seed": 0,
            "type": "Perlin"
        },
        {
            "enableRandom": true,
            "freq": 6.6,
            "lac": 1.66912,
            "name": "TextureFlow_Module",
            "oct": 6,
            "seed": 0,
            "type": "RidgedMulti"
        },
        {
            "enableRandom": true,
            "freq": 2.5,
            "name": "TurboDesert",
            "pow": 0.55,
            "rough": 1.2,
            "seed": 0,
            "src1": "ModuleDesert",
            "type": "Turbulence"
        },
        {
            "enableRandom": false,
            "freq": 0.5,
            "name": "cylinderPole",
            "type": "Cylinders"
        },
        {
            "enableRandom": true,
            "freq": 4.5,
            "name": "turboPoles",
            "pow": 0.2,
            "rough": 3.2,
            "seed": 0,
            "src1": "cylinderPole",
            "type": "Turbulence"
        }
    ],
    "randomFactors": [
        0
    ],
    "reflectionMap": "imageBump",
    "renderers": [
        {
            "alphaImage": "",
            "backgroundImage": "",
            "bumpMap": "",
            "destImage": "image1",
            "enabledLight": false,
            "gradientInfo": [
                [
                    -1,
                    12,
                    97,
                    0,
                    255
                ],
                [
                    -0.05604719764011801,
                    115,
                    146,
                    0,
                    255
                ],
                [
                    0.9156626506024097,
                    0,
                    195,
                    0,
                    255
                ],
                [
                    1,
                    255,
                    255,
                    255,
                    255
                ]
            ],
            "heightMap": "heightMap",
            "lightBrightness": 2,
            "lightContrast": 1.5,
            "name": "renderer1",
            "randomGradient": false
        },
        {
            "alphaImage": "",
            "backgroundImage": "image1",
            "bumpMap": "",
            "destImage": "image1",
            "enabledLight": false,
            "gradientInfo": [
                [
                    -1,
                    251,
                    247,
                    244,
                    0
                ],
                [
                    -0.7991967871485943,
                    7,
                    0,
                    0,
                    0
                ],
                [
                    -0.028112449799196804,
                    208,
                    209,
                    95,
                    65
                ],
                [
                    1,
                    232,
                    212,
                    117,
                    135
                ]
            ],
            "heightMap": "heightMapDesert",
            "lightBrightness": 2,
            "lightContrast": 1.4,
            "name": "renderer2",
            "randomGradient": false
        },
        {
            "alphaImage": "",
            "backgroundImage": "image1",
            "bumpMap": "",
            "destImage": "ImageSea",
            "enabledLight": false,
            "gradientInfo": [
                [
                    -1,
                    0,
                    31,
                    174,
                    255
                ],
                [
                    -0.5903614457831325,
                    0,
                    14,
                    239,
                    255
                ],
                [
                    0.024096385542168752,
                    0,
                    80,
                    235,
                    255
                ],
                [
                    0.16867469879518082,
                    85,
                    226,
                    234,
                    255
                ],
                [
                    0.18338108882521498,
                    109,
                    185,
                    114,
                    255
                ],
                [
                    0.2530120481927711,
                    120,
                    166,
                    59,
                    255
                ],
                [
                    0.3373493975903614,
                    214,
                    176,
                    146,
                    0
                ],
                [
                    0.5060240963855422,
                    127,
                    207,
                    251,
                    0
                ],
                [
                    0.9036144578313252,
                    129,
                    167,
                    59,
                    255
                ],
                [
                    0.9397590361445782,
                    117,
                    225,
                    251,
                    255
                ],
                [
                    1,
                    0,
                    203,
                    224,
                    255
                ]
            ],
            "heightMap": "HeightMapSea",
            "lightBrightness": 1,
            "lightContrast": 1,
            "name": "renderer3",
            "randomGradient": false
        },
        {
            "alphaImage": "",
            "backgroundImage": "ImageSea",
            "bumpMap": "",
            "destImage": "ImageSea",
            "enabledLight": false,
            "gradientInfo": [
                [
                    -1,
                    0,
                    0,
                    0,
                    0
                ],
                [
                    0.3293172690763053,
                    254,
                    251,
                    248,
                    0
                ],
                [
                    0.37349397590361444,
                    255,
                    253,
                    251,
                    255
                ],
                [
                    1,
                    255,
                    254,
                    252,
                    255
                ]
            ],
            "heightMap": "heightMapPole",
            "lightBrightness": 1,
            "lightContrast": 1,
            "name": "renderer4",
            "randomGradient": false
        },
        {
            "alphaImage": "",
            "backgroundImage": "",
            "bumpMap": "",
            "destImage": "imageBump",
            "enabledLight": false,
            "gradientInfo": [
                [
                    -1,
                    255,
                    255,
                    255,
                    255
                ],
                [
                    0.167,
                    255,
                    255,
                    255,
                    255
                ],
                [
                    0.168,
                    0,
                    0,
                    0,
                    255
                ],
                [
                    0.91,
                    0,
                    0,
                    0,
                    255
                ],
                [
                    0.911,
                    255,
                    255,
                    255,
                    255
                ],
                [
                    1,
                    255,
                    255,
                    255,
                    255
                ]
            ],
            "heightMap": "HeightMapSea",
            "lightBrightness": 1,
            "lightContrast": 1,
            "name": "renderer5",
            "randomGradient": false
        },
        {
            "alphaImage": "",
            "backgroundImage": "",
            "bumpMap": "",
            "destImage": "imageNormal",
            "enabledLight": false,
            "gradientInfo": [
                [
                    0.012844036697247763,
                    0,
                    0,
                    0,
                    255
                ],
                [
                    0.8560000000000001,
                    255,
                    255,
                    255,
                    255
                ],
                [
                    1,
                    255,
                    255,
                    255,
                    255
                ]
            ],
            "heightMap": "TextureFlow_heightMap",
            "lightBrightness": 1,
            "lightContrast": 1,
            "name": "renderer0006",
            "randomGradient": false
        },
        {
            "alphaImage": "",
            "backgroundImage": "imageNormal",
            "bumpMap": "",
            "destImage": "imageNormal",
            "enabledLight": false,
            "gradientInfo": [
                [
                    -1,
                    0,
                    0,
                    0,
                    255
                ],
                [
                    0.2550458715596331,
                    0,
                    0,
                    0,
                    255
                ],
                [
                    0.5743119266055046,
                    0,
                    0,
                    0,
                    0
                ],
                [
                    0.8445595854922279,
                    0,
                    0,
                    0,
                    0
                ],
                [
                    0.9039999999999999,
                    255,
                    255,
                    255,
                    0
                ],
                [
                    1,
                    255,
                    255,
                    255,
                    255
                ]
            ],
            "heightMap": "HeightMapSea",
            "lightBrightness": 1,
            "lightContrast": 1,
            "name": "renderer0007",
            "randomGradient": false
        }
    ],
    "size": [
        1600,
        800
    ]
}

)TX";




static std::string DESERT_REDDISH = R"TX(
    {
    "bounds": [
        -90,
        90,
        -180,
        180
    ],
    "builderType": "sphere",
    "bumpMap": "",
    "cloudMap": "",
    "colorMap": "BaseImage",
    "heightMapBuilders": [
        {
            "dest": "BeginLayer_heightMap",
            "name": "BeginLayer_noiseMapBuilder1",
            "seamless": true,
            "source": "BeginLayer_Module"
        },
        {
            "dest": "EndLayer_heightMap",
            "name": "EndLayer_noiseMapBuilder1",
            "seamless": true,
            "source": "Module_40"
        }
    ],
    "heightMaps": [
        "BeginLayer_heightMap",
        "EndLayer_heightMap"
    ],
    "images": [
        "BaseImage"
    ],
    "modules": [
        {
            "enableDist": true,
            "enableRandom": true,
            "freq": 4.3,
            "name": "BeginLayer_Module",
            "seed": 0,
            "type": "Voronoi"
        },
        {
            "cpoints": [
                [
                    -1,
                    -1
                ],
                [
                    -0.55,
                    0.5
                ],
                [
                    -0.25,
                    0.5
                ],
                [
                    1,
                    -1
                ]
            ],
            "enableRandom": true,
            "name": "EndLayer_Module",
            "src1": "BeginLayer_Module",
            "type": "Curve"
        },
        {
            "enableRandom": false,
            "freq": 1.4,
            "name": "Module_20",
            "pow": 0.21,
            "rough": 4.2,
            "seed": 0,
            "src1": "EndLayer_Module",
            "type": "Turbulence"
        },
        {
            "enableRandom": false,
            "freq": 1.8,
            "lac": 2.3,
            "name": "Module_30",
            "oct": 6,
            "pers": 0.4,
            "seed": 0,
            "type": "Perlin"
        },
        {
            "enableRandom": false,
            "name": "Module_40",
            "src1": "Module_30",
            "src2": "Module_50",
            "type": "Avg"
        },
        {
            "bias": 0.5,
            "enableRandom": false,
            "name": "Module_50",
            "scale": 0.5,
            "src1": "Module_20",
            "type": "ScaleBias"
        }
    ],
    "randomFactors": [
        0.1,
        0.2,
        0.4,
        0.6,
        0.8
    ],
    "reflectionMap": "",
    "renderers": [
        {
            "backgroundImage": "",
            "destImage": "BaseImage",
            "enabledLight": true,
            "gradientInfo": [
                [
                    -1,
                    185,
                    78,
                    24,
                    255
                ],
                [
                    -0.6,
                    207,
                    10,
                    76,
                    255
                ],
                [
                    0,
                    198,
                    146,
                    25,
                    255
                ],
                [
                    0.6000000000000001,
                    167,
                    88,
                    10,
                    255
                ],
                [
                    1,
                    255,
                    94,
                    13,
                    255
                ]
            ],
            "heightMap": "EndLayer_heightMap",
            "lightBrightness": 2,
            "lightContrast": 1.5,
            "name": "BeginLayer_renderer",
            "randomFactor": [
                75,
                25,
                25
            ],
            "randomGradient": false
        }
    ],
    "size": [
        1024,
        512
    ]
}

)TX";




static std::string EARTHLIKE_03 = R"TX(
    {
    "bounds": [
        -90,
        90,
        -180,
        180
    ],
    "builderType": "sphere",
    "bumpMap": "normal",
    "cloudMap": "specular",
    "colorMap": "image1",
    "heightMapBuilders": [
        {
            "dest": "destMap",
            "name": "builder",
            "seamless": true,
            "source": "Module2"
        },
        {
            "dest": "destMap2",
            "name": "builder2",
            "seamless": true,
            "source": "clamp"
        },
        {
            "dest": "destMap3",
            "name": "builder3",
            "seamless": true,
            "source": "Module2Exp"
        },
        {
            "dest": "destMap4",
            "name": "builder4",
            "seamless": true,
            "source": "PolesTurbo"
        }
    ],
    "heightMaps": [
        "destMap",
        "destMap2",
        "destMap3",
        "destMap4"
    ],
    "images": [
        "image1",
        "normal",
        "specular"
    ],
    "modules": [
        {
            "bias": -0.1,
            "enableRandom": false,
            "name": "Ctl",
            "scale": 0.9,
            "src1": "CtlPerlin",
            "type": "ScaleBias"
        },
        {
            "enableRandom": false,
            "freq": 1.1,
            "lac": 5.7,
            "name": "CtlPerlin",
            "oct": 5,
            "pers": 0.35,
            "seed": 0,
            "type": "Perlin"
        },
        {
            "enableRandom": false,
            "freq": 0.1,
            "lac": 3.7,
            "name": "CtlPerlin2",
            "oct": 5,
            "pers": 0.25,
            "seed": 0,
            "type": "Perlin"
        },
        {
            "enableRandom": false,
            "freq": 1.2,
            "name": "Module2",
            "pow": 0.15,
            "rough": 1.3,
            "seed": 0,
            "src1": "ModulePerlin",
            "type": "Turbulence"
        },
        {
            "enableRandom": false,
            "exp": 1.15,
            "name": "Module2Exp",
            "src1": "Module2",
            "type": "Exponent"
        },
        {
            "enableRandom": false,
            "freq": 1.65,
            "lac": 2.3,
            "name": "ModulePerlin",
            "oct": 6,
            "pers": 0.45,
            "seed": 0,
            "type": "Perlin"
        },
        {
            "enableRandom": false,
            "freq": 3,
            "name": "PolesTurbo",
            "pow": 0.25,
            "rough": 6,
            "seed": 0,
            "src1": "Poli",
            "type": "Turbulence"
        },
        {
            "enableRandom": false,
            "freq": 0.5,
            "name": "Poli",
            "type": "Cylinders"
        },
        {
            "enableRandom": false,
            "lbound": 0,
            "name": "clamp",
            "src1": "sel",
            "type": "Clamp",
            "ubound": 1
        },
        {
            "enableRandom": false,
            "lbound": 0.1,
            "name": "clamp2",
            "src1": "sel2",
            "type": "Clamp",
            "ubound": 1
        },
        {
            "enableRandom": false,
            "name": "const",
            "type": "Const",
            "value": 0
        },
        {
            "ctl": "Ctl",
            "enableRandom": false,
            "lbound": 0.12,
            "name": "sel",
            "src1": "const",
            "src2": "Module2",
            "type": "Select",
            "ubound": 0.7,
            "value": 1
        },
        {
            "ctl": "CtlPerlin2",
            "enableRandom": false,
            "lbound": 0.4,
            "name": "sel2",
            "src1": "const",
            "src2": "Module2",
            "type": "Select",
            "ubound": 0.7,
            "value": 1
        }
    ],
    "randomFactors": [
        0
    ],
    "reflectionMap": "specular",
    "renderers": [
        {
            "alphaImage": "",
            "backgroundImage": "",
            "bumpMap": "",
            "destImage": "image1",
            "enabledLight": false,
            "gradientInfo": [
                [
                    -1,
                    37,
                    29,
                    138,
                    255
                ],
                [
                    -0.6,
                    52,
                    77,
                    142,
                    255
                ],
                [
                    0.1100000000000001,
                    68,
                    140,
                    135,
                    255
                ],
                [
                    0.1200000000000001,
                    95,
                    156,
                    51,
                    255
                ],
                [
                    0.55,
                    48,
                    128,
                    53,
                    255
                ],
                [
                    0.95,
                    38,
                    89,
                    52,
                    255
                ],
                [
                    1,
                    100,
                    119,
                    42,
                    255
                ]
            ],
            "heightMap": "destMap",
            "lightBrightness": 2.1,
            "lightContrast": 0.2,
            "name": "renderer1",
            "randomFactor": [
                20,
                10,
                10
            ],
            "randomGradient": false
        },
        {
            "alphaImage": "",
            "backgroundImage": "",
            "bumpMap": "",
            "destImage": "normal",
            "enabledLight": false,
            "gradientInfo": [
                [
                    -1,
                    0,
                    0,
                    0,
                    255
                ],
                [
                    0.0018348623853210455,
                    0,
                    0,
                    0,
                    255
                ],
                [
                    1,
                    255,
                    255,
                    255,
                    255
                ]
            ],
            "heightMap": "destMap",
            "lightBrightness": 2.1,
            "lightContrast": 0.2,
            "name": "rendererNormal",
            "randomGradient": false
        },
        {
            "alphaImage": "",
            "backgroundImage": "",
            "bumpMap": "",
            "destImage": "specular",
            "enabledLight": false,
            "gradientInfo": [
                [
                    -1,
                    255,
                    255,
                    255,
                    255
                ],
                [
                    0.06788990825688068,
                    255,
                    255,
                    255,
                    255
                ],
                [
                    0.1200000000000001,
                    0,
                    0,
                    0,
                    255
                ],
                [
                    0.55,
                    0,
                    0,
                    0,
                    255
                ],
                [
                    0.95,
                    45,
                    98,
                    53,
                    255
                ],
                [
                    1,
                    117,
                    70,
                    38,
                    255
                ]
            ],
            "heightMap": "destMap",
            "lightBrightness": 2.1,
            "lightContrast": 0.2,
            "name": "rendererSpecular",
            "randomGradient": false
        },
        {
            "alphaImage": "",
            "backgroundImage": "image1",
            "bumpMap": "",
            "destImage": "image1",
            "enabledLight": false,
            "gradientInfo": [
                [
                    -1,
                    26,
                    67,
                    50,
                    0
                ],
                [
                    -0.6,
                    51,
                    179,
                    165,
                    0
                ],
                [
                    -0.14,
                    47,
                    20,
                    71,
                    0
                ],
                [
                    0.10000000000000009,
                    66,
                    50,
                    49,
                    0
                ],
                [
                    0.1100000000000001,
                    134,
                    116,
                    93,
                    128
                ],
                [
                    0.21999999999999997,
                    91,
                    53,
                    44,
                    255
                ],
                [
                    0.5,
                    92,
                    128,
                    80,
                    128
                ],
                [
                    0.71,
                    185,
                    182,
                    168,
                    0
                ],
                [
                    1,
                    174,
                    202,
                    193,
                    0
                ]
            ],
            "heightMap": "destMap2",
            "lightBrightness": 2,
            "lightContrast": 0.2,
            "name": "renderer2",
            "randomFactor": [
                20,
                40,
                40
            ],
            "randomGradient": false
        },
        {
            "alphaImage": "",
            "backgroundImage": "image1",
            "bumpMap": "",
            "destImage": "image1",
            "enabledLight": false,
            "gradientInfo": [
                [
                    -1,
                    0,
                    0,
                    0,
                    0
                ],
                [
                    0.33000000000000007,
                    79,
                    99,
                    36,
                    0
                ],
                [
                    0.3999999999999999,
                    79,
                    149,
                    36,
                    0
                ],
                [
                    0.47,
                    179,
                    0,
                    36,
                    0
                ],
                [
                    0.7,
                    79,
                    99,
                    36,
                    192
                ],
                [
                    0.9300000000000002,
                    224,
                    248,
                    255,
                    128
                ],
                [
                    1,
                    224,
                    248,
                    255,
                    255
                ]
            ],
            "heightMap": "destMap3",
            "lightBrightness": 2.1,
            "lightContrast": 0.01,
            "name": "renderer3",
            "randomGradient": false
        },
        {
            "alphaImage": "",
            "backgroundImage": "image1",
            "bumpMap": "",
            "destImage": "image1",
            "enabledLight": false,
            "gradientInfo": [
                [
                    -1,
                    0,
                    0,
                    0,
                    0
                ],
                [
                    0.29000000000000004,
                    224,
                    248,
                    255,
                    0
                ],
                [
                    0.30000000000000004,
                    224,
                    248,
                    255,
                    255
                ],
                [
                    1,
                    224,
                    248,
                    255,
                    255
                ]
            ],
            "heightMap": "destMap4",
            "lightBrightness": 2.4,
            "lightContrast": 1.01,
            "name": "renderer4",
            "randomGradient": false
        }
    ],
    "size": [
        1024,
        512
    ]
}

)TX";

static map<std::string, std::string> TEMPLATES = {
    {"OUTRE2",OUTRE2},
    {"DESERT",DESERT},
    {"OUTRE",OUTRE},
    {"PREGARDEN2",PREGARDEN2},
    {"FUNKYCLOUD",FUNKYCLOUD},
    {"EARTHLIKE_12",EARTHLIKE_12},
    {"ROCKBALL_02",ROCKBALL_02},
    {"PREGARDEN3",PREGARDEN3},
    {"CANYON_01",CANYON_01},
    {"EARTHLIKE_ISLAND",EARTHLIKE_ISLAND},
    {"GASGIANT4",GASGIANT4},
    {"POSTGARDEN",POSTGARDEN},
    {"HOTHOUSE",HOTHOUSE},
    {"DESERT_CREAM",DESERT_CREAM},
    {"ICEBALL",ICEBALL},
    {"GLACIER",GLACIER},
    {"GASGIANT3",GASGIANT3},
    {"PREGARDEN",PREGARDEN},
    {"GASGIANT22",GASGIANT22},
    {"OUTRE4",OUTRE4},
    {"EARTHLIKE_01",EARTHLIKE_01},
    {"ROCKBALL_03",ROCKBALL_03},
    {"POSTGARDEN_3",POSTGARDEN_3},
    {"MULTILAYERED_EARTHLIKERND",MULTILAYERED_EARTHLIKERND},
    {"EARTHLIKE_01_1",EARTHLIKE_01_1},
    {"EARTHLIKE_02",EARTHLIKE_02},
    {"GASGIANT5",GASGIANT5},
    {"DESERT2",DESERT2},
    {"CLOUD",CLOUD},
    {"GASGIANTOK",GASGIANTOK},
    {"ROCKBALL",ROCKBALL},
    {"EARTHLIKE_04",EARTHLIKE_04},
    {"EARTHLIKE_10",EARTHLIKE_10},
    {"CHUNK",CHUNK},
    {"EARTHLIKE_11",EARTHLIKE_11},
    {"GASGIANT21",GASGIANT21},
    {"ALIENPEAKSVORONOI",ALIENPEAKSVORONOI},
    {"GASGIANTORIGINALOK",GASGIANTORIGINALOK},
    {"GASGIANT2OK",GASGIANT2OK},
    {"EARTHLIKE_REALISTIC",EARTHLIKE_REALISTIC},
    {"DESERT_REDDISH",DESERT_REDDISH},
    {"EARTHLIKE_03",EARTHLIKE_03}
};


#endif // PRESETS_H
