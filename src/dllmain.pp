
unit dllmain;
interface

{
  Automatically converted by H2Pas 1.0.0 from dllmain.h
  The following command line parameters were used:
    -l
    libplanetnoise
    -D
    -p
    -w
    -v
    dllmain.h
}

    const
      External_library='kernel32'; {Setup as you need}

    { Pointers to basic pascal types, inserted by h2pas conversion program.}
    Type
      PLongint  = ^Longint;
      PSmallInt = ^SmallInt;
      PByte     = ^Byte;
      PWord     = ^Word;
      PDWord    = ^DWord;
      PDouble   = ^Double;

    Type
    PCPoint  = ^CPoint;
    PCTextureBuilder  = ^CTextureBuilder;
    PGradientInfoStruct  = ^GradientInfoStruct;
{$IFDEF FPC}
{$PACKRECORDS C}
{$ENDIF}


{$ifndef DLLMAIN_H}
{$define DLLMAIN_H}  
{$include <errno.h>}
{$include <string.h>}

  const
    BOOLEAN = longint;    
    TRUE = 1;    
    FALSE = 0;    
(* Const before type ignored *)
(* error 
const int ERR_NAME_IS_NULL=13001;
 in declarator_list *)
(* Const before type ignored *)
(* error 
const int ERR_SRC1_IS_NULL=13002;
 in declarator_list *)
(* Const before type ignored *)
(* error 
const int ERR_SRC2_IS_NULL=13003;
 in declarator_list *)
(* Const before type ignored *)
(* error 
const int ERR_SRC3_IS_NULL=13004;
 in declarator_list *)
(* Const before type ignored *)
(* error 
const int ERR_SRC4_IS_NULL=13005;
 in declarator_list *)
(* Const before type ignored *)
(* error 
const int ERR_CTL_IS_NULL=13006;
 in declarator_list *)
(* Const before type ignored *)
(* error 
const int ERR_PATH_IS_NULL=13007;
 in declarator_list *)
(* Const before type ignored *)
(* error 
const int ERR_FILE_IS_NULL=13008;
 in declarator_list *)
(* Const before type ignored *)
(* error 
const int ERR_STRING_IS_NULL=13009;
 in declarator_list *)
(* Const before type ignored *)
(* error 
const int ERR_MODULE_NOT_PRESENT=13100;
 in declarator_list *)
(* Const before type ignored *)
(* error 
const int ERR_IMAGE_NOT_PRESENT=13101;
 in declarator_list *)
(* Const before type ignored *)
(* error 
const int ERR_RENDERER_NOT_PRESENT=13102;
 in declarator_list *)
(* Const before type ignored *)
(* error 
const int ERR_HEIGHTMAP_NOT_PRESENT=13103;
 in declarator_list *)
(* Const before type ignored *)
(* error 
const int ERR_HEIGHTMAPDESC_NOT_PRESENT=13104;
 in declarator_list *)
(* Const before type ignored *)
(* error 
const int ERR_ARRAY_IS_NULL_OR_EMPTY=13105;
 in declarator_list *)
(* Const before type ignored *)
(* error 
const int ERR_ARRAY_ELEM_LESS_EQUAL_ZERO=13106;
 in declarator_list *)
(* Const before type ignored *)
(* error 
const int ERR_MODULE_ALREADY_PRESENT=13200;
 in declarator_list *)
(* Const before type ignored *)
(* error 
const int ERR_IMAGE_ALREADY_PRESENT=13201;
 in declarator_list *)
(* Const before type ignored *)
(* error 
const int ERR_RENDERER_ALREADY_PRESENT=13202;
 in declarator_list *)
(* Const before type ignored *)
(* error 
const int ERR_HEIGHTMAP_ALREADY_PRESENT=13203;
 in declarator_list *)
(* Const before type ignored *)
(* error 
const int ERR_HEIGHTMAPDESC_ALREADY_PRESENT=13204;
 in declarator_list *)
(* Const before type ignored *)
(* error 
const int ERR_PREPARE_TEXTURE_INVALID_PARAM=13250;
 in declarator_list *)
(* Const before type ignored *)
(* error 
const int ERR_PREPARE_TEXTURE_NO_MODULE=13250;
 in declarator_list *)
(* Const before type ignored *)
(* error 
const int ERR_TEXTURE_OUT_OF_MEMORY=13990;
 in declarator_list *)
(* Const before type ignored *)
(* error 
const int ERR_TEXTURE_GENERIC=13999;
 in declarator_list *)
(* error 
    if (_sz == nullptr || _sz[0]=='\0' ) { errno = _err; return; }
in declaration at line 39 *)
(* error 
    if (_sz == nullptr || _sz[0]=='\0' ) { errno = _err; return; }
in declaration at line 39 *)
(* error 
    if (_sz == nullptr || _sz[0]=='\0' ) { errno = _err; return; }
in declaration at line 42 *)

      var
        _rv : return;cvar;public;
(* error 
    if (_sz == nullptr || _sz[0]=='\0' ) { errno = _err; return _rv; }
in declaration at line 45 *)
(* error 
    if (!_b) { errno = _err; return; }
in declaration at line 45 *)
(* error 
    if (!_b) { errno = _err; return; }
in declaration at line 57 *)
(* error 
char *  __declspec(dllexport) __stdcall createTexture(const char* pszJsonFile, const char * pszPath, const char *pszFilename);
(* error 
char *  __declspec(dllexport) __stdcall createTexture(const char* pszJsonFile, const char * pszPath, const char *pszFilename);
(* error 
char *  __declspec(dllexport) __stdcall createTexture(const char* pszJsonFile, const char * pszPath, const char *pszFilename);
 in declarator_list *)
 in declarator_list *)
 in declarator_list *)

    type
      PCTextureBuilder = ^CTextureBuilder;
      CTextureBuilder = pointer;

      PGradientInfoStruct = ^GradientInfoStruct;
      GradientInfoStruct = record
          position : double;
          red : longint;
          green : longint;
          blue : longint;
          alpha : longint;
        end;

      PCPoint = ^CPoint;
      CPoint = record
          x : double;
          y : double;
        end;
(* error 
CTextureBuilder __declspec(dllexport) __stdcall tb_new() ;
 in declarator_list *)

      var
 : CTextureBuilder;
(* error 
CTextureBuilder __declspec(dllexport) __stdcall tb_newWithParam( BOOLEAN bEarthlike,
(* error 
    const char * pszTexture1,
(* error 
    const char * pszTexture2,
(* error 
    double seaLevel,
(* error 
    double iceLevel) ;
 in declarator_list *)
 in declarator_list *)
 in declarator_list *)
 in declarator_list *)
 in declarator_list *)
(* error 
void __declspec(dllexport) __stdcall tb_free(CTextureBuilder ptr) ;
 in declarator_list *)
 : pointer;
(* error 
void __declspec(dllexport) __stdcall tb_setOutputFileName(CTextureBuilder ptr, const char* pszFilename) ;
(* error 
void __declspec(dllexport) __stdcall tb_setOutputFileName(CTextureBuilder ptr, const char* pszFilename) ;
 in declarator_list *)
 in declarator_list *)
(* error 
void __declspec(dllexport) __stdcall tb_buildTextureFromJson(CTextureBuilder ptr, const char* pszTpl, const char* pszPath ) ;
(* error 
void __declspec(dllexport) __stdcall tb_buildTextureFromJson(CTextureBuilder ptr, const char* pszTpl, const char* pszPath ) ;
(* error 
void __declspec(dllexport) __stdcall tb_buildTextureFromJson(CTextureBuilder ptr, const char* pszTpl, const char* pszPath ) ;
 in declarator_list *)
 in declarator_list *)
 in declarator_list *)
(* error 
void __declspec(dllexport) __stdcall tb_buildTextureFromJsonString(CTextureBuilder ptr, const char* pszData, const char* pszPath ) ;
(* error 
void __declspec(dllexport) __stdcall tb_buildTextureFromJsonString(CTextureBuilder ptr, const char* pszData, const char* pszPath ) ;
(* error 
void __declspec(dllexport) __stdcall tb_buildTextureFromJsonString(CTextureBuilder ptr, const char* pszData, const char* pszPath ) ;
 in declarator_list *)
 in declarator_list *)
 in declarator_list *)
(* error 
void __declspec(dllexport) __stdcall tb_setOutputName(CTextureBuilder ptr, const char* pszName );
(* error 
void __declspec(dllexport) __stdcall tb_setOutputName(CTextureBuilder ptr, const char* pszName );
 in declarator_list *)
 in declarator_list *)
(* error 
void __declspec(dllexport) __stdcall tb_renderTexture(CTextureBuilder ptr, const char* pszPath );
(* error 
void __declspec(dllexport) __stdcall tb_renderTexture(CTextureBuilder ptr, const char* pszPath );
 in declarator_list *)
 in declarator_list *)
(* error 
void __declspec(dllexport) __stdcall tb_setSize(CTextureBuilder ptr, int xSize, int ySize);
(* error 
void __declspec(dllexport) __stdcall tb_setSize(CTextureBuilder ptr, int xSize, int ySize);
(* error 
void __declspec(dllexport) __stdcall tb_setSize(CTextureBuilder ptr, int xSize, int ySize);
 in declarator_list *)
 in declarator_list *)
 in declarator_list *)
(* error 
void __declspec(dllexport) __stdcall tb_NewAbs (CTextureBuilder ptr, const char* szName, const char*  src1, BOOLEAN enableRandom );
(* error 
void __declspec(dllexport) __stdcall tb_NewAbs (CTextureBuilder ptr, const char* szName, const char*  src1, BOOLEAN enableRandom );
(* error 
void __declspec(dllexport) __stdcall tb_NewAbs (CTextureBuilder ptr, const char* szName, const char*  src1, BOOLEAN enableRandom );
(* error 
void __declspec(dllexport) __stdcall tb_NewAbs (CTextureBuilder ptr, const char* szName, const char*  src1, BOOLEAN enableRandom );
 in declarator_list *)
 in declarator_list *)
 in declarator_list *)
 in declarator_list *)
(* error 
void __declspec(dllexport) __stdcall tb_NewAdd (CTextureBuilder ptr, const char* szName,
(* error 
void __declspec(dllexport) __stdcall tb_NewAdd (CTextureBuilder ptr, const char* szName,
(* error 
    const char *   src1,
(* error 
    const char *   src2,
(* error 
    BOOLEAN enableRandom );
 in declarator_list *)
 in declarator_list *)
 in declarator_list *)
 in declarator_list *)
 in declarator_list *)
(* error 
void __declspec(dllexport) __stdcall tb_NewAvg (CTextureBuilder ptr, const char* szName,
(* error 
void __declspec(dllexport) __stdcall tb_NewAvg (CTextureBuilder ptr, const char* szName,
(* error 
    const char *   src1,
(* error 
    const char *   src2,
(* error 
    BOOLEAN enableRandom );
 in declarator_list *)
 in declarator_list *)
 in declarator_list *)
 in declarator_list *)
 in declarator_list *)
(* error 
void __declspec(dllexport) __stdcall tb_NewAvg4 (CTextureBuilder ptr, const char* szName,
(* error 
void __declspec(dllexport) __stdcall tb_NewAvg4 (CTextureBuilder ptr, const char* szName,
(* error 
    const char *   src1,
(* error 
    const char *   src2,
(* error 
    const char *   src3,
(* error 
    const char *   src4, BOOLEAN enableRandom );
(* error 
    const char *   src4, BOOLEAN enableRandom );
 in declarator_list *)
 in declarator_list *)
 in declarator_list *)
 in declarator_list *)
 in declarator_list *)
 in declarator_list *)
 in declarator_list *)
(* error 
void __declspec(dllexport) __stdcall tb_NewBillow (CTextureBuilder ptr, const char* szName, int seed,  double freq , double lac , double pers ,  int oct  ,  BOOLEAN enableRandom );
(* error 
void __declspec(dllexport) __stdcall tb_NewBillow (CTextureBuilder ptr, const char* szName, int seed,  double freq , double lac , double pers ,  int oct  ,  BOOLEAN enableRandom );
(* error 
void __declspec(dllexport) __stdcall tb_NewBillow (CTextureBuilder ptr, const char* szName, int seed,  double freq , double lac , double pers ,  int oct  ,  BOOLEAN enableRandom );
(* error 
void __declspec(dllexport) __stdcall tb_NewBillow (CTextureBuilder ptr, const char* szName, int seed,  double freq , double lac , double pers ,  int oct  ,  BOOLEAN enableRandom );
(* error 
void __declspec(dllexport) __stdcall tb_NewBillow (CTextureBuilder ptr, const char* szName, int seed,  double freq , double lac , double pers ,  int oct  ,  BOOLEAN enableRandom );
(* error 
void __declspec(dllexport) __stdcall tb_NewBillow (CTextureBuilder ptr, const char* szName, int seed,  double freq , double lac , double pers ,  int oct  ,  BOOLEAN enableRandom );
(* error 
void __declspec(dllexport) __stdcall tb_NewBillow (CTextureBuilder ptr, const char* szName, int seed,  double freq , double lac , double pers ,  int oct  ,  BOOLEAN enableRandom );
(* error 
void __declspec(dllexport) __stdcall tb_NewBillow (CTextureBuilder ptr, const char* szName, int seed,  double freq , double lac , double pers ,  int oct  ,  BOOLEAN enableRandom );
 in declarator_list *)
 in declarator_list *)
 in declarator_list *)
 in declarator_list *)
 in declarator_list *)
 in declarator_list *)
 in declarator_list *)
 in declarator_list *)
(* error 
void __declspec(dllexport) __stdcall tb_NewBlend (CTextureBuilder ptr, const char* szName, const char *   src1,
(* error 
void __declspec(dllexport) __stdcall tb_NewBlend (CTextureBuilder ptr, const char* szName, const char *   src1,
(* error 
void __declspec(dllexport) __stdcall tb_NewBlend (CTextureBuilder ptr, const char* szName, const char *   src1,
(* error 
    const char *   src2,
(* error 
    const char *   ctl, BOOLEAN enableRandom );
(* error 
    const char *   ctl, BOOLEAN enableRandom );
 in declarator_list *)
 in declarator_list *)
 in declarator_list *)
 in declarator_list *)
 in declarator_list *)
 in declarator_list *)
(* error 
void __declspec(dllexport) __stdcall tb_NewCheckerboard (CTextureBuilder ptr, const char* szName);
(* error 
void __declspec(dllexport) __stdcall tb_NewCheckerboard (CTextureBuilder ptr, const char* szName);
 in declarator_list *)
 in declarator_list *)
(* error 
void __declspec(dllexport) __stdcall tb_NewClamp (CTextureBuilder ptr, const char* szName, const char *   src1, double uBound, double lBound, BOOLEAN enableRandom );
(* error 
void __declspec(dllexport) __stdcall tb_NewClamp (CTextureBuilder ptr, const char* szName, const char *   src1, double uBound, double lBound, BOOLEAN enableRandom );
(* error 
void __declspec(dllexport) __stdcall tb_NewClamp (CTextureBuilder ptr, const char* szName, const char *   src1, double uBound, double lBound, BOOLEAN enableRandom );
(* error 
void __declspec(dllexport) __stdcall tb_NewClamp (CTextureBuilder ptr, const char* szName, const char *   src1, double uBound, double lBound, BOOLEAN enableRandom );
(* error 
void __declspec(dllexport) __stdcall tb_NewClamp (CTextureBuilder ptr, const char* szName, const char *   src1, double uBound, double lBound, BOOLEAN enableRandom );
(* error 
void __declspec(dllexport) __stdcall tb_NewClamp (CTextureBuilder ptr, const char* szName, const char *   src1, double uBound, double lBound, BOOLEAN enableRandom );
 in declarator_list *)
 in declarator_list *)
 in declarator_list *)
 in declarator_list *)
 in declarator_list *)
 in declarator_list *)
(* error 
void __declspec(dllexport) __stdcall tb_NewConst (CTextureBuilder ptr, const char* szName, double value,BOOLEAN enableRandom );
(* error 
void __declspec(dllexport) __stdcall tb_NewConst (CTextureBuilder ptr, const char* szName, double value,BOOLEAN enableRandom );
(* error 
void __declspec(dllexport) __stdcall tb_NewConst (CTextureBuilder ptr, const char* szName, double value,BOOLEAN enableRandom );
(* error 
void __declspec(dllexport) __stdcall tb_NewConst (CTextureBuilder ptr, const char* szName, double value,BOOLEAN enableRandom );
 in declarator_list *)
 in declarator_list *)
 in declarator_list *)
 in declarator_list *)
(* error 
void __declspec(dllexport) __stdcall tb_NewCos (CTextureBuilder ptr, const char* szName, const char *   src1, double freq, double exp, double value,
(* error 
void __declspec(dllexport) __stdcall tb_NewCos (CTextureBuilder ptr, const char* szName, const char *   src1, double freq, double exp, double value,
(* error 
void __declspec(dllexport) __stdcall tb_NewCos (CTextureBuilder ptr, const char* szName, const char *   src1, double freq, double exp, double value,
(* error 
void __declspec(dllexport) __stdcall tb_NewCos (CTextureBuilder ptr, const char* szName, const char *   src1, double freq, double exp, double value,
(* error 
void __declspec(dllexport) __stdcall tb_NewCos (CTextureBuilder ptr, const char* szName, const char *   src1, double freq, double exp, double value,
(* error 
void __declspec(dllexport) __stdcall tb_NewCos (CTextureBuilder ptr, const char* szName, const char *   src1, double freq, double exp, double value,
(* error 
    BOOLEAN enableRandom );
 in declarator_list *)
 in declarator_list *)
 in declarator_list *)
 in declarator_list *)
 in declarator_list *)
 in declarator_list *)
 in declarator_list *)
(* error 
void __declspec(dllexport) __stdcall tb_NewCache (CTextureBuilder ptr, const char* szName, const char *   src1);
(* error 
void __declspec(dllexport) __stdcall tb_NewCache (CTextureBuilder ptr, const char* szName, const char *   src1);
(* error 
void __declspec(dllexport) __stdcall tb_NewCache (CTextureBuilder ptr, const char* szName, const char *   src1);
 in declarator_list *)
 in declarator_list *)
 in declarator_list *)
(* error 
void __declspec(dllexport) __stdcall tb_NewCurve (CTextureBuilder ptr, const char* szName, const char *   src1,
(* error 
void __declspec(dllexport) __stdcall tb_NewCurve (CTextureBuilder ptr, const char* szName, const char *   src1,
(* error 
void __declspec(dllexport) __stdcall tb_NewCurve (CTextureBuilder ptr, const char* szName, const char *   src1,
(* error 
    CPOINT *cPoints,
(* error 
    int numPoints,
(* error 
    BOOLEAN enableRandom );
 in declarator_list *)
 in declarator_list *)
 in declarator_list *)
 in declarator_list *)
 in declarator_list *)
 in declarator_list *)
(* error 
void __declspec(dllexport) __stdcall tb_NewCylinders (CTextureBuilder ptr, const char* szName, double freq, BOOLEAN enableRandom );
(* error 
void __declspec(dllexport) __stdcall tb_NewCylinders (CTextureBuilder ptr, const char* szName, double freq, BOOLEAN enableRandom );
(* error 
void __declspec(dllexport) __stdcall tb_NewCylinders (CTextureBuilder ptr, const char* szName, double freq, BOOLEAN enableRandom );
(* error 
void __declspec(dllexport) __stdcall tb_NewCylinders (CTextureBuilder ptr, const char* szName, double freq, BOOLEAN enableRandom );
 in declarator_list *)
 in declarator_list *)
 in declarator_list *)
 in declarator_list *)
(* error 
void __declspec(dllexport) __stdcall tb_NewDisplace (CTextureBuilder ptr, const char* szName, const char *   src1,
(* error 
void __declspec(dllexport) __stdcall tb_NewDisplace (CTextureBuilder ptr, const char* szName, const char *   src1,
(* error 
void __declspec(dllexport) __stdcall tb_NewDisplace (CTextureBuilder ptr, const char* szName, const char *   src1,
(* error 
    const char *   src2,
(* error 
    const char *   src3,
(* error 
    const char *  src4,
(* error 
    const char *   ctl, BOOLEAN enableRandom );
(* error 
    const char *   ctl, BOOLEAN enableRandom );
 in declarator_list *)
 in declarator_list *)
 in declarator_list *)
 in declarator_list *)
 in declarator_list *)
 in declarator_list *)
 in declarator_list *)
 in declarator_list *)
(* error 
void __declspec(dllexport) __stdcall tb_NewExponent (CTextureBuilder ptr, const char* szName, const char *   src1, double exp,
(* error 
void __declspec(dllexport) __stdcall tb_NewExponent (CTextureBuilder ptr, const char* szName, const char *   src1, double exp,
(* error 
void __declspec(dllexport) __stdcall tb_NewExponent (CTextureBuilder ptr, const char* szName, const char *   src1, double exp,
(* error 
void __declspec(dllexport) __stdcall tb_NewExponent (CTextureBuilder ptr, const char* szName, const char *   src1, double exp,
(* error 
    BOOLEAN enableRandom );
 in declarator_list *)
 in declarator_list *)
 in declarator_list *)
 in declarator_list *)
 in declarator_list *)
(* error 
void __declspec(dllexport) __stdcall tb_NewInvert (CTextureBuilder ptr, const char* szName, const char *   src1,
(* error 
void __declspec(dllexport) __stdcall tb_NewInvert (CTextureBuilder ptr, const char* szName, const char *   src1,
(* error 
void __declspec(dllexport) __stdcall tb_NewInvert (CTextureBuilder ptr, const char* szName, const char *   src1,
(* error 
    BOOLEAN enableRandom );
 in declarator_list *)
 in declarator_list *)
 in declarator_list *)
 in declarator_list *)
(* error 
void __declspec(dllexport) __stdcall tb_NewMax (CTextureBuilder ptr, const char* szName, const char *   src1,
(* error 
void __declspec(dllexport) __stdcall tb_NewMax (CTextureBuilder ptr, const char* szName, const char *   src1,
(* error 
void __declspec(dllexport) __stdcall tb_NewMax (CTextureBuilder ptr, const char* szName, const char *   src1,
(* error 
                                                const char *   src2, BOOLEAN enableRandom );
(* error 
                                                const char *   src2, BOOLEAN enableRandom );
 in declarator_list *)
 in declarator_list *)
 in declarator_list *)
 in declarator_list *)
 in declarator_list *)
(* error 
void __declspec(dllexport) __stdcall tb_NewMin (CTextureBuilder ptr, const char* szName, const char *   src1, const char *   src2, BOOLEAN enableRandom );
(* error 
void __declspec(dllexport) __stdcall tb_NewMin (CTextureBuilder ptr, const char* szName, const char *   src1, const char *   src2, BOOLEAN enableRandom );
(* error 
void __declspec(dllexport) __stdcall tb_NewMin (CTextureBuilder ptr, const char* szName, const char *   src1, const char *   src2, BOOLEAN enableRandom );
(* error 
void __declspec(dllexport) __stdcall tb_NewMin (CTextureBuilder ptr, const char* szName, const char *   src1, const char *   src2, BOOLEAN enableRandom );
(* error 
void __declspec(dllexport) __stdcall tb_NewMin (CTextureBuilder ptr, const char* szName, const char *   src1, const char *   src2, BOOLEAN enableRandom );
 in declarator_list *)
 in declarator_list *)
 in declarator_list *)
 in declarator_list *)
 in declarator_list *)
(* error 
void __declspec(dllexport) __stdcall tb_NewMultiply (CTextureBuilder ptr, const char* szName,
(* error 
void __declspec(dllexport) __stdcall tb_NewMultiply (CTextureBuilder ptr, const char* szName,
(* error 
                                                     const char *   src1, const char *   src2, BOOLEAN enableRandom );
(* error 
                                                     const char *   src1, const char *   src2, BOOLEAN enableRandom );
(* error 
                                                     const char *   src1, const char *   src2, BOOLEAN enableRandom );
 in declarator_list *)
 in declarator_list *)
 in declarator_list *)
 in declarator_list *)
 in declarator_list *)
(* error 
void __declspec(dllexport) __stdcall tb_NewPower (CTextureBuilder ptr, const char* szName, const char *
(* error 
void __declspec(dllexport) __stdcall tb_NewPower (CTextureBuilder ptr, const char* szName, const char *
(* error 
void __declspec(dllexport) __stdcall tb_NewPower (CTextureBuilder ptr, const char* szName, const char *
(* error 
                                                  src1, const char *   src2, BOOLEAN enableRandom );
(* error 
                                                  src1, const char *   src2, BOOLEAN enableRandom );
 in declarator_list *)
 in declarator_list *)
 in declarator_list *)
 in declarator_list *)
 in declarator_list *)
(* error 
void __declspec(dllexport) __stdcall tb_NewPerlin (CTextureBuilder ptr, const char* szName, int seed,  double freq , double lac , double pers ,
(* error 
void __declspec(dllexport) __stdcall tb_NewPerlin (CTextureBuilder ptr, const char* szName, int seed,  double freq , double lac , double pers ,
(* error 
void __declspec(dllexport) __stdcall tb_NewPerlin (CTextureBuilder ptr, const char* szName, int seed,  double freq , double lac , double pers ,
(* error 
void __declspec(dllexport) __stdcall tb_NewPerlin (CTextureBuilder ptr, const char* szName, int seed,  double freq , double lac , double pers ,
(* error 
void __declspec(dllexport) __stdcall tb_NewPerlin (CTextureBuilder ptr, const char* szName, int seed,  double freq , double lac , double pers ,
(* error 
void __declspec(dllexport) __stdcall tb_NewPerlin (CTextureBuilder ptr, const char* szName, int seed,  double freq , double lac , double pers ,
(* error 
    int oct  ,  BOOLEAN enableRandom );
(* error 
    int oct  ,  BOOLEAN enableRandom );
 in declarator_list *)
 in declarator_list *)
 in declarator_list *)
 in declarator_list *)
 in declarator_list *)
 in declarator_list *)
 in declarator_list *)
 in declarator_list *)
(* error 
void __declspec(dllexport) __stdcall tb_NewRidgedMulti (CTextureBuilder ptr, const char* szName, int seed,  double freq , double lac ,
(* error 
void __declspec(dllexport) __stdcall tb_NewRidgedMulti (CTextureBuilder ptr, const char* szName, int seed,  double freq , double lac ,
(* error 
void __declspec(dllexport) __stdcall tb_NewRidgedMulti (CTextureBuilder ptr, const char* szName, int seed,  double freq , double lac ,
(* error 
void __declspec(dllexport) __stdcall tb_NewRidgedMulti (CTextureBuilder ptr, const char* szName, int seed,  double freq , double lac ,
(* error 
void __declspec(dllexport) __stdcall tb_NewRidgedMulti (CTextureBuilder ptr, const char* szName, int seed,  double freq , double lac ,
(* error 
    int oct  ,  BOOLEAN enableRandom );
(* error 
    int oct  ,  BOOLEAN enableRandom );
 in declarator_list *)
 in declarator_list *)
 in declarator_list *)
 in declarator_list *)
 in declarator_list *)
 in declarator_list *)
 in declarator_list *)
(* error 
void __declspec(dllexport) __stdcall tb_NewRidgedMulti2 (CTextureBuilder ptr, const char* szName, int seed,  double freq , double lac ,
(* error 
void __declspec(dllexport) __stdcall tb_NewRidgedMulti2 (CTextureBuilder ptr, const char* szName, int seed,  double freq , double lac ,
(* error 
void __declspec(dllexport) __stdcall tb_NewRidgedMulti2 (CTextureBuilder ptr, const char* szName, int seed,  double freq , double lac ,
(* error 
void __declspec(dllexport) __stdcall tb_NewRidgedMulti2 (CTextureBuilder ptr, const char* szName, int seed,  double freq , double lac ,
(* error 
void __declspec(dllexport) __stdcall tb_NewRidgedMulti2 (CTextureBuilder ptr, const char* szName, int seed,  double freq , double lac ,
(* error 
    double gain, double exp, int oct,double offset, BOOLEAN enableRandom );
(* error 
    double gain, double exp, int oct,double offset, BOOLEAN enableRandom );
(* error 
    double gain, double exp, int oct,double offset, BOOLEAN enableRandom );
(* error 
    double gain, double exp, int oct,double offset, BOOLEAN enableRandom );
(* error 
    double gain, double exp, int oct,double offset, BOOLEAN enableRandom );
 in declarator_list *)
 in declarator_list *)
 in declarator_list *)
 in declarator_list *)
 in declarator_list *)
 in declarator_list *)
 in declarator_list *)
 in declarator_list *)
 in declarator_list *)
 in declarator_list *)
(* error 
void __declspec(dllexport) __stdcall tb_NewRotatePoint (CTextureBuilder ptr, const char* szName, const char *   src1,
(* error 
void __declspec(dllexport) __stdcall tb_NewRotatePoint (CTextureBuilder ptr, const char* szName, const char *   src1,
(* error 
void __declspec(dllexport) __stdcall tb_NewRotatePoint (CTextureBuilder ptr, const char* szName, const char *   src1,
(* error 
    double x, double y, double z, BOOLEAN enableRandom );
(* error 
    double x, double y, double z, BOOLEAN enableRandom );
(* error 
    double x, double y, double z, BOOLEAN enableRandom );
(* error 
    double x, double y, double z, BOOLEAN enableRandom );
 in declarator_list *)
 in declarator_list *)
 in declarator_list *)
 in declarator_list *)
 in declarator_list *)
 in declarator_list *)
 in declarator_list *)
(* error 
void __declspec(dllexport) __stdcall tb_NewScaleBias (CTextureBuilder ptr, const char* szName, const char *   src1,
(* error 
void __declspec(dllexport) __stdcall tb_NewScaleBias (CTextureBuilder ptr, const char* szName, const char *   src1,
(* error 
void __declspec(dllexport) __stdcall tb_NewScaleBias (CTextureBuilder ptr, const char* szName, const char *   src1,
(* error 
    double scale, double bias, BOOLEAN enableRandom );
(* error 
    double scale, double bias, BOOLEAN enableRandom );
(* error 
    double scale, double bias, BOOLEAN enableRandom );
 in declarator_list *)
 in declarator_list *)
 in declarator_list *)
 in declarator_list *)
 in declarator_list *)
 in declarator_list *)
(* error 
void __declspec(dllexport) __stdcall tb_NewSin (CTextureBuilder ptr, const char* szName, const char *   src1, double freq, double exp, double value,
(* error 
void __declspec(dllexport) __stdcall tb_NewSin (CTextureBuilder ptr, const char* szName, const char *   src1, double freq, double exp, double value,
(* error 
void __declspec(dllexport) __stdcall tb_NewSin (CTextureBuilder ptr, const char* szName, const char *   src1, double freq, double exp, double value,
(* error 
void __declspec(dllexport) __stdcall tb_NewSin (CTextureBuilder ptr, const char* szName, const char *   src1, double freq, double exp, double value,
(* error 
void __declspec(dllexport) __stdcall tb_NewSin (CTextureBuilder ptr, const char* szName, const char *   src1, double freq, double exp, double value,
(* error 
void __declspec(dllexport) __stdcall tb_NewSin (CTextureBuilder ptr, const char* szName, const char *   src1, double freq, double exp, double value,
(* error 
    BOOLEAN enableRandom );
 in declarator_list *)
 in declarator_list *)
 in declarator_list *)
 in declarator_list *)
 in declarator_list *)
 in declarator_list *)
 in declarator_list *)
(* error 
void __declspec(dllexport) __stdcall tb_NewSpheres (CTextureBuilder ptr, const char* szName,  double freq, BOOLEAN enableRandom );
(* error 
void __declspec(dllexport) __stdcall tb_NewSpheres (CTextureBuilder ptr, const char* szName,  double freq, BOOLEAN enableRandom );
(* error 
void __declspec(dllexport) __stdcall tb_NewSpheres (CTextureBuilder ptr, const char* szName,  double freq, BOOLEAN enableRandom );
(* error 
void __declspec(dllexport) __stdcall tb_NewSpheres (CTextureBuilder ptr, const char* szName,  double freq, BOOLEAN enableRandom );
 in declarator_list *)
 in declarator_list *)
 in declarator_list *)
 in declarator_list *)
(* error 
void __declspec(dllexport) __stdcall tb_NewTranslatePoint (CTextureBuilder ptr, const char* szName, const char *   src1,
(* error 
void __declspec(dllexport) __stdcall tb_NewTranslatePoint (CTextureBuilder ptr, const char* szName, const char *   src1,
(* error 
void __declspec(dllexport) __stdcall tb_NewTranslatePoint (CTextureBuilder ptr, const char* szName, const char *   src1,
(* error 
    double x, double y, double z, BOOLEAN enableRandom );
(* error 
    double x, double y, double z, BOOLEAN enableRandom );
(* error 
    double x, double y, double z, BOOLEAN enableRandom );
(* error 
    double x, double y, double z, BOOLEAN enableRandom );
 in declarator_list *)
 in declarator_list *)
 in declarator_list *)
 in declarator_list *)
 in declarator_list *)
 in declarator_list *)
 in declarator_list *)
(* error 
void __declspec(dllexport) __stdcall tb_NewScalePoint (CTextureBuilder ptr, const char* szName, const char *   src1,
(* error 
void __declspec(dllexport) __stdcall tb_NewScalePoint (CTextureBuilder ptr, const char* szName, const char *   src1,
(* error 
void __declspec(dllexport) __stdcall tb_NewScalePoint (CTextureBuilder ptr, const char* szName, const char *   src1,
(* error 
    double x, double y, double z, BOOLEAN enableRandom );
(* error 
    double x, double y, double z, BOOLEAN enableRandom );
(* error 
    double x, double y, double z, BOOLEAN enableRandom );
(* error 
    double x, double y, double z, BOOLEAN enableRandom );
 in declarator_list *)
 in declarator_list *)
 in declarator_list *)
 in declarator_list *)
 in declarator_list *)
 in declarator_list *)
 in declarator_list *)
(* error 
void __declspec(dllexport) __stdcall tb_NewTurbulence (CTextureBuilder ptr, const char* szName, const char *   src1,
(* error 
void __declspec(dllexport) __stdcall tb_NewTurbulence (CTextureBuilder ptr, const char* szName, const char *   src1,
(* error 
void __declspec(dllexport) __stdcall tb_NewTurbulence (CTextureBuilder ptr, const char* szName, const char *   src1,
(* error 
    int seed , double freq , double pow , double rough , BOOLEAN enableRandom );
(* error 
    int seed , double freq , double pow , double rough , BOOLEAN enableRandom );
(* error 
    int seed , double freq , double pow , double rough , BOOLEAN enableRandom );
(* error 
    int seed , double freq , double pow , double rough , BOOLEAN enableRandom );
(* error 
    int seed , double freq , double pow , double rough , BOOLEAN enableRandom );
 in declarator_list *)
 in declarator_list *)
 in declarator_list *)
 in declarator_list *)
 in declarator_list *)
 in declarator_list *)
 in declarator_list *)
 in declarator_list *)
(* error 
void __declspec(dllexport) __stdcall tb_NewTurbulence2 (CTextureBuilder ptr, const char* szName, const char *   src1,
(* error 
void __declspec(dllexport) __stdcall tb_NewTurbulence2 (CTextureBuilder ptr, const char* szName, const char *   src1,
(* error 
void __declspec(dllexport) __stdcall tb_NewTurbulence2 (CTextureBuilder ptr, const char* szName, const char *   src1,
(* error 
    int seed , double freq , double pow , double rough , BOOLEAN enableRandom );
(* error 
    int seed , double freq , double pow , double rough , BOOLEAN enableRandom );
(* error 
    int seed , double freq , double pow , double rough , BOOLEAN enableRandom );
(* error 
    int seed , double freq , double pow , double rough , BOOLEAN enableRandom );
(* error 
    int seed , double freq , double pow , double rough , BOOLEAN enableRandom );
 in declarator_list *)
 in declarator_list *)
 in declarator_list *)
 in declarator_list *)
 in declarator_list *)
 in declarator_list *)
 in declarator_list *)
 in declarator_list *)
(* error 
void __declspec(dllexport) __stdcall tb_NewTurbulenceBillow (CTextureBuilder ptr, const char* szName, const char *   src1,
(* error 
void __declspec(dllexport) __stdcall tb_NewTurbulenceBillow (CTextureBuilder ptr, const char* szName, const char *   src1,
(* error 
void __declspec(dllexport) __stdcall tb_NewTurbulenceBillow (CTextureBuilder ptr, const char* szName, const char *   src1,
(* error 
    int seed , double freq , double pow , double rough , BOOLEAN enableRandom );
(* error 
    int seed , double freq , double pow , double rough , BOOLEAN enableRandom );
(* error 
    int seed , double freq , double pow , double rough , BOOLEAN enableRandom );
(* error 
    int seed , double freq , double pow , double rough , BOOLEAN enableRandom );
(* error 
    int seed , double freq , double pow , double rough , BOOLEAN enableRandom );
 in declarator_list *)
 in declarator_list *)
 in declarator_list *)
 in declarator_list *)
 in declarator_list *)
 in declarator_list *)
 in declarator_list *)
 in declarator_list *)
(* error 
void __declspec(dllexport) __stdcall tb_NewTurbulenceRidged (CTextureBuilder ptr, const char* szName, const char *   src1,
(* error 
void __declspec(dllexport) __stdcall tb_NewTurbulenceRidged (CTextureBuilder ptr, const char* szName, const char *   src1,
(* error 
void __declspec(dllexport) __stdcall tb_NewTurbulenceRidged (CTextureBuilder ptr, const char* szName, const char *   src1,
(* error 
    int seed , double freq , double pow , double rough , BOOLEAN enableRandom );
(* error 
    int seed , double freq , double pow , double rough , BOOLEAN enableRandom );
(* error 
    int seed , double freq , double pow , double rough , BOOLEAN enableRandom );
(* error 
    int seed , double freq , double pow , double rough , BOOLEAN enableRandom );
(* error 
    int seed , double freq , double pow , double rough , BOOLEAN enableRandom );
 in declarator_list *)
 in declarator_list *)
 in declarator_list *)
 in declarator_list *)
 in declarator_list *)
 in declarator_list *)
 in declarator_list *)
 in declarator_list *)
(* error 
void __declspec(dllexport) __stdcall tb_NewVoronoi (CTextureBuilder ptr, const char* szName,
(* error 
void __declspec(dllexport) __stdcall tb_NewVoronoi (CTextureBuilder ptr, const char* szName,
(* error 
    int seed , double freq , double displ , BOOLEAN enableDispl , BOOLEAN enableRandom );
(* error 
    int seed , double freq , double displ , BOOLEAN enableDispl , BOOLEAN enableRandom );
(* error 
    int seed , double freq , double displ , BOOLEAN enableDispl , BOOLEAN enableRandom );
(* error 
    int seed , double freq , double displ , BOOLEAN enableDispl , BOOLEAN enableRandom );
(* error 
    int seed , double freq , double displ , BOOLEAN enableDispl , BOOLEAN enableRandom );
 in declarator_list *)
 in declarator_list *)
 in declarator_list *)
 in declarator_list *)
 in declarator_list *)
 in declarator_list *)
 in declarator_list *)
(* error 
void __declspec(dllexport) __stdcall tb_NewTerrace (CTextureBuilder ptr, const char* szName, const char *   src1,
(* error 
void __declspec(dllexport) __stdcall tb_NewTerrace (CTextureBuilder ptr, const char* szName, const char *   src1,
(* error 
void __declspec(dllexport) __stdcall tb_NewTerrace (CTextureBuilder ptr, const char* szName, const char *   src1,
(* error 
     CPOINT *cPoints, int numPoints, BOOLEAN invert, BOOLEAN enableRandom );
(* error 
     CPOINT *cPoints, int numPoints, BOOLEAN invert, BOOLEAN enableRandom );
(* error 
     CPOINT *cPoints, int numPoints, BOOLEAN invert, BOOLEAN enableRandom );
(* error 
     CPOINT *cPoints, int numPoints, BOOLEAN invert, BOOLEAN enableRandom );
 in declarator_list *)
 in declarator_list *)
 in declarator_list *)
 in declarator_list *)
 in declarator_list *)
 in declarator_list *)
 in declarator_list *)
(* error 
void __declspec(dllexport) __stdcall tb_createHeightMapDescriptor(CTextureBuilder ptr, const char* szName);
(* error 
void __declspec(dllexport) __stdcall tb_createHeightMapDescriptor(CTextureBuilder ptr, const char* szName);
 in declarator_list *)
 in declarator_list *)
(* error 
void __declspec(dllexport) __stdcall tb_createImageDescriptor(CTextureBuilder ptr, const char* szName);
(* error 
void __declspec(dllexport) __stdcall tb_createImageDescriptor(CTextureBuilder ptr, const char* szName);
 in declarator_list *)
 in declarator_list *)
(* error 
void __declspec(dllexport) __stdcall tb_createNoiseMapBuilderDescriptor(CTextureBuilder ptr, const char* szName, const char *   module, const char*  heightMap);
(* error 
void __declspec(dllexport) __stdcall tb_createNoiseMapBuilderDescriptor(CTextureBuilder ptr, const char* szName, const char *   module, const char*  heightMap);
(* error 
void __declspec(dllexport) __stdcall tb_createNoiseMapBuilderDescriptor(CTextureBuilder ptr, const char* szName, const char *   module, const char*  heightMap);
(* error 
void __declspec(dllexport) __stdcall tb_createNoiseMapBuilderDescriptor(CTextureBuilder ptr, const char* szName, const char *   module, const char*  heightMap);
 in declarator_list *)
 in declarator_list *)
 in declarator_list *)
 in declarator_list *)
(* error 
void __declspec(dllexport) __stdcall tb_createRendererDescriptor (
(* error 
    const char* szName,
(* error 
    const char *heightMap ,
(* error 
    GRADIENTINFOSTRUCT *gradientInfo,
(* error 
    int numGradientInfo,
(* error 
    const char *destImage,
(* error 
    const char *backgroundImage ,
(* error 
    const char *alphaImage  ,
(* error 
    const char *bumpMap ,
(* error 
    double lightBrightness ,
(* error 
    double lightContrast ,
(* error 
    BOOLEAN enabledLight ,
(* error 
    BOOLEAN randomGradient ,
(* error 
    int bgRed,
(* error 
    int bgGreen,
(* error 
    int bgBlue,
(* error 
    int bgAlpha);
 in declarator_list *)
 in declarator_list *)
 in declarator_list *)
 in declarator_list *)
 in declarator_list *)
 in declarator_list *)
 in declarator_list *)
 in declarator_list *)
 in declarator_list *)
 in declarator_list *)
 in declarator_list *)
 in declarator_list *)
 in declarator_list *)
 in declarator_list *)
 in declarator_list *)
 in declarator_list *)
 in declarator_list *)
(* error 
int __declspec(dllexport) tb_HasModule(CTextureBuilder ptr, const char *szName) ;
(* error 
int __declspec(dllexport) tb_HasModule(CTextureBuilder ptr, const char *szName) ;
 in declarator_list *)
 in declarator_list *)
(* error 
int __declspec(dllexport) tb_HasRenderer(CTextureBuilder ptr, const char *szName) ;
(* error 
int __declspec(dllexport) tb_HasRenderer(CTextureBuilder ptr, const char *szName) ;
 in declarator_list *)
 in declarator_list *)
(* error 
int __declspec(dllexport) tb_HasImage(CTextureBuilder ptr, const char *szName) ;
(* error 
int __declspec(dllexport) tb_HasImage(CTextureBuilder ptr, const char *szName) ;
 in declarator_list *)
 in declarator_list *)
(* error 
int __declspec(dllexport) tb_HasHeightMap(CTextureBuilder ptr, const char *szName);
(* error 
int __declspec(dllexport) tb_HasHeightMap(CTextureBuilder ptr, const char *szName);
 in declarator_list *)
 in declarator_list *)
(* error 
int __declspec(dllexport) tb_HasHeightMapBuilder(CTextureBuilder ptr, const char *szName);
(* error 
int __declspec(dllexport) tb_HasHeightMapBuilder(CTextureBuilder ptr, const char *szName);
 in declarator_list *)
 in declarator_list *)
(* error 
void __declspec(dllexport)tb_AddRandomFactors(CTextureBuilder ptr, const double *iRandomFactors, int numFactors );
(* error 
void __declspec(dllexport)tb_AddRandomFactors(CTextureBuilder ptr, const double *iRandomFactors, int numFactors );
(* error 
void __declspec(dllexport)tb_AddRandomFactors(CTextureBuilder ptr, const double *iRandomFactors, int numFactors );
 in declarator_list *)
 in declarator_list *)
 in declarator_list *)
(* error 
void __declspec(dllexport) tb_PrepareTexture(CTextureBuilder ptr);
 in declarator_list *)
 : pointer;
(* error 
void __declspec(dllexport) tb_WriteTextureToPath(CTextureBuilder ptr, const char* szPath);
(* error 
void __declspec(dllexport) tb_WriteTextureToPath(CTextureBuilder ptr, const char* szPath);
 in declarator_list *)
 in declarator_list *)
(* error 
void __declspec(dllexport) tb_LoadTextureFromJsonFile(CTextureBuilder ptr, const char* szPath);
(* error 
void __declspec(dllexport) tb_LoadTextureFromJsonFile(CTextureBuilder ptr, const char* szPath);
 in declarator_list *)
 in declarator_list *)
(* error 
void __declspec(dllexport) tb_LoadTextureFromJsonString(CTextureBuilder ptr, const char* szString);
(* error 
void __declspec(dllexport) tb_LoadTextureFromJsonString(CTextureBuilder ptr, const char* szString);
 in declarator_list *)
 in declarator_list *)
(* error 
char * __declspec(dllexport) tb_ToJsonString(CTextureBuilder ptr);
 in declarator_list *)
 : char;
{$endif}
    { LIBNOISE_H }

implementation


end.
