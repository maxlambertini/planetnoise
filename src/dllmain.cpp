
#include <texturebuilder/texturebuilder.h>
#include <iostream>
#include <fstream>
#include <tuple>
#include <texturebuilder/texturebuilder.h>
#include <iostream>
#include <fstream>
#include <tuple>
extern "C" {
#include "dllmain.h"
}
#include "presets.h"

using namespace std;
using namespace nlohmann;

extern "C" // only required if using g++
{
void EXPORT  createParamTexture(
    BOOLEAN bEarthlike,
    const char * pszTexture1,
    const char * pszTexture2,
    double seaLevel,
    double iceLevel,
    int width,
    const char * pszPath,
    const char *pszFilename,
    char *szRes)
{
    CHECK_SZ(pszTexture1,ERR_STRING_IS_NULL)
    CHECK_SZ(pszTexture2,ERR_STRING_IS_NULL)
    CHECK_SZ(pszPath,ERR_STRING_IS_NULL)
    CHECK_SZ(pszFilename,ERR_STRING_IS_NULL)
    srand (time(NULL));
    auto tex1 = std::string(pszTexture1);
    auto tex2 = std::string(pszTexture2);
    auto path = std::string(pszPath);
    auto filename = std::string(pszFilename);
    TextureBuilder tb;
    tb.initialize((bool)bEarthlike, tex1, tex2, seaLevel, iceLevel);
    tb.setSize(width, width/2);
    json j;
    tb.toJson(j);
    string tpl = path+"/"+filename+".texjson";
    ofstream o(tpl);
    o << setw(4) << j << endl;
    tb.setOutputFolder(path);
    tb.setOutputFileName(filename);
    tb.buildTextureFromJson(tpl, path);
    std::string result = filename + "|" + tb.colorMap() + "|" + tb.bumpMap() + "|"  + tb.reflectionMap() + "|" + tb.cloudMap() + "|";
    //result.copy(szRes, result.length(), 0);
    return;
}

char * EXPORT  createTexture(const char* pszJsonFile, const char * pszPath, const char *pszFilename)
{
    srand (time(NULL));
    CHECK_SZ_RET(pszJsonFile,ERR_STRING_IS_NULL, nullptr)
    CHECK_SZ_RET(pszPath,ERR_STRING_IS_NULL, nullptr)
    CHECK_SZ_RET(pszFilename,ERR_STRING_IS_NULL,  nullptr)


    auto tpl = std::string(pszJsonFile);
    auto path = std::string(pszPath);
    auto filename = std::string(pszFilename);
    TextureBuilder tb = TextureBuilder{};
    tb.setOutputFileName(filename);
    tb.buildTextureFromJson(tpl, path);
    std::string result = filename + "|" + tb.colorMap() + "|" + tb.bumpMap() + "|"  + tb.reflectionMap() + "|" + tb.cloudMap() + "|";

    char *szRes = (char *) malloc(sizeof(char *)*result.length()+1);
    memset((void *)szRes, 0,sizeof(char *)*result.length()+1 );


    result.copy(szRes, result.length(), 0);
    return szRes;
}


#ifdef _WIN32
BOOLEAN EXPORT   DllMain(HANDLE hModule, DWORD  ul_reason_for_call, LPVOID lpReserved)
{
    switch (ul_reason_for_call)
    {
    case DLL_PROCESS_ATTACH:
        break;
    case DLL_THREAD_ATTACH:
        break;
    case DLL_THREAD_DETACH:
        break;
    case DLL_PROCESS_DETACH:
        break;
    }
    return TRUE;
}
#endif

typedef void* CTextureBuilder;


CTextureBuilder EXPORT  tb_new()
{
    return reinterpret_cast<CTextureBuilder>(new TextureBuilder{});    
}



CTextureBuilder EXPORT  tb_newWithParam( BOOLEAN bEarthlike,
    const char * pszTexture1,
    const char * pszTexture2,
    double seaLevel,
    double iceLevel)
{
    CHECK_SZ_RET(pszTexture1,ERR_STRING_IS_NULL, nullptr)
    CHECK_SZ_RET(pszTexture2,ERR_STRING_IS_NULL, nullptr)

    auto tex1 = std::string(pszTexture1);
    auto tex2 = std::string(pszTexture2);
    TextureBuilder* tb = new TextureBuilder();
    tb->initialize(bEarthlike, tex1, tex2, seaLevel, iceLevel);
    return reinterpret_cast<CTextureBuilder>(tb);
}


void EXPORT  tb_free(CTextureBuilder ptr)
{
    delete reinterpret_cast<TextureBuilder *>(ptr);
}

void EXPORT  tb_setOutputFileName(CTextureBuilder ptr, const char* pszFilename)
{
    CHECK_SZ(pszFilename,ERR_STRING_IS_NULL)

    auto tb = reinterpret_cast<TextureBuilder *>(ptr);
    auto filename = std::string(pszFilename);
    tb->setOutputFileName(filename);

}

void EXPORT  tb_buildTextureFromJson(CTextureBuilder ptr, const char* pszTpl, const char* pszPath )
{
    CHECK_SZ(pszTpl,ERR_STRING_IS_NULL)
    CHECK_SZ(pszPath,ERR_STRING_IS_NULL)

    auto tb = reinterpret_cast<TextureBuilder *>(ptr);
    auto tpl = std::string(pszTpl);
    auto path = std::string(pszPath);
    tb->buildTextureFromJson(tpl, path);
}

void EXPORT  tb_buildTextureFromJsonString(CTextureBuilder ptr, const char* pszData, const char* pszPath )
{
    CHECK_SZ(pszData,ERR_STRING_IS_NULL)
    CHECK_SZ(pszPath,ERR_STRING_IS_NULL)
    auto tb = reinterpret_cast<TextureBuilder *>(ptr);
    auto data = std::string(pszData);
    auto path = std::string(pszPath);
    tb->buildTextureFromJsonString(data, path);
}

void EXPORT  tb_setOutputName(CTextureBuilder ptr, const char* pszName )
{
    CHECK_SZ(pszName,ERR_STRING_IS_NULL)
    auto tb = reinterpret_cast<TextureBuilder *>(ptr);
    auto data = std::string(pszName);
    tb->setOutputFileName(data);
}

void logfun(string s) {
    cout << s << endl;
}

void EXPORT  tb_renderTexture(CTextureBuilder ptr, const char* pszPath )
{
    CHECK_SZ(pszPath,ERR_STRING_IS_NULL)
    auto tb = reinterpret_cast<TextureBuilder *>(ptr);
    tb->setLoggingFunction(logfun);
    cout << "Acquisito Texture Builder" << endl;
    auto path = std::string(pszPath);
    cout << "Acquisito Path" << endl;
    std::string data;
    try {
        data = tb->toJsonString();
    }
    catch (string sData) {
        cout << "ERROR! " << sData << endl;
    }
    catch (...) {
        cout << "UNDEFINED_ERROR";
        errno =13666;
        return ;
    }

    cout << "Acquisito json" << endl;
    json j;
    tb->toJson(j);
    cout << "Convertito json" << endl;
    string tpl = path+"/"+ tb->outputFileName() +".texjson";
    ofstream o(tpl);
    o << setw(4) << j << endl;
    tb->setOutputFolder(path);
    tb->buildTextureFromJsonString(data, path);
}

void EXPORT  tb_RenderTextureFromJsonString(CTextureBuilder ptr, const char *pszJson, const char* pszPath )
{
    CHECK_SZ(pszPath,ERR_STRING_IS_NULL)
    CHECK_SZ(pszJson,ERR_STRING_IS_NULL)
    auto tb = reinterpret_cast<TextureBuilder *>(ptr);
    tb->setLoggingFunction(logfun);
    cout << "Acquisito Texture Builder" << endl;
    auto path = std::string(pszPath);
    cout << "Acquisito Path" << endl;
    std::string data = std::string(pszJson);
    cout << "Acquisito json" << endl;
    json j;
    tb->toJson(j);
    cout << "Convertito json" << endl;
    string tpl = path+"/"+ tb->outputFileName() +".texjson";
    ofstream o(tpl);
    o << setw(4) << j << endl;
    tb->setOutputFolder(path);
    tb->buildTextureFromJsonString(data, path);
}


//
void EXPORT  tb_setSize(CTextureBuilder ptr, int xSize, int ySize)  {
    auto tb = reinterpret_cast<TextureBuilder *>(ptr);
    tb->setSize(xSize, ySize);
}

void EXPORT  tb_NewAbs (CTextureBuilder ptr, const char* szName, const char*  src1, BOOLEAN enableRandom )
{
    CHECK_SZ(szName,ERR_NAME_IS_NULL)
    CHECK_SZ(src1,ERR_SRC1_IS_NULL)
    CHECK_COND((tb_HasModule(ptr, szName) == 0), ERR_MODULE_ALREADY_PRESENT )

    auto tb = reinterpret_cast<TextureBuilder *>(ptr);    
    auto s_name = std::string(szName);
    auto s_src1 = std::string(src1);
    auto m_src1 = tb->modDesc().at(s_src1);
    tb->newAbs(s_name, m_src1, enableRandom);
}

void EXPORT  tb_NewAdd (CTextureBuilder ptr, const char* szName,
    const char *   src1,
    const char *   src2,
    BOOLEAN enableRandom )
{
    CHECK_SZ(szName,ERR_NAME_IS_NULL)
    CHECK_SZ(src1,ERR_SRC1_IS_NULL)
    CHECK_SZ(src2,ERR_SRC2_IS_NULL)
    CHECK_COND((tb_HasModule(ptr, szName) == 0), ERR_MODULE_ALREADY_PRESENT )

    auto tb = reinterpret_cast<TextureBuilder *>(ptr);
    auto s_name = std::string(szName);
    auto s_src1 = std::string(src1);
    auto m_src1 = tb->modDesc().at(s_src1);
    auto s_src2 = std::string(src2);
    auto m_src2 = tb->modDesc().at(s_src2);
    tb->newAdd(s_name, m_src1, m_src2,enableRandom);
}

void EXPORT  tb_NewAvg (CTextureBuilder ptr, const char* szName,
    const char *   src1,
    const char *   src2,
    BOOLEAN enableRandom )
{
    CHECK_SZ(szName,ERR_NAME_IS_NULL)
    CHECK_SZ(src1,ERR_SRC1_IS_NULL)
    CHECK_SZ(src2,ERR_SRC2_IS_NULL)
    CHECK_COND((tb_HasModule(ptr, szName) == 0), ERR_MODULE_ALREADY_PRESENT )

    auto tb = reinterpret_cast<TextureBuilder *>(ptr);
    auto s_name = std::string(szName);
    auto s_src1 = std::string(src1);
    auto m_src1 = tb->modDesc().at(s_src1);
    auto s_src2 = std::string(src2);
    auto m_src2 = tb->modDesc().at(s_src2);
    tb->newAvg(s_name, m_src1, m_src2,enableRandom);
}

void EXPORT  tb_NewAvg4 (CTextureBuilder ptr, const char* szName,
    const char *   src1,
    const char *   src2,
    const char *   src3,
    const char *   src4, BOOLEAN enableRandom )
{
    CHECK_SZ(szName,ERR_NAME_IS_NULL)
    CHECK_SZ(src1,ERR_SRC1_IS_NULL)
    CHECK_SZ(src2,ERR_SRC2_IS_NULL)
    CHECK_SZ(src3,ERR_SRC3_IS_NULL)
    CHECK_SZ(src4,ERR_SRC4_IS_NULL)
    CHECK_COND((tb_HasModule(ptr, szName) == 0), ERR_MODULE_ALREADY_PRESENT )

    auto tb = reinterpret_cast<TextureBuilder *>(ptr);
    auto s_name = std::string(szName);
    auto s_src1 = std::string(src1);
    auto m_src1 = tb->modDesc().at(s_src1);
    auto s_src2 = std::string(src2);
    auto m_src2 = tb->modDesc().at(s_src2);
    auto s_src3 = std::string(src3);
    auto m_src3 = tb->modDesc().at(s_src3);
    auto s_src4 = std::string(src4);
    auto m_src4 = tb->modDesc().at(s_src4);
    tb->newAvg4(s_name, m_src1, m_src2, m_src3, m_src4, enableRandom);
}

void EXPORT  tb_NewBillow (CTextureBuilder ptr, const char* szName, int seed,  double freq , double lac , double pers ,  int oct  ,  BOOLEAN enableRandom )
{
    CHECK_SZ(szName,ERR_NAME_IS_NULL)
    CHECK_COND((tb_HasModule(ptr, szName) == 0), ERR_MODULE_ALREADY_PRESENT )

    auto tb = reinterpret_cast<TextureBuilder *>(ptr);
    auto s_name = std::string(szName);
    tb->newBillow(s_name,  seed,freq,lac,pers,oct, enableRandom);
}

void EXPORT  tb_NewBlend (CTextureBuilder ptr, const char* szName, const char *   src1,
    const char *   src2,
    const char *   ctl, BOOLEAN enableRandom )
{
    CHECK_SZ(szName,ERR_NAME_IS_NULL)
    CHECK_SZ(src1,ERR_SRC1_IS_NULL)
    CHECK_SZ(src2,ERR_SRC2_IS_NULL)
    CHECK_COND((tb_HasModule(ptr, szName) == 0), ERR_MODULE_ALREADY_PRESENT )


    auto tb = reinterpret_cast<TextureBuilder *>(ptr);
    auto s_name = std::string(szName);
    auto s_src1 = std::string(src1);
    auto m_src1 = tb->modDesc().at(s_src1);
    auto s_src2 = std::string(src2);
    auto m_src2 = tb->modDesc().at(s_src2);
    auto s_ctl = std::string(ctl);
    auto m_ctl = tb->modDesc().at(s_ctl);
    tb->newBlend(s_name, m_src1, m_src2, m_ctl,enableRandom);
}

void EXPORT  tb_NewCheckerboard (CTextureBuilder ptr, const char* szName)
{
    CHECK_SZ(szName,ERR_NAME_IS_NULL)
    CHECK_COND((tb_HasModule(ptr, szName) == 0), ERR_MODULE_ALREADY_PRESENT )

    auto tb = reinterpret_cast<TextureBuilder *>(ptr);
    auto s_name = std::string(szName);
    tb->newCheckerboard(s_name);
}

void EXPORT  tb_NewClamp (CTextureBuilder ptr, const char* szName, const char *   src1, double uBound, double lBound, BOOLEAN enableRandom )
{
    CHECK_SZ(szName,ERR_NAME_IS_NULL)
    CHECK_SZ(src1,ERR_SRC1_IS_NULL)
    CHECK_COND((tb_HasModule(ptr, szName) == 0), ERR_MODULE_ALREADY_PRESENT )

    auto tb = reinterpret_cast<TextureBuilder *>(ptr);
    auto s_name = std::string(szName);
    auto s_src1 = std::string(src1);
    auto m_src1 = tb->modDesc().at(s_src1);
    tb->newClamp(s_name, m_src1,uBound,lBound, enableRandom);
}

void EXPORT  tb_NewConst (CTextureBuilder ptr, const char* szName, double value,BOOLEAN enableRandom )
{
    CHECK_SZ(szName,ERR_NAME_IS_NULL)
    CHECK_COND((tb_HasModule(ptr, szName) == 0), ERR_MODULE_ALREADY_PRESENT )

    auto tb = reinterpret_cast<TextureBuilder *>(ptr);
    auto s_name = std::string(szName);
    tb->newConst(s_name, value, enableRandom);
}

void EXPORT  tb_NewCos (CTextureBuilder ptr, const char* szName, const char *   src1, double freq, double exp, double value,
    BOOLEAN enableRandom )
{
    CHECK_SZ(szName,ERR_NAME_IS_NULL)
    CHECK_SZ(src1,ERR_SRC1_IS_NULL)
    CHECK_COND((tb_HasModule(ptr, szName) == 0), ERR_MODULE_ALREADY_PRESENT )

    auto tb = reinterpret_cast<TextureBuilder *>(ptr);
    auto s_name = std::string(szName);
    auto s_src1 = std::string(src1);
    auto m_src1 = tb->modDesc().at(s_src1);
    tb->newCos(s_name, m_src1, freq,exp,value, enableRandom);
}

void EXPORT  tb_NewCache (CTextureBuilder ptr, const char* szName, const char *   src1)
{
    CHECK_SZ(szName,ERR_NAME_IS_NULL)
    CHECK_SZ(src1,ERR_SRC1_IS_NULL)
    CHECK_COND((tb_HasModule(ptr, szName) == 0), ERR_MODULE_ALREADY_PRESENT )

    auto tb = reinterpret_cast<TextureBuilder *>(ptr);
    auto s_name = std::string(szName);
    auto s_src1 = std::string(src1);
    auto m_src1 = tb->modDesc().at(s_src1);
    tb->newCache(s_name,m_src1);
}

void EXPORT  tb_NewCurve (CTextureBuilder ptr, const char* szName, const char *   src1,
    CPOINT *cPoints,
    int numPoints,
    BOOLEAN enableRandom )
{
    CHECK_SZ(szName,ERR_NAME_IS_NULL)
    CHECK_SZ(src1,ERR_SRC1_IS_NULL)
    CHECK_COND((cPoints != nullptr), ERR_ARRAY_IS_NULL_OR_EMPTY)
    CHECK_COND((numPoints > 0), ERR_ARRAY_ELEM_LESS_EQUAL_ZERO)
    CHECK_COND((tb_HasModule(ptr, szName) == 0), ERR_MODULE_ALREADY_PRESENT )

    auto tb = reinterpret_cast<TextureBuilder *>(ptr);
    auto s_name = std::string(szName);
    auto s_src1 = std::string(src1);
    auto m_src1 = tb->modDesc().at(s_src1);
    std::vector<std::tuple<double,double>> points;
    for (auto h = 0; h < numPoints; h++) {
        points.push_back(make_tuple(cPoints[h].x, cPoints[h].y));
    }
    tb->newCurve(s_name, m_src1, points, enableRandom);
}

void EXPORT  tb_NewCylinders (CTextureBuilder ptr, const char* szName, double freq, BOOLEAN enableRandom )
{
    CHECK_SZ(szName,ERR_NAME_IS_NULL)

    auto tb = reinterpret_cast<TextureBuilder *>(ptr);
    auto s_name = std::string(szName);
    tb->newCylinders(s_name, freq,enableRandom);
}

void EXPORT  tb_NewDisplace (CTextureBuilder ptr, const char* szName, const char *   src1,
    const char *   src2,
    const char *   src3,
    const char *  src4,
    const char *   ctl, BOOLEAN enableRandom )
{
    CHECK_SZ(szName,ERR_NAME_IS_NULL)
    CHECK_SZ(src1,ERR_SRC1_IS_NULL)
    CHECK_SZ(src2,ERR_SRC2_IS_NULL)
    CHECK_SZ(src3,ERR_SRC3_IS_NULL)
    CHECK_SZ(src4,ERR_SRC4_IS_NULL)
    CHECK_SZ(ctl,ERR_CTL_IS_NULL)
    CHECK_COND((tb_HasModule(ptr, szName) == 0), ERR_MODULE_ALREADY_PRESENT )
    auto tb = reinterpret_cast<TextureBuilder *>(ptr);
    auto s_name = std::string(szName);
    auto s_src1 = std::string(src1);
    auto m_src1 = tb->modDesc().at(s_src1);
    auto s_src2 = std::string(src2);
    auto m_src2 = tb->modDesc().at(s_src2);
    auto s_src3 = std::string(src3);
    auto m_src3 = tb->modDesc().at(s_src3);
    auto s_src4 = std::string(src4);
    auto m_src4 = tb->modDesc().at(s_src4);
    auto s_ctl = std::string(ctl);
    auto m_ctl = tb->modDesc().at(s_ctl);
    tb->newDisplace(s_name, m_src1,m_src2, m_src3, m_src4, m_ctl, enableRandom);

}

void EXPORT  tb_NewExponent (CTextureBuilder ptr, const char* szName, const char *   src1, double exp,
    BOOLEAN enableRandom )
{
    CHECK_SZ(szName,ERR_NAME_IS_NULL)
    CHECK_SZ(src1,ERR_SRC1_IS_NULL)
    CHECK_COND((tb_HasModule(ptr, szName) == 0), ERR_MODULE_ALREADY_PRESENT )
    auto tb = reinterpret_cast<TextureBuilder *>(ptr);
    auto name = std::string(szName);
    auto s_src1 = std::string(src1);
    auto m_src1 = tb->modDesc().at(s_src1);
    tb->newExponent(name,m_src1,exp,enableRandom);

}

void EXPORT  tb_NewInvert (CTextureBuilder ptr, const char* szName, const char *   src1,
    BOOLEAN enableRandom )
{
    CHECK_SZ(szName,ERR_NAME_IS_NULL)
    CHECK_SZ(src1,ERR_SRC1_IS_NULL)
    CHECK_COND((tb_HasModule(ptr, szName) == 0), ERR_MODULE_ALREADY_PRESENT )
    auto tb = reinterpret_cast<TextureBuilder *>(ptr);
    auto name = std::string(szName);
    auto s_src1 = std::string(src1);
    auto m_src1 = tb->modDesc().at(s_src1);
    tb->newInvert(name,m_src1,enableRandom);
}

void EXPORT  tb_NewMax (CTextureBuilder ptr, const char* szName, const char *   src1,
                                                const char *   src2, BOOLEAN enableRandom )
{
    CHECK_SZ(szName,ERR_NAME_IS_NULL)
    CHECK_SZ(src1,ERR_SRC1_IS_NULL)
    CHECK_SZ(src2,ERR_SRC2_IS_NULL)
    CHECK_COND((tb_HasModule(ptr, szName) == 0), ERR_MODULE_ALREADY_PRESENT )
    auto tb = reinterpret_cast<TextureBuilder *>(ptr);
    auto name = std::string(szName);
    auto s_src1 = std::string(src1);
    auto m_src1 = tb->modDesc().at(s_src1);
    auto s_src2 = std::string(src2);
    auto m_src2 = tb->modDesc().at(s_src2);
    tb->newMax(name,m_src1,m_src2,enableRandom);

}

void EXPORT  tb_NewMin (CTextureBuilder ptr, const char* szName, const char *   src1, const char *   src2, BOOLEAN enableRandom )
{
    CHECK_SZ(szName,ERR_NAME_IS_NULL)
    CHECK_SZ(src1,ERR_SRC1_IS_NULL)
    CHECK_SZ(src2,ERR_SRC2_IS_NULL)
    CHECK_COND((tb_HasModule(ptr, szName) == 0), ERR_MODULE_ALREADY_PRESENT )
    auto tb = reinterpret_cast<TextureBuilder *>(ptr);
    auto name = std::string(szName);
    auto s_src1 = std::string(src1);
    auto m_src1 = tb->modDesc().at(s_src1);
    auto s_src2 = std::string(src2);
    auto m_src2 = tb->modDesc().at(s_src2);
    tb->newMin(name,m_src1,m_src2,enableRandom);
}

void EXPORT  tb_NewMultiply (CTextureBuilder ptr, const char* szName,
                                                     const char *   src1, const char *   src2, BOOLEAN enableRandom )
{
    CHECK_SZ(szName,ERR_NAME_IS_NULL)
    CHECK_SZ(src1,ERR_SRC1_IS_NULL)
    CHECK_SZ(src2,ERR_SRC2_IS_NULL)
    CHECK_COND((tb_HasModule(ptr, szName) == 0), ERR_MODULE_ALREADY_PRESENT )
    auto tb = reinterpret_cast<TextureBuilder *>(ptr);
    auto name = std::string(szName);
    auto s_src1 = std::string(src1);
    auto m_src1 = tb->modDesc().at(s_src1);
    auto s_src2 = std::string(src2);
    auto m_src2 = tb->modDesc().at(s_src2);
    tb->newMultiply(name,m_src1,m_src2,enableRandom);
}

void EXPORT  tb_NewPower (CTextureBuilder ptr, const char* szName, const char *
                                                  src1, const char *   src2, BOOLEAN enableRandom )
{
    CHECK_SZ(szName,ERR_NAME_IS_NULL)
    CHECK_SZ(src1,ERR_SRC1_IS_NULL)
    CHECK_SZ(src2,ERR_SRC2_IS_NULL)
    CHECK_COND((tb_HasModule(ptr, szName) == 0), ERR_MODULE_ALREADY_PRESENT )
    auto tb = reinterpret_cast<TextureBuilder *>(ptr);
    auto name = std::string(szName);
    auto s_src1 = std::string(src1);
    auto m_src1 = tb->modDesc().at(s_src1);
    auto s_src2 = std::string(src2);
    auto m_src2 = tb->modDesc().at(s_src2);
    tb->newPower(name,m_src1,m_src2,enableRandom);

}

void EXPORT  tb_NewPerlin (CTextureBuilder ptr, const char* szName, int seed,  double freq , double lac , double pers ,
    int oct  ,  BOOLEAN enableRandom )
{
    CHECK_SZ(szName,ERR_NAME_IS_NULL)
    CHECK_COND((tb_HasModule(ptr, szName) == 0), ERR_MODULE_ALREADY_PRESENT )
    auto tb = reinterpret_cast<TextureBuilder *>(ptr);
    auto name = std::string(szName);
    tb->newPerlin(name, seed, freq, lac, pers, oct, enableRandom);
}

void EXPORT  tb_NewRidgedMulti (CTextureBuilder ptr, const char* szName, int seed,  double freq , double lac ,
    int oct  ,  BOOLEAN enableRandom )
{
    CHECK_SZ(szName,ERR_NAME_IS_NULL)
    CHECK_COND((tb_HasModule(ptr, szName) == 0), ERR_MODULE_ALREADY_PRESENT )
    auto tb = reinterpret_cast<TextureBuilder *>(ptr);
    auto name = std::string(szName);
    tb->newRidgedMulti(name,seed,freq,lac,oct,enableRandom);
}

void EXPORT  tb_NewRidgedMulti2 (CTextureBuilder ptr, const char* szName, int seed,  double freq , double lac ,
    double gain, double exp, int oct,double offset, BOOLEAN enableRandom )
{
    CHECK_SZ(szName,ERR_NAME_IS_NULL)
    CHECK_COND((tb_HasModule(ptr, szName) == 0), ERR_MODULE_ALREADY_PRESENT )
    auto tb = reinterpret_cast<TextureBuilder *>(ptr);
    auto name = std::string(szName);
    tb->newRidgedMulti2(name,seed,freq,lac,gain,exp,oct,offset,enableRandom);
}

void EXPORT  tb_NewRotatePoint (CTextureBuilder ptr, const char* szName, const char *   src1,
    double x, double y, double z, BOOLEAN enableRandom )
{
    CHECK_SZ(szName,ERR_NAME_IS_NULL)
    CHECK_SZ(src1,ERR_SRC1_IS_NULL)
    CHECK_COND((tb_HasModule(ptr, szName) == 0), ERR_MODULE_ALREADY_PRESENT )
    auto tb = reinterpret_cast<TextureBuilder *>(ptr);
    auto name = std::string(szName);
    auto s_src1 = std::string(src1);
    auto m_src1 = tb->modDesc().at(s_src1);
    tb->newRotatePoint(name,m_src1,x,y,z,enableRandom);
}

void EXPORT  tb_NewScaleBias (CTextureBuilder ptr, const char* szName, const char *   src1,
    double scale, double bias, BOOLEAN enableRandom )
{
    CHECK_SZ(szName,ERR_NAME_IS_NULL)
    CHECK_SZ(src1,ERR_SRC1_IS_NULL)
    CHECK_COND((tb_HasModule(ptr, szName) == 0), ERR_MODULE_ALREADY_PRESENT )
    auto tb = reinterpret_cast<TextureBuilder *>(ptr);
    auto name = std::string(szName);
    auto s_src1 = std::string(src1);
    auto m_src1 = tb->modDesc().at(s_src1);
    tb->newScaleBias(name,m_src1, scale,bias, enableRandom);

}

void EXPORT  tb_NewSin (CTextureBuilder ptr, const char* szName, const char *   src1, double freq, double exp, double value,
    BOOLEAN enableRandom )
{
    CHECK_SZ(szName,ERR_NAME_IS_NULL)
    CHECK_SZ(src1,ERR_SRC1_IS_NULL)
    CHECK_COND((tb_HasModule(ptr, szName) == 0), ERR_MODULE_ALREADY_PRESENT )
    auto tb = reinterpret_cast<TextureBuilder *>(ptr);
    auto name = std::string(szName);
    auto s_src1 = std::string(src1);
    auto m_src1 = tb->modDesc().at(s_src1);
    tb->newSin(name, m_src1, freq,exp,enableRandom);
}

void EXPORT  tb_NewSpheres (CTextureBuilder ptr, const char* szName,  double freq, BOOLEAN enableRandom )
{
    CHECK_SZ(szName,ERR_NAME_IS_NULL)
    CHECK_COND((tb_HasModule(ptr, szName) == 0), ERR_MODULE_ALREADY_PRESENT )
    auto tb = reinterpret_cast<TextureBuilder *>(ptr);
    auto name = std::string(szName);
    tb->newSpheres(name, freq, enableRandom);
}

void EXPORT  tb_NewTranslatePoint (CTextureBuilder ptr, const char* szName, const char *   src1,
    double x, double y, double z, BOOLEAN enableRandom )
{
    CHECK_SZ(szName,ERR_NAME_IS_NULL)
    CHECK_SZ(src1,ERR_SRC1_IS_NULL)
    CHECK_COND((tb_HasModule(ptr, szName) == 0), ERR_MODULE_ALREADY_PRESENT )
    auto tb = reinterpret_cast<TextureBuilder *>(ptr);
    auto name = std::string(szName);
    auto s_src1 = std::string(src1);
    auto m_src1 = tb->modDesc().at(s_src1);
    tb->newTranslatePoint(name, m_src1, x,y,z,enableRandom);
}

void EXPORT  tb_NewScalePoint (CTextureBuilder ptr, const char* szName, const char *   src1,
    double x, double y, double z, BOOLEAN enableRandom )
{
    CHECK_SZ(szName,ERR_NAME_IS_NULL)
    CHECK_SZ(src1,ERR_SRC1_IS_NULL)
    CHECK_COND((tb_HasModule(ptr, szName) == 0), ERR_MODULE_ALREADY_PRESENT )
    auto tb = reinterpret_cast<TextureBuilder *>(ptr);
    auto name = std::string(szName);
    auto s_src1 = std::string(src1);
    auto m_src1 = tb->modDesc().at(s_src1);
    tb->newScalePoint(name, m_src1, x,y,z,enableRandom);
}

void EXPORT  tb_NewTurbulence (CTextureBuilder ptr, const char* szName, const char *   src1,
    int seed , double freq , double pow , double rough , BOOLEAN enableRandom )
{
    CHECK_SZ(szName,ERR_NAME_IS_NULL)
    CHECK_SZ(src1,ERR_SRC1_IS_NULL)
    CHECK_COND((tb_HasModule(ptr, szName) == 0), ERR_MODULE_ALREADY_PRESENT )
    auto tb = reinterpret_cast<TextureBuilder *>(ptr);
    auto name = std::string(szName);
    auto s_src1 = std::string(src1);
    auto m_src1 = tb->modDesc().at(s_src1);
    tb->newTurbulence(name,m_src1,seed,freq,pow,rough,enableRandom);
}

void EXPORT  tb_NewTurbulence2 (CTextureBuilder ptr, const char* szName, const char *   src1,
    int seed , double freq , double pow , double rough , BOOLEAN enableRandom )
{
    CHECK_SZ(szName,ERR_NAME_IS_NULL)
    CHECK_SZ(src1,ERR_SRC1_IS_NULL)
    CHECK_COND((tb_HasModule(ptr, szName) == 0), ERR_MODULE_ALREADY_PRESENT )
    auto tb = reinterpret_cast<TextureBuilder *>(ptr);
    auto name = std::string(szName);
    auto s_src1 = std::string(src1);
    auto m_src1 = tb->modDesc().at(s_src1);
    tb->newTurbulence2(name,m_src1,seed,freq,pow,rough,enableRandom);

}

void EXPORT  tb_NewTurbulenceBillow (CTextureBuilder ptr, const char* szName, const char *   src1,
    int seed , double freq , double pow , double rough , BOOLEAN enableRandom )
{
    CHECK_SZ(szName,ERR_NAME_IS_NULL)
    CHECK_SZ(src1,ERR_SRC1_IS_NULL)
    CHECK_COND((tb_HasModule(ptr, szName) == 0), ERR_MODULE_ALREADY_PRESENT )
    auto tb = reinterpret_cast<TextureBuilder *>(ptr);
    auto name = std::string(szName);
    auto s_src1 = std::string(src1);
    auto m_src1 = tb->modDesc().at(s_src1);
    tb->newTurbulenceBillow(name,m_src1,seed,freq,pow,rough,enableRandom);

}

void EXPORT  tb_NewTurbulenceRidged (CTextureBuilder ptr, const char* szName, const char *   src1,
    int seed , double freq , double pow , double rough , BOOLEAN enableRandom )
{
    CHECK_SZ(szName,ERR_NAME_IS_NULL)
    CHECK_SZ(src1,ERR_SRC1_IS_NULL)
    CHECK_COND((tb_HasModule(ptr, szName) == 0), ERR_MODULE_ALREADY_PRESENT )
    auto tb = reinterpret_cast<TextureBuilder *>(ptr);
    auto name = std::string(szName);
    auto s_src1 = std::string(src1);
    auto m_src1 = tb->modDesc().at(s_src1);
    tb->newTurbulenceRidged(name,m_src1,seed,freq,pow,rough,enableRandom);

}

void EXPORT  tb_NewVoronoi (CTextureBuilder ptr, const char* szName,
    int seed , double freq , double displ , BOOLEAN enableDispl , BOOLEAN enableRandom )
{
    CHECK_SZ(szName,ERR_NAME_IS_NULL)
    CHECK_COND((tb_HasModule(ptr, szName) == 0), ERR_MODULE_ALREADY_PRESENT )
    auto tb = reinterpret_cast<TextureBuilder *>(ptr);
    auto name = std::string(szName);
    tb->newVoronoi(name,seed,freq,displ, enableDispl,enableRandom);
}

void EXPORT  tb_NewTerrace (CTextureBuilder ptr, const char* szName, const char *   src1,
     CPOINT *cPoints, int numPoints, BOOLEAN invert, BOOLEAN enableRandom )
{
    CHECK_SZ(szName,ERR_NAME_IS_NULL)
    CHECK_SZ(src1,ERR_SRC1_IS_NULL)
    CHECK_COND((tb_HasModule(ptr, szName) == 0), ERR_MODULE_ALREADY_PRESENT )
    CHECK_COND((cPoints != nullptr), ERR_ARRAY_IS_NULL_OR_EMPTY)
    CHECK_COND((numPoints > 0), ERR_ARRAY_ELEM_LESS_EQUAL_ZERO)
    auto tb = reinterpret_cast<TextureBuilder *>(ptr);
    auto name = std::string(szName);
    auto s_src1 = std::string(src1);
    auto m_src1 = tb->modDesc().at(s_src1);
    std::vector<std::tuple<double,double>> points;
    for (auto h = 0; h < numPoints; h++) {
        points.push_back(make_tuple(cPoints[h].x, cPoints[h].y));
    }

    tb->newTerrace(name, m_src1,points, invert, enableRandom);
}

void EXPORT  tb_createHeightMapDescriptor(CTextureBuilder ptr, const char* szName)
{
    CHECK_SZ(szName,ERR_NAME_IS_NULL)
    CHECK_COND((tb_HasHeightMap(ptr, szName) == 0), ERR_HEIGHTMAPDESC_ALREADY_PRESENT )
    auto tb = reinterpret_cast<TextureBuilder *>(ptr);
    auto name = std::string(szName);
    tb->createHeightMapDescriptor(name);
}

void EXPORT  tb_createImageDescriptor(CTextureBuilder ptr, const char* szName)
{
    CHECK_SZ(szName,ERR_NAME_IS_NULL)
    CHECK_COND((tb_HasImage(ptr, szName) == 0), ERR_IMAGE_ALREADY_PRESENT )
    auto tb = reinterpret_cast<TextureBuilder *>(ptr);
    auto name = std::string(szName);
    tb->createImageDescriptor(name);
}

void EXPORT  tb_createNoiseMapBuilderDescriptor(CTextureBuilder ptr, const char* szName, const char *   module, const char*  heightMap)
{
    CHECK_SZ(szName,ERR_NAME_IS_NULL)
    CHECK_SZ(module ,ERR_NAME_IS_NULL)
    CHECK_SZ(heightMap   ,ERR_NAME_IS_NULL)
    CHECK_COND((tb_HasHeightMapBuilder(ptr, szName) == 0), ERR_IMAGE_ALREADY_PRESENT )
    CHECK_COND((tb_HasModule(ptr, module) != 0), ERR_MODULE_NOT_PRESENT )
    CHECK_COND((tb_HasHeightMap(ptr, heightMap) != 0), ERR_HEIGHTMAP_NOT_PRESENT )
    auto tb = reinterpret_cast<TextureBuilder *>(ptr);
    auto name = std::string(szName);
    auto s_src1 = std::string(module);
    auto m_src1 = tb->modDesc().at(s_src1);
    auto s_hmdesc = std::string(heightMap);
    auto hm_desc = tb->hmDesc().at(s_hmdesc);
    tb->createNoiseMapBuilderDescriptor(name, m_src1, hm_desc);
}



void EXPORT  tb_createRendererDescriptor (
    CTextureBuilder ptr,
    const char* szName,
    const char *heightMap ,
    GRADIENTINFOSTRUCT *gradientInfo,
    int numGradientInfo,
    const char *destImage,
    const char *backgroundImage ,
    const char *alphaImage  ,
    const char *bumpMap ,
    double lightBrightness ,
    double lightContrast ,
    BOOLEAN enabledLight ,
    BOOLEAN randomGradient ,
    int bgRed,
    int bgGreen,
    int bgBlue,
    int bgAlpha)
{
    CHECK_SZ(szName,ERR_NAME_IS_NULL)
    CHECK_SZ(destImage,ERR_STRING_IS_NULL)
    CHECK_COND((tb_HasImage(ptr, destImage) != 0), ERR_IMAGE_NOT_PRESENT )
    CHECK_COND((gradientInfo != nullptr), ERR_ARRAY_IS_NULL_OR_EMPTY)
    CHECK_COND((numGradientInfo > 0), ERR_ARRAY_ELEM_LESS_EQUAL_ZERO)

    auto tb = reinterpret_cast<TextureBuilder *>(ptr);
    auto name = std::string(szName);
    auto s_hm = std::string(heightMap);
    auto m_heightmap = tb->hmDesc().at(s_hm);
    auto s_destImage = std::string(destImage);
    auto i_destImage = tb->imDesc().at(s_destImage);

    auto i_backgroundImage = backgroundImage != nullptr ?
            tb->imDesc().at(std::string(backgroundImage)) : nullptr;

    auto i_alphaImage = alphaImage != nullptr ?
            tb->imDesc().at(std::string(alphaImage)) : nullptr;

    auto hm_bumpMap = bumpMap != nullptr ?
            tb->hmDesc().at(std::string(bumpMap)) :
                nullptr;

    auto rgba_tuple = make_tuple(bgRed,bgGreen,bgBlue, bgAlpha);

    std::vector<GradientInfo> v_gradientInfo;
    for (int h = 0; h < numGradientInfo; h++) {
        v_gradientInfo.push_back(GradientInfo{
                                     gradientInfo[h].position,
                                     gradientInfo[h].red,
                                     gradientInfo[h].green,
                                     gradientInfo[h].blue,
                                     gradientInfo[h].alpha
                                 });
    }
    tb->createRendererDescriptor(name, m_heightmap,v_gradientInfo,
                                 i_destImage,
                                 i_backgroundImage,
                                 i_alphaImage,
                                 hm_bumpMap,
                                 lightBrightness,
                                 lightContrast,
                                 enabledLight,
                                 randomGradient,
                                 rgba_tuple);


}


int EXPORT tb_HasModule(CTextureBuilder ptr, const char *szName) {
    auto tb = reinterpret_cast<TextureBuilder *>(ptr);
    auto name = std::string(szName);
    auto it = tb->modDesc().find(name);
    if (it == tb->modDesc().end())
        return 0;
    else
        return 1;
}

int EXPORT tb_HasRenderer(CTextureBuilder ptr, const char *szName) {
    auto tb = reinterpret_cast<TextureBuilder *>(ptr);
    auto name = std::string(szName);
    auto it = tb->rndDesc().find(name);
    if (it == tb->rndDesc().end())
        return 0;
    else
        return 1;
}

int EXPORT tb_HasImage(CTextureBuilder ptr, const char *szName) {
    auto tb = reinterpret_cast<TextureBuilder *>(ptr);
    auto name = std::string(szName);
    auto it = tb->imDesc().find(name);
    if (it == tb->imDesc().end())
        return 0;
    else
        return 1;
}

int EXPORT tb_HasHeightMap(CTextureBuilder ptr, const char *szName) {
    auto tb = reinterpret_cast<TextureBuilder *>(ptr);
    auto name = std::string(szName);
    auto it = tb->hmDesc().find(name);
    if (it == tb->hmDesc().end())
        return 0;
    else
        return 1;

}


int EXPORT tb_HasHeightMapBuilder(CTextureBuilder ptr, const char *szName) {
    auto tb = reinterpret_cast<TextureBuilder *>(ptr);
    auto name = std::string(szName);
    auto it = tb->nmbDesc().find(name);
    if (it == tb->nmbDesc().end())
        return 0;
    else
        return 1;
}

void EXPORT tb_AddRandomFactors(CTextureBuilder ptr, const double *iRandomFactors, int numFactors ) {
    auto tb = reinterpret_cast<TextureBuilder *>(ptr);
    std::vector<double> randomFactors;
    for (auto h = 0; h < numFactors; h++) {
        randomFactors.push_back(iRandomFactors[h]);
    }
    tb->addRandomFactors(randomFactors);

}

void EXPORT tb_WriteTextureToPath(CTextureBuilder ptr, const char *szPath) {
    auto tb = reinterpret_cast<TextureBuilder *>(ptr);
    auto path= std::string(szPath);
    tb->writeTextureToPath(path);
}

void EXPORT tb_PrepareTexture(CTextureBuilder ptr) {
    auto tb = reinterpret_cast<TextureBuilder *>(ptr);
    try {
        tb->setBuilderType("sphere");
        tb->setBounds();
        tb->connectDescriptorsAndPrepare();
    }
    catch (noise::ExceptionInvalidParam &err) {
        cerr << "buildTextureFromJson 1" << endl;
        errno = ERR_PREPARE_TEXTURE_INVALID_PARAM;
        return;
    }
    catch (noise::ExceptionNoModule &err) {
        cerr << "buildTextureFromJson 2" << endl;
        errno = ERR_PREPARE_TEXTURE_NO_MODULE;
        return;
    }
    catch (noise::ExceptionOutOfMemory ) {
        cerr << "buildTextureFromJson 3" << endl;
        errno = ERR_TEXTURE_OUT_OF_MEMORY;
        return;
    }
    catch (noise::Exception &err) {
        cerr << "buildTextureFromJson 4" << endl;
        errno = ERR_TEXTURE_GENERIC;
        return;
    }
    catch (std::string &err) {
        cerr << "buildTextureFromJson " << err << endl;
        errno = ERR_TEXTURE_GENERIC;
        return;
    }
    catch (...) {
        cerr << "buildTextureFromJson 6" << endl;
        errno = ERR_TEXTURE_GENERIC;
        return;
    }
}

void EXPORT tb_LoadTextureFromJsonFile(CTextureBuilder ptr, const char* szPath) {
    auto tb = reinterpret_cast<TextureBuilder *>(ptr);
    auto path= std::string(szPath);
    tb->loadTextureFromJson(path);
}

void EXPORT tb_LoadTextureFromJsonString(CTextureBuilder ptr, const char* szString) {
    auto tb = reinterpret_cast<TextureBuilder *>(ptr);
    auto path= std::string(szString);
    tb->loadTextureFromJsonString(szString);

}

char * EXPORT tb_ToJsonString(CTextureBuilder ptr) {
    auto tb = reinterpret_cast<TextureBuilder *>(ptr);
    auto json= tb->toJsonString();
    char *szRes = (char *) malloc(sizeof(char *)*json.length()+1);
    memset((void *)szRes, 0,sizeof(char *)*json.length()+1 );


    json.copy(szRes, json.length(), 0);
    return szRes;

}

void EXPORT  tb_Select (CTextureBuilder ptr, const char* szName, const char *
                                                  src1, const char *   src2,
                        const char * ctl,
                        double uBound, double lBound, double value,
                        BOOLEAN enableRandom )
{
    CHECK_SZ(szName,ERR_NAME_IS_NULL)
    CHECK_SZ(src1,ERR_SRC1_IS_NULL)
    CHECK_SZ(src2,ERR_SRC2_IS_NULL)
    CHECK_SZ(ctl,ERR_CTL_IS_NULL)
    CHECK_COND((tb_HasModule(ptr, szName) == 0), ERR_MODULE_ALREADY_PRESENT )
    auto tb = reinterpret_cast<TextureBuilder *>(ptr);
    auto name = std::string(szName);
    auto s_src1 = std::string(src1);
    auto m_src1 = tb->modDesc().at(s_src1);
    auto s_src2 = std::string(src2);
    auto m_src2 = tb->modDesc().at(s_src2);
    auto s_ctl = std::string(ctl);
    auto m_ctl = tb->modDesc().at(s_ctl);
    tb->newSelect(name, m_src1, m_src2, m_ctl, uBound, lBound, value, enableRandom);

}

char * EXPORT GetJsonPreset(const char *szName) {
    if (szName == nullptr) {
        return nullptr;
    }
    auto name = std::string(szName);
    auto it = TEMPLATES.find(name);
    if (it == TEMPLATES.end())
        return nullptr;
    else {
        auto sTemplate = TEMPLATES[name];
        char *szRes = (char *) malloc(sizeof(char *)*sTemplate.length()+1);
        memset((void *)szRes, 0,sizeof(char *)*sTemplate.length()+1 );
        sTemplate.copy(szRes, sTemplate.length(), 0);
        return szRes;
    }
}

void EXPORT  tb_setColorMap(CTextureBuilder ptr, const char  *pszTexture1)
{
    CHECK_SZ(pszTexture1,ERR_STRING_IS_NULL)
    auto tb = reinterpret_cast<TextureBuilder *>(ptr);
    tb->setColorMap(pszTexture1);
}

void EXPORT  tb_setBumpMap(CTextureBuilder ptr, const char  *pszTexture1)
{
    CHECK_SZ(pszTexture1,ERR_STRING_IS_NULL)
    auto tb = reinterpret_cast<TextureBuilder *>(ptr);
    tb->setBumpMap(pszTexture1);
}

void EXPORT  tb_setSpecMap(CTextureBuilder ptr, const char  *pszTexture1)
{
    CHECK_SZ(pszTexture1,ERR_STRING_IS_NULL)
    auto tb = reinterpret_cast<TextureBuilder *>(ptr);
    tb->setReflectionMap(pszTexture1);
}

void EXPORT  tb_setCloudMap(CTextureBuilder ptr, const char  *pszTexture1)
{
    CHECK_SZ(pszTexture1,ERR_STRING_IS_NULL)
    auto tb = reinterpret_cast<TextureBuilder *>(ptr);
    tb->setCloudMap(pszTexture1);
}



} // extern "C"
