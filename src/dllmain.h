#ifndef DLLMAIN_H
#define DLLMAIN_H

#ifdef _WIN32
#include <windows.h>
#define EXPORT __declspec(dllexport)
#else
#define EXPORT
#endif


#include <errno.h>
#include <string.h>

#define BOOLEAN int
#define TRUE 1
#define FALSE 0

const int ERR_NAME_IS_NULL=13001;
const int ERR_SRC1_IS_NULL=13002;
const int ERR_SRC2_IS_NULL=13003;
const int ERR_SRC3_IS_NULL=13004;
const int ERR_SRC4_IS_NULL=13005;
const int ERR_CTL_IS_NULL=13006;
const int ERR_PATH_IS_NULL=13007;
const int ERR_FILE_IS_NULL=13008;
const int ERR_STRING_IS_NULL=13009;
const int ERR_MODULE_NOT_PRESENT=13100;
const int ERR_IMAGE_NOT_PRESENT=13101;
const int ERR_RENDERER_NOT_PRESENT=13102;
const int ERR_HEIGHTMAP_NOT_PRESENT=13103;
const int ERR_HEIGHTMAPDESC_NOT_PRESENT=13104;
const int ERR_ARRAY_IS_NULL_OR_EMPTY=13105;
const int ERR_ARRAY_ELEM_LESS_EQUAL_ZERO=13106;
const int ERR_MODULE_ALREADY_PRESENT=13200;
const int ERR_IMAGE_ALREADY_PRESENT=13201;
const int ERR_RENDERER_ALREADY_PRESENT=13202;
const int ERR_HEIGHTMAP_ALREADY_PRESENT=13203;
const int ERR_HEIGHTMAPDESC_ALREADY_PRESENT=13204;
const int ERR_PREPARE_TEXTURE_INVALID_PARAM=13250;
const int ERR_PREPARE_TEXTURE_NO_MODULE=13250;
const int ERR_TEXTURE_OUT_OF_MEMORY=13990;
const int ERR_TEXTURE_GENERIC=13999;


#define CHECK_SZ(_sz,_err) \
    if (_sz == nullptr || _sz[0]=='\0' ) { errno = _err; return; }

#define CHECK_SZ_RET(_sz,_err, _rv) \
    if (_sz == nullptr || _sz[0]=='\0' ) { errno = _err; return _rv; }

#define CHECK_COND(_b,_err) \
    if (!_b) { errno = _err; return; }


void EXPORT  createParamTexture(
    BOOLEAN bEarthlike,
    const char * pszTexture1,
    const char * pszTexture2,
    double seaLevel,
    double iceLevel,
    int width,
    const char * pszPath,
    const char *pszFilename,
    char *szRes);
char *  EXPORT  createTexture(const char* pszJsonFile, const char * pszPath, const char *pszFilename);

typedef void* CTextureBuilder;

typedef struct GradientInfoStruct {
    double position;
    int red;
    int green;
    int blue;
    int alpha;
} GRADIENTINFOSTRUCT;

typedef struct CPoint {
    double x;
    double y;
} CPOINT;

CTextureBuilder EXPORT  tb_new() ;

CTextureBuilder EXPORT  tb_newWithParam( BOOLEAN bEarthlike,
    const char * pszTexture1,
    const char * pszTexture2,
    double seaLevel,
    double iceLevel) ;
void EXPORT  tb_free(CTextureBuilder ptr) ;

void EXPORT  tb_setOutputFileName(CTextureBuilder ptr, const char* pszFilename) ;

void EXPORT  tb_buildTextureFromJson(CTextureBuilder ptr, const char* pszTpl, const char* pszPath ) ;

void EXPORT  tb_buildTextureFromJsonString(CTextureBuilder ptr, const char* pszData, const char* pszPath ) ;

void EXPORT  tb_setOutputName(CTextureBuilder ptr, const char* pszName );

void EXPORT  tb_renderTexture(CTextureBuilder ptr, const char* pszPath );

void EXPORT  tb_setSize(CTextureBuilder ptr, int xSize, int ySize);
void EXPORT  tb_NewAbs (CTextureBuilder ptr, const char* szName, const char*  src1, BOOLEAN enableRandom );

void EXPORT  tb_NewAdd (CTextureBuilder ptr, const char* szName,
    const char *   src1,
    const char *   src2,
    BOOLEAN enableRandom );
void EXPORT  tb_NewAvg (CTextureBuilder ptr, const char* szName,
    const char *   src1,
    const char *   src2,
    BOOLEAN enableRandom );
void EXPORT  tb_NewAvg4 (CTextureBuilder ptr, const char* szName,
    const char *   src1,
    const char *   src2,
    const char *   src3,
    const char *   src4, BOOLEAN enableRandom );
void EXPORT  tb_NewBillow (CTextureBuilder ptr, const char* szName, int seed,  double freq , double lac , double pers ,  int oct  ,  BOOLEAN enableRandom );
void EXPORT  tb_NewBlend (CTextureBuilder ptr, const char* szName, const char *   src1,
    const char *   src2,
    const char *   ctl, BOOLEAN enableRandom );

void EXPORT  tb_NewCheckerboard (CTextureBuilder ptr, const char* szName);
void EXPORT  tb_NewClamp (CTextureBuilder ptr, const char* szName, const char *   src1, double uBound, double lBound, BOOLEAN enableRandom );
void EXPORT  tb_NewConst (CTextureBuilder ptr, const char* szName, double value,BOOLEAN enableRandom );
void EXPORT  tb_NewCos (CTextureBuilder ptr, const char* szName, const char *   src1, double freq, double exp, double value,
    BOOLEAN enableRandom );
void EXPORT  tb_NewCache (CTextureBuilder ptr, const char* szName, const char *   src1);
void EXPORT  tb_NewCurve (CTextureBuilder ptr, const char* szName, const char *   src1,
    CPOINT *cPoints,
    int numPoints,
    BOOLEAN enableRandom );
void EXPORT  tb_NewCylinders (CTextureBuilder ptr, const char* szName, double freq, BOOLEAN enableRandom );
void EXPORT  tb_NewDisplace (CTextureBuilder ptr, const char* szName, const char *   src1,
    const char *   src2,
    const char *   src3,
    const char *  src4,
    const char *   ctl, BOOLEAN enableRandom );
void EXPORT  tb_NewExponent (CTextureBuilder ptr, const char* szName, const char *   src1, double exp,
    BOOLEAN enableRandom );
void EXPORT  tb_NewInvert (CTextureBuilder ptr, const char* szName, const char *   src1,
    BOOLEAN enableRandom );
void EXPORT  tb_NewMax (CTextureBuilder ptr, const char* szName, const char *   src1,
                                                const char *   src2, BOOLEAN enableRandom );
void EXPORT  tb_NewMin (CTextureBuilder ptr, const char* szName, const char *   src1, const char *   src2, BOOLEAN enableRandom );
void EXPORT  tb_NewMultiply (CTextureBuilder ptr, const char* szName,
                                                     const char *   src1, const char *   src2, BOOLEAN enableRandom );

void EXPORT  tb_NewPower (CTextureBuilder ptr, const char* szName, const char *
                                                  src1, const char *   src2, BOOLEAN enableRandom );
void EXPORT  tb_NewPerlin (CTextureBuilder ptr, const char* szName, int seed,  double freq , double lac , double pers ,
    int oct  ,  BOOLEAN enableRandom );
void EXPORT  tb_NewRidgedMulti (CTextureBuilder ptr, const char* szName, int seed,  double freq , double lac ,
    int oct  ,  BOOLEAN enableRandom );
void EXPORT  tb_NewRidgedMulti2 (CTextureBuilder ptr, const char* szName, int seed,  double freq , double lac ,
    double gain, double exp, int oct,double offset, BOOLEAN enableRandom );
void EXPORT  tb_NewRotatePoint (CTextureBuilder ptr, const char* szName, const char *   src1,
    double x, double y, double z, BOOLEAN enableRandom );

void EXPORT  tb_NewScaleBias (CTextureBuilder ptr, const char* szName, const char *   src1,
    double scale, double bias, BOOLEAN enableRandom );
void EXPORT  tb_NewSin (CTextureBuilder ptr, const char* szName, const char *   src1, double freq, double exp, double value,
    BOOLEAN enableRandom );
void EXPORT  tb_NewSpheres (CTextureBuilder ptr, const char* szName,  double freq, BOOLEAN enableRandom );
void EXPORT  tb_NewTranslatePoint (CTextureBuilder ptr, const char* szName, const char *   src1,
    double x, double y, double z, BOOLEAN enableRandom );
void EXPORT  tb_NewScalePoint (CTextureBuilder ptr, const char* szName, const char *   src1,
    double x, double y, double z, BOOLEAN enableRandom );
void EXPORT  tb_NewTurbulence (CTextureBuilder ptr, const char* szName, const char *   src1,
    int seed , double freq , double pow , double rough , BOOLEAN enableRandom );
void EXPORT  tb_NewTurbulence2 (CTextureBuilder ptr, const char* szName, const char *   src1,
    int seed , double freq , double pow , double rough , BOOLEAN enableRandom );
void EXPORT  tb_NewTurbulenceBillow (CTextureBuilder ptr, const char* szName, const char *   src1,
    int seed , double freq , double pow , double rough , BOOLEAN enableRandom );
void EXPORT  tb_NewTurbulenceRidged (CTextureBuilder ptr, const char* szName, const char *   src1,
    int seed , double freq , double pow , double rough , BOOLEAN enableRandom );
void EXPORT  tb_NewVoronoi (CTextureBuilder ptr, const char* szName,
    int seed , double freq , double displ , BOOLEAN enableDispl , BOOLEAN enableRandom );
void EXPORT  tb_NewTerrace (CTextureBuilder ptr, const char* szName, const char *   src1,
     CPOINT *cPoints, int numPoints, BOOLEAN invert, BOOLEAN enableRandom );
void EXPORT  tb_createHeightMapDescriptor(CTextureBuilder ptr, const char* szName);
void EXPORT  tb_createImageDescriptor(CTextureBuilder ptr, const char* szName);
void EXPORT  tb_createNoiseMapBuilderDescriptor(CTextureBuilder ptr, const char* szName, const char *   module, const char*  heightMap);
void EXPORT  tb_createRendererDescriptor (
    CTextureBuilder ptr,
    const char* szName,
    const char *heightMap ,
    GRADIENTINFOSTRUCT *gradientInfo,
    int numGradientInfo,
    const char *destImage,
    const char *backgroundImage ,
    const char *alphaImage  ,
    const char *bumpMap ,
    double lightBrightness ,
    double lightContrast ,
    BOOLEAN enabledLight ,
    BOOLEAN randomGradient ,
    int bgRed,
    int bgGreen,
    int bgBlue,
    int bgAlpha);

int EXPORT tb_HasModule(CTextureBuilder ptr, const char *szName) ;
int EXPORT tb_HasRenderer(CTextureBuilder ptr, const char *szName) ;
int EXPORT tb_HasImage(CTextureBuilder ptr, const char *szName) ;
int EXPORT tb_HasHeightMap(CTextureBuilder ptr, const char *szName);
int EXPORT tb_HasHeightMapBuilder(CTextureBuilder ptr, const char *szName);
void EXPORT tb_AddRandomFactors(CTextureBuilder ptr, const double *iRandomFactors, int numFactors );

void EXPORT tb_PrepareTexture(CTextureBuilder ptr);
void EXPORT tb_WriteTextureToPath(CTextureBuilder ptr, const char* szPath);

void EXPORT tb_LoadTextureFromJsonFile(CTextureBuilder ptr, const char* szPath);
void EXPORT tb_LoadTextureFromJsonString(CTextureBuilder ptr, const char* szString);
char * EXPORT tb_ToJsonString(CTextureBuilder ptr);
void EXPORT  tb_Select (CTextureBuilder ptr, const char* szName, const char *
                        src1, const char *   src2,
                        const char * ctl,
                        double uBound, double lBound, double value,
                        BOOLEAN enableRandom );
char * EXPORT GetJsonPreset(const char *szName);
void EXPORT  tb_RenderTextureFromJsonString(CTextureBuilder ptr, const char *pszJson, const char* pszPath );

void EXPORT  tb_setColorMap(CTextureBuilder ptr, const char  *pszTexture1);
void EXPORT  tb_setBumpMap(CTextureBuilder ptr, const char  *pszTexture1);
void EXPORT  tb_setSpecMap(CTextureBuilder ptr, const char  *pszTexture1);
void EXPORT  tb_setCloudMap(CTextureBuilder ptr, const char  *pszTexture1);


#endif // LIBNOISE_H
