/*
############################################################################
#
# This file is part of Warp2010, (C) Massimiliano Lambertini - 2009
# Contact: m.lambertini@gmail.com
#
This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA#
############################################################################
*/

#ifndef ColorOPS_H
#define ColorOPS_H

#include <map>
#include <ssg_structures.h>
#include <noiseutils.h>

using namespace std;

namespace ColorOps {

    inline noise::utils::Color randomHSLColor() {
        int l = 128 + SSGX::dx(127);
        noise::utils::Color c;
        c.setColorFromHsv( SSGX::dn(255), SSGX::dn(255) ,l);
        return c;
    }

    inline noise::utils::Color randomPlanetColor() {
        int s = noise::utils::basePlanetColors.size();
        return noise::utils::basePlanetColors[SSGX::dn(s)];
    }

    inline noise::utils::Color randomBeachColor() {
        int s = noise::utils::baseBeachColors.size();
        return noise::utils::baseBeachColors[SSGX::dn(s)];
    }


    inline noise::utils::Color randomColor() { return noise::utils::Color (static_cast<noise::uint8>(SSGX::dn(255)),
                                                                          static_cast<noise::uint8>(SSGX::dn(255)),
                                                                          static_cast<noise::uint8>(SSGX::dn(255)),
                                                                           255); }
    inline noise::utils::Color irregularLightenColor(const noise::utils::Color &clr, int step=25) {
        int dStep = step / 2;
        int rSrep = 1+dStep / 4;
        noise::utils::Color res(clr);
        int newHue = (res.hsv().h + SSGX::dx(dStep) -rSrep) % 255;
        int newSat = (res.hsv().s + SSGX::dx(dStep) -rSrep) % 255;
        int newLt = (res.hsv().v + SSGX::dx(step)) % 255;
        if (newLt < 144)
            newLt = 144 + SSGX::dx(110);
        res.setColorFromHsv( static_cast<noise::uint8>(newHue),
                    static_cast<noise::uint8>(newSat),
                    static_cast<noise::uint8>(newLt) );
        return res;
    }
    inline noise::utils::Color irregularDarkenColor(const noise::utils::Color &clr, int step=25) {
        int dStep = step / 2;
        int rSrep = 1+dStep / 4;
        noise::utils::Color res(clr);
        int newHue = (res.hsv().h + SSGX::dx(dStep) -rSrep) % 255;
        int newSat = (res.hsv().s + SSGX::dx(dStep) -rSrep) % 255;
        int newLt = (res.hsv().v - SSGX::dx(step)) % 255;
        if (newLt < 144)
            newLt = 144 + SSGX::dx(110);
        res.setColorFromHsv( static_cast<noise::uint8>(newHue),
                    static_cast<noise::uint8>(newSat),
                    static_cast<noise::uint8>(newLt) );
        return res;
    }

    inline map<double, noise::utils::Color> lightningGradient (int nSteps = 7,int nColorStep = 14,
                                                   const noise::utils::Color& startColor = randomColor(),
                                                   int randomFactor = -1,
                                                   bool randomAlpha = false,
                                                   int baseAlpha = 0,
                                                   int devAlpha = 64)
    {
        map<double,noise::utils::Color> colorMap;
        double dStart = -1.0;
        double dStep = (2.0 / (double)nSteps);
        noise::utils::Color theColor = startColor;
        if (randomAlpha)
            theColor.alpha = (noise::uint8)(32+SSGX::dn(192));
        for (auto h = 0; h < nSteps-1; h++) {
            colorMap.insert({dStart,theColor});
            auto x = SSGX::d100();
            if (x > randomFactor)
                theColor = theColor.darken(nColorStep);
            else {
                theColor.setColorFromHsv(
                                         (theColor.hsv().h + SSGX::dn(50) -25 ) % 255
                                , theColor.hsv().s, theColor.hsv().v);
                auto newLt = theColor.hsv().v;
                if (newLt < 144)
                    newLt = 144 + (noise::uint8)(SSGX::dx(110));
                theColor.setColorFromHsv(theColor.hsv().h,theColor.hsv().s, newLt);
            }
            if (randomAlpha)
                theColor.alpha = (noise::uint8)(baseAlpha+SSGX::dn(devAlpha));
            dStart += dStep;
        }
        colorMap.insert({dStart,theColor});
        return colorMap;
    }

    inline map<double, noise::utils::Color> evolveGradient  (int nSteps = 7,
                                                            const noise::utils::Color& startColor = noise::utils::Color::RandomHSVColor(),
                                                            bool randomAlpha = false,
                                                            int baseAlpha = 0,
                                                            int devAlpha = 64)
    {
        map<double,noise::utils::Color> colorMap;
        double dStart = -1.0;
        double dStep = (2.0 / (double)(nSteps+1));
        auto c1 = startColor;
        for (auto h = 0; h < nSteps; h++) {
            c1 = c1.EvolveColor();
            if (randomAlpha)
                c1.alpha = (noise::uint8)(baseAlpha+SSGX::dn(devAlpha));
            colorMap.insert({dStart, c1});
            dStart += dStep;
        }
        c1 = c1.EvolveColor();
        if (randomAlpha)
            c1.alpha = (noise::uint8)(baseAlpha+SSGX::dn(devAlpha));
        colorMap.insert({dStart, c1});
        return colorMap;
    }

    inline map<double, noise::utils::Color> earthlikeGradients  (int nSteps = 7,
                                                            bool randomAlpha = false,
                                                            int baseAlpha = 0,
                                                            int devAlpha = 64)
    {
        map<double,noise::utils::Color> colorMap;
        double dStart = -1.0;
        double dStep = (2.0 / (double)(nSteps+1));
        auto c1 = ColorOps::randomPlanetColor();
        for (auto h = 0; h < nSteps; h++) {
            if (randomAlpha)
                c1.alpha = (noise::uint8)(baseAlpha+SSGX::dn(devAlpha));
            colorMap.insert({dStart, c1});
            dStart += dStep;
            c1 = ColorOps::randomPlanetColor().EvolveColor();
        }
        c1 = ColorOps::randomPlanetColor().EvolveColor();
        if (randomAlpha)
            c1.alpha = (noise::uint8)(baseAlpha+SSGX::dn(devAlpha));
        colorMap.insert({dStart, c1});
        return colorMap;
    }


    inline map<double, noise::utils::Color> darkeningGradient (int nSteps = 7,int nColorStep = 14,
                                                   const noise::utils::Color& startColor = randomColor(),
                                                   int randomFactor = -1,
                                                   bool randomAlpha = false,
                                                   int baseAlpha = 0,
                                                   int devAlpha = 64)
    {
        map<double,noise::utils::Color> colorMap;
        double dStart = -1.0;
        double dStep = (2.0 / (double)nSteps);
        noise::utils::Color theColor = startColor;
        if (randomAlpha)
            theColor.alpha = (noise::uint8)(32+SSGX::dn(192));
        for (auto h = 0; h < nSteps-1; h++) {
            colorMap.insert({dStart,theColor});
            auto x = SSGX::d100();
            if (x > randomFactor)
                theColor = theColor.lighten(nColorStep);
            else {
                theColor.setColorFromHsv((theColor.hsv().h + (noise::uint8)SSGX::dn(50) -25 ) % 255
                                , theColor.hsv().s, theColor.hsv().v);
                auto newLt = theColor.hsv().v;
                if (newLt < 144)
                    newLt = 144 + SSGX::dx(110);
                theColor.setColorFromHsv(theColor.hsv().h,theColor.hsv().s, newLt);
            }
            if (randomAlpha)
                theColor.alpha = (noise::uint8)(baseAlpha+SSGX::dn(devAlpha));
            dStart += dStep;;
        }
        colorMap.insert({dStart,theColor});
        return colorMap;
    }

    inline map<double, noise::utils::Color> randomGradient (int nSteps = 7, int nColorStep=14,
                                                const noise::utils::Color& startColor = randomColor(),
                                                bool randomAlpha = false,
                                                int baseAlpha = 32,
                                                int devAlpha = 96)
    {
        if (SSGX::d100() %2 == 0)
            return darkeningGradient(nSteps, nColorStep, startColor,-1, randomAlpha, baseAlpha, devAlpha);
        else
            return lightningGradient(nSteps, nColorStep, startColor, -1,randomAlpha, baseAlpha, devAlpha);
    }




}


#endif // noise::utils::ColorOPS_H
