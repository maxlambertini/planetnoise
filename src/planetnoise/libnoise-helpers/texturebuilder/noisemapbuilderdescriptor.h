/*
############################################################################
#
# This file is part of Warp2010, (C) Massimiliano Lambertini - 2009
# Contact: m.lambertini@gmail.com
#
This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA#
############################################################################
*/

#ifndef NOISEMAPBUILDERDESCRIPTOR_H
#define NOISEMAPBUILDERDESCRIPTOR_H

#include <tuple>
#include <noiseutils.h>
#include <ssg_structures.h>
#include "libnoise-helpers_global.h"
#include <memory>
#include <algorithm>
#include <json.hpp>
#include "moduledescriptor.h"
#include "heightmapdescriptor.h"

using namespace std;
using namespace nlohmann;

/*
 * utils::NoiseMapBuilderSphere heightMapBuilder;
    heightMapBuilder.SetDestSize (_sizeX, _sizeY);
    heightMapBuilder.SetBounds (-90.0, 90.0, -180.0, 180.0);

    heightMapBuilder.SetSourceModule (scaleBase);
    heightMapBuilder.SetDestNoiseMap (heightMap);
    heightMapBuilder.Build();

    heightMapBuilder.SetSourceModule (expBase);
    heightMapBuilder.SetDestNoiseMap (heightMapPeak);
    heightMapBuilder.Build();
    */



using namespace noise::module;
using namespace std;

enum class NoiseMapBuilderType {
    CYLINDER,
    PLANE,
    SPHERE
};


class LIBNOISEHELPERSSHARED_EXPORT NoiseMapBuilderDescriptor
{

    map<std::string,shared_ptr<Module>> _modules;
    map<std::string,shared_ptr<utils::NoiseMap>> _noiseMaps;
    map<std::string,shared_ptr<utils::NoiseMapBuilder>> _noiseMapBuilders;


    std::tuple<int,int> _size = std::tuple<int,int>(1024,512);
    std::tuple<double,double,double,double> _bounds = std::tuple<double,double,double,double>(-90.0,90.0,-180.0,180.0);

    std::string _src1 = "perlin1";
    std::string _name = "builder1";
    std::string _dest = "heightmap1";
    shared_ptr<Module> _currentModule;
    shared_ptr<utils::NoiseMapBuilder> _currentNoiseMapBuilder;
    shared_ptr<utils::NoiseMap> _currentNoiseMap;
    NoiseMapBuilderType _builderType;
    bool _seamless;

    shared_ptr<utils::NoiseMapBuilder> makeCylinderBuilder();
    shared_ptr<utils::NoiseMapBuilder> makePlaneBuilder();
    shared_ptr<utils::NoiseMapBuilder> makeSphereBuilder();


public:

    std::string luaDeclaration() { return _name + "=NoiseMapBuilderSphere.new()"; }
    std::string luaInitialization();

    NoiseMapBuilderDescriptor& connectSrcModule()
    {
        if (_src1 == "") throw "Source module src1 unreferenced in NoiseMapBuilder " + _name;
        if (_src1 != "" && (_modules.find(_src1) != _modules.end()) ) {
            noise::module::Module *mod = _modules[_src1].get();
            utils::NoiseMapBuilder *nmb =
                    this->_noiseMapBuilders[this->name()].get();
            nmb->SetSourceModule(*mod);
            //_currentModule = mod;
            //this->_noiseMapBuilders[this->name()].data()->SetSourceModule(*mod);
        }
        else
            throw "Source module "+_src1+" referenced but not actually defined. Please check names or define it";
        if (_dest == "") throw "Destination noisemap dest unreferenced in NoiseMapBuilder " + _name;
        if (_dest != "" && _noiseMaps.find(_dest)!= _noiseMaps.end() ) {
            utils::NoiseMap *mod = _noiseMaps[_dest].get();
            utils::NoiseMapBuilder *nmb =
                    this->_noiseMapBuilders[this->name()].get();
            nmb->SetDestNoiseMap(*mod);
            //_currentModule = mod;
            //this->_noiseMapBuilders[this->name()].data()->SetSourceModule(*mod);
        }
        else
            throw "Noisemap "+_dest+" referenced but not actually defined. Please check names or define it";
        return *this;
    }

    explicit NoiseMapBuilderDescriptor();

    static shared_ptr<NoiseMapBuilderDescriptor> create(string name, shared_ptr<ModuleDescriptor>  module, shared_ptr<HeightMapDescriptor>  heightMap) {
        auto desc = make_shared<NoiseMapBuilderDescriptor>();
        desc->setName(name);
        desc->setSourceModule(module->name());
        desc->setDest(heightMap->name());
        return desc;
    }

    const std::string& name() { return _name; }
    void  setName(const std::string& n) {_name = n; }

    const std::string& dest() { return _dest; }
    void setDest(const std::string& n) {_dest = n; }

    map<std::string, shared_ptr<Module>>& modules() { return _modules; }
    void setModules(const map<std::string, shared_ptr<Module>>& m) { _modules = m; }

    map<std::string,shared_ptr<utils::NoiseMap>>& noiseMaps() { return _noiseMaps; }
    void setNoiseMaps(const map<std::string,shared_ptr<utils::NoiseMap>>& v) { _noiseMaps = v; }

    int getSizeX() { return std::get<0>(_size); }
    int getSizeY() { return std::get<1>(_size); }

    std::tuple<int,int>& size() { return _size; }
    void setSize (int x, int y ) { _size = std::tuple<int,int>(x,y); }

    std::tuple<double,double,double,double> bounds() { return _bounds; }
    void setBounds (double south = -90.0, double north = 90.0, double west = -180.0, double east = 180.0) {
        _bounds = std::tuple<double,double,double,double>(south,north,west,east);
    }

    void setNoiseMapBuilders(const map<std::string, shared_ptr<utils::NoiseMapBuilder>>& v ) { _noiseMapBuilders = v; }
    map<std::string, shared_ptr<utils::NoiseMapBuilder>>& noiseMapBuilders() { return _noiseMapBuilders; }

    NoiseMapBuilderType builderType() {return  _builderType ; }
    void setBuilderType (NoiseMapBuilderType t) { _builderType = t; }

    shared_ptr<Module> currentModule() { return _currentModule; }
    void setCurrentModule(shared_ptr<Module> m) { _currentModule = m; }

    std::string& sourceModule() { return _src1; }
    void setSourceModule(const std::string& src) { _src1 = src; }

    bool seamless() { return _seamless; }
    void setSeamless(bool v) { _seamless = v; }

    void fromJson(const json& j) {
        _dest = j["dest"].get<string>();
        _name = j["name"].get<string>();
        _src1 = j["source"].get<string>();
        _seamless = j["seamless"].get<bool>();
        /*
        auto sType = json["type"].toString();
        _builderType = NoiseMapBuilderType::SPHERE;
        if (sType ==  "plane") _builderType = NoiseMapBuilderType::PLANE;
        if (sType ==  "cylinder") _builderType = NoiseMapBuilderType::CYLINDER;
        auto a = json["size"].toArray();
        _size = std::tuple<int,int>(a[0].toInt(),a[1].toInt());
        auto a1 = json["bounds"].toArray();
        _bounds = std::tuple<double,double,double,double>(
                a1[0].toDouble(),
                a1[1].toDouble(),
                a1[2].toDouble(),
                a1[3].toDouble()
                );
        */
    }

    void toJson(json& j) {
        //{ name="name", "type"="cylinder|plane|sphere","size:=[w,h], "bounds":[N,S,W,E], "src1":"source_mod","dest":"height_map",
        //"seamless:"seamless":true}
        /*
        switch (_builderType) {
            case NoiseMapBuilderType::CYLINDER:
                json["type"] = "cylinder";
                break;
            case NoiseMapBuilderType::PLANE:
                json["type"] = "plane";
                break;
            case NoiseMapBuilderType::SPHERE:
                json["type"] = "sphere";
                break;
            default:
                json["type"] = "sphere";
                break;
        }
        */
        j["name"] = _name;
        j["dest"] = _dest;
        j["source"] = _src1;
        j["seamless"] = _seamless;
        /*
        QJsonArray a;
        a.append(std::get<0>(_size));
        a.append(std::get<1>(_size));
        json["size"] = a;
        QJsonArray a1;
        a1.append(std::get<0>(_bounds));
        a1.append(std::get<1>(_bounds));
        a1.append(std::get<2>(_bounds));
        a1.append(std::get<3>(_bounds));
        json["bounds"] = a1;
        */
    }

    shared_ptr<utils::NoiseMapBuilder> makeBuilder();

};

#endif // NOISEMAPBUILDERDESCRIPTOR_H
