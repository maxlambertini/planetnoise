#include "textureworkflowcreator.h"

#include "texturebuilder.h"
#include <memory>
#include <qcolorops.h>
#include "ssg_structures.h"
#include <string>
#include <algorithm>
#include <iostream>
#include <fstream>

using namespace std;


void TextureWorkflowCreator::initializeRandomFactors()
{
    _textureBuilder->randomFactors().push_back(0.1);
    _textureBuilder->randomFactors().push_back(0.12);
    _textureBuilder->randomFactors().push_back(0.13);
    _textureBuilder->randomFactors().push_back(0.15);
    _textureBuilder->randomFactors().push_back(0.18);
    _textureBuilder->randomFactors().push_back(0.23);
    _textureBuilder->randomFactors().push_back(0.31);
    cerr << "Using random factors: " << _textureBuilder->useRandomFactors() << endl;
}

shared_ptr<ModuleDescriptor> TextureWorkflowCreator::initializeBaseGeneratorModule(string textureType, string prefix)
{
    shared_ptr<ModuleDescriptor> p(new ModuleDescriptor());
    cerr << "Setting texture type to " << textureType;
    if (textureType == "" ) {
        vector<string> data = {
            "Perlin", "Perlin", "Perlin", "Perlin",
            "Perlin", "Perlin", "Perlin", "Perlin",
            "Perlin", "Perlin", "Perlin", "Perlin",
            "Perlin", "Perlin", "Perlin", "Perlin",
            "Perlin", "Perlin", "Perlin", "Perlin",
            "Perlin", "Perlin", "Perlin", "Perlin",
            "Perlin", "Perlin", "Perlin", "Perlin",
            "Perlin", "Perlin", "Perlin", "Perlin",
            "Perlin", "Perlin", "Perlin", "Perlin",
            "Perlin", "Perlin", "Perlin", "Perlin",
            "Perlin", "Perlin", "Perlin", "Perlin",
            "Perlin", "Perlin", "Perlin", "Perlin",
            "Billow" , "Billow" ,
            "Billow" , "Billow" ,
            "Billow" , "Billow" ,
            "Billow" , "Billow" ,
            "Billow" , "Billow" ,
            "Billow" , "Billow" ,
            "RidgedMulti" , "RidgedMulti2" ,
            "RidgedMulti" , "RidgedMulti2" ,
            "RidgedMulti" , "RidgedMulti2" ,
            "Voronoi",
            "RidgedMulti" , "RidgedMulti2" ,
            "Voronoi",
            "RidgedMulti" , "RidgedMulti2" ,
            "Voronoi"
        };
        p.get()->setModuleType(data[SSGX::d1000() % data.size()]);
    } else {
        p.get()->setModuleType(textureType);
    }
    p.get()->setName(prefix+"_Module");
    p.get()->setupPropertiesToExport(p.get()->moduleType());
    p.get()->setEnableRandom(true);
    p.get()->setEnabledist(true);

    float curLac = p->lac()*.2;
    float curFreq = p->freq()*.2;
    float curPers = p->pers()*.2;

    cerr << endl << curPers << ", " << curFreq << ", " << curLac << endl;

    curPers =p->pers()+( SSGX::floatRand()*curPers - curPers/2.0);
    curFreq = p->freq()+(SSGX::floatRand()*curFreq - curFreq/2.0);
    curLac = p->lac()-(SSGX::floatRand()*curLac - curLac/2.0);

    cerr << curPers << ", " << curFreq << ", " << curLac << endl;

    p->setFreq(curFreq);
    p->setPers(curPers);
    p->setLac(curLac);

    return p;
}

bool TextureWorkflowCreator::handleTurbulenceForBaseModule(shared_ptr<ModuleDescriptor> p)
{
    bool bTurbo = false;
    string sTurboName = p.get()->name()+"_turbo";
    if (SSGX::d1000() % 3 == 0 || p.get()->moduleType()=="Voronoi") {
        bTurbo = true;
        auto turbo_select = SSGX::d1000() % 4;
        switch (turbo_select) {
        case 0: {
            shared_ptr<ModuleDescriptor> pTurbo1 = std::make_shared<ModuleDescriptor>(ModuleDescriptor::CreateTurbulence(sTurboName,p.get()->name()));
            _textureBuilder->modDesc().insert({sTurboName,pTurbo1});
            break;
        }
        case 1: {
            shared_ptr<ModuleDescriptor> pTurbo = std::make_shared<ModuleDescriptor>(ModuleDescriptor::CreateTurbulenceRidged(sTurboName,p.get()->name()));
            _textureBuilder->modDesc().insert({sTurboName,pTurbo});
            cerr << "Created turbulence ridged " << endl;
            break;
        }
        case 2: {
            shared_ptr<ModuleDescriptor> pTurbo2 = std::make_shared<ModuleDescriptor>(ModuleDescriptor::CreateTurbulenceBillow(sTurboName,p.get()->name()));
            _textureBuilder->modDesc().insert({sTurboName,pTurbo2});
            break;
        }
        case 3: {
            shared_ptr<ModuleDescriptor> pTurbo3 = std::make_shared<ModuleDescriptor>(ModuleDescriptor::CreateTurbulence2(sTurboName,p.get()->name()));
            _textureBuilder->modDesc().insert({sTurboName,pTurbo3});
            break;
        }
        default: {
            shared_ptr<ModuleDescriptor> pTurbo4 = std::make_shared<ModuleDescriptor>(ModuleDescriptor::CreateTurbulence(sTurboName,p.get()->name()));
            _textureBuilder->modDesc().insert({sTurboName,pTurbo4});
            break;
        }
        }
    }
    return bTurbo;
}

void TextureWorkflowCreator::createNoiseMapBuilderForWorkflow(shared_ptr<ModuleDescriptor> p, string sTurboName, shared_ptr<HeightMapDescriptor> hmp, bool bTurbo, string prefix)
{
    shared_ptr<NoiseMapBuilderDescriptor> nmbd(new NoiseMapBuilderDescriptor());
    nmbd.get()->setName(prefix+"_noiseMapBuilder1");
    if (!bTurbo)
        nmbd.get()->setSourceModule(p.get()->name());
    else {
        cerr << "Setting turbulence " << sTurboName << endl;
        nmbd.get()->setSourceModule(sTurboName);
    }
    nmbd.get()->setSize(1600,800);
    nmbd.get()->setDest(hmp.get()->name());
    _textureBuilder->nmbDesc().insert({nmbd.get()->name(),nmbd});
}

shared_ptr<RendererDescriptor> TextureWorkflowCreator::createRendererForWorkflow(shared_ptr<HeightMapDescriptor> hmp, string backgroundImage, string prefix)
{
    shared_ptr<RendererDescriptor> rdp(new RendererDescriptor());
    rdp.get()->setRandomFactor(20,40,60);
    rdp.get()->setName(prefix+"_renderer");
    rdp.get()->setHeightmap(hmp.get()->name());
    rdp.get()->gradientInfo().clear();
    if (backgroundImage == "") {
        //auto rndGradient = ColorOps::randomGradient(12,20, ColorOps::randomPlanetColor());
        auto rndGradient = ColorOps::earthlikeGradients(3);
        for (auto i = rndGradient.begin(); i != rndGradient.end(); ++i) {
            double d = i->first;
            noise::utils::Color& c = i->second;
	    if (d > 0.99 && d < 1.0)
                d = 0.99;
            rdp.get()->gradientInfo().push_back(GradientInfo(
                        d,
                        (int)c.red,
                        (int)c.green,
                        (int)c.blue,
                        255)
                        );
        }
        auto c = ColorOps::randomHSLColor();
        rdp.get()->gradientInfo().push_back(GradientInfo(1.0,c.red,c.green,c.blue,c.alpha));
    } else {
        //auto rndGradient = ColorOps::randomGradient(12,20, ColorOps::randomHSLColor(),true,0,128);
        auto rndGradient = ColorOps::evolveGradient(6, ColorOps::randomPlanetColor(),true,0,32);
        for (auto i = rndGradient.begin(); i != rndGradient.end(); ++i) {
	    double d = i->first;
       	    if (d > 0.98 && d < 1.0)
                d = 0.98;
	     rdp.get()->gradientInfo().push_back(GradientInfo(
                        i->first,
                        i->second.red,
                        i->second.green,
                        i->second.blue,
                        SSGX::d6() < 2 ? 192 : i->second.alpha)
                        );
        }
        auto c = ColorOps::randomHSLColor();
        rdp.get()->gradientInfo().push_back(GradientInfo(1.0,c.red,c.green,c.blue,c.alpha));
    }

    return rdp;
}

void TextureWorkflowCreator::finalizeRendererForLastWorkflow(shared_ptr<RendererDescriptor> rdp, bool last)
{
    if (last) {
        //rdp.get()->setEnabledlight(true);
        rdp.get()->setLightbrightness(2.0);
        rdp.get()->setLightcontrast(1.4);
        auto c =  rdp.get()->gradientInfo().size() / 3;
        auto d = c+ SSGX::dn(c);
        // qDebug() << "last enabled. " << c << ", " << d;
        for (auto h = 0; h < static_cast<int>(rdp.get()->gradientInfo().size()); ++h) {
            if (h < static_cast<int>(d)) {
                auto t = rdp.get()->gradientInfo()[h];
                auto newT = std::tuple<double,int,int,int,int>(
                            std::get<0>(t),
                            std::get<1>(t),
                            std::get<2>(t),
                            std::get<3>(t),
                            255
                            );
                rdp.get()->gradientInfo()[h] = newT;
            } else {
                auto t = rdp.get()->gradientInfo()[h];
                auto newT = std::tuple<double,int,int,int,int>(
                            std::get<0>(t),
                            std::get<1>(t),
                            std::get<2>(t),
                            std::get<3>(t),
                            0
                            );
                rdp.get()->gradientInfo()[h] = newT;

            }
        }
    }
}

void TextureWorkflowCreator::createTextureWorkflow(string prefix, bool bCreateImage ,
                                           string backgroundImage ,
                                           string destImage, 
                                           bool last, 
                                           bool hasBump,
                                           string textureType)
{
    shared_ptr<ModuleDescriptor> p = initializeBaseGeneratorModule(textureType, prefix);
    _textureBuilder->modDesc().insert({p.get()->name(),p});


    shared_ptr<HeightMapDescriptor> hmp(new HeightMapDescriptor());
    hmp.get()->setName(prefix+"_heightMap");
    _textureBuilder->hmDesc().insert({hmp.get()->name(), hmp});

    if (bCreateImage) {
        shared_ptr<ImageDescriptor> imp(new ImageDescriptor());
        imp.get()->setName(destImage);
        _textureBuilder->imDesc().insert({imp.get()->name(),imp});
    }

    /*
    auto bTurbo = handleTurbulenceForBaseModule(p);
    string sTurboName = p.get()->name()+"_turbo";

    createNoiseMapBuilderForWorkflow(p, sTurboName, hmp, bTurbo, prefix);
    */
    createNoiseMapBuilderForWorkflow(p, p->name(), hmp, false, prefix);

    shared_ptr<RendererDescriptor> rdp = createRendererForWorkflow(hmp, backgroundImage, prefix);

    finalizeRendererForLastWorkflow(rdp, last);
    rdp.get()->setBackgroundImage(backgroundImage);
    rdp.get()->setDestImage(destImage);

    rdp.get()->setEnabledlight(false);
    rdp.get()->setLightbrightness(2.0);
    rdp.get()->setLightcontrast(1.4);
    if (hmp.get()->name()!= "BeginLayer_heightMap") {
        rdp.get()->setBumpMap("BeginLayer_heightMap");
    }
    _textureBuilder->addRendererDescriptor(rdp.get()->name(),rdp);
}

void TextureWorkflowCreator::createBumpMap4(string mod1, string mod2, string mod3, string mod4, double dStart)
{
    auto modDesc1 = _textureBuilder->modDesc()[mod1];
    auto modDesc2 = _textureBuilder->modDesc()[mod2];
    auto modDesc3 = _textureBuilder->modDesc()[mod3];
    auto modDesc4 = _textureBuilder->modDesc()[mod4];

    string last_mod = "AvgBumpMapModule"; 

    shared_ptr<ModuleDescriptor> mdAvg = make_shared<ModuleDescriptor>(ModuleDescriptor());
    string md ="Avg4";
    mdAvg->setModuleType(md);
    mdAvg->setSrc1(mod1);
    mdAvg->setSrc2(mod2);
    mdAvg->setSrc3(mod3);
    mdAvg->setSrc4(mod4);
    mdAvg->setName(last_mod);
    _textureBuilder->modDesc().insert({mdAvg.get()->name(),mdAvg});

    if (SSGX::floatRand() < 0.99) {
        shared_ptr<ModuleDescriptor> mdExp = std::make_shared<ModuleDescriptor>(ModuleDescriptor::CreateExp(last_mod+"_e",last_mod));
        last_mod = last_mod +"_e";
        _textureBuilder->modDesc().insert({mdExp.get()->name(),mdExp});
        shared_ptr<ModuleDescriptor> mdTurbo = std::make_shared<ModuleDescriptor>(ModuleDescriptor::CreateTurbulence(last_mod+"_t",last_mod));
        last_mod = last_mod +"_t";
        _textureBuilder->modDesc().insert({mdTurbo.get()->name(),mdTurbo});
    }


    shared_ptr<HeightMapDescriptor> hmp(new HeightMapDescriptor());
    hmp.get()->setName("bump_heightMap");
    _textureBuilder->hmDesc().insert({hmp.get()->name(), hmp});

    shared_ptr<ImageDescriptor> imp(new ImageDescriptor());
    imp.get()->setName("img_bump");
    _textureBuilder->imDesc().insert({imp.get()->name(),imp});

    shared_ptr<NoiseMapBuilderDescriptor> nmbd(new NoiseMapBuilderDescriptor());
    nmbd.get()->setName("noiseMapBuilderBump");
    nmbd->setSourceModule(last_mod);
    nmbd.get()->setSize(1600,800);
    nmbd.get()->setDest(hmp.get()->name());
    _textureBuilder->nmbDesc().insert({nmbd.get()->name(),nmbd});

    shared_ptr<RendererDescriptor> rdp2(new RendererDescriptor());
    //rdp2.get()->setRandomFactor(40,80,120);
    rdp2.get()->setName("bumpmap_renderer");
    rdp2.get()->setHeightmap(hmp.get()->name());
    rdp2.get()->gradientInfo().clear();
    rdp2->setEnabledlight(false);
    rdp2->setBackgroundImage("");
    rdp2->gradientInfo().push_back(std::tuple<double,int,int,int,int>(-1.0  ,  0,  0,  0,255));
    rdp2->gradientInfo().push_back(std::tuple<double,int,int,int,int>( 1.0  ,255,255,255,255));
    rdp2.get()->setDestImage("img_bump");
    _textureBuilder->addRendererDescriptor(rdp2.get()->name(),rdp2);

    if ( dStart > -1.0)
    {
        shared_ptr<RendererDescriptor> rdpb(new RendererDescriptor());
        //rdp2.get()->setRandomFactor(40,80,120);
        rdpb.get()->setName("bumpmap_renderer_mask");
        rdpb.get()->setHeightmap("heightMapSea");
        rdpb.get()->gradientInfo().clear();
        rdpb->setEnabledlight(false);
        rdpb->setBackgroundImage("img_bump");
        rdpb->gradientInfo().push_back(std::tuple<double,int,int,int,int>(-1.0  ,  0,  0,  0,255));
        rdpb->gradientInfo().push_back(std::tuple<double,int,int,int,int>(dStart, 0,  0,  0,255));
        rdpb->gradientInfo().push_back(std::tuple<double,int,int,int,int>(dStart + 0.1,  0 , 0,  0,0));
        rdpb->gradientInfo().push_back(std::tuple<double,int,int,int,int>( 1.0  ,0,0,0,0));
        rdpb.get()->setDestImage("img_bump");
        _textureBuilder->addRendererDescriptor(rdpb.get()->name(),rdpb);
    }

}

void TextureWorkflowCreator::createBumpMap(string mod1, string mod2, double dStart)
{
    auto modDesc1 = _textureBuilder->modDesc()[mod1];
    auto modDesc2 = _textureBuilder->modDesc()[mod2];
    string last_mod = "AvgBumpMapModule"; 

    shared_ptr<ModuleDescriptor> mdAvg = make_shared<ModuleDescriptor>(ModuleDescriptor());
    int mdType = SSGX::d10();
    string md ="Avg";
    if (mdType < 3)
        md = "Avg";
    else if (mdType < 6)
        md = "Min";
    else
        md = "Max";
    mdAvg->setModuleType(md);
    mdAvg->setSrc1(mod1);
    mdAvg->setSrc2(mod2);
    mdAvg->setName("AvgBumpMapModule");
    _textureBuilder->modDesc().insert({mdAvg.get()->name(),mdAvg});

    if (SSGX::floatRand() < 0.99) {
        shared_ptr<ModuleDescriptor> mdExp = std::make_shared<ModuleDescriptor>(ModuleDescriptor::CreateExp(last_mod+"_e",last_mod));
        last_mod = last_mod +"_e";
        _textureBuilder->modDesc().insert({mdExp.get()->name(),mdExp});
        shared_ptr<ModuleDescriptor> mdTurbo = std::make_shared<ModuleDescriptor>(ModuleDescriptor::CreateTurbulence(last_mod+"_t",last_mod));
        last_mod = last_mod +"_t";
        _textureBuilder->modDesc().insert({mdTurbo.get()->name(),mdTurbo});
    }

    shared_ptr<HeightMapDescriptor> hmp(new HeightMapDescriptor());
    hmp.get()->setName("bump_heightMap");
    _textureBuilder->hmDesc().insert({hmp.get()->name(), hmp});

    shared_ptr<ImageDescriptor> imp(new ImageDescriptor());
    imp.get()->setName("img_bump");
    _textureBuilder->imDesc().insert({imp.get()->name(),imp});

    shared_ptr<NoiseMapBuilderDescriptor> nmbd(new NoiseMapBuilderDescriptor());
    nmbd.get()->setName("noiseMapBuilderBump");
    nmbd->setSourceModule(last_mod);
    nmbd.get()->setSize(1600,800);
    nmbd.get()->setDest(hmp.get()->name());
    _textureBuilder->nmbDesc().insert({nmbd.get()->name(),nmbd});

    shared_ptr<RendererDescriptor> rdp2(new RendererDescriptor());
    //rdp2.get()->setRandomFactor(40,80,120);
    rdp2.get()->setName("bumpmap_renderer");
    rdp2.get()->setHeightmap(hmp.get()->name());
    rdp2.get()->gradientInfo().clear();
    rdp2->setEnabledlight(false);
    rdp2->setBackgroundImage("");
    rdp2->gradientInfo().push_back(std::tuple<double,int,int,int,int>(-1.0  ,  0,  0,  0,255));
    rdp2->gradientInfo().push_back(std::tuple<double,int,int,int,int>( 1.0  ,255,255,255,255));
    rdp2.get()->setDestImage("img_bump");
    _textureBuilder->addRendererDescriptor(rdp2.get()->name(),rdp2);

    if ( dStart > -1.0)
    {
        shared_ptr<RendererDescriptor> rdpb(new RendererDescriptor());
        //rdp2.get()->setRandomFactor(40,80,120);
        rdpb.get()->setName("bumpmap_renderer_mask");
        rdpb.get()->setHeightmap("heightMapSea");
        rdpb.get()->gradientInfo().clear();
        rdpb->setEnabledlight(false);
        rdpb->setBackgroundImage("img_bump");
        rdpb->gradientInfo().push_back(std::tuple<double,int,int,int,int>(-1.0  ,  0,  0,  0,255));
        rdpb->gradientInfo().push_back(std::tuple<double,int,int,int,int>(dStart, 0,  0,  0,255));
        rdpb->gradientInfo().push_back(std::tuple<double,int,int,int,int>(dStart + 0.1,  0 , 0,  0,0));
        rdpb->gradientInfo().push_back(std::tuple<double,int,int,int,int>( 1.0  ,0,0,0,0));
        rdpb.get()->setDestImage("img_bump");
        _textureBuilder->addRendererDescriptor(rdpb.get()->name(),rdpb);
    }

}

void TextureWorkflowCreator::prepareBumpHeightmap()
{
    shared_ptr<ModuleDescriptor> mdRidged = make_shared<ModuleDescriptor>(ModuleDescriptor());
    string last_mod = "RidgedBumpMapModule";
    mdRidged->setModuleType("Perlin");
    mdRidged->setOct(8);
    mdRidged->setFreq(6.3);
    mdRidged->setLac(3.6);
    mdRidged->setName(last_mod);
    _textureBuilder->modDesc().insert({mdRidged.get()->name(),mdRidged});

    if (SSGX::floatRand() < 0.99) {
        shared_ptr<ModuleDescriptor> mdExp = std::make_shared<ModuleDescriptor>(ModuleDescriptor::CreateExp(last_mod+"_e",last_mod));
        last_mod = last_mod +"_e";
        _textureBuilder->modDesc().insert({mdExp.get()->name(),mdExp});
        shared_ptr<ModuleDescriptor> mdTurbo = std::make_shared<ModuleDescriptor>(ModuleDescriptor::CreateTurbulence(last_mod+"_t",last_mod,0.75, 0.05, 1.25));
        last_mod = last_mod +"_t";
        _textureBuilder->modDesc().insert({mdTurbo.get()->name(),mdTurbo});
    }

    shared_ptr<HeightMapDescriptor> hmp(new HeightMapDescriptor());
    hmp.get()->setName("bump_heightMap");
    _textureBuilder->hmDesc().insert({hmp.get()->name(), hmp});

    shared_ptr<ImageDescriptor> imp(new ImageDescriptor());
    imp.get()->setName("img_bump");
    _textureBuilder->imDesc().insert({imp.get()->name(),imp});

    shared_ptr<NoiseMapBuilderDescriptor> nmbd(new NoiseMapBuilderDescriptor());
    nmbd.get()->setName("noiseMapBuilderBump");
    nmbd->setSourceModule(last_mod);
    nmbd.get()->setSize(1600,800);
    nmbd.get()->setDest(hmp.get()->name());
    _textureBuilder->nmbDesc().insert({nmbd.get()->name(),nmbd});
}

void TextureWorkflowCreator::createBumpMapSea(double dStart)
{

    shared_ptr<RendererDescriptor> rdp2(new RendererDescriptor());
    //rdp2.get()->setRandomFactor(40,80,120);
    rdp2.get()->setName("bumpmap_renderer");
    rdp2.get()->setHeightmap("bump_heightMap");
    rdp2.get()->gradientInfo().clear();
    rdp2->setEnabledlight(false);
    rdp2->setBackgroundImage("");
    rdp2->gradientInfo().push_back(std::tuple<double,int,int,int,int>(-1.0  ,  0,  0,  0,255));
    rdp2->gradientInfo().push_back(std::tuple<double,int,int,int,int>(-.4  ,  0,  0,  0,255));
    rdp2->gradientInfo().push_back(std::tuple<double,int,int,int,int>( 0.0  ,255,255,255,255));
    rdp2->gradientInfo().push_back(std::tuple<double,int,int,int,int>( .6  ,  0,  0,  0,255));
    rdp2->gradientInfo().push_back(std::tuple<double,int,int,int,int>( 1.0  ,0,0,0,255));
    rdp2.get()->setDestImage("img_bump");
    _textureBuilder->addRendererDescriptor(rdp2.get()->name(),rdp2);

    if ( dStart > -1.0)
    {
        shared_ptr<RendererDescriptor> rdpb(new RendererDescriptor());
        //rdp2.get()->setRandomFactor(40,80,120);
        rdpb.get()->setName("bumpmap_renderer_mask");
        rdpb.get()->setHeightmap("heightMapSea");
        rdpb.get()->gradientInfo().clear();
        rdpb->setEnabledlight(false);
        rdpb->setBackgroundImage("img_bump");
        rdpb->gradientInfo().push_back(std::tuple<double,int,int,int,int>(-1.0  ,  0,  0,  0,255));
        rdpb->gradientInfo().push_back(std::tuple<double,int,int,int,int>(dStart+0.0005, 0,  0,  0,255));
        rdpb->gradientInfo().push_back(std::tuple<double,int,int,int,int>(dStart + 0.15  > 0.9 ? 0.9 : dStart+0.15,  0 , 0,  0,0));
        rdpb->gradientInfo().push_back(std::tuple<double,int,int,int,int>( 1.0  ,0,0,0,0));
        rdpb.get()->setDestImage("img_bump");
        _textureBuilder->addRendererDescriptor(rdpb.get()->name(),rdpb);
    }



}

void TextureWorkflowCreator::createGrassAndDesert(string destImage) 
{
    //grass
    shared_ptr<RendererDescriptor> rdp2(new RendererDescriptor());
    //rdp2.get()->setRandomFactor(40,80,120);
    rdp2.get()->setName("GrassLayer_renderer");
    rdp2.get()->setHeightmap("BeginLayer_heightMap");
    rdp2.get()->gradientInfo().clear();
    rdp2->setEnabledlight(false);

    rdp2->setDestImage(destImage);
    rdp2->setBackgroundImage(destImage);
    rdp2->gradientInfo().push_back(std::tuple<double,int,int,int,int>(-1.0,114,111,68,0));
    rdp2->gradientInfo().push_back(std::tuple<double,int,int,int,int>(-0.71,93,105,53,0));
    rdp2->gradientInfo().push_back(std::tuple<double,int,int,int,int>(-0.57,74,75,65,0));
    rdp2->gradientInfo().push_back(std::tuple<double,int,int,int,int>(-0.42,76,84,47,0));
    rdp2->gradientInfo().push_back(std::tuple<double,int,int,int,int>(-0.28,46,51,19,141));
    rdp2->gradientInfo().push_back(std::tuple<double,int,int,int,int>(-0.039,46,51,19,140));
    rdp2->gradientInfo().push_back(std::tuple<double,int,int,int,int>(0.21,76,84,47,140));
    rdp2->gradientInfo().push_back(std::tuple<double,int,int,int,int>(0.57,103,131,34,9));
    rdp2->gradientInfo().push_back(std::tuple<double,int,int,int,int>(0.60,73,73,48,0));
    rdp2->gradientInfo().push_back(std::tuple<double,int,int,int,int>(1.0,99,168,165,0));


    _textureBuilder->addRendererDescriptor(rdp2.get()->name(),rdp2);    

    //desert
    shared_ptr<RendererDescriptor> rdpd(new RendererDescriptor());
    //rdp2.get()->setRandomFactor(40,80,120);
    rdpd.get()->setName("DesertLayer_renderer");
    rdpd.get()->setHeightmap("heightMapPole");
    rdpd.get()->gradientInfo().clear();
    rdpd->setEnabledlight(false);

    rdpd->setDestImage(destImage);
    rdpd->setBackgroundImage(destImage);
    rdpd->gradientInfo().push_back(std::tuple<double,int,int,int,int>(-1.0 ,254,238,197,0));
    rdpd->gradientInfo().push_back(std::tuple<double,int,int,int,int>(-0.93,237,235,212,00));
    rdpd->gradientInfo().push_back(std::tuple<double,int,int,int,int>(-0.87,223,214,183,30));
    rdpd->gradientInfo().push_back(std::tuple<double,int,int,int,int>(-0.81,228,168,134,75));
    rdpd->gradientInfo().push_back(std::tuple<double,int,int,int,int>(-0.75,237,235,212,45));
    rdpd->gradientInfo().push_back(std::tuple<double,int,int,int,int>(-0.68,224,220, 56,0));
    rdpd->gradientInfo().push_back(std::tuple<double,int,int,int,int>(1.0  , 99,168,165,0));


    _textureBuilder->addRendererDescriptor(rdpd.get()->name(),rdpd);    

}

void TextureWorkflowCreator::createSnowFromBumpmap(string destImage)
{

    shared_ptr<RendererDescriptor> rdp2(new RendererDescriptor());
    //rdp2.get()->setRandomFactor(40,80,120);
    rdp2.get()->setName("snow_renderer");
    rdp2.get()->setHeightmap("bump_heightMap");
    rdp2.get()->gradientInfo().clear();
    rdp2->setEnabledlight(true);
    rdp2->setLightbrightness(2.1);
    rdp2->setLightcontrast(2.1);
    //rdp2->setEnabledlight(false);

    rdp2->setDestImage(destImage);
    rdp2->setBackgroundImage(destImage);
    rdp2->gradientInfo().push_back(std::tuple<double,int,int,int,int>(-1.0  ,   0,   0,   0,   0));
    rdp2->gradientInfo().push_back(std::tuple<double,int,int,int,int>(-.4   ,   0,   0,   0,   0));
    rdp2->gradientInfo().push_back(std::tuple<double,int,int,int,int>( -0.23 , 255, 255, 255,   0));
    rdp2->gradientInfo().push_back(std::tuple<double,int,int,int,int>( 0.0  , 255, 255, 255, 128));
    rdp2->gradientInfo().push_back(std::tuple<double,int,int,int,int>( 0.23  , 255, 255, 255,   0));
    rdp2->gradientInfo().push_back(std::tuple<double,int,int,int,int>( .6   ,   0,   0,   0,   0));
    rdp2->gradientInfo().push_back(std::tuple<double,int,int,int,int>( 1.0  ,   0,   0,   0,   0));

    _textureBuilder->addRendererDescriptor(rdp2.get()->name(),rdp2);

}

void TextureWorkflowCreator::createGeoMap(string mod1, double dStart) {
    auto modDesc1 = _textureBuilder->modDesc()[mod1];
    shared_ptr<ModuleDescriptor> mdAvg = make_shared<ModuleDescriptor>(ModuleDescriptor());
    shared_ptr<HeightMapDescriptor> hmp(new HeightMapDescriptor());
    hmp.get()->setName("geo_heightMap");
    _textureBuilder->hmDesc().insert({hmp.get()->name(), hmp});

    shared_ptr<ImageDescriptor> imp(new ImageDescriptor());
    imp.get()->setName("img_geo");
    _textureBuilder->imDesc().insert({imp.get()->name(),imp});

    shared_ptr<NoiseMapBuilderDescriptor> nmbd(new NoiseMapBuilderDescriptor());
    nmbd.get()->setName("noiseMapBuilderGeo");
    nmbd->setSourceModule(mod1);
    nmbd.get()->setSize(1600,800);
    nmbd.get()->setDest(hmp.get()->name());
    _textureBuilder->nmbDesc().insert({nmbd.get()->name(),nmbd});


    shared_ptr<RendererDescriptor> rdp2(new RendererDescriptor());
    //rdp2.get()->setRandomFactor(40,80,120);
    rdp2.get()->setName("geo_renderer");
    rdp2.get()->setHeightmap(hmp.get()->name());
    rdp2.get()->gradientInfo().clear();
    rdp2->setEnabledlight(false);
    rdp2->setBackgroundImage("");
    rdp2->gradientInfo().push_back(std::tuple<double,int,int,int,int>(-1.0  ,  255,  255,  255,255));
    rdp2->gradientInfo().push_back(std::tuple<double,int,int,int,int>( 1.0  ,255,255,255,255));
    rdp2.get()->setDestImage(+"img_geo");
    _textureBuilder->addRendererDescriptor(rdp2.get()->name(),rdp2);

    if ( dStart > -1.0)
    {
        shared_ptr<RendererDescriptor> rdpb(new RendererDescriptor());
        //rdp2.get()->setRandomFactor(40,80,120);
        rdpb.get()->setName("geomap_renderer_geo");
        rdpb.get()->setHeightmap("heightMapSea");
        rdpb.get()->gradientInfo().clear();
        rdpb->setEnabledlight(false);
        rdpb->setBackgroundImage("img_geo");
        rdpb->gradientInfo().push_back(std::tuple<double,int,int,int,int>(-1.0  ,  255,  255,  255,255));
        rdpb->gradientInfo().push_back(std::tuple<double,int,int,int,int>(dStart-0.00000001, 255,  255,  255,255));
        rdpb->gradientInfo().push_back(std::tuple<double,int,int,int,int>(dStart,  0 , 0,  0,255));
        rdpb->gradientInfo().push_back(std::tuple<double,int,int,int,int>(dStart+0.0005,  224 , 224,  224,255));
        rdpb->gradientInfo().push_back(std::tuple<double,int,int,int,int>(dStart+0.051,  224 , 224,  224, 255));
        rdpb->gradientInfo().push_back(std::tuple<double,int,int,int,int>(1.0,  224 , 224,  224, 255));
        rdpb.get()->setDestImage("img_geo");
        _textureBuilder->addRendererDescriptor(rdpb.get()->name(),rdpb);
    }
}

void TextureWorkflowCreator::createSpecMap(string mod1, double dStart) {
    auto modDesc1 = _textureBuilder->modDesc()[mod1];
    shared_ptr<ModuleDescriptor> mdAvg = make_shared<ModuleDescriptor>(ModuleDescriptor());
    shared_ptr<HeightMapDescriptor> hmp(new HeightMapDescriptor());
    hmp.get()->setName("spec_heightMap");
    _textureBuilder->hmDesc().insert({hmp.get()->name(), hmp});

    shared_ptr<ImageDescriptor> imp(new ImageDescriptor());
    imp.get()->setName("img_spec");
    _textureBuilder->imDesc().insert({imp.get()->name(),imp});

    shared_ptr<NoiseMapBuilderDescriptor> nmbd(new NoiseMapBuilderDescriptor());
    nmbd.get()->setName("noiseMapBuilderSpec");
    nmbd->setSourceModule(mod1);
    nmbd.get()->setSize(1600,800);
    nmbd.get()->setDest(hmp.get()->name());
    _textureBuilder->nmbDesc().insert({nmbd.get()->name(),nmbd});


    shared_ptr<RendererDescriptor> rdp2(new RendererDescriptor());
    //rdp2.get()->setRandomFactor(40,80,120);
    rdp2.get()->setName("specmap_renderer");
    rdp2.get()->setHeightmap(hmp.get()->name());
    rdp2.get()->gradientInfo().clear();
    rdp2->setEnabledlight(false);
    rdp2->setBackgroundImage("");
    rdp2->gradientInfo().push_back(std::tuple<double,int,int,int,int>(-1.0  ,  255,  255,  255,255));
    rdp2->gradientInfo().push_back(std::tuple<double,int,int,int,int>( 1.0  ,255,255,255,255));
    rdp2.get()->setDestImage(+"img_spec");
    _textureBuilder->addRendererDescriptor(rdp2.get()->name(),rdp2);

    if ( dStart > -1.0)
    {
        shared_ptr<RendererDescriptor> rdpb(new RendererDescriptor());
        //rdp2.get()->setRandomFactor(40,80,120);
        rdpb.get()->setName("specmap_renderer_spec");
        rdpb.get()->setHeightmap("heightMapSea");
        rdpb.get()->gradientInfo().clear();
        rdpb->setEnabledlight(false);
        rdpb->setBackgroundImage("img_spec");
        rdpb->gradientInfo().push_back(std::tuple<double,int,int,int,int>(-1.0  ,  255,  255,  255,255));
        rdpb->gradientInfo().push_back(std::tuple<double,int,int,int,int>(dStart-0.00000001, 255,  255,  255,255));
        rdpb->gradientInfo().push_back(std::tuple<double,int,int,int,int>(dStart,  0 , 0,  0,255));
        rdpb->gradientInfo().push_back(std::tuple<double,int,int,int,int>( 1.0  ,0,0,0,255));
        rdpb.get()->setDestImage("img_spec");
        _textureBuilder->addRendererDescriptor(rdpb.get()->name(),rdpb);
    }



}

void TextureWorkflowCreator::createPoleWorkflow(string backgroundImage, double poleLevel, double poleDeviation) {
    shared_ptr<HeightMapDescriptor> hmdPole = make_shared<HeightMapDescriptor>(HeightMapDescriptor());
    hmdPole.get()->setName("heightMapPole");
    _textureBuilder->hmDesc().insert({hmdPole.get()->name(), hmdPole});


    shared_ptr<ModuleDescriptor> mdCyl = make_shared<ModuleDescriptor>(ModuleDescriptor());
    shared_ptr<ModuleDescriptor> mdTurbo = make_shared<ModuleDescriptor>(ModuleDescriptor());

    mdCyl.get()->setEnableRandom(false);
    mdCyl.get()->setFreq(0.5);
    mdCyl.get()->setName("CylinderPole");
    mdCyl.get()->setModuleType("Cylinders");

    _textureBuilder->modDesc().insert({mdCyl.get()->name(),mdCyl});

    mdTurbo.get()->setName("AATurboPoles");
    mdTurbo.get()->setModuleType("Turbulence");
    mdTurbo.get()->setSrc1(mdCyl.get()->name());
    mdTurbo.get()->setPow(0.3+(SSGX::floatRand()*0.1-0.05));
    mdTurbo.get()->setRough(3.2+(SSGX::floatRand()*0.5-0.25));
    mdTurbo.get()->setFreq(4.5+(SSGX::floatRand()*2.0-1.0));

    _textureBuilder->modDesc().insert({mdTurbo.get()->name(),mdTurbo});

    shared_ptr<NoiseMapBuilderDescriptor> hmbPole = make_shared<NoiseMapBuilderDescriptor>(NoiseMapBuilderDescriptor());
    auto pHmb = hmbPole.get();
    pHmb->setDest("heightMapPole");
    pHmb->setName("heightMapBuilderPole");
    pHmb->setSeamless(true);
    pHmb->setSourceModule(mdTurbo.get()->name());
    _textureBuilder->nmbDesc().insert({hmbPole.get()->name(),hmbPole});


    shared_ptr<RendererDescriptor> renderPole = make_shared<RendererDescriptor>(RendererDescriptor());
    auto prnd = renderPole.get();
    prnd->setAlphaImage("");
    prnd->setBumpMap("BeginLayer_heightMap");
    prnd->setBackgroundImage(backgroundImage);
    prnd->setDestImage(backgroundImage);
    prnd->setEnabledlight(false);
    prnd->setHeightmap("heightMapPole");
    prnd->setName("RendererPole");
    prnd->setRandomGradient(false);


    double dStep1 = poleLevel - poleDeviation / 2.0 + (SSGX::floatRand()*poleDeviation);
    double dStep2 = dStep1+(SSGX::floatRand()*0.005)+0.00001;
    cerr << "dstep1: " << dStep1 << ", dstep2: " << dStep2 << endl;
    prnd->gradientInfo().clear();
    prnd->gradientInfo().push_back(std::tuple<double,int,int,int,int>(-1.0,0,0,0,0));
    prnd->gradientInfo().push_back(std::tuple<double,int,int,int,int>(dStep1,230,240,250,0));
    prnd->gradientInfo().push_back(std::tuple<double,int,int,int,int>(dStep2,248,254,255,255));
    prnd->gradientInfo().push_back(std::tuple<double,int,int,int,int>(1.0,255,255,255,255));

    _textureBuilder->addRendererDescriptor(prnd->name(),renderPole);

}



double TextureWorkflowCreator::createSeaWorkflow(string backgroundImage, double baseSeaLevel , double baseSeaDeviation) {
    shared_ptr<HeightMapDescriptor> hmdPole = make_shared<HeightMapDescriptor>(HeightMapDescriptor());
    hmdPole.get()->setName("heightMapSea");
    _textureBuilder->hmDesc().insert({hmdPole.get()->name(), hmdPole});


    shared_ptr<ModuleDescriptor> mdCyl = make_shared<ModuleDescriptor>(ModuleDescriptor::CreatePerlin("SeaModule"));
    shared_ptr<ModuleDescriptor> mdTurbo = make_shared<ModuleDescriptor>(ModuleDescriptor());

    mdCyl->setOct(8);
    mdCyl->setEnableRandom(true);
    mdCyl->setFreq(1.1+SSGX::floatRand()*0.4);
    mdCyl->setLac(3.5+SSGX::floatRand()*0.5);
    mdCyl->setPers(0.28+SSGX::floatRand()*0.08);

    std::cerr << "SEA MASK: " << mdCyl->oct() << ", " << mdCyl->freq() << ", " << mdCyl->lac() << ", " << mdCyl->pers() << endl;

    _textureBuilder->modDesc().insert({mdCyl.get()->name(),mdCyl});

    /*
     *         {
            "enableRandom": true,
            "freq": 4.5,
            "name": "turboPoles",
            "pow": 0.2,
            "rough": 3.2,
            "seed": 0,
            "src1": "cylinderPole",
            "type": "Turbulence"
        }
    */
    mdTurbo.get()->setName("AATurboSea");
    mdTurbo.get()->setModuleType("Turbulence");
    mdTurbo.get()->setSrc1(mdCyl.get()->name());
    mdTurbo.get()->setPow(0.05+(SSGX::floatRand()*0.4));
    mdTurbo.get()->setRough(1.0+(SSGX::floatRand()*2.0));
    mdTurbo.get()->setFreq(0.2+(SSGX::floatRand()*1.0));

    _textureBuilder->modDesc().insert({mdTurbo.get()->name(),mdTurbo});

    shared_ptr<NoiseMapBuilderDescriptor> hmbPole = make_shared<NoiseMapBuilderDescriptor>(NoiseMapBuilderDescriptor());
    auto pHmb = hmbPole.get();
    pHmb->setDest("heightMapSea");
    pHmb->setName("heightMapBuilderSea");
    pHmb->setSeamless(true);
    pHmb->setSourceModule(mdTurbo.get()->name());
    _textureBuilder->nmbDesc().insert({hmbPole.get()->name(),hmbPole});

    shared_ptr<RendererDescriptor> renderPole = make_shared<RendererDescriptor>(RendererDescriptor());
    auto prnd = renderPole.get();
    prnd->setAlphaImage("");
    prnd->setBumpMap("BeginLayer_heightMap");
    prnd->setBackgroundImage(backgroundImage);
    prnd->setDestImage(backgroundImage);
    prnd->setEnabledlight(false);
    prnd->setHeightmap("heightMapSea");
    prnd->setName("RendererSea");
    prnd->setRandomGradient(false);
    prnd->setLightbrightness(2.0);
    prnd->setLightcontrast(1.4);


    double dStep1 = baseSeaLevel; ///-baseSeaDeviation/2.0  + SSGX::floatRand()*baseSeaDeviation;
    double dStep2 = dStep1+0.05+(SSGX::floatRand()*0.1);
    double dStep3 = dStep2+0.1;
    double dStep4 = dStep3+0.25;

    auto c1 = ColorOps::irregularLightenColor(ColorOps::randomBeachColor() , 90);
    auto c2 = ColorOps::irregularLightenColor(ColorOps::randomBeachColor() , 60);
    auto c3 = ColorOps::irregularLightenColor(ColorOps::randomBeachColor() , 30);

    cerr << "dstep1: " << dStep1 << ", dstep2: " << dStep2 << endl;
    //prnd->setRandomFactor(2,1,5);
    prnd->gradientInfo().clear();
    prnd->gradientInfo().push_back(std::tuple<double,int,int,int,int>   (-1.0,   20,  36,101,255));
    prnd->gradientInfo().push_back(std::tuple<double,int,int,int,int>(dStep1-0.025, 23, 50, 115,255));
    prnd->gradientInfo().push_back(std::tuple<double,int,int,int,int>(dStep1-0.01 , 26, 58, 123,255));
    prnd->gradientInfo().push_back(std::tuple<double,int,int,int,int>(dStep1,       34, 62,145,255));
    prnd->gradientInfo().push_back(std::tuple<double,int,int,int,int>(dStep1+0.0001, c1.red, c1.green,c1.blue,128));
    prnd->gradientInfo().push_back(std::tuple<double,int,int,int,int>(dStep2, c1.red, c1.green,c1.blue,64));
    prnd->gradientInfo().push_back(std::tuple<double,int,int,int,int>(dStep3, c2.red, c2.green,c2.blue,32));
    prnd->gradientInfo().push_back(std::tuple<double,int,int,int,int>(dStep4, c3.red, c3.green,c3.blue,  0));
    prnd->gradientInfo().push_back(std::tuple<double,int,int,int,int>(   1.0, 255, 255,255,  0));

    _textureBuilder->addRendererDescriptor(prnd->name(),renderPole);
    return dStep2;
}
