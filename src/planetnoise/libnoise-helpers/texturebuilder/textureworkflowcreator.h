#ifndef TEXTUREWORKFLOWCREATOR_H
#define TEXTUREWORKFLOWCREATOR_H

#include "texturebuilder.h"
#include <memory>
#include <qcolorops.h>
#include "ssg_structures.h"
#include <string>
#include <algorithm>
#include <iostream>
#include <fstream>

using namespace std;

class TextureWorkflowCreator
{
    TextureBuilder* _textureBuilder = nullptr;
    bool _earthlike = false;
    string _texture1 = "";
    string _texture2 = "";
public:
    TextureBuilder* textureBuilder() { return _textureBuilder; }
    void setTextureBuilder (TextureBuilder* tb) { _textureBuilder = tb; }

    TextureWorkflowCreator(bool earthlike = false, string texture1 = "", string texture2 = "") :
        _earthlike(earthlike),
        _texture1(texture1),
        _texture2(texture2) {

    }
    void createSnowFromBumpmap(string destImage);
    void createGrassAndDesert (string destImage);

    void createTextureWorkflow(string prefix = "Texture1",
                               bool bCreateImage = false,
                               string backgroundImage = "",
                               string destImage = "",
                               bool last = false,
                               bool hasBump = false,
                               string textureType="");

    void createPoleWorkflow (std::string backgroundImage, double poleLevel = 0.9, double poleDeviation=0.06);
    double createSeaWorkflow (std::string backgroundImage, double baseSeaLevel = 0.5, double baseSeaDeviation=0.2);
    void createBumpMap(std::string mod1, std::string mod2, double dStart = -1.0);
    void createBumpMap4(string mod1, string mod2, string mod3, string mod4, double dStart = -1.0);
    void createBumpMapSea(double dStart = -1.0);
    void createSpecMap(std::string mod1, double dStart = -1.0);
    void createGeoMap(std::string mod1, double dStart = -1.0);
    void initializeRandomFactors();
    void prepareBumpHeightmap();

private:
    shared_ptr<ModuleDescriptor> initializeBaseGeneratorModule(string textureType, string prefix);
    bool handleTurbulenceForBaseModule(shared_ptr<ModuleDescriptor> p);
    void createNoiseMapBuilderForWorkflow(shared_ptr<ModuleDescriptor> p, string sTurboName, shared_ptr<HeightMapDescriptor> hmp, bool bTurbo, string prefix);
    shared_ptr<RendererDescriptor> createRendererForWorkflow(shared_ptr<HeightMapDescriptor> hmp, string backgroundImage, string prefix);
    void finalizeRendererForLastWorkflow(shared_ptr<RendererDescriptor> rdp, bool last);
};

#endif // TEXTUREWORKFLOWCREATOR_H
